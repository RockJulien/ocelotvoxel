//--------------------------------------------------------------------------------------
//  File: VoxelTriPlanarBasicTexturing.fx
//   
//  Description: Shader file for passing voxels through
//               the pipeline and being able to draw it.
//               Basic Tri-planar texturing will be applied 
//               avoiding stretching of texture on the 
//               plane perpendicular to the one on which
//               coordinates are computed (one texture per
//               plane merged in one applied to the iso-
//               surface).         
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    27/11/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

#include "EffectHelper.fx"

cbuffer cbPerFrame
{
	OcelotSpotLight gLight;
	OcelotFog gFog;
	float3 gEyePosW;
};

cbuffer cbPerObject
{
	matrix gWorld;
	matrix gView;
	matrix gProjection;
	matrix gWorldInvTrans;
};

// Non-numeric components.
Texture2D gTexture[9];
//Texture2DArray gTexture;

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;  // avoid fuzzy part (kind of supersampler).
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VoxelVertexShader_INPUT
{
	float3 Posl     : POSITION;  // local position
	float3 NormalL  : NORMAL;    // local normal
};

//--------------------------------------------------------------------------------------
struct VoxelVertexShader_OUTPUT
{
    float4 PosS      : SV_POSITION; // screen position
	float4 PosW      : POSITION;    // world position
	float3 NormalW   : NORMAL;      // world normal
	float3 TexC      : TEXCOORD0;   // texture coordinate.
	float  FogLerp   : FOG;         // the fog interpolation value.
};

//--------------------------------------------------------------------------------------
// Vertex Shader 
//--------------------------------------------------------------------------------------
VoxelVertexShader_OUTPUT VS( VoxelVertexShader_INPUT vIn)
{
    VoxelVertexShader_OUTPUT output = (VoxelVertexShader_OUTPUT)0;

	// Convert from local to world space, then to sreen space.
	output.PosS = mul( float4(vIn.Posl, 1.0f), gWorld);
	output.PosW = output.PosS;
	output.PosS = mul( output.PosS, gView );
	output.PosS = mul( output.PosS, gProjection );

	// Texture coordinate for the vertex.
	output.TexC = vIn.Posl / 1.0f;

	// Convert from local to world normal
	output.NormalW = vIn.NormalL;

	/********************************* Add Fog *****************************/
	// Compute the distance from the eye to the pixel
	float dist = distance(output.PosW, gEyePosW);

	// Then, compute the interpolation value for the Fog.
	output.FogLerp = saturate((dist - gFog.start) / gFog.range);

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VoxelVertexShader_OUTPUT input ) : SV_Target
{
	float4 Color  = float4( 0.0f, 0.0f, 0.0f, 0.0f );

	// Determine the blend weights for the 3 planar projections.
    // N_orig is the vertex-interpolated normal vector.
    float3 blendWeights = abs( input.NormalW );   // Tighten up the blending zone:
    blendWeights  = ( blendWeights - 0.2f ) * 7;
    blendWeights  = max( blendWeights, 0.1f );    // Force weights to sum to 1.0 (very important!)
    blendWeights /= ( blendWeights.x + blendWeights.y + blendWeights.z ).xxx;
    // Now determine a color value and bump vector for each of the 3
    // projections, blend them, and store blended results in these two
    // vectors:
    float4 blendedColor; // .w hold spec value
    float texScale = 1.0f / 16.0f;
    {
        // Compute the UV coords for each of the 3 planar projections.
        // texScale (default ~ 1.0) determines how big the textures appear.
        float2 coord1 = input.TexC.yz * texScale;
        float2 coord2 = input.TexC.zx * texScale;
        float2 coord3 = input.TexC.xy * texScale;
        // This is where you would apply conditional displacement mapping.
        //if (blendWeights.x > 0) coord1 = . . .
        //if (blendWeights.y > 0) coord2 = . . .
        //if (blendWeights.z > 0) coord3 = . . .
        // Sample color maps for each projection, at those UV coords.
		// Put your texture in the order you want they appear
        float4 col1 = gTexture[6].Sample(gAnisoSampler, coord1); // PosX face
        float4 col2 = gTexture[3].Sample(gAnisoSampler, coord2); // PosY face
        float4 col3 = gTexture[0].Sample(gAnisoSampler, coord3); // PosZ face
        float4 col4 = gTexture[6].Sample(gAnisoSampler, coord1); // NegX face
        float4 col5 = gTexture[3].Sample(gAnisoSampler, coord2); // NegY face
        float4 col6 = gTexture[0].Sample(gAnisoSampler, coord3); // NegZ face

        col1 *= ( input.NormalW.y + 1.0f ) / 2.0f;
        col2 *= ( input.NormalW.y + 1.0f ) / 2.0f;
        col3 *= ( input.NormalW.y + 1.0f ) / 2.0f;

        col4 *= ( -input.NormalW.y + 1.0f ) / 2.0f;
        col5 *= ( -input.NormalW.y + 1.0f ) / 2.0f;
        col6 *= ( -input.NormalW.y + 1.0f ) / 2.0f;

        col1 += col4;
        col2 += col5;
        col3 += col6;
         // Finally, blend the results of the 3 planar projections.
        blendedColor = col1.xyzw * blendWeights.xxxx +
                       col2.xyzw * blendWeights.yyyy +
                       col3.xyzw * blendWeights.zzzz;
    }
    float angle = saturate(dot( input.NormalW, gLight.direction ));
    float4 tempColor = blendedColor * ( gLight.diffuse * angle + gLight.ambient );

	// Add fog in last
	Color = lerp( tempColor, gFog.fogColor, input.FogLerp );

	return Color;
}

//--------------------------------------------------------------------------------------
// Technique containing passe(s) to apply.
//--------------------------------------------------------------------------------------
technique10 TriPlanarTexturing
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
    }
}
