//--------------------------------------------------------------------------------------
//  File: DeferredParticleSystem.fx
//   
//  Description: Shader file for drawing particles as a whole.
//               This Deferred method will compare the Z-depth
//               of particles correcting the depth problem due to
//               independent drawn particles of other rendering 
//               processes. (more realistic and no overlapp incoherence).
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    11/10/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

cbuffer cbPerFrame
{
	float3 gLightDir;
	float3 gEyePosW;
	float3 gForward;
	float3 gSide;
	float3 gUp;
	float  gTime;
};

cbuffer cbPerObject
{
	matrix gWorld;
	matrix gView;
	matrix gProj;
	matrix gInvViewProj;
};

cbuffer cbGlowLight
{
	float4 gPosIntensity;
	float4 gColor;
	float3 gAttenuation;
};

// Non-numeric component.
Texture2D gTextureParticle;
Texture2D gColorParticle;

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;  // avoid fuzzy part (kind of supersampler).
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// DepthStates & BlendStates.
//--------------------------------------------------------------------------------------
DepthStencilState DisableDepth
{
    DepthEnable = FALSE;
    DepthWriteMask = 0;
    DepthFunc = LESS_EQUAL;
};

DepthStencilState DepthRead
{
    DepthEnable = TRUE;
    DepthWriteMask = 0;
    DepthFunc = LESS_EQUAL;
};

BlendState DeferredBlending
{
	AlphaToCoverageEnable = FALSE;
    BlendEnable[0] = TRUE;
    BlendEnable[1] = TRUE;
    SrcBlend = ONE;
    DestBlend = INV_SRC_ALPHA;
    BlendOp = ADD;
    SrcBlendAlpha = ONE;
    DestBlendAlpha = INV_SRC_ALPHA;
    BlendOpAlpha = ADD;
    RenderTargetWriteMask[0] = 0x0F;
    RenderTargetWriteMask[1] = 0x0F;
};

BlendState CompositeBlending
{
	AlphaToCoverageEnable = FALSE;
    BlendEnable[0] = TRUE;
    BlendEnable[1] = FALSE;
    SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA;
    BlendOp = ADD;
    SrcBlendAlpha = ZERO;
    DestBlendAlpha = ZERO;
    BlendOpAlpha = ADD;
    RenderTargetWriteMask[0] = 0x0F;
    RenderTargetWriteMask[1] = 0x0F;
};

RasterizerState RSSolid
{
	FillMode = Solid;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure corresponding to the C++ layout.
//--------------------------------------------------------------------------------------
struct VS_PARTICLEINPUT
{
	float4 Posl       : POSITION;  // Particle's local position
	float2 TexC       : TEXCOORD;  // U/V texture coordinates.
	float2 XLifeYRot  : LIFENROT;  // Particle's life in X, Particle's rotation in Y.
	float4 Color	  : COLOR0;    // Particle's color.
};

//--------------------------------------------------------------------------------------
// Vertex shader output structure / Pixel shader input.
//--------------------------------------------------------------------------------------
struct VS_PARTICLEOUTPUT
{
    float4 Position        : SV_POSITION; // Particle's offscreen position.
    float3 TexC            : TEXCOORD0;   // U/V texture coordinates.
    float3 SinCosThetaLife : TEXCOORD1;   // Particle's sin(rot) in X, Particle's cos(rot) in Y and Particle's life in Z.
    float4 Color	       : COLOR0;      // Particle's color.
};

struct VS_SCREENOUTPUT
{
    float4 PosS            : SV_POSITION; // Particle's screen position  
};

//--------------------------------------------------------------------------------------
// Particle's vertex shader for rendering part.
//--------------------------------------------------------------------------------------
VS_PARTICLEOUTPUT ParticleVS( VS_PARTICLEINPUT input )
{
    VS_PARTICLEOUTPUT output = (VS_PARTICLEOUTPUT)0;

	output.Position = mul( input.Posl, gWorld );	
	float3 WorldPos = output.Position.xyz;
	
	output.Position = mul( output.Position, gView );
    output.Position = mul( output.Position, gProj );
    output.TexC.xy  = input.TexC; 
    output.Color    = input.Color;

	// Loop over the glow lights (from the explosions) and light our particle
	float runningintensity = 0;

	float3 delta = gPosIntensity.xyz - WorldPos;
	float distSq = dot(delta, delta);
	float3 dist  = float3(1.0f,/*sqrt(distSq)*/ 0.0f, distSq);
		
	float fatten = 1.0f / dot( gAttenuation, dist );
		
	float intensity   = fatten * gPosIntensity.w * gColor.w;
	runningintensity += intensity;
	output.Color     += intensity * gColor;
	
	output.TexC.z  = runningintensity;
    
    // Rotate our texture coordinates
    float rotation = -input.XLifeYRot.y;
    output.SinCosThetaLife.x = sin(rotation);
    output.SinCosThetaLife.y = cos(rotation);
    output.SinCosThetaLife.z = input.XLifeYRot.x;
    
    return output;    
}

//--------------------------------------------------------------------------------------
// Deferred buffer containing particles to centralize.
//--------------------------------------------------------------------------------------
struct PBUFFER_OUTPUT
{
	float4 Color0 : SV_TARGET0;
	float4 Color1 : SV_TARGET1;
};

//--------------------------------------------------------------------------------------
// Particle's pixel shader for rendering part.
//--------------------------------------------------------------------------------------
PBUFFER_OUTPUT ParticlesDeferredPS(VS_PARTICLEOUTPUT input)
{ 
	PBUFFER_OUTPUT output;
	
	float4 diffuse = gTextureParticle.Sample(gAnisoSampler, input.TexC.xy);
	
	// unbias
	float3 norm    = diffuse.xyz * 2 - 1;
	
	// rotate our texture coordinate and our normal basis
	float3 rotnorm;
	float fSinTheta = input.SinCosThetaLife.x;
	float fCosTheta = input.SinCosThetaLife.y;
	
	rotnorm.x = fCosTheta * norm.x - fSinTheta * norm.y;
	rotnorm.y = fSinTheta * norm.x + fCosTheta * norm.y;
	rotnorm.z = norm.z;
	
	// rebias
	norm = rotnorm;
	
	// Fade
	float alpha      = diffuse.a * (1.0f - input.SinCosThetaLife.z);
	float4 normalpha = float4(norm.xy * alpha, input.TexC.z * alpha, alpha);

	output.Color0 = normalpha;
	output.Color1 = input.Color * alpha;
	
	return output;
}

//--------------------------------------------------------------------------------------
// Composite particles vertex shader method.
//--------------------------------------------------------------------------------------
VS_SCREENOUTPUT CompositeParticlesVS(float4 Position : POSITION)
{
    VS_SCREENOUTPUT output;

    output.PosS = Position;
    
    return output;    
}

//--------------------------------------------------------------------------------------
// Composite particles pixel shader method.
//--------------------------------------------------------------------------------------
float4 CompositeParticlesPS(VS_SCREENOUTPUT input) : SV_TARGET
{
	// Load the particle normal data, opacity, and color
	float3 loadpos        = float3(input.PosS.xy, 0);
    float4 particlebuffer = gTextureParticle.Load(loadpos);
    float4 particlecolor  = gColorParticle.Load(loadpos);
    
    // Recreate z-component of the normal
    float normZ     = sqrt(1 - particlebuffer.x * particlebuffer.x + particlebuffer.y * particlebuffer.y);
    float3 normal   = float3(particlebuffer.xy, normZ);
    float intensity = particlebuffer.z;

    // move normal into world space
    float3 worldnorm;
    worldnorm  = -normal.x * gSide;
    worldnorm += normal.y * gUp;
    worldnorm += -normal.z * gForward;
    
    // light
    float lighting    = max(0.1f, dot(worldnorm, -gLightDir));
    
    float3 flashcolor = particlecolor.xyz * intensity;
    float3 lightcolor = particlecolor.xyz * lighting;
    float3 lerpcolor  = lerp(lightcolor, flashcolor, intensity);
    float4 color      = float4(lerpcolor, particlebuffer.a);
    
    return color;
}

//--------------------------------------------------------------------------------------
// Technique containing passe(s) to apply: Deferred method as Pixel Shader.
//--------------------------------------------------------------------------------------
technique10 RenderParticlesToBuffer
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, ParticleVS()));
        SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, ParticlesDeferredPS()));

		SetBlendState(DeferredBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
        SetDepthStencilState(DepthRead, 0);
        SetRasterizerState(RSSolid);
    }
}

//--------------------------------------------------------------------------------------
// Technique containing methods for centralizing particles before to do above.
//--------------------------------------------------------------------------------------
technique10 CompositeParticlesToScene
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, CompositeParticlesVS()));
        SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, CompositeParticlesPS()));

		SetBlendState(CompositeBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
        SetDepthStencilState(DisableDepth, 0);
        SetRasterizerState(RSSolid);
    }
}
