//--------------------------------------------------------------------------------------
//  File: VoxelTriPlanarTexturing.fx
//   
//  Description: Shader file for passing voxels through
//               the pipeline and being able to draw it.
//               Tri-planar texturing will be applied 
//               avoiding stretching of texture on the 
//               plane perpendicular to the one on which
//               coordinates are computed (one texture per
//               plane merged in one applied to the iso-
//               surface).         
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    01/11/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

#include "EffectHelper.fx"

cbuffer cbPerFrame
{
	OcelotSpotLight gLight;
	OcelotSpotLight gTorchLight;
	OcelotFog gFog;
	float3 gEyePosW;
	bool   gTorcheOn;
};

cbuffer cbPerObject
{
	matrix gWorld;
	matrix gView;
	matrix gProjection;
	matrix gWorldInvTrans;
};

// Non-numeric components.
Texture2D gTexture[9];
//Texture2DArray gTexture;

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;  // avoid fuzzy part (kind of supersampler).
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VoxelVertexShader_INPUT
{
	float3 Posl     : POSITION;  // local position
	float3 NormalL  : NORMAL;    // local normal
};

//--------------------------------------------------------------------------------------
struct VoxelVertexShader_OUTPUT
{
    float4 PosS      : SV_POSITION; // screen position
	float4 PosW      : POSITION;    // world position
	float3 NormalW   : NORMAL;      // world normal
	float3 Tan1Eye   : EYETANGENTSPACE1;   // Eye Tangent Space 1
	float3 Tan1Light : LIGHTTANGENTSPACE1; // Light Tangent Space 1
	float2 Tan2Eye   : EYETANGENTSPACE2;   // Eye Tangent Space 2
	float2 Tan2Light : LIGHTTANGENTSPACE2; // Light Tangent Space 2
	//float3 TangentW : TANGENT;    // world tangent
	//float2 TexC     : TEXCOORD;   // U/V texture coordinates.
	float4 TexMapXY  : MATERIAL0;   // two texture map indexes Txy/Uxy (on X & Y) for the four vertically aligned faces of the cubic projection and Alpha (on Z).
	float4 TexMapZ   : MATERIAL1;   // two texture map indexes T+z/T-z/U+z/U-z (on X, Y, Z & W) for the posZ face and negZ face of the cubic projection.
	float  FogLerp   : FOG;         // the fog interpolation value.
};

//--------------------------------------------------------------------------------------
// Vertex Shader 
//--------------------------------------------------------------------------------------
VoxelVertexShader_OUTPUT VS( VoxelVertexShader_INPUT vIn)
{
    VoxelVertexShader_OUTPUT output = (VoxelVertexShader_OUTPUT)0;

	// Convert from local to world space, then to sreen space.
	output.PosS = mul( float4(vIn.Posl, 1.0f), gWorld);	
	output.PosW = output.PosS;
	
	output.PosS = mul( output.PosS, gView );
    output.PosS = mul( output.PosS, gProjection );

	// Convert from local to world normal
	float3 wNorm = mul(float4(vIn.NormalL, 0.0f), gWorldInvTrans).xyz;
	output.NormalW = normalize(wNorm);

	// Initialize texture map indexes (is hard coded but can be passed through the shader as well).
	// Up to 255 textures to choose. (In this demo: texture are Dirt (0) Grass(3) and Rock(6)
	// and texture between those indexes are _NORMAL and _SPECULAR
	output.TexMapXY.x = 6.0f / 255.0f;    // Grab the Nth texture as primary texture for the four aligned faces.
	output.TexMapXY.y = 6.0f / 255.0f;    // Grab the Nth texture as secondary texture for the four aligned faces.
	output.TexMapXY.z = 1.0f;             // Alpha blend factor. (NOTE: W is not used).
	output.TexMapZ.x  = 3.0f / 255.0f;    // Grab the Nth texture as primary texture for the positive z face.
	output.TexMapZ.y  = 3.0f / 255.0f;    // Grab the Nth texture as primary texture for the negative z face.
	output.TexMapZ.z  = 3.0f / 255.0f;    // Grab the Nth texture as secondary texture for the positive z face.
	output.TexMapZ.w  = 3.0f / 255.0f;    // Grab the Nth texture as secondary texture for the negative z face.

	// Transform from object space to tangent space.
	float3 toEye   = normalize(gEyePosW - output.PosW.xyz);        // from object to camera.
	float3 toLight = normalize(gLight.position - output.PosW.xyz); // from object to light.

	// Compute the normal magnitude.
	float3 normalMagnitude = output.NormalW * output.NormalW;
	normalMagnitude.xy = normalMagnitude.x + normalMagnitude.yz;
	normalMagnitude.xy = max(normalMagnitude.xy, 0.03125f);

	// Compute the cross product between each vector dir and the normal
	float3 VCrossNEye   = cross(toEye, output.NormalW);
	float3 VCrossNLight = cross(toLight, output.NormalW);

	// Then, apply transform for each vector dir in the two tangent space.
	// NOTE: N dot V will be the same for both tangent space, thus compute
	// once.
	float4 tempEye;
	float4 tempLight;
	tempEye.xy   = output.NormalW.xz * toEye.yx - output.NormalW.yx * toEye.xz;
	tempEye.zw   = output.NormalW.xz * VCrossNEye.yx - output.NormalW.yx * VCrossNEye.xz;
	tempLight.xy = output.NormalW.xz * toLight.yx - output.NormalW.yx * toLight.xz;
	tempLight.zw = output.NormalW.xz * VCrossNLight.yx - output.NormalW.yx * VCrossNLight.xz;

	output.Tan1Eye.xy   = tempEye.xz * rsqrt(normalMagnitude.x);
	output.Tan1Eye.z    = dot(output.NormalW, toEye);   // same for both.
	output.Tan2Eye      = tempEye.yw * rsqrt(normalMagnitude.y);
	output.Tan1Light.xy = tempLight.xz * rsqrt(normalMagnitude.x);
	output.Tan1Light.z  = dot(output.NormalW, toLight); // same for both.
	output.Tan2Light    = tempLight.yw * rsqrt(normalMagnitude.y);

	/********************************* Add Fog *****************************/
	// Compute the distance from the eye to the pixel
	float dist = distance(output.PosW, gEyePosW);

	// Then, compute the interpolation value for the Fog.
	output.FogLerp = saturate((dist - gFog.start) / gFog.range);

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VoxelVertexShader_OUTPUT input ) : SV_Target
{
	float4 Color  = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Compute a vector that holds one for values of "S" that should
	// be negated (will contain either 0 or 1).
	float3 flip = float3((input.NormalW.x < 0.0f), (input.NormalW.y >= 0.0f), (input.NormalW.z < 0.0f));
	
	// Compute the blend weights, delta = 1/2 or 0.5 eliminating streaking (delta = streakness).
	float3 blendWeight = saturate(abs(normalize(input.NormalW)) - 0.5f);
	// m = 4, weight power of m (m = sharpness)
	blendWeight *= blendWeight;
	blendWeight *= blendWeight;

	// Normalize result by dividing by the sum of its components.
	blendWeight /= dot(blendWeight, float3(1.0f, 1.0f, 1.0f));

	// Select the texture map indexes T and U for either the pos or neg Z face.
	float2 zFaceIndex = lerp(input.TexMapZ.xz, input.TexMapZ.yw, flip.z);

	// Compute the three sets of texture coordinates for use with a 
	// texture array for a cubic projection.
	// Sx and Tx in both texCoord1 and texCoord2 (on X & Z) Note: remember that Tx = Ty = Pz
	// Sy and Ty in both texCoord1 and texCoord2 (on Y & Z)       and Tz = Py.
	// Sz and Tz in texCoord3 (on X & Z) 
	float4 texCoord1; // equal to (Sx, Sy, Tx | Ty, Txy).
	float4 texCoord2; // equal to (Sx, Sy, Tx | Ty, Uxy).
	float4 texCoord3; // equal to (Sz, Tz, T+-z, U+-z).
	texCoord1.xy  = lerp(input.PosW.yx, -input.PosW.yx, flip.xy);
	texCoord1.z   = input.PosW.z;
	texCoord1.w   = input.TexMapXY.x * 255.0f;

	texCoord2.xyz = texCoord1.xyz;
	texCoord2.w   = input.TexMapXY.y * 255.0f;

	texCoord3.x   = lerp(input.PosW.x, -input.PosW.x, flip.z);
	texCoord3.y   = input.PosW.y;
	texCoord3.zw  = zFaceIndex * 255.0f;

	// Then, compute color samples for each face.
	// Positive faces
	float4 posXFaceColor = gTexture[6].Sample(gAnisoSampler, texCoord1.xz); // (W) if Tex2DArr as text index
	float4 posYFaceColor = gTexture[3].Sample(gAnisoSampler, texCoord1.yz); // (W) if Tex2DArr
	float4 posZFaceColor = gTexture[6].Sample(gAnisoSampler, texCoord3.xy); // (Z) if Tex2DArr
	// Negative faces
	float4 negXFaceColor = gTexture[6].Sample(gAnisoSampler, texCoord2.xz); // (W) if Tex2DArr
	float4 negYFaceColor = gTexture[3].Sample(gAnisoSampler, texCoord2.yz); // (W) if Tex2DArr
	float4 negZFaceColor = gTexture[6].Sample(gAnisoSampler, texCoord3.xy); // (W) if Tex2DArr

	// Blend tri-planar samples independently (by sign).
	float4 posColor = posXFaceColor * blendWeight.x + posYFaceColor * blendWeight.y + posZFaceColor * blendWeight.z;
	float4 negColor = negXFaceColor * blendWeight.x + negYFaceColor * blendWeight.y + negZFaceColor * blendWeight.z;

	// Finally, blend primary and secondary colors (that is pos faces color and neg faces color) together.
	float4 tempColor = lerp(posColor, negColor, input.TexMapXY.z);

	// Now, compute diffuse and specular components thanks to per-vertex two tangent space components.
	float3 lightTan1 = normalize(input.Tan1Light);
	float3 lightTan2 = normalize(float3(input.Tan2Light.x, input.Tan2Light.y, input.Tan1Light.z));
	float3 eyeTan1   = normalize(lightTan1 + normalize(input.Tan1Eye));
	float3 eyeTan2   = normalize(lightTan2 + normalize(float3(input.Tan2Eye.x, input.Tan2Eye.y, input.Tan1Eye.z)));

	float3 blendToAdd;

	// Fetch primary and Secondary Normal on each axis, interpolate and expand.
	float3 temp1 = gTexture[6].Sample(gAnisoSampler, texCoord1.xz).xyz; // (W) if Tex2DArr
	float3 temp2 = gTexture[8].Sample(gAnisoSampler, texCoord2.xz).xyz; // (W) if Tex2DArr
	float3 normX = lerp(temp1.xyz, temp2.xyz, input.TexMapXY.z) * 2.0f - 1.0f;

	temp1 = gTexture[6].Sample(gAnisoSampler, texCoord1.yz).xyz; // (W) if Tex2DArr
	temp2 = gTexture[8].Sample(gAnisoSampler, texCoord2.yz).xyz; // (W) if Tex2DArr
	float3 normY = lerp(temp1.xyz, temp2.xyz, input.TexMapXY.z) * 2.0f - 1.0f;

	temp1 = gTexture[0].Sample(gAnisoSampler, texCoord3.xy).xyz; // (Z) if Tex2DArr
	temp2 = gTexture[3].Sample(gAnisoSampler, texCoord3.xy).xyz; // (W) if Tex2DArr
	float3 normZ = lerp(temp1.xyz, temp2.xyz, input.TexMapXY.z) * 2.0f - 1.0f;

	// Compute the blend to add between the normal computed and the light dir in tan space
	// for having the diffuse component.
	blendToAdd.x = saturate(dot(normX, lightTan1));
	blendToAdd.y = saturate(dot(normY, lightTan1));
	blendToAdd.z = saturate(dot(normZ, lightTan2));
	float diffuseToAdd = dot(blendToAdd, blendWeight);

	// Compute the blend to add between the normal computed and the eye dir in tan space
	// for having the specular component.
	blendToAdd.x = saturate(dot(normX, eyeTan1));
	blendToAdd.y = saturate(dot(normY, eyeTan1));
	blendToAdd.z = saturate(dot(normZ, eyeTan2));
	float specularToAdd = dot(blendToAdd, blendWeight);

	// Add diff and spec.
	tempColor.rgb *= diffuseToAdd * specularToAdd;

	// Add fog in last
	Color = lerp(tempColor, gFog.fogColor, input.FogLerp);

	return Color;
}

//--------------------------------------------------------------------------------------
// Technique containing passe(s) to apply.
//--------------------------------------------------------------------------------------
technique10 TriPlanarTexturing
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
    }
}
