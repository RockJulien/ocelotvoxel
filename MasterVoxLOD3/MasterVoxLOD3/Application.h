#ifndef DEF_APPLICATION_H
#define DEF_APPLICATION_H

#include "DirectXAPI\DXOcelotApp.h"
#include "Audio\AudioManager.h"
#include "TextHelper\OcelotDXText.h"
#include "DataStructures\Linked2List.h"
#include "Geometry\CubeMap.h"
#include "Geometry\Cube.h"
#include "Geometry\PyraCylinder.h"
#include "Geometry\Sphere.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "Physics\OcelotParticleSystemFactory.h"
#include "VoxelTerrain\OcelotTerrainManager.h"
#include "VoxelTerrain\SimpleLODExample.h"

using namespace Ocelot;

BIGINT WINAPI WinMain( HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPSTR     lpCmdLine,
					   BIGINT    nShowCmd );

class Application : public DXOcelotApp
{
private:

	// Attributes
	//Linked2List<OcelotDXParticleSystem*> m_particles;
	SimpleLODExample*                    m_test;
	OcelotTerrainManager*                m_terrain;
	TriPlanarBumpMaterial*               m_terrainMaterial;
	TriPlanarTexturePalette*             m_terrMatPalette;
	ID3D10RasterizerState*				 m_wireFrameMode;
	OcelotParticleSystemFactory*         m_particleBuilder;
	Linked2List<IOcelotMesh*>			 m_meshes;
	OcelotCamera*						 m_camera;
	IOcelotCameraController*			 m_controller;
	OcelotLight*						 m_light; // Global light.
	OcelotLight*                         m_visitorLight;
	OcelotFog*							 m_fog;
	Vector2								 m_vOldMousePos;
	FFLOAT								 m_fElapsedTime;
	BBOOL								 m_bBounds;
	BBOOL								 m_bWireFrame;

	// Boolean for avoiding to refresh if nothing changed.
	BBOOL                                m_bRefreshScale;
	BBOOL                                m_bRefreshPosition;
	BBOOL                                m_bRefreshDirection;

	// Text User Information
	OcelotDXText*                        m_terrainInfo;
	OcelotDXText*                        m_cameraInfo;

	// Private Method
	VVOID refreshCameraInfo();
	VVOID refreshTerrainInfo();

	// Just for convenience and code understanding
	VVOID RefreshScale() { m_bRefreshScale = true;}
	VVOID DontRefreshScale() { m_bRefreshScale = false;}
	VVOID RefreshPosition() { m_bRefreshPosition = true;}
	VVOID DontRefreshPosition() { m_bRefreshPosition = false;}
	VVOID RefreshDirection() { m_bRefreshDirection = true;}
	VVOID DontRefreshDirection() { m_bRefreshDirection = false;}

public:

	// Constructor & Destructor
	Application();
	~Application();

	// Methods
	BIGINT  run();
	VVOID   update(FFLOAT deltaTime);
	VVOID   renderFrame();
	VVOID   initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPWSTR windowName);
	VVOID   handleInput();
	VVOID   clean3DDevice();
	VVOID   resize();
	LRESULT AppProc(UBIGINT message,WPARAM wParam,LPARAM lParam);

};

#endif