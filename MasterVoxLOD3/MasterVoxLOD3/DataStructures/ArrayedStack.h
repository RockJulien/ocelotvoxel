#ifndef DEF_ARRAYEDSTACK_H
#define DEF_ARRAYEDSTACK_H

#include "Array1D.h"

template<class TheType>
class ArrayedStack : public Array1D<TheType>
{
private:

	// Attributes
	BIGINT m_iTop;

public:

	// Constructor & Destructor
	ArrayedStack(BIGINT iNbCell);
	~ArrayedStack();

	// Methods
	BBOOL   push(TheType uData);
	VVOID   pop();
	TheType top();

	// Accessors
	inline BIGINT Size() const { return m_iTop;}
};

template<class TheType>
ArrayedStack<TheType>::ArrayedStack(BIGINT iNbCell) :
Array1D<TheType>(iNbCell), m_iTop(0)
{

}

template<class TheType>
ArrayedStack<TheType>::~ArrayedStack()
{
	Array1D<TheType>::~Array1D<TheType>();
}

template<class TheType>
BBOOL   ArrayedStack<TheType>::push(TheType uData)
{
	if(m_iNbData != m_iTop)
	{
		m_uData[m_iTop] = uData;
		m_iTop++;
		return true;
	}

	return false;
}

template<class TheType>
VVOID   ArrayedStack<TheType>::pop()
{
	if(m_iTop > 0)
		m_iTop--;
}

template<class TheType>
TheType ArrayedStack<TheType>::top()
{
	return m_uData[m_iTop - 1];
}

#endif