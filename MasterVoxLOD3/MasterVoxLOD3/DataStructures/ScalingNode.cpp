#include "ScalingNode.h"

ScalingNode::ScalingNode() :
SceneNode(), m_fScaleX(1.0f), m_fScaleY(1.0f), m_fScaleZ(1.0f)
{
	m_parent     = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matScaling.Matrix4x4Identity();
}

ScalingNode::ScalingNode(float scaleXYZ) :
SceneNode(), m_fScaleX(scaleXYZ), m_fScaleY(scaleXYZ), m_fScaleZ(scaleXYZ)
{
	m_parent     = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matScaling.Matrix4x4Identity();
}

ScalingNode::ScalingNode(float scaleX, float scaleY, float scaleZ) :
SceneNode(), m_fScaleX(scaleX), m_fScaleY(scaleY), m_fScaleZ(scaleZ)
{
	m_parent     = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matScaling.Matrix4x4Identity();
}

ScalingNode::ScalingNode(const Vector3& scale) : 
SceneNode(), m_fScaleX(scale.getX()), m_fScaleY(scale.getY()), m_fScaleZ(scale.getZ())
{
	m_parent     = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matScaling.Matrix4x4Identity();
}

ScalingNode::~ScalingNode()
{
	SceneNode::destroy();
}

void ScalingNode::update(float deltaTime)
{
	// reset each frame
	m_matWorld.Matrix4x4Identity();
	m_matScaling.Matrix4x4Identity();

	if(m_parent)
	{
		// apply the matrix of the parent
		m_matWorld = m_matWorld * m_parent->getWorld();
	}

	// apply the scale of the world matrix
	m_matScaling.Matrix4x4Scaling(m_matScaling, m_fScaleX, m_fScaleY, m_fScaleZ);

	Matrix4x4 invWorld;
	invWorld.Matrix4x4Identity();
	m_matWorld.Matrix4x4Inverse(invWorld);

	invWorld.Matrix4x4Mutliply(invWorld, m_matScaling);
	invWorld.Matrix4x4Inverse(m_matWorld);
	//m_matWorld = m_matWorld * m_matScaling;

	// update children
	SceneNode::update(deltaTime);
}

void	 ScalingNode::applyScale(float sX, float sY, float sZ)
{
	m_fScaleX = sX;
	m_fScaleY = sY;
	m_fScaleZ = sZ;
}

void	 ScalingNode::applyTrans(float tX, float tY, float tZ)
{

}

void	 ScalingNode::applyRotat(float rX, float rY, float rZ)
{

}
