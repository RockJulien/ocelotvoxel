#ifndef DEF_LINKEDHASHTABLE_H
#define DEF_LINKEDHASHTABLE_H

#include "HashCell.h"
#include "Linked2List.h"
#include "Array1D.h"

template<class KeyType, class DataType>
class LinkedHashTable
{
public:

	// Save fingers
	typedef HashCell<KeyType, DataType> Cell;

private:

	// Attributes
	Array1D<Linked2List<Cell*>*> m_hTable;
	UBIGINT                      m_iSize;
	UBIGINT                      m_iNbCell;
	UBIGINT                      (*m_hashing)(KeyType);

public:

	// Constructor & Destructor
	LinkedHashTable();
	LinkedHashTable(UBIGINT iSize, UBIGINT (*hashing)(KeyType));
	~LinkedHashTable();

	// Methods
	Cell* begin();
	VVOID insert(KeyType uKey, DataType uData);
	Cell* search(KeyType uKey);
	BBOOL remove(KeyType uKey);
	Cell* last();
	VVOID map(UBIGINT size);
	VVOID unmap();

	// Access Operator for iterating through the hashTable.
	List2Iterator<Cell*> operator[] (UBIGINT iIndex);

	// Accessors
	inline UBIGINT Size() { return m_iSize;}
	inline UBIGINT Pairs() { return m_iNbCell;}
	inline UBIGINT ListSize(UBIGINT listIndex) { return m_hTable[listIndex]->Size();}
};

template<class KeyType, class DataType>
LinkedHashTable<KeyType, DataType>::LinkedHashTable() : 
m_hTable(0), m_iSize(0), m_hashing(HashInteger), m_iNbCell(0)
{

}

template<class KeyType, class DataType>
LinkedHashTable<KeyType, DataType>::LinkedHashTable(UBIGINT iSize, UBIGINT (*hashing)(KeyType)) : 
m_hTable(iSize), m_iSize(iSize), m_hashing(hashing), m_iNbCell(0)
{
	for(UBIGINT currList = 0; currList < (UBIGINT)m_hTable.Size(); currList++)
	{
		// Initialize Lists.
		m_hTable[currList] = new Linked2List<Cell*>();
	}
}

template<class KeyType, class DataType>
LinkedHashTable<KeyType, DataType>::~LinkedHashTable()
{
	if(m_hTable.Size() != 0)
	{
		for(UBIGINT currList = 0; currList < (UBIGINT)m_hTable.Size(); currList++)
		{
			// Destroy cell pointer in each list
			List2Iterator<Cell*> iter = m_hTable[currList]->iterator();
			for(iter.begin(); iter.notEnd(); iter.next())
			{
				Cell* toDelete = iter.data();
				DeletePointer(toDelete);
			}

			// Because the list is in the array and that the array uses
			// malloc/free for releasing memory, so destructor of Linked2List
			// is not called (only when new/delete is used), so use the function 
			// intended in that case and force the destruction (prevent from 
			// memory leaks equivalent to 12 bytes times size of list).
			//iter.List()->release();
			DeletePointer(m_hTable[currList]);
		}
	}
}

template<class KeyType, class DataType>
HashCell<KeyType, DataType>* LinkedHashTable<KeyType, DataType>::begin()
{
	Cell* toReturn = NULL;

	List2Iterator<Cell*> iter = m_hTable[0]->iterator();

	while(iter.notEnd())
	{
		// if any
		toReturn = iter.data();

		// else, iterate.
		iter.next();
	}

	// if nothing, return NULL pointer.
	return toReturn;
}

template<class KeyType, class DataType>
VVOID LinkedHashTable<KeyType, DataType>::insert(KeyType uKey, DataType uData)
{
	Cell* newCell = new Cell(uKey, uData);
	// first hashing function changeable plus result re-hashed by the modulo method.
	UBIGINT index = m_hashing(uKey) % m_iSize;
	m_hTable[index]->putBACK(newCell);
	m_iNbCell++;
}

template<class KeyType, class DataType>
HashCell<KeyType, DataType>* LinkedHashTable<KeyType, DataType>::search(KeyType uKey)
{
	// first find the right slot in the table.
	UBIGINT index = m_hashing(uKey) % m_iSize;
	// then, iterate through the linked list for the data.
	List2Iterator<Cell*> iter = m_hTable[index]->iterator();

	while(iter.notEnd())
	{
		// if found, return
		if(iter.data()->Key() == uKey)
			return iter.data();

		// else, iterate.
		iter.next();
	}

	// if nothing, return NULL pointer.
	return 0;
}

template<class KeyType, class DataType>
BBOOL LinkedHashTable<KeyType, DataType>::remove(KeyType uKey)
{
	// first find the right slot in the table.
	UBIGINT index = m_hashing(uKey) % m_iSize;
	// then, iterate through the linked list for the data.
	List2Iterator<Cell*> iter = m_hTable[index]->iterator();

	while(iter.notEnd())
	{
		// if found, remove and return true.
		if(iter.data()->Key() == uKey)
		{
			// First, delete the cell in the node.
			Cell* toDelete = iter.data();
			DeletePointer(toDelete);

			// Then, release the list node.
			m_hTable[index]->remove(iter);
			m_iNbCell--;
			return true;
		}

		// else, iterate through the list.
		iter.next();
	}

	// if nothing found.
	return false;
}

template<class KeyType, class DataType>
HashCell<KeyType, DataType>* LinkedHashTable<KeyType, DataType>::last()
{
	Cell* toReturn = NULL;

	// Find the last list with something inside.
	UBIGINT listsCounter = 0;
	while((listsCounter < m_iSize) && (ListSize(listsCounter) != 0))
	{
		listsCounter++;
	}

	// Grab the list iterator and find the last.
	List2Iterator<Cell*> iter = m_hTable[listsCounter - 1]->iterator();

	while(iter.notEnd())
	{
		// if any
		toReturn = iter.data();

		// else, iterate.
		iter.next();
	}

	// if nothing, return NULL pointer.
	return toReturn;
}

template<class KeyType, class DataType>
VVOID LinkedHashTable<KeyType, DataType>::map(UBIGINT size)
{
	// if not empty, clear.
	unmap();

	m_hTable.map(size);

	// restore parameters.
	m_iSize   = size;
	m_iNbCell = 0;

	// prepare lists.
	for(UBIGINT currList = 0; currList < (UBIGINT)m_hTable.Size(); currList++)
	{
		// Initialize Lists.
		m_hTable[currList] = new Linked2List<Cell*>();
	}
}

template<class KeyType, class DataType>
VVOID LinkedHashTable<KeyType, DataType>::unmap()
{
	// if not empty, clear.
	if(m_hTable.Size() != 0)
	{
		for(UBIGINT currList = 0; currList < (UBIGINT)m_hTable.Size(); currList++)
		{
			// Destroy cell pointer in each list
			List2Iterator<Cell*> iter = m_hTable[currList]->iterator();
			for(iter.begin(); iter.notEnd(); iter.next())
			{
				Cell* toDelete = iter.data();
				DeletePointer(toDelete);
			}

			// Because the list is in the array and that the array uses
			// malloc/free for releasing memory, so destructor of Linked2List
			// is not called (only when new/delete is used), so use the function 
			// intended in that case and force the destruction (prevent from 
			// memory leaks equivalent to 12 bytes times size of list).
			//iter.List()->release();
			DeletePointer(m_hTable[currList]);
		}
	}

	// unmap the array of lists.
	m_hTable.unmap();
}

template<class KeyType, class DataType>
List2Iterator<HashCell<KeyType, DataType>*> LinkedHashTable<KeyType, DataType>::operator[] (UBIGINT iIndex)
{
	return m_hTable[iIndex]->iterator();
}

/**************************** Some useful hashing functions ******************************/
inline UBIGINT HashInteger(UBIGINT toHash)
{
	UBIGINT toReturn = 0;

	do
	{
		// hash on a decimal base,
		// so per digit and insure 
		// that it process at least 
		// once whence the "do".
		toReturn += toHash % 10;
		toHash /= 10;
	}
	while(toHash > 1);

	return toReturn;
}

inline UBIGINT HashString(const CCHAR* toHash)
{
	UBIGINT toReturn = 0;

	UBIGINT len = (UBIGINT)strlen(toHash);

	for(UBIGINT currChar = 0; currChar < len; currChar++)
	{
		toReturn += ((currChar + 1) * toHash[currChar]);
	}

	return toReturn;
}

inline UBIGINT HashString(const std::string toHash)
{
	return HashString(toHash.c_str());
}

inline UBIGINT HashString(const WCHAR* toHash)
{
	UBIGINT toReturn = 0;

	UBIGINT len = (UBIGINT)wcslen(toHash);

	for(UBIGINT currChar = 0; currChar < len; currChar++)
	{
		toReturn += ((currChar + 1) * toHash[currChar]);
	}

	return toReturn;
}

inline UBIGINT HashString(const std::wstring toHash)
{
	return HashString(toHash.c_str());
}

#endif