#include "SceneNode.h"

SceneNode::SceneNode() : m_parent(NULL)
{

}

SceneNode::~SceneNode()
{
	if(m_lChildren.Size() != 0)
	{
		destroy();
	}
}

void SceneNode::Release()
{
	if(this)
	{
		delete this;
	}
}

void SceneNode::update(float deltaTime)
{
	List2Data<SceneNode*>* temp = m_lChildren.First();

	if(temp!= NULL)
	{
	   while(temp)
	   {
		   temp->Data()->update(deltaTime);
		   temp = temp->Next();
	   }
	}
}

void SceneNode::destroy()
{
	List2Data<SceneNode*>* temp = m_lChildren.First();

	// destroy all my children first
	if(temp!= NULL)
	{
	   while(temp)
	   {
		  SceneNode* toRelease = temp->Data();
		  toRelease->destroy();
		  DeletePointer(toRelease);
		  temp = temp->Next();
	   }
	}
}

void SceneNode::addChild(SceneNode* newNode)
{
	// add a node in my superCool list
	newNode->setParent(this);
	m_lChildren.putBACK(newNode);
}
