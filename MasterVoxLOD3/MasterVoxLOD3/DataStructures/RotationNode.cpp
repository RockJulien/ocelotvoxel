#include "RotationNode.h"

RotationNode::RotationNode() :
SceneNode(), m_fRotationX(1.0f), m_fRotationY(1.0f), m_fRotationZ(1.0f)
{
	m_parent      = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matRotation.Matrix4x4Identity();
}

RotationNode::RotationNode(float rotationXYZ) :
SceneNode(), m_fRotationX(rotationXYZ), m_fRotationY(rotationXYZ), m_fRotationZ(rotationXYZ)
{
	m_parent      = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matRotation.Matrix4x4Identity();
}

RotationNode::RotationNode(float rotationX, float rotationY, float rotationZ) :
SceneNode(), m_fRotationX(rotationX), m_fRotationY(rotationY), m_fRotationZ(rotationZ)
{
	m_parent      = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matRotation.Matrix4x4Identity();
}

RotationNode::RotationNode(const Vector3& rotation) :
SceneNode(), m_fRotationX(rotation.getX()), m_fRotationY(rotation.getY()), m_fRotationZ(rotation.getZ())
{
	m_parent      = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matRotation.Matrix4x4Identity();
}

RotationNode::~RotationNode()
{
	SceneNode::destroy();
}

void     RotationNode::update(float deltaTime)
{
	// reset each frame matrices
	m_matWorld.Matrix4x4Identity();
	m_matRotation.Matrix4x4Identity();

	if(m_parent)
	{
		// apply matrix of the parent
		m_matWorld = m_matWorld * m_parent->getWorld(); // no method for multiplying
	}

	// apply rotation of the node
	m_matRotation.Matrix4x4RotationGeneralized(m_matRotation, m_fRotationX, m_fRotationY, m_fRotationZ);

	// rotate only regarding to the mesh and not apply potential previous applied translation or whatever.
	Matrix4x4 invWorld;
	invWorld.Matrix4x4Identity();
	m_matWorld.Matrix4x4Inverse(invWorld);

	invWorld.Matrix4x4Mutliply(invWorld, m_matRotation);
	invWorld.Matrix4x4Inverse(m_matWorld);
	//m_matWorld = m_matWorld * m_matRotation;

	// update children
	SceneNode::update(deltaTime);
}

void	 RotationNode::applyScale(float sX, float sY, float sZ)
{

}

void	 RotationNode::applyTrans(float tX, float tY, float tZ)
{

}

void	 RotationNode::applyRotat(float rX, float rY, float rZ)
{
	m_fRotationX = rX;
	m_fRotationY = rY;
	m_fRotationZ = rZ;
}
