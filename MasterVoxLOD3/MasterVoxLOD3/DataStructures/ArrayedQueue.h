#ifndef DEF_ARRAYEDQUEUE_H
#define DEF_ARRAYEDQUEUE_H

#include "Array1D.h"

template<class TheType>
class ArrayedQueue : public Array1D<TheType>
{
private:

	// Attributes
	BIGINT m_iFront;
	BIGINT m_iNbCell;

public:

	// Constructor & Destructor
	ArrayedQueue(BIGINT iNbCell);
	~ArrayedQueue();

	// Methods
	BBOOL   putBack(TheType uData);
	VVOID   popFRONT();
	TheType poleValue();

	// Accessors
	// Access Operator
	TheType& operator[] (BIGINT iIndex);
};

template<class TheType>
ArrayedQueue<TheType>::ArrayedQueue(BIGINT iNbCell) :
Array1D<TheType>(iNbCell), m_iFront(0), m_iNbCell(0)
{

}

template<class TheType>
ArrayedQueue<TheType>::~ArrayedQueue()
{
	Array1D<TheType>::~Array1D<TheType>();
}

template<class TheType>
BBOOL   ArrayedQueue<TheType>::putBack(TheType uData)
{
	// the modulo wrap around the array for making a circular queue.
	if(m_iNbData != m_iNbCell)
	{
		m_uData[(m_iNbCell + m_iFront) % m_iNbData] = uData;
		m_iNbCell++;
		return true;
	}

	return false;
}

template<class TheType>
VVOID   ArrayedQueue<TheType>::popFRONT()
{
	if(m_iNbCell > 0)
	{
		m_iNbCell--;
		m_iFront++;

		// check if still something in the queue
		// else reset to 0.
		if(m_iFront == m_iNbData)
			m_iFront = 0;
	}
}

template<class TheType>
TheType ArrayedQueue<TheType>::poleValue()
{
	return m_uData[m_iFront];
}

template<class TheType>
TheType& ArrayedQueue<TheType>::operator[] (BIGINT iIndex)
{
	return m_uData[(iIndex + m_iFront) % m_iNbData];
}

#endif