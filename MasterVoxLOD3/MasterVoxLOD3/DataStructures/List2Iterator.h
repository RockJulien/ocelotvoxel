#ifndef DEF_LIST2ITERATOR_H
#define DEF_LIST2ITERATOR_H

template<class TheType>
class Linked2List;

template<class TheType>
class List2Iterator
{
private:

	// Attributes
	List2Data<TheType>*   m_dCurrCell;
	Linked2List<TheType>* m_lList;

public:

	// Constructor & Destructor
	List2Iterator(Linked2List<TheType>* list = 0, List2Data<TheType>* currCell = 0);
	~List2Iterator();

	// Methods
	VVOID    begin();
	VVOID    next();
	VVOID    previous();
	TheType  data();
	VVOID    setData(TheType uData);
	BBOOL    notEnd();

	// Accessors
	inline List2Data<TheType>*   CurrCell() { return m_dCurrCell;}
	inline Linked2List<TheType>* List() { return m_lList;}
	inline VVOID                 SetCurrCell(List2Data<TheType>* curr) { m_dCurrCell = curr;}
	inline VVOID                 SetList(Linked2List<TheType>* list) { m_lList = list;}
};

template<class TheType>
List2Iterator<TheType>::List2Iterator(Linked2List<TheType>* list, List2Data<TheType>* currCell):
m_lList(list), m_dCurrCell(currCell)
{

}

template<class TheType>
List2Iterator<TheType>::~List2Iterator()
{

}

template<class TheType>
VVOID    List2Iterator<TheType>::begin()
{
	if(m_lList)
		m_dCurrCell = m_lList->First();
}

template<class TheType>
VVOID    List2Iterator<TheType>::next()
{
	if(m_dCurrCell)
		m_dCurrCell = m_dCurrCell->Next();
}

template<class TheType>
VVOID    List2Iterator<TheType>::previous()
{
	if(m_dCurrCell)
		m_dCurrCell = m_dCurrCell->Previous();
}

template<class TheType>
TheType  List2Iterator<TheType>::data()
{
	return m_dCurrCell->Data();
}

template<class TheType>
VVOID    List2Iterator<TheType>::setData(TheType uData)
{
	m_dCurrCell->SetData(uData);
}

template<class TheType>
BBOOL    List2Iterator<TheType>::notEnd()
{
	if(m_dCurrCell)
		return true;
	else
		return false;
}

#endif