#ifndef DEF_TREEITERATOR_H
#define DEF_TREEITERATOR_H

#include "Linked2List.h"
#include "TreeNode.h"

template<class TheType, class SearchType>
class TreeIterator
{
public:

	// Re-Definition
	typedef TreeNode<TheType, SearchType> Node;

private:

	// Attributes
	Node*                m_currNode;
	List2Iterator<Node*> m_childIterator;

public:

	// Constructor & Destructor
	TreeIterator(Node* node = 0);
	~TreeIterator();

	// Methods
	VVOID   backToRoot();
	VVOID   levelUP();
	VVOID   levelDOWN();
	VVOID   firstChild();
	VVOID   nextChild();
	VVOID   prevChild();
	VVOID   lastChild();
	VVOID   putChildBACK(Node* newBrother);
	VVOID   putChildFRONT(Node* newBrother);
	VVOID   insertChildBefore(Node* newBrother);
	VVOID   insertChildAfter(Node* newBrother);
	VVOID   removeChild();
	BBOOL   childValid();
	TheType currData();
	TheType childData();
	VVOID   resetIterator();
	VVOID   preOrder(Node* root, VVOID (*functionToApply)(Node*));
	VVOID   postOrder(Node* root, VVOID (*functionToApply)(Node*));

	// Assignment Operator
	VVOID operator= (Node* node);

	// Accessors
	inline Node* CurrNode() const { return m_currNode;}
};

template<class TheType, class SearchType>
TreeIterator<TheType, SearchType>::TreeIterator(Node* node = 0) :
m_currNode(node)
{
	//resetIterator();
}

template<class TheType, class SearchType>
TreeIterator<TheType, SearchType>::~TreeIterator()
{

}

template<class TheType, class SearchType>
VVOID TreeIterator<TheType, SearchType>::backToRoot()
{
	while(m_currNode->Parent())
		levelUP();
}

template<class TheType, class SearchType>
VVOID TreeIterator<TheType, SearchType>::levelUP()
{
	if(m_currNode)
		m_currNode = m_currNode->Parent();

	resetIterator();
}

template<class TheType, class SearchType>
VVOID TreeIterator<TheType, SearchType>::levelDOWN()
{
	if(m_childIterator.notEnd())
	{
		m_currNode = m_childIterator.data();
		resetIterator();
	}
}

template<class TheType, class SearchType>
VVOID TreeIterator<TheType, SearchType>::firstChild()
{
	m_childIterator.begin();
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::nextChild()
{
	m_childIterator.next();
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::prevChild()
{
	m_childIterator.previous();
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::lastChild()
{
	while(m_childIterator.CurrCell()->Next())
		m_childIterator.next();
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::putChildBACK(Node* newBrother)
{
	// add the child at the back to the list
	if(m_currNode)
		m_currNode->Children()->putBACK(newBrother);
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::putChildFRONT(Node* newBrother)
{
	// add the child at the front to the list
	if(m_currNode)
		m_currNode->Children()->putFRONT(newBrother);
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::insertChildBefore(Node* newBrother)
{
	// first, move the iterator to the previous one.
	prevChild();

	// then, insert before the previous iterator position.
	if(m_currNode)
		m_currNode->Children()->insert(m_childIterator, newBrother);
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::insertChildAfter(Node* newBrother)
{
	if(m_currNode)
		m_currNode->Children()->insert(m_childIterator, newBrother);
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::removeChild()
{
	if(m_currNode)
	{
		m_currNode->Children()->remove(m_childIterator);
	}
}

template<class TheType, class SearchType>
BBOOL   TreeIterator<TheType, SearchType>::childValid()
{
	return m_childIterator.notEnd();
}

template<class TheType, class SearchType>
TheType TreeIterator<TheType, SearchType>::currData()
{
	return m_currNode->Data();
}

template<class TheType, class SearchType>
TheType TreeIterator<TheType, SearchType>::childData()
{
	return m_childIterator.data()->Data();
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::resetIterator()
{
	if(m_currNode)
	{
		Linked2List<Node*>* tempList = m_currNode->Children();
		m_childIterator = tempList->iterator();
		//tempList.SetSize(0); // Block the destructor.
	}
	else
	{
		m_childIterator.SetList(NULL);
		m_childIterator.SetCurrCell(NULL);
	}
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::operator= (Node* node)
{
	m_currNode = node;
	resetIterator();
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::preOrder(Node* root, VVOID (*functionToApply)(Node*))
{
	functionToApply(root);

	Linked2List<Node*>* tempList   = root->Children();
	List2Iterator<Node*> iterator = tempList->iterator();
	// iterate through the children for recursively
	// apply stuff to apply.
	for(iterator.begin(); iterator.notEnd(); iterator.next())
		preOrder(iterator.data(), functionToApply);

	//tempList->SetSize(0); // block the destruction of the copied list destructor. (Will be automatic when linked list copy assignment definition will be done with a boolean telling if a list is a copy or the original when trying to delete.
}

template<class TheType, class SearchType>
VVOID   TreeIterator<TheType, SearchType>::postOrder(Node* root, VVOID (*functionToApply)(Node*))
{
	Linked2List<Node*>* tempList   = root->Children();
	List2Iterator<Node*> iterator = tempList->iterator();
	// iterate through the children for recursively
	// apply stuff to apply.
	for(iterator.begin(); iterator.notEnd(); iterator.next())
		postOrder(iterator.data(), functionToApply);

	//tempList->SetSize(0); // block the destruction of the copied list destructor. (Will be automatic when linked list copy assignment definition will be done with a boolean telling if a list is a copy or the original when trying to delete.

	functionToApply(root);
}

#endif