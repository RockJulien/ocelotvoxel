#ifndef DEF_GRAPGBRIDGE_H
#define DEF_GRAPGBRIDGE_H

template<class NodeType, class BridgeType>
class GraphNode;

template<class NodeType, class BridgeType>
class GraphBridge
{
public:

	// Re-Definition
	typedef GraphNode<NodeType, BridgeType> Node;

private:

	// Attributes
	Node*      m_nNode;
	BridgeType m_uCost;

public:

	// Constructor & Destructor
	GraphBridge(BridgeType uCost);
	~GraphBridge();

	// Methods


	// Accessors
	inline BridgeType Cost() const { return m_uCost;}
	inline Node*      Connection() const { return m_nNode;}
	inline VVOID      SetCost(BridgeType cost) { m_uCost = cost;}
	inline VVOID      SetConnection(Node* conn) { m_nNode = conn;}
};

template<class NodeType, class BridgeType>
GraphBridge<NodeType, BridgeType>::GraphBridge(BridgeType uCost) :
m_uCost(uCost), m_nNode(0)
{

}

template<class NodeType, class BridgeType>
GraphBridge<NodeType, BridgeType>::~GraphBridge()
{

}

#endif