#include "SceneGraph.h"

SceneGraph::SceneGraph() :
m_vBasePosition(Vector3(0.0f)), m_root(NULL)
{

}

SceneGraph::SceneGraph(const Vector3& startPosition) : 
m_vBasePosition(startPosition), m_root(NULL)
{

}

SceneGraph::~SceneGraph()
{
	destroy();
}

SceneGraph* SceneGraph::instance()
{
	static SceneGraph instance;

	return &instance;
}

void SceneGraph::addNode(SceneNode* node)
{
	// if it is the beginning of the scene graph
	if(m_root == NULL)
	{
		if(m_vBasePosition != 0.0f)
			m_root = new TranslationNode(m_vBasePosition);
		else 
			m_root = new TranslationNode(Vector3(0.0f));
	}

	// then add new node as the child of the root of 
	// the scene graph
	m_root->addChild(node);
}

void SceneGraph::update(float deltaTime) 
{
	// update the root and its child or children
	// the children of its children and so on.
	if(m_root != NULL)
	{
		m_root->update(deltaTime);
	}
}

void SceneGraph::Release() 
{
	if(this)
		delete this;
}

void SceneGraph::destroy() 
{
	if(m_root)
	{
		m_root->destroy();
		DeletePointer(m_root);
	}
}

TranslationNode* SceneGraph::Root() const
{
	return m_root;
}
