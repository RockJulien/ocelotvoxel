#include "GeometryNode.h"

//using namespace Ocelot;

GeometryNode::GeometryNode() : SceneNode()//, m_mIEntity(NULL)
{
	m_parent = NULL;
}

/*GeometryNode::GeometryNode(IDXBaseMesh* entity) : SceneNode(), m_mIEntity(entity)
{
	m_parent = NULL;
}*/

GeometryNode::~GeometryNode()
{
	SceneNode::destroy();
}

void GeometryNode::update(float deltaTime)
{
	// update geometry first
	//m_mIEntity->setWorld(m_parent->getWorld());
	//m_mIEntity->update(deltaTime);

	// then update children
	SceneNode::update(deltaTime);
}
