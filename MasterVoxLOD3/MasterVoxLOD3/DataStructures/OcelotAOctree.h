#ifndef DEF_OCELOTAOCTREE_H
#define DEF_OCELOTAOCTREE_H

#include "ATreeNode.h"

namespace Ocelot
{
	template<class TheType, class SearchType>
	class OcelotAOctree : public ATreeNode<TheType, SearchType>
	{
	private:

		// Attributes

	public:

		// Constructor & Destructor
		OcelotAOctree();
		OcelotAOctree(TheType uData);
		~OcelotAOctree();

		// Methods


		// Accessors

	};

	template<class TheType, class SearchType>
	OcelotAOctree<TheType, SearchType>::OcelotAOctree() :
	ATreeNode<TheType, SearchType>()
	{

	}

	template<class TheType, class SearchType>
	OcelotAOctree<TheType, SearchType>::OcelotAOctree(TheType uData) :
	ATreeNode<TheType, SearchType>(uData, 8)
	{

	}

	template<class TheType, class SearchType>
	OcelotAOctree<TheType, SearchType>::~OcelotAOctree()
	{

	}
}

#endif