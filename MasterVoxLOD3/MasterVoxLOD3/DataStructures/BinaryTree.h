#ifndef DEF_BINARYTREE_H
#define DEF_BINARYTREE_H

#include "DataStructureUtil.h"

template<class TheType>
class BinaryTree
{
public:

	// Re-Definition
	typedef BinaryTree<TheType> Node;

private:

	// Attributes
	TheType m_uData;
	Node*   m_nParent;
	Node*   m_nLeft;
	Node*   m_nRight;

public:

	// Constructor & Destructor
	BinaryTree(TheType uData);
	~BinaryTree();

	// Methods
	VVOID   destroy();
	UBIGINT count();
	VVOID   preOrder(Node* node, VVOID (*functionToApply)(Node*));
	VVOID   postOrder(Node* node, VVOID (*functionToApply)(Node*));
	VVOID   inOrder(Node* node, VVOID (*functionToApply)(Node*));

	// Accessors
	inline TheType Data() const { return m_uData;}
	inline Node*   Parent() const { return m_nParent;}
	inline Node*   Left() const { return m_nLeft;}
	inline Node*   Right() const { return m_nRight;}
	inline VVOID   SetParent(Node* parent) { m_nParent = parent;}
	inline VVOID   SetLeft(Node* left) { m_nLeft = left;}
	inline VVOID   SetRight(Node* right) { m_nRight = right;}
};

template<class TheType>
BinaryTree<TheType>::BinaryTree(TheType uData) :
m_uData(uData), m_nParent(0), m_nLeft(0), m_nRight(0)
{

}

template<class TheType>
BinaryTree<TheType>::~BinaryTree()
{
	destroy();
}

template<class TheType>
VVOID   BinaryTree<TheType>::destroy()
{
	DeletePointer(m_nLeft);
	DeletePointer(m_nRight);
}

template<class TheType>
UBIGINT BinaryTree<TheType>::count()
{
	UBIGINT c = 1;
	// check if left child.
	if(m_nLeft)
		c += m_nLeft->count();
	// check if right child.
	if(m_nRight)
		c += m_nRight->count();

	return c;
}

template<class TheType>
VVOID   BinaryTree<TheType>::preOrder(Node* node, VVOID (*functionToApply)(Node*))
{
	functionToApply(node);
	preOrder(node->Left(), functionToApply);
	preOrder(node->Right(), functionToApply);
}

template<class TheType>
VVOID   BinaryTree<TheType>::postOrder(Node* node, VVOID (*functionToApply)(Node*))
{
	postOrder(node->Left(), functionToApply);
	postOrder(node->Right(), functionToApply);
	functionToApply(node);
}

template<class TheType>
VVOID   BinaryTree<TheType>::inOrder(Node* node, VVOID (*functionToApply)(Node*))
{
	inOrder(node->Left(), functionToApply);
	functionToApply(node);
	inOrder(node->Right(), functionToApply);
}

#endif