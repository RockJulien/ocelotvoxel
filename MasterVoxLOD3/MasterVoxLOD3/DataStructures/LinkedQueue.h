#ifndef DEF_LINKEDQUEUE_H
#define DEF_LINKEDQUEUE_H

#include "Linked2List.h"

template<class TheType>
class LinkedQueue : public Linked2List<TheType>
{
private:

	// Attributes

public:

	// Constructor & Destructor
	LinkedQueue();
	~LinkedQueue();

	// Methods
	VVOID   pushBACK(TheType uData);
	VVOID   popFRONT();
	TheType poleValue();

	// Accessors

};

template<class TheType>
LinkedQueue<TheType>::LinkedQueue() :
Linked2List<TheType>()
{

}

template<class TheType>
LinkedQueue<TheType>::~LinkedQueue()
{
	Linked2List<TheType>::~Linked2List<TheType>();
}

template<class TheType>
VVOID   LinkedQueue<TheType>::pushBACK(TheType uData)
{
	putBACK(uData);
}

template<class TheType>
VVOID   LinkedQueue<TheType>::popFRONT()
{
	removeFRONT();
}

template<class TheType>
TheType LinkedQueue<TheType>::poleValue()
{
	return m_dFirst->Data();
}

#endif