#ifndef DEF_GRAPHSCENE_H
#define DEF_GRAPHSCENE_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   SceneGraph.h/.cpp
/
/ Description: This file provide a way to create a scene graph for 
/              allowing multiple entities to move in a same way
/              regarding to their world position with just a translation
/              rotation or different scaling regarding to each other.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "TranslationNode.h"
#include "RotationNode.h"
#include "ScalingNode.h"
#include "GeometryNode.h"

#define SG SceneGraph::instance()

class SceneGraph
{
private:

		TranslationNode* m_root;
		Vector3          m_vBasePosition;

		// copy & assignment
		SceneGraph(const SceneGraph&);
		SceneGraph& operator = (const SceneGraph&);

public:

		// Constructors
	    SceneGraph();
		SceneGraph(const Vector3& startPosition);
		//Destructor
		~SceneGraph();

		// Singleton
static SceneGraph* instance();

		// Methods
		void addNode(SceneNode* node);
		void update(float deltaTime); // update our scene node
		void Release();               // release this object from the memory
		void destroy();               // destroy all the children

		// Accessors
		TranslationNode* Root() const;
};

#endif