#ifndef DEF_DOFNODE_H
#define DEF_DOFNODE_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   TranslationNode.h/.cpp
/
/ Description: This file provide a way to create a translation node 
/              which will translate an entity regarding to its parent
/              world position for allowing complex group or object 
/              displacement and therefore move together.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "SceneNode.h"

class TranslationNode : public SceneNode   // transformation node which apply a transformation to the entity
{
private:

	// Attributes of the translation node
	float      m_fTranslationX;
	float      m_fTranslationY;
	float      m_fTranslationZ;
	Matrix4x4  m_matTranslation;

public:
	
	// Constructor & Destructor
	TranslationNode();
	TranslationNode(float translationXYZ);
	TranslationNode(float translationX, float translationY, float translationZ);
	TranslationNode(const Vector3& translation);
	~TranslationNode();

	// Methods
	void     update(float deltaTime);
	void	 applyScale(float sX, float sY, float sZ);
	void	 applyTrans(float tX, float tY, float tZ);
	void	 applyRotat(float rX, float rY, float rZ);

	// Accessors
	float    TranslationX() const { return m_fTranslationX;}
	float    TranslationY() const { return m_fTranslationY;}
	float    TranslationZ() const { return m_fTranslationZ;}
	Vector3  TranslationXYZ() const { return Vector3(m_fTranslationX, m_fTranslationY, m_fTranslationZ);}
	void     setTranslation(float translationXYZ) { m_fTranslationX = translationXYZ; m_fTranslationY = translationXYZ; m_fTranslationZ = translationXYZ;}
	void     setTranslation(float translationX, float translationY, float translationZ) { m_fTranslationX = translationX; m_fTranslationY = translationY; m_fTranslationZ = translationZ;}
	void     setTranslation(Vector3& translation) { m_fTranslationX = translation.getX(); m_fTranslationY = translation.getY(); m_fTranslationZ = translation.getZ();}
};

#endif