#ifndef DEF_LISTITERATOR_H
#define DEF_LISTITERATOR_H

template<class TheType>
class LinkedList;

template<class TheType>
class ListIterator
{
private:

	// Attributes
	ListData<TheType>*   m_dCurrCell;
	LinkedList<TheType>* m_lList;

public:

	// Constructor & Destructor
	ListIterator(LinkedList<TheType>* list = 0, ListData<TheType>* currCell = 0);
	~ListIterator();

	// Methods
	VVOID    begin();
	VVOID    next();
	TheType& data();
	BBOOL    notEnd();

	// Accessors
	inline LinkedList<TheType>* List() const { return m_lList;}
	inline ListData<TheType>* CurrCell() const { return m_dCurrCell;}
};

template<class TheType>
ListIterator<TheType>::ListIterator(LinkedList<TheType>* list, ListData<TheType>* currCell) :
m_lList(list), m_dCurrCell(currCell)
{

}

template<class TheType>
ListIterator<TheType>::~ListIterator()
{

}

template<class TheType>
VVOID    ListIterator<TheType>::begin()
{
	if(m_lList)
		m_dCurrCell = m_lList->First();
}

template<class TheType>
VVOID    ListIterator<TheType>::next()
{
	if(m_dCurrCell)
		m_dCurrCell = m_dCurrCell->Next();
}

template<class TheType>
TheType& ListIterator<TheType>::data()
{
	return m_dCurrCell->Data();
}

template<class TheType>
BBOOL    ListIterator<TheType>::notEnd()
{
	if(m_dCurrCell)
		return true;
	else
		return false;
}

#endif