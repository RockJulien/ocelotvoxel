#ifndef DEF_TREENODE_H
#define DEF_TREENODE_H

#include "Linked2List.h"

template<class TheType, class SearchType>
class TreeNode
{
public:

		// Re-definition
		typedef TreeNode<TheType, SearchType> Node;

protected:

		// Attributes
		TheType             m_uData;
		Node*               m_nParent;
		Linked2List<Node*>* m_lChildren;

public:

		// Constructor & Destructor
		TreeNode();
		TreeNode(TheType uData);
virtual ~TreeNode();

		// Methods
virtual VVOID   addChild(Node* newChild, SMALLINT index = -1);
		VVOID   removeFirstChild();
		VVOID   removeLastChild();
virtual VVOID   remove(SMALLINT index);
		VVOID   destroy();
		UBIGINT subCount();
		Node*   search(Node* (*searchFunction)(Node*, SearchType toSearch), SearchType toSearch, BBOOL upDown);
		VVOID   applyToTree(VVOID (*functionToApply)(Node*));

		// Accessors
		inline TheType              Data() const { return m_uData;}
		inline Node*                Parent() const { return m_nParent;}
		inline Linked2List<Node*>*  Children() const { return m_lChildren;}
		inline List2Iterator<Node*> Iterator() const { return m_lChildren->iterator();}
		inline VVOID                SetData(TheType uData) { m_uData = uData;}
		inline VVOID                SetParent(Node* parent) { m_nParent = parent;}
			   VVOID                SetChildren(Linked2List<Node*>* children) 
									{ 
										if(m_lChildren) 
											destroy();

										m_lChildren = children;

										List2Iterator<Node*> iter = m_lChildren->iterator();
										for(iter.begin(); iter.notEnd(); iter.next())
										{
											Node* temp = iter.data();
											temp->SetParent(this);
										}
									}
};

template<class TheType, class SearchType>
TreeNode<TheType, SearchType>::TreeNode() :
m_nParent(0)
{
	m_lChildren = new Linked2List<Node*>();
}

template<class TheType, class SearchType>
TreeNode<TheType, SearchType>::TreeNode(TheType uData) :
m_nParent(0), m_uData(uData)
{
	m_lChildren = new Linked2List<Node*>();
}

template<class TheType, class SearchType>
TreeNode<TheType, SearchType>::~TreeNode()
{
	destroy();
}

template<class TheType, class SearchType>
VVOID   TreeNode<TheType, SearchType>::addChild(Node* newChild, SMALLINT index)
{
	// if index is equal to one of the already existing children
	// and index is smaller than the size of the list.
	if((index >= 0) && (index < (USMALLINT)m_lChildren->Size()))
	{
		// overwrite the one at the index.
		USMALLINT counter = 0;
		List2Iterator<Node*> iter = m_lChildren->iterator();
		for(iter.begin(); iter.notEnd(); iter.next(), counter++)
		{
			if(counter == (USMALLINT)index)
			{
				Node* temp = iter.data();
				// Copy element from a pointed node to the other one
				// Will modify the object which is already in the list.
				temp->SetData(newChild->Data());
				temp->SetParent(this);

				Linked2List<Node*>* toAddList = newChild->Children();

				if(toAddList->Size() != 0) // concatenate.
				{
					List2Iterator<Node*> iter2  = toAddList->iterator();
					for(iter2.begin(); iter2.notEnd(); iter2.next())
					{
						// Create new pointer because old one will be destroyed when deleting newChild below.
						Node* tempNew = new Node(iter2.data()->Data());
						tempNew->SetParent(temp);
						tempNew->SetChildren(new Linked2List<Node*>(*iter2.data()->Children()));
						temp->addChild(tempNew);
					}
				}

				// Delete the newOne* for freeing memory needed no more.
				DeletePointer(newChild);
				return; // break;
			}
		}
	}
	// if index is greater than the list size or index equal -1.
	else if((index >= (USMALLINT)m_lChildren->Size()) || (index < 0))
	{
		// add directly at the suite (BACK).
		newChild->SetParent(this);
		m_lChildren->putBACK(newChild);
	}
}

template<class TheType, class SearchType>
VVOID   TreeNode<TheType, SearchType>::removeLastChild()
{
	// remove to the back of the list.
	// first destroy the intern pointer not auto managed by the list destructor.
	Node* toDestroy = m_lChildren->Last()->Data();
	DeletePointer(toDestroy);

	// Then, remove the list node.
	m_lChildren->removeBACK();
}

template<class TheType, class SearchType>
VVOID   TreeNode<TheType, SearchType>::removeFirstChild()
{
	// remove to the front of the list.
	// first destroy the intern pointer not auto managed by the list destructor.
	Node* toDestroy = m_lChildren->First()->Data();
	DeletePointer(toDestroy);

	// Then, remove the list node.
	m_lChildren->removeFRONT();
}

template<class TheType, class SearchType>
VVOID   TreeNode<TheType, SearchType>::remove(SMALLINT index)
{
	if(index < 0)
		return;

	// if index is in the range of the list, remove it
	if((index >= 0) && (index < (USMALLINT)m_lChildren->Size()))
	{
		// overwrite the one at the index.
		USMALLINT counter = 0;
		List2Iterator<Node*> iter = m_lChildren->iterator();
		for(iter.begin(); iter.notEnd(); iter.next(), counter++)
		{
			if(counter == (USMALLINT)index)
			{
				List2Data<Node*>* temp = iter.CurrCell();

				// First, destroy the intern pointer
				Node* internalNode = temp->Data();
				DeletePointer(internalNode);
				
				// Then, destroy the needed no more one.
				m_lChildren->remove(iter);

				return; // break;
			}
		}
	}
	// else nothing.
}

template<class TheType, class SearchType>
VVOID   TreeNode<TheType, SearchType>::destroy()
{
	if(m_lChildren->Size() != 0)
	{
		List2Iterator<Node*> iterator = m_lChildren->iterator();
		for(iterator.begin(); iterator.notEnd(); iterator.next())
		{
			Node* temp = iterator.data();
			DeletePointer(temp);
		}
	}

	DeletePointer(m_lChildren);
}

template<class TheType, class SearchType>
UBIGINT TreeNode<TheType, SearchType>::subCount()
{
	// 1 because of this node itself.
	UBIGINT c = 1;
	// iterate for counting subNodes of the first one until the last leaf.
	List2Iterator<Node*> iterator = m_lChildren->iterator();
	for(iterator.begin(); iterator.notEnd(); iterator.next())
		c += iterator.data()->subCount();

	return c;
}

template<class TheType, class SearchType>
TreeNode<TheType, SearchType>* TreeNode<TheType, SearchType>::search(Node* (*searchFunction)(Node*, SearchType toSearch), SearchType toSearch, BBOOL upDown)
{
	if(upDown)
	{
		Node* toReturn = searchFunction(this, toSearch);
		if(toReturn) // if found, return.
			return toReturn;

		List2Iterator<Node*> iter = m_lChildren->iterator();
		for(iter.begin(); iter.notEnd(); iter.next())
		{
			toReturn = iter.data()->search(searchFunction, toSearch, upDown);
			if(toReturn) // If founded in children.
				return toReturn;
		}
	}
	else
	{
		Node* toReturn = NULL;

		List2Iterator<Node*> iter = m_lChildren->iterator();
		for(iter.begin(); iter.notEnd(); iter.next())
		{
			toReturn = iter.data()->search(searchFunction, toSearch, upDown);
			if(toReturn) // If founded in children.
				return toReturn;
		}

		toReturn = searchFunction(this, toSearch);
		if(toReturn) // if found, return.
			return toReturn;
	}

	// Else, in the case nothing has been found.
	return NULL;
}

template<class TheType, class SearchType>
VVOID   TreeNode<TheType, SearchType>::applyToTree(VVOID (*functionToApply)(Node*))
{
	functionToApply(this);

	List2Iterator<Node*> iter = m_lChildren->iterator();
	for(iter.begin(); iter.notEnd(); iter.next())
		iter.data()->applyToTree(functionToApply);
}

#endif