#ifndef DEF_OCTREE_H
#define DEF_OCTREE_H

#include "TreeNode.h"
#include "../Maths/Vector3.h"

UBIGINT g_iNodeLevel;

						enum OctreeCorner
						{
							BOTTOMFRONTLEFT,
							BOTTOMBACKLEFT,
							TOPFRONTLEFT,
							TOPBACKLEFT,
							BOTTOMFRONTRIGHT,
							BOTTOMBACKRIGHT,
							TOPFRONTRIGHT,
							TOPBACKRIGHT
						};

template<class TheType, class SearchType>
class Octree : public TreeNode<TheType, SearchType>
{
private:

	// Attributes
	UBIGINT m_iNbUnknownData;
	Vector3 m_vCenter;    // center of vertices in the voxel (as a container) or the voxel itself (as a 3D pixel).
	FFLOAT  m_fDiameter;  // the diameter of the voxel (needed for sub division)
	BBOOL   m_bLeaf;      // for knowing if it is a node or a leaf of the global octree.

	// private methods
	VVOID setData(TheType uData, UBIGINT iNbUData);

public:

	// Constructor & Destructor
	Octree();
	~Octree();

	// Methods
	VVOID create(TheType uData, UBIGINT iNbUData);
	VVOID createNode(TheType uData, UBIGINT iNbUData, Vector3 vCenter, FFLOAT fDiam);
	VVOID createNodeEnd();
	VVOID release();

	// Accessors
	inline UBIGINT NbUDATA() const { return m_iNbUnknownData;}
	inline Vector3 Center() const { return m_vCenter;}
	inline FFLOAT  Diameter() const { return m_fDiameter;}
};

template<class TheType, class SearchType>
Octree<TheType, SearchType>::Octree() :
TreeNode<TheType, SearchType>(), m_iNbUnknownData(0), m_vCenter(0.0f),
m_fDiameter(0.0f)
{
	
}

template<class TheType, class SearchType>
Octree<TheType, SearchType>::~Octree()
{

}

template<class TheType, class SearchType>
VVOID Octree<TheType, SearchType>::create(TheType uData, UBIGINT iNbUData)
{
	m_iNbUnknownData = iNbUData;

	// set the data and compute the diameter and the center of the voxel.
	setData(uData, iNbUData);

	createNode(uData, iNbUData, m_vCenter, m_fDiameter);
}

template<class TheType, class SearchType>
VVOID Octree<TheType, SearchType>::setData(TheType uData, UBIGINT iNbUData)
{
	if((!uData) || (iNbUData <= 0))
		return;

	float CX = 0.0f;
	float CY = 0.0f;
	float CZ = 0.0f;

	// first find the center
	for(UBIGINT currPoint = 0; currPoint < iNbUData; currPoint++)
	{
		CX = m_vCenter.getX() + uData[currPoint].getX();
		CY = m_vCenter.getY() + uData[currPoint].getY();
		CZ = m_vCenter.getZ() + uData[currPoint].getZ();
		m_vCenter.x(CX);
		m_vCenter.y(CY);
		m_vCenter.z(CZ);
	}

	CX = m_vCenter.getX() / (float)iNbUData;
	CY = m_vCenter.getY() / (float)iNbUData;
	CZ = m_vCenter.getZ() / (float)iNbUData;
	m_vCenter.x(CX);
	m_vCenter.y(CY);
	m_vCenter.z(CZ);

	float widthMax  = 0.0f;
	float heightMax = 0.0f;
	float depthMax  = 0.0f;

	// second find the diameter
	for(UBIGINT currPoint = 0; currPoint < iNbUData; i++)
	{
		// will find only the radius.
		float wid = absf(uData[currPoint].getX() - m_vCenter.getX());
		float hei = absf(uData[currPoint].getY() - m_vCenter.getY());
		float dep = absf(uData[currPoint].getZ() - m_vCenter.getZ());

		if(wid > widthMax) widthMax = wid;
		if(hei > heightMax) heightMax = hei;
		if(dep > depthMax) depthMax = dep;
	}

	// transform in diameter
	widthMax  *= 2;
	heightMax *= 2;
	depthMax  *= 2;

	// and find the biggest.
	m_fDiameter = widthMax;
	if(heightMax > m_fDiameter) m_fDiameter = heightMax;
	if(depthMax > m_fDiameter) m_fDiameter = depthMax;
}

template<class TheType, class SearchType>
VVOID Octree<TheType, SearchType>::createNode(TheType uData, UBIGINT iNbUData, Vector3 vCenter, FFLOAT fDiam)
{

}

template<class TheType, class SearchType>
VVOID Octree<TheType, SearchType>::createNodeEnd()
{

}

template<class TheType, class SearchType>
VVOID Octree<TheType, SearchType>::release()
{
	TreeNode<TheType, SearchType>::destroy();
}

#endif