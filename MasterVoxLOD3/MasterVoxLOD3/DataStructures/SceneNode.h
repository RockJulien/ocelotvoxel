#ifndef DEF_SCENENODE_H
#define DEF_SCENENODE_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   SceneNode.h/.cpp
/
/ Description: This file provide a basic scene node with basic
/              components that every node of a scene graph needs.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Linked2List.h"
#include "..\Maths\Matrix4x4.h"

class SceneNode
{
protected:

		SceneNode*			     m_parent;
		Matrix4x4			     m_matWorld;  // change with the Phyre world matrix
		Linked2List<SceneNode*>  m_lChildren;

public:

		// Constructor & Destructor
		SceneNode();
virtual ~SceneNode();

		// Methods
		void Release();               // release this object from the memory
virtual void update(float deltaTime); // update our scene node
		void destroy();               // destroy all the children
		void addChild(SceneNode* newNode); // add a child
virtual void applyScale(float sX, float sY, float sZ) = 0;
virtual void applyTrans(float tX, float tY, float tZ) = 0;
virtual void applyRotat(float rX, float rY, float rZ) = 0;

		// Accessors
		inline Linked2List<SceneNode*> getList() const { return m_lChildren;}
		inline void                    setWorld(Matrix4x4 matWorld) { m_matWorld = matWorld;}
		inline Matrix4x4               getWorld() const { return m_matWorld;}
		inline SceneNode*			   Parent() const { return m_parent;}
		inline void					   setParent(SceneNode* parent) { m_parent = parent;}
};

#endif