#ifndef DEF_ATREENODE_H
#define DEF_ATREENODE_H

#include "Array1D.h"

namespace Ocelot
{
	template<class TheType, class SearchType>
	class ATreeNode
	{
	public:

			// Re-definition
			typedef ATreeNode<TheType, SearchType> Node;

	protected:

			// Attributes
			TheType        m_uData;
			Array1D<Node*> m_aChildren;

	public:

			// Constructor & Destructor
			ATreeNode();
			ATreeNode(TheType uData, UBIGINT ChildCount);
			ATreeNode(ATreeNode& toCopy);
			ATreeNode& operator= (ATreeNode& toAssign);
	virtual ~ATreeNode();

			// Methods
	        Node* search(Node* (*searchFunction)(Node*, SearchType toSearch), SearchType toSearch);
			VVOID applyToTree(VVOID (*functionToApply)(Node*));
			VVOID updateTree(VVOID (*updateFunction)(Node*, FFLOAT deltaTime), FFLOAT deltaTime);
			VVOID destroy();

			// Accessors
	inline  TheType Data() { return m_uData;}
	inline  Node*   Child(UBIGINT childIndex) { return m_aChildren[childIndex];}
	inline  VVOID   SetData(TheType uData) { m_uData = uData;}
	inline  VVOID   SetChild(UBIGINT childIndex, Node* sibling) { m_aChildren[childIndex] = sibling;}
	};

	template<class TheType, class SearchType>
	ATreeNode<TheType, SearchType>::ATreeNode() :
	m_aChildren(8)
	{
		for(UBIGINT currChild = 0; currChild < 8; currChild++)
			m_aChildren[currChild] = NULL;
	}

	template<class TheType, class SearchType>
	ATreeNode<TheType, SearchType>::ATreeNode(TheType uData, UBIGINT ChildCount) :
	m_aChildren(ChildCount), m_uData(uData)
	{
		for(UBIGINT currChild = 0; currChild < ChildCount; currChild++)
			m_aChildren[currChild] = NULL;
	}

	template<class TheType, class SearchType>
	ATreeNode<TheType, SearchType>::ATreeNode(ATreeNode<TheType, SearchType>& toCopy)
	{
		m_uData = toCopy.m_uData;

		m_aChildren.map(toCopy.m_aChildren.Size());

		for(UBIGINT currChild = 0; currChild < (UBIGINT)m_aChildren.Size(); currChild++)
			m_aChildren[currChild] = toCopy.m_aChildren[currChild];

		toCopy.m_aChildren.unmap();
	}

	template<class TheType, class SearchType>
	ATreeNode<TheType, SearchType>& ATreeNode<TheType, SearchType>::operator= (ATreeNode<TheType, SearchType>& toAssign)
	{
		m_uData = toAssign.m_uData;

		for(UBIGINT currChild = 0; currChild < (UBIGINT)m_aChildren.Size(); currChild++)
			if(m_aChildren[currChild])
				DeletePointer(m_aChildren[currChild]);

		m_aChildren.resize(toAssign.m_aChildren.Size());

		for(UBIGINT currChild = 0; currChild < (UBIGINT)m_aChildren.Size(); currChild++)
			m_aChildren[currChild] = toAssign.m_aChildren[currChild];

		toAssign.m_aChildren.unmap();

		return *this;
	}

	template<class TheType, class SearchType>
	ATreeNode<TheType, SearchType>::~ATreeNode()
	{
		destroy();
	}

	template<class TheType, class SearchType>
	ATreeNode<TheType, SearchType>* ATreeNode<TheType, SearchType>::search(Node* (*searchFunction)(Node*, SearchType toSearch), SearchType toSearch)
	{
		Node* toReturn = searchFunction(this, toSearch);
		if(toReturn)
			return toReturn;

		for(UBIGINT currChild = 0; currChild < (UBIGINT)m_aChildren.Size(); currChild++)
			if(m_aChildren[currChild])
			{
				toReturn = m_aChildren[currChild]->search(searchFunction, toSearch);
				if(toReturn)
					return toReturn;
			}

		return NULL;
	}

	template<class TheType, class SearchType>	
	VVOID ATreeNode<TheType, SearchType>::applyToTree(VVOID (*functionToApply)(Node*))
	{
		functionToApply(this);

		for(UBIGINT currChild = 0; currChild < (UBIGINT)m_aChildren.Size(); currChild++)
			if(m_aChildren[currChild])
				m_aChildren[currChild]->applyToTree(functionToApply);
	}

	template<class TheType, class SearchType>
	VVOID ATreeNode<TheType, SearchType>::updateTree(VVOID (*updateFunction)(Node*, FFLOAT deltaTime), FFLOAT deltaTime)
	{
		updateFunction(this, deltaTime);

		for(UBIGINT currChild = 0; currChild < (UBIGINT)m_aChildren.Size(); currChild++)
			if(m_aChildren[currChild])
				m_aChildren[currChild]->updateTree(updateFunction, deltaTime);
	}

	template<class TheType, class SearchType>	
	VVOID ATreeNode<TheType, SearchType>::destroy()
	{
		for(UBIGINT currChild = 0; currChild < (UBIGINT)m_aChildren.Size(); currChild++)
		{
			Node* temp = m_aChildren[currChild];
			DeletePointer(temp);
		}
	}
}

#endif