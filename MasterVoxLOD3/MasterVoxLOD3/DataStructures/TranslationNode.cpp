#include "TranslationNode.h"

TranslationNode::TranslationNode() :
SceneNode(), m_fTranslationX(1.0f), 
m_fTranslationY(1.0f), m_fTranslationZ(1.0f)
{
	m_parent         = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matTranslation.Matrix4x4Identity();
}

TranslationNode::TranslationNode(float translationXYZ) :
SceneNode(), m_fTranslationX(translationXYZ), 
m_fTranslationY(translationXYZ), m_fTranslationZ(translationXYZ)
{
	m_parent         = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matTranslation.Matrix4x4Identity();
}

TranslationNode::TranslationNode(float translationX, float translationY, float translationZ) :
SceneNode(), m_fTranslationX(translationX), 
m_fTranslationY(translationY), m_fTranslationZ(translationZ)
{
	m_parent         = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matTranslation.Matrix4x4Identity();
}

TranslationNode::TranslationNode(const Vector3& translation) : 
SceneNode(), m_fTranslationX(translation.getX()), 
m_fTranslationY(translation.getY()), m_fTranslationZ(translation.getZ())
{
	m_parent         = NULL;
	m_matWorld.Matrix4x4Identity();
	m_matTranslation.Matrix4x4Identity();
}

TranslationNode::~TranslationNode()
{
	SceneNode::destroy();
}

void TranslationNode::update(float deltaTime)
{
	// reset each frame
	m_matWorld.Matrix4x4Identity();
	m_matTranslation.Matrix4x4Identity();

	if(m_parent)
	{
		// apply the matrix of the parent
		m_matWorld = m_matWorld * m_parent->getWorld();
	}

	// make the transformation thanks to the new position for example
	m_matTranslation.Matrix4x4Translation(m_matTranslation, m_fTranslationX, m_fTranslationY, m_fTranslationZ);

	Matrix4x4 invWorld;
	invWorld.Matrix4x4Identity();
	m_matWorld.Matrix4x4Inverse(invWorld);

	invWorld.Matrix4x4Mutliply(invWorld, m_matTranslation);
	invWorld.Matrix4x4Inverse(m_matWorld);
	//m_matWorld = m_matWorld * m_matTranslation;

	// update children
	SceneNode::update(deltaTime);
}

void	 TranslationNode::applyScale(float sX, float sY, float sZ)
{

}

void	 TranslationNode::applyTrans(float tX, float tY, float tZ)
{
	m_fTranslationX = tX;
	m_fTranslationY = tY;
	m_fTranslationZ = tZ;
}

void	 TranslationNode::applyRotat(float rX, float rY, float rZ)
{

}
