#ifndef DEF_OCELOTCAMERAFACTORY_H
#define DEF_OCELOTCAMERAFACTORY_H

#include "OcelotOrbiterController.h"
#include "OcelotFlyerController.h"

namespace Ocelot
{

#define ControlFact OcelotCameraFactory::instance()

						enum OcelotControllerType
						{
							CAMERA_CONTROLLER_ORBITER,
							CAMERA_CONTROLLER_FLYER,
							CAMERA_CONTROLLER_WALKER
						};

	class OcelotCameraFactory
	{
	private:

			// Attributes
			IOcelotCameraController* m_currController;
			OcelotControllerType     m_iType;

			// Forbidden Constructor, Copy and Assignment.
			OcelotCameraFactory(){}
			OcelotCameraFactory(const OcelotCameraFactory&);
			OcelotCameraFactory& operator = (const OcelotCameraFactory&);

	public:

			// Destructor
			~OcelotCameraFactory();

			// Singleton.
	static  OcelotCameraFactory* instance();

			// Methods
			IOcelotCameraController* createController(OcelotCamera* camera, OcelotControllerType type, FFLOAT speed = 20.0f);

			// Accessors
	inline  OcelotControllerType Type() const { return m_iType;}
	};
}

#endif