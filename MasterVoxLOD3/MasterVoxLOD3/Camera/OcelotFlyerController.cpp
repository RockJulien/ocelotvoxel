#include "OcelotFlyerController.h"

using namespace Ocelot;

OcelotFlyerController::OcelotFlyerController() : 
OcelotOrbiterController(), m_fSpeed(20.0f)
{

}

OcelotFlyerController::OcelotFlyerController(OcelotCamera* camera, FFLOAT speed) : 
OcelotOrbiterController(camera), m_fSpeed(speed)
{

}

OcelotFlyerController::~OcelotFlyerController()
{

}

VVOID OcelotFlyerController::update(FFLOAT deltaTime)
{
	m_fDeltaTime = deltaTime;

	// update the camera.
	m_instance->update(deltaTime);

	// handle inputs.
	handleInput();
}

VVOID OcelotFlyerController::handleInput()
{
	// check for inputs if any.
	if(InMng->isPressed(Keyboard, OCELOT_UP))
		moveBackNForth(Forward);

	if(InMng->isPressed(Keyboard, OCELOT_DOWN))
		moveBackNForth(Backward);

	if(InMng->isPressed(Keyboard, OCELOT_LEFT))
		moveLeftNRight(LeftSide);

	if(InMng->isPressed(Keyboard, OCELOT_RIGHT))
		moveLeftNRight(RightSide);

	// apply rotation movements
	applyMovements();
}

VVOID OcelotFlyerController::applyMovements()
{
	OcelotOrbiterController::applyMovements();
}

VVOID OcelotFlyerController::pitch(FFLOAT angle)
{
	OcelotOrbiterController::pitch(angle);
}

VVOID OcelotFlyerController::yaw(FFLOAT angle)
{
	OcelotOrbiterController::yaw(angle);
}

VVOID OcelotFlyerController::roll(FFLOAT angle)
{
	// only for flying simulator camera.
	// very special.
}

VVOID OcelotFlyerController::moveBackNForth(Direction dir)
{
	// grab useful attributes.
	OcelotCameraInfo tempInfo = m_instance->Info();

	Vector3 eye    = tempInfo.Eye();
	Vector3 lookAt = tempInfo.LookAt();

	switch(dir)
	{
	case Forward:
				 eye += (m_fSpeed * m_fDeltaTime) * lookAt;
				 break;
	case Backward:
				 eye += -(m_fSpeed * m_fDeltaTime) * lookAt;
				 break;
	}

	// reset camera's attributes.
	tempInfo.SetEye(eye);
	m_instance->SetInfo(tempInfo);
}

VVOID OcelotFlyerController::moveLeftNRight(Direction dir)
{
	// grab useful attributes.
	OcelotCameraInfo tempInfo = m_instance->Info();

	Vector3 eye = tempInfo.Eye();

	switch(dir)
	{
	case LeftSide:
		eye += -(m_fSpeed * m_fDeltaTime) * m_vRight;
		break;
	case RightSide:
		eye += (m_fSpeed * m_fDeltaTime) * m_vRight;
		break;
	}

	// reset camera's attributes.
	tempInfo.SetEye(eye);
	m_instance->SetInfo(tempInfo);
}
