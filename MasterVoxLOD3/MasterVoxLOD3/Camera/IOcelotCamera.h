#ifndef DEF_IOCELOTCAMERA_H
#define DEF_IOCELOTCAMERA_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Camera.h/cpp
/   
/  Description: This object is an interface for a basic camera
/               simply allowing to see what is rendered.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    01/02/2012
/*****************************************************************/

#include "CameraDataType.h"
#include "..\DataStructures\DataStructureUtil.h"

namespace Ocelot
{
	class IOcelotCamera
	{
	public:

			// Constructor & Destructor
			IOcelotCamera(){}
	virtual ~IOcelotCamera(){}

			// Methods
	virtual VVOID update(FFLOAT deltaTime) = 0;
	virtual VVOID computeFrustumPlane()    = 0;
	virtual BBOOL cullMesh(const FFLOAT& posX, const FFLOAT& posY, const FFLOAT& posZ, const FFLOAT& radius) = 0;
	};
}

#endif