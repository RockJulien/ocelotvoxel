#ifndef DEF_OCELOTFLYERCONTROLLER_H
#define DEF_OCELOTFLYERCONTROLLER_H

#include "..\Input\InputManager.h"
#include "OcelotOrbiterController.h"

namespace Ocelot
{
										enum Direction
										{
											Forward,
											Backward,
											LeftSide,
											RightSide
										};

	class OcelotFlyerController : public OcelotOrbiterController
	{
	protected:

			// Attributes
			FFLOAT m_fSpeed;     // fly or walk speed.
			FFLOAT m_fDeltaTime; // keep a track of the elapsed time between two frames.

			// Private methods
			VVOID moveBackNForth(Direction dir);
			VVOID moveLeftNRight(Direction dir);

	public:

			// Constructor & Destructor
			OcelotFlyerController();
			OcelotFlyerController(OcelotCamera* camera, FFLOAT speed = 20.0f);
	virtual ~OcelotFlyerController();

			// Methods
	virtual VVOID update(FFLOAT deltaTime);
	virtual VVOID handleInput();
	virtual VVOID applyMovements();
	virtual VVOID pitch(FFLOAT angle);
	virtual VVOID yaw(FFLOAT angle);
	virtual VVOID roll(FFLOAT angle);

			// Accessors
	inline  FFLOAT Speed() const { return m_fSpeed;}
	inline  VVOID  SetSpeed(FFLOAT speed) { m_fSpeed = speed;}
	};
}

#endif