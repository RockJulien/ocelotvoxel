#ifndef DEF_IOCELOTCAMERACONTROLLER_H
#define DEF_IOCELOTCAMERACONTROLLER_H

#include "CameraDataType.h"
#include "..\DataStructures\DataStructureUtil.h"

namespace Ocelot
{
	class IOcelotCameraController
	{
	public:

			// Constructor & Destructor
			IOcelotCameraController(){}
	virtual ~IOcelotCameraController(){}

			// Methods
	virtual VVOID update(FFLOAT deltaTime) = 0;
	virtual VVOID handleInput()            = 0;
	virtual VVOID applyMovements()		   = 0;
	virtual VVOID pitch(FFLOAT angle)      = 0;
	virtual VVOID yaw(FFLOAT angle)        = 0;
	virtual VVOID roll(FFLOAT angle)       = 0;
	};
}

#endif