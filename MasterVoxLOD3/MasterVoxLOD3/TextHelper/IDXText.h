#ifndef DEF_IDXTEXT_DEF
#define DEF_IDXTEXT_DEF

#include "Text.h"

namespace Ocelot
{
					enum TextDrawType
					{
						TEXT_TYPE_DRAW_SINGLE,
						TEXT_TYPE_DRAW_MULTI,
						TEXT_TYPE_DRAW_3D
					};

	class IDXText : public Text
	{
	public:

			// Constructor & Destructor
			IDXText() : Text() {}
	virtual ~IDXText() {}

			// Methods
	virtual VVOID Update(FFLOAT deltaTime) = 0;
	virtual VVOID Render(BIGINT leftPos, BIGINT topPos, BIGINT windowWidth = 0, BIGINT windowHeight = 0) = 0;
	virtual VVOID OnResize()			   = 0;
	virtual VVOID CreateDXFont()		   = 0;
	virtual VVOID CreateSprite()           = 0;
	virtual VVOID CreateText3D()		   = 0;
	};
}

#endif