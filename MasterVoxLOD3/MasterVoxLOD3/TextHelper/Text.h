#ifndef DEF_TEXT_DEF
#define DEF_TEXT_DEF

#include "..\StringHelper\STRINGHelpers.h"

namespace Ocelot
{
	class Text
	{
	protected:

			// Attributes
			UBIGINT  m_iIndex;
			LWSTRING m_sLines;

	public:

			// Constructor & Destructor
			Text();
	virtual ~Text();

			// Methods
			VVOID addLine(WSTRING newStringOnLine);
			VVOID appendToLine(WSTRING stringToAppend, UBIGINT line);
			VVOID refresh(WSTRING toChangeWith, UBIGINT line);
			VVOID eraseLine(UBIGINT line);

			// Accessors
	inline  LWSTRING* Lines() { return &m_sLines;}
	inline  UBIGINT   Index() const { return m_iIndex;}
	inline  VVOID     SetIndex(UBIGINT index) { m_iIndex = index;}
	};
}

#endif