#ifndef DEF_OCELOTDXTEXT_H
#define DEF_OCELOTDXTEXT_H

#include "IDXText.h"
//#include <d3dx9.h>
#include "..\Geometry\BaseDXData.h"

namespace Ocelot
{
	typedef ID3DX10Font*   FontPtr;
	typedef ID3DX10Sprite* SpritePtr;
	typedef ID3DX10Mesh*   MeshPtr;

					class OcelotDXTextInfo : public BaseDXData
					{
					protected:

						// Attributes
						BIGINT    m_iHeight;
						UBIGINT   m_iWidth;
						UBIGINT   m_iWeight;
						UBIGINT   m_iMipLevel;
						BBOOL     m_bItalic;
						UBIGINT   m_iCharSet;
						UBIGINT   m_iPrecision;
						UBIGINT   m_iQuality;
						UBIGINT   m_iPitchNFam;
						WSTRING   m_sTextStyle;
						Color     m_textColor;

					public:

						// Constructor & Destructor
						OcelotDXTextInfo();
						OcelotDXTextInfo(DevPtr device, CamPtr camera, Color color, BIGINT height, UBIGINT width = 0, UBIGINT weight = FW_BOLD, UBIGINT mipLevel = 1, BBOOL italic = false, UBIGINT charSet = DEFAULT_CHARSET, UBIGINT precision = OUT_DEFAULT_PRECIS, UBIGINT quality = DEFAULT_QUALITY, UBIGINT pitchNFam = DEFAULT_PITCH | FF_DONTCARE, WSTRING textStyle = L"Arial");
						~OcelotDXTextInfo();

						// Accessors
						inline BIGINT  Height() const { return m_iHeight;}
						inline UBIGINT Width() const { return m_iWidth;}
						inline UBIGINT Weight() const { return m_iWeight;}
						inline UBIGINT MipLevel() const { return m_iMipLevel;}
						inline BBOOL   IsItalic() const { return m_bItalic;}
						inline UBIGINT CharSet() const { return m_iCharSet;}
						inline UBIGINT Precision() const { return m_iPrecision;}
						inline UBIGINT Quality() const { return m_iQuality;}
						inline UBIGINT PitchNName() const { return m_iPitchNFam;}
						inline WSTRING TextStyle() const { return m_sTextStyle;}
						inline Color   TextColor() const { return m_textColor;}
						inline VVOID   SetHeight(BIGINT height) { m_iHeight = height;}
						inline VVOID   SetWidth(UBIGINT width) { m_iWidth = width;}
						inline VVOID   SetWeight(UBIGINT weight) { m_iWeight = weight;}
						inline VVOID   SetMipLevel(UBIGINT mipLevel) { m_iMipLevel = mipLevel;}
						inline VVOID   SetItalic(BBOOL state) { m_bItalic = state;}
						inline VVOID   SetCharSet(UBIGINT charSet) { m_iCharSet = charSet;}
						inline VVOID   SetPrecision(UBIGINT precision) { m_iPrecision = precision;}
						inline VVOID   SetQuality(UBIGINT quality) { m_iQuality = quality;}
						inline VVOID   SetPitchNFam(UBIGINT pitchFam) { m_iPitchNFam = pitchFam;}
						inline VVOID   SetTextStyle(WSTRING textStyle) { m_sTextStyle = textStyle;}
						inline VVOID   SetColor(Color col) { m_textColor = col;}
					};

	class OcelotDXText : public IDXText
	{
	protected:

		// Attributes
		OcelotDXTextInfo m_info;
		TextDrawType     m_type;
		FontPtr          m_font;
		SpritePtr        m_textSprite;
		MeshPtr          m_3DTextMesh;

		// Private Methods
		VVOID CreateD3DXTextMesh();

	public:

		// Constructor & Destructor
		OcelotDXText();
		OcelotDXText(OcelotDXTextInfo info, TextDrawType type);
		~OcelotDXText();

		// Methods
		VVOID Update(FFLOAT deltaTime);
		VVOID Render(BIGINT leftPos, BIGINT topPos, BIGINT windowWidth = 0, BIGINT windowHeight = 0);
		VVOID OnResize();
		//VVOID OnLostDevice();
		VVOID CreateDXFont();
		VVOID CreateSprite();
		VVOID CreateText3D();

		// Accessors
		inline OcelotDXTextInfo Info() const { return m_info;}
		inline VVOID            SetInfo(OcelotDXTextInfo info) { m_info = info;}
	};
}

#endif