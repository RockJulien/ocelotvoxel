#ifndef DEF_AABBOX_H
#define DEF_AABBOX_H

#include "..\DataStructures\Array1D.h"
#include "..\Geometry\Line.h"
#include "IIntersectable.h"
#include "IRenderable.h"

namespace Ocelot
{
							enum AABBGenerator
							{
								AABB_GENERATOR_MINMAX,
								AABB_GENERATOR_EXTENT,
								AABB_GENERATOR_RADIUS
							};

	class AABBox : public IIntersectable, public IRenderable
	{
	private:

		// Attributes
		BaseDXData     m_baseData;
		Matrix4x4      m_matBoundsWorld;
		Array1D<Line*> m_bounds;
		Vector3        m_vMin;
		Vector3        m_vCenter;
		Vector3        m_vMax;

		// Private Methods
		VVOID unmap();

	public:

		// Constructor & Destructor
		AABBox();
		AABBox(BaseDXData base, const Vector3& minPoint, const Vector3& maxPoint, AABBGenerator gen);
		AABBox(BaseDXData base, const Vector3& start, const FFLOAT& rangeX, const FFLOAT& rangeY, const FFLOAT& rangeZ, AABBGenerator gen);
		AABBox(BaseDXData base, const FFLOAT& startX, const FFLOAT& startY, const FFLOAT& startZ, const FFLOAT& rangeX, const FFLOAT& rangeY, const FFLOAT& rangeZ, AABBGenerator gen);
		AABBox(AABBox& toCopy);
		AABBox& operator= (AABBox& toAssign);
		~AABBox();

		// Methods to use once
		BIGINT createInstance();
		VVOID  releaseInstance();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT renderMesh();

		// Intersection forced methods by inheritance.
		BBOOL  intersect(const Vector3& vec);
		BBOOL  intersect(const Ray& fired);
		BBOOL  intersect(const IIntersectable& toCheck, BoundType type);

		// Others.
		BBOOL  contains(const Vector3& point) const;

		// Operator overload.
		friend std::wofstream& operator<< (std::wofstream& output, AABBox box)
		{
			output << "Min: " << box.Minimum().getX() << " , " << box.Minimum().getY() << " , " << box.Minimum().getZ() << "\r\n";
			output << "Max: " << box.Maximum().getX() << " , " << box.Maximum().getY() << " , " << box.Maximum().getZ() << "\r\n";

			return output;
		}
		BBOOL operator== (const AABBox& toTest) const;
		BBOOL operator!= (const AABBox& toTest) const;

		// Generic Accessors.
		FFLOAT X(AttributeType toGet) const;
		FFLOAT Y(AttributeType toGet) const;
		FFLOAT Z(AttributeType toGet) const;

		// Accessors.
		inline Vector3 Minimum() const { return m_vMin;}
		inline Vector3 Maximum() const { return m_vMax;}
		inline Vector3 Center() const { return m_vCenter;}
		inline Vector3 Scale() const { return (m_vMax - m_vMin);}
		inline Line*   GetLine(USMALLINT lineIndex) { return m_bounds[lineIndex];}
		inline VVOID   SetBaseData(BaseDXData base) { m_baseData = base;}
		inline VVOID   SetWorld(Matrix4x4 world) { m_matBoundsWorld = world;}
	};
}

#endif