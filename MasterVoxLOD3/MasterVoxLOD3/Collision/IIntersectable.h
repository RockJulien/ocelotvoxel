#ifndef DEF_IINTERSECTABLE_H
#define DEF_IINTERSECTABLE_H

#include "..\DataStructures\DataStructureUtil.h"
#include "..\Maths\Ray.h"

namespace Ocelot
{
					enum BoundType
					{
						BOUNDTYPE_OBB,
						BOUNDTYPE_AABB,
						BOUNDTYPE_SPHERE
					};

					enum AttributeType
					{
						ATTRIBUTETYPE_MINIMUM,
						ATTRIBUTETYPE_MAXIMUM,
						ATTRIBUTETYPE_CENTER,
						ATTRIBUTETYPE_RADIUS
					};

	class IIntersectable
	{
	public:

			// Constructor & Destructor
			IIntersectable(){}
	virtual ~IIntersectable(){}

			// Methods
	virtual BBOOL intersect(const Vector3& vec) = 0;
	virtual BBOOL intersect(const Ray& fired)   = 0;
	virtual BBOOL intersect(const IIntersectable& toCheck, BoundType type) = 0;

			// Generic Accessors.
	virtual FFLOAT X(AttributeType toGet) const = 0;
	virtual FFLOAT Y(AttributeType toGet) const = 0;
	virtual FFLOAT Z(AttributeType toGet) const = 0;
	};
}

#endif