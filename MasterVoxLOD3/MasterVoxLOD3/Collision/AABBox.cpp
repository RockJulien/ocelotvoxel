#include "AABBox.h"

using namespace Ocelot;

AABBox::AABBox() : 
IIntersectable(), IRenderable(), m_baseData(), m_vMin(0.0f), m_vCenter(0.5f), m_vMax(1.0f)
{
	// twelve lines for a bounding box.
	m_bounds.resize(12);

	for(USMALLINT currLine = 0; currLine < 12; currLine++)
		m_bounds[currLine] = NULL;
}

AABBox::AABBox(BaseDXData base, const Vector3& minPoint, const Vector3& maxPoint, AABBGenerator gen) :
IIntersectable(), IRenderable(), m_baseData(base)
{
	switch(gen)
	{
	case AABB_GENERATOR_MINMAX:
							{
								m_vMin    = minPoint;
								m_vMax    = maxPoint;
								m_vCenter = m_vMin + (Scale() * 0.5f);
							}
							break;
	case AABB_GENERATOR_EXTENT:
							{
								m_vMin    = minPoint;
								m_vMax    = minPoint + maxPoint; // add the X, Y and Z range to the original (minimum starting point) one.
								m_vCenter = m_vMin + (Scale() * 0.5f);
							}
							break;
	case AABB_GENERATOR_RADIUS:
							{
								m_vCenter = minPoint; // will be the center.
								m_vMin    = minPoint - maxPoint; // max point contains radius in every axes.
								m_vMax    = minPoint + maxPoint;
							}
							break;
	}

	// twelve lines for a bounding box.
	m_bounds.resize(12);

	for(USMALLINT currLine = 0; currLine < 12; currLine++)
		m_bounds[currLine] = NULL;
}

AABBox::AABBox(BaseDXData base, const Vector3& start, const FFLOAT& rangeX, const FFLOAT& rangeY, const FFLOAT& rangeZ, AABBGenerator gen) :
IIntersectable(), IRenderable(), m_baseData(base)
{
	switch(gen)
	{
	case AABB_GENERATOR_MINMAX:
							{
								m_vMin    = start;
								m_vMax    = Vector3(rangeX, rangeY, rangeZ);
								m_vCenter = m_vMin + (Scale() * 0.5f);
							}
							break;
	case AABB_GENERATOR_EXTENT:
							{
								m_vMin    = start;
								m_vMax    = start + Vector3(rangeX, rangeY, rangeZ); // add the X, Y and Z range to the original (minimum starting point) one.
								m_vCenter = m_vMin + (Scale() * 0.5f);
							}
							break;
	case AABB_GENERATOR_RADIUS:
							{
								m_vCenter = start; // will be the center.
								m_vMin    = start - Vector3(rangeX, rangeY, rangeZ); // max point contains radius in every axes.
								m_vMax    = start + Vector3(rangeX, rangeY, rangeZ);
							}
							break;
	}

	// twelve lines for a bounding box.
	m_bounds.resize(12);

	for(USMALLINT currLine = 0; currLine < 12; currLine++)
		m_bounds[currLine] = NULL;
}

AABBox::AABBox(BaseDXData base, const FFLOAT& startX, const FFLOAT& startY, const FFLOAT& startZ, const FFLOAT& rangeX, const FFLOAT& rangeY, const FFLOAT& rangeZ, AABBGenerator gen) :
IIntersectable(), IRenderable(), m_baseData(base)
{
	switch(gen)
	{
	case AABB_GENERATOR_MINMAX:
							{
								m_vMin    = Vector3(startX, startY, startZ);
								m_vMax    = Vector3(rangeX, rangeY, rangeZ);
								m_vCenter = m_vMin + (Scale() * 0.5f);
							}
							break;
	case AABB_GENERATOR_EXTENT:
							{
								m_vMin    = Vector3(startX, startY, startZ);
								m_vMax    = Vector3(startX, startY, startZ) + Vector3(rangeX, rangeY, rangeZ); // add the X, Y and Z range to the original (minimum starting point) one.
								m_vCenter = m_vMin + (Scale() * 0.5f);
							}
							break;
	case AABB_GENERATOR_RADIUS:
							{
								m_vCenter = Vector3(startX, startY, startZ); // will be the center.
								m_vMin    = Vector3(startX, startY, startZ) - Vector3(rangeX, rangeY, rangeZ); // max point contains radius in every axes.
								m_vMax    = Vector3(startX, startY, startZ) + Vector3(rangeX, rangeY, rangeZ);
							}
							break;
	}

	// twelve lines for a bounding box.
	m_bounds.resize(12);

	for(USMALLINT currLine = 0; currLine < 12; currLine++)
		m_bounds[currLine] = NULL;
}

AABBox::AABBox(AABBox& toCopy)
{
	// Copy these elements without problem
	m_baseData       = toCopy.m_baseData;
	m_matBoundsWorld = toCopy.m_matBoundsWorld;
	m_vMin           = toCopy.m_vMin;
	m_vCenter        = toCopy.m_vCenter;
	m_vMax           = toCopy.m_vMax;

	// Particular for the array.
	m_bounds.resize(12);
	for(USMALLINT currLine = 0; currLine < 12; currLine++)
		m_bounds[currLine] = NULL;

	if(toCopy.m_bounds.Size() != 0)
		for(USMALLINT currLine = 0; currLine < 12; currLine++)
			m_bounds[currLine] = toCopy.GetLine(currLine);

	toCopy.unmap();
}

AABBox& AABBox::operator= (AABBox& toAssign)
{
	// Copy these elements without problem
	m_baseData       = toAssign.m_baseData;
	m_matBoundsWorld = toAssign.m_matBoundsWorld;
	m_vMin           = toAssign.m_vMin;
	m_vCenter        = toAssign.m_vCenter;
	m_vMax           = toAssign.m_vMax;

	if(m_bounds.Size() == 0)
	{
		m_bounds.resize(12);
		for(USMALLINT currLine = 0; currLine < 12; currLine++)
			m_bounds[currLine] = NULL;
	}

	for(USMALLINT currLine = 0; currLine < 12; currLine++)
	{
		// First, release Line of this if any
		if(m_bounds[currLine])
			DeletePointer(m_bounds[currLine]);

		// Then copy.
		if(toAssign.m_bounds.Size() != 0)
			m_bounds[currLine] = toAssign.GetLine(currLine);
	}

	toAssign.unmap();

	return *this;
}

AABBox::~AABBox()
{
	releaseInstance();
}

VVOID AABBox::unmap()
{
	m_bounds.unmap();
}

BIGINT AABBox::createInstance()
{
	HRESULT hr = S_OK;

	releaseInstance();

	// compute every point of the cube.
	FFLOAT minX = m_vMin.getX();
	FFLOAT minY = m_vMin.getY();
	FFLOAT minZ = m_vMin.getZ();
	FFLOAT maxX = m_vMax.getX();
	FFLOAT maxY = m_vMax.getY();
	FFLOAT maxZ = m_vMax.getZ();

	// face with minimum as leftBottomMin
	Vector3 rightBottomMin(maxX, minY, minZ);
	Vector3 rightTopMin(maxX, maxY, minZ);
	Vector3 leftTopMin(minX, maxY, minZ);

	// face with maximum as rightTopMax
	Vector3 rightBottomMax(maxX, minY, maxZ);
	Vector3 leftTopMax(minX, maxY, maxZ);
	Vector3 leftBottomMax(minX, minY, maxZ);

	// front face
	LineInfo info(m_baseData.Device(), m_baseData.GetCamera(), L"", L".\\Effects\\Line.fx", m_vMin, rightBottomMin);
	m_bounds[0] = new Line(info);
	info.SetFrom(rightBottomMin);
	info.SetTo(rightTopMin); 
	m_bounds[1] = new Line(info);
	info.SetFrom(rightTopMin);
	info.SetTo(leftTopMin); 
	m_bounds[2] = new Line(info);
	info.SetFrom(leftTopMin);
	info.SetTo(m_vMin); 
	m_bounds[3] = new Line(info);

	// right face
	info.SetFrom(rightBottomMin);
	info.SetTo(rightBottomMax); 
	m_bounds[4] = new Line(info);
	info.SetFrom(rightBottomMax);
	info.SetTo(m_vMax); 
	m_bounds[5] = new Line(info);
	info.SetFrom(m_vMax);
	info.SetTo(rightTopMin); 
	m_bounds[6] = new Line(info);

	// left face
	info.SetFrom(m_vMin);
	info.SetTo(leftBottomMax); 
	m_bounds[7] = new Line(info);
	info.SetFrom(leftBottomMax);
	info.SetTo(leftTopMax); 
	m_bounds[8] = new Line(info);
	info.SetFrom(leftTopMax);
	info.SetTo(leftTopMin); 
	m_bounds[9] = new Line(info);

	// back face
	info.SetFrom(m_vMax);
	info.SetTo(leftTopMax); 
	m_bounds[10] = new Line(info);
	info.SetFrom(rightBottomMax);
	info.SetTo(leftBottomMax); 
	m_bounds[11] = new Line(info);

	for(UBIGINT currLine = 0; currLine < (UBIGINT)m_bounds.Size(); currLine++)
	{
		HR(m_bounds[currLine]->createInstance());
	}

	return (BIGINT)hr;
}

VVOID  AABBox::releaseInstance()
{
	if(m_bounds.Size() != 0)
		for(UBIGINT currLine = 0; currLine < (UBIGINT)m_bounds.Size(); currLine++)
		{
			if(m_bounds[currLine])
			{
				Line* toDestroy = m_bounds[currLine];
				DeletePointer(toDestroy);
			}
		}
}

VVOID  AABBox::update(FFLOAT deltaTime)
{
	for(UBIGINT currLine = 0; currLine < (UBIGINT)m_bounds.Size(); currLine++)
	{
		// update the line's world matrix
		m_bounds[currLine]->SetWorld(m_matBoundsWorld);
		// update the meshes.
		m_bounds[currLine]->update(deltaTime);
	}
}

BIGINT AABBox::renderMesh()
{
	HRESULT hr = S_OK;

	for(UBIGINT currLine = 0; currLine < (UBIGINT)m_bounds.Size(); currLine++)
	{
		HR(m_bounds[currLine]->renderMesh());
	}

	return (BIGINT)hr;
}

BBOOL  AABBox::intersect(const Vector3& vec)
{
	return (vec.getX() >= m_vMin.getX()) && (vec.getX() <= m_vMax.getX()) &&
		   (vec.getY() >= m_vMin.getY()) && (vec.getY() <= m_vMax.getY()) &&
		   (vec.getZ() >= m_vMin.getZ()) && (vec.getZ() <= m_vMax.getZ());
}

BBOOL  AABBox::intersect(const Ray& fired)
{
	//The ray is tested against 3 slabs, where a slab is the distance between
	//parallel planes (we have 3 axes hence there will be 3 slabs), if the origin
	//of the ray lies to the side of any of these slabs, there is no intersection.
	//If there is, it will intersect at two places (giving two t's for the ray equation:
	//r = origin + (t * direction). If the minimum of these is larger than than the 
	//maximum, then there is no overlap (technically it means the quadratic solved to
	//obtain t has complex "imaginary" numbers as a solution, which we don't want)
	FFLOAT interpMin = -FLT_MAX;
	FFLOAT interpMax = FLT_MAX;

	//for each slab
	for(USMALLINT slab = 0; slab < 3; ++slab)
	{
		if(fabsf(fired.GetDirection()[slab]) < FLT_EPSILON)
		{
			//ray is parallel to the slab, no intersection if the ray is
			//not lying within slab
			if(fired.GetOrigin()[slab] < m_vMin[slab] || fired.GetOrigin()[slab] > m_vMax[slab])
			{
				return false;
			}
		} 
		else
		{
			//Find two intersection values
			FFLOAT invDir = 1.0f / fired.GetDirection()[slab];

			FFLOAT t1 = (m_vMin[slab] - fired.GetOrigin()[slab]) * invDir;
			FFLOAT t2 = (m_vMax[slab] - fired.GetOrigin()[slab]) * invDir;

			//t1 is the near slab intersection, so it should be swapped with t2
			//if this condition is true
			if(t1 > t2)
			{
				FFLOAT temp = t1;
				t1 = t2;
				t2 = temp;
			}

			interpMin = interpMin > t1 ? interpMin : t1;
			interpMax = interpMax < t2 ? interpMax : t2;

			if(interpMin > interpMax)
			{
				return false; //no slab intersection if tMin > tMax
			}
		}
	}

	return true;
}

BBOOL  AABBox::intersect(const IIntersectable& toCheck, BoundType type)
{
	switch(type)
	{
	case BOUNDTYPE_OBB:
		{

		}
		break;
	case BOUNDTYPE_AABB:
					{
						//Determines intersection by checking overlap on each axis
						//Exit out early if no intersection is found on a single axis

						//X-axis check
						if((m_vMax.getX() < toCheck.X(ATTRIBUTETYPE_MINIMUM)) || (m_vMin.getX() > toCheck.X(ATTRIBUTETYPE_MAXIMUM)))
						{
							return false;
						}

						//Y-axis check
						if((m_vMax.getY() < toCheck.Y(ATTRIBUTETYPE_MINIMUM)) || (m_vMin.getY() > toCheck.Y(ATTRIBUTETYPE_MAXIMUM)))
						{
							return false;
						}

						//Z-axis check
						if((m_vMax.getZ() < toCheck.Z(ATTRIBUTETYPE_MINIMUM)) || (m_vMin.getZ() > toCheck.Z(ATTRIBUTETYPE_MAXIMUM)))
						{
							return false;
						}

						return true;
					}
					break;
	case BOUNDTYPE_SPHERE:
					{
						// First, find the closest point of the AABBox regarding to the sphere.
						Vector3 closest;
						FFLOAT sphereCenterX = toCheck.X(ATTRIBUTETYPE_CENTER);
						FFLOAT sphereCenterY = toCheck.Y(ATTRIBUTETYPE_CENTER);
						FFLOAT sphereCenterZ = toCheck.Z(ATTRIBUTETYPE_CENTER);

						// Clamp posX value between Xmin & Xmax
						FFLOAT posX = sphereCenterX;
						Clamp<FFLOAT, FFLOAT, FFLOAT>(m_vMin.getX(), posX, m_vMax.getX());
						// Clamp posY value between Ymin & Ymax
						FFLOAT posY = sphereCenterY;
						Clamp<FFLOAT, FFLOAT, FFLOAT>(m_vMin.getY(), posY, m_vMax.getY());
						// Clamp posZ value between Zmin & Zmax
						FFLOAT posZ = sphereCenterZ;
						Clamp<FFLOAT, FFLOAT, FFLOAT>(m_vMin.getZ(), posZ, m_vMax.getZ());
						// store it.
						closest = Vector3(posX, posY, posZ);

						// then, compute the vector from the closest point and the sphere center.
						Vector3 vec = closest - Vector3(sphereCenterX, sphereCenterY, sphereCenterZ);

						// and check the distance between the vec length and the sphere radius.
						return vec.Vec3DotProduct(vec) < SQUARE(toCheck.X(ATTRIBUTETYPE_RADIUS));
					}
					break;
	default:
		return false;
	}

	return false;
}

BBOOL  AABBox::contains(const Vector3& point) const
{
	return (m_vMin.getX() <= point.getX()) && (point.getX() <= m_vMax.getX()) &&
		   (m_vMin.getY() <= point.getY()) && (point.getY() <= m_vMax.getY()) &&
		   (m_vMin.getZ() <= point.getZ()) && (point.getZ() <= m_vMax.getZ());
}

/*std::wofstream& operator<< (std::wofstream& output, AABBox box)
{
	output << "Min: " << box.Minimum().getX() << " , " << box.Minimum().getY() << " , " << box.Minimum().getZ() << "\n";
	output << "Max: " << box.Maximum().getX() << " , " << box.Maximum().getY() << " , " << box.Maximum().getZ() << "\n";
	output << "Cen: " << box.Center().getX()  << " , " << box.Center().getY()  << " , " << box.Center().getZ()  << "\n";

	return output;
}*/

BBOOL  AABBox::operator== (const AABBox& toTest) const
{
	return (Equal<Vector3>(this->m_vMin, toTest.Minimum()) && Equal<Vector3>(this->m_vMax, toTest.Maximum()));
}

BBOOL  AABBox::operator!= (const AABBox& toTest) const
{
	return !(*this == toTest);
}

FFLOAT AABBox::X(AttributeType toGet) const
{
	FFLOAT toReturn;

	switch(toGet)
	{
	case ATTRIBUTETYPE_MINIMUM:
							toReturn = m_vMin.getX();
							break;
	case ATTRIBUTETYPE_MAXIMUM:
							toReturn = m_vMax.getX();
							break;
	case ATTRIBUTETYPE_CENTER:
							toReturn = m_vCenter.getX();
							break;
	default:
		toReturn = -0.0f;
	}

	return toReturn;
}

FFLOAT AABBox::Y(AttributeType toGet) const
{
	FFLOAT toReturn;

	switch(toGet)
	{
	case ATTRIBUTETYPE_MINIMUM:
							toReturn = m_vMin.getY();
							break;
	case ATTRIBUTETYPE_MAXIMUM:
							toReturn = m_vMax.getY();
							break;
	case ATTRIBUTETYPE_CENTER:
							toReturn = m_vCenter.getY();
							break;
	default:
		toReturn = -0.0f;
	}

	return toReturn;
}

FFLOAT AABBox::Z(AttributeType toGet) const
{
	FFLOAT toReturn;

	switch(toGet)
	{
	case ATTRIBUTETYPE_MINIMUM:
		toReturn = m_vMin.getZ();
		break;
	case ATTRIBUTETYPE_MAXIMUM:
		toReturn = m_vMax.getZ();
		break;
	case ATTRIBUTETYPE_CENTER:
		toReturn = m_vCenter.getZ();
		break;
	default:
		toReturn = -0.0f;
	}

	return toReturn;
}
