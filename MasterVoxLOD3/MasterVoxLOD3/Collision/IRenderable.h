#ifndef DEF_IRENDERABLE_H
#define DEF_IRENDERABLE_H

#include "..\DataStructures\DataStructureUtil.h"

namespace Ocelot
{
	class IRenderable
	{
	public:

			// Constructor & Destructor
			IRenderable(){}
	virtual ~IRenderable(){}

			// Methods to use once.
	virtual BIGINT createInstance()         = 0;
	virtual VVOID  releaseInstance()        = 0;

			// Methods per frame.
	virtual VVOID  update(FFLOAT deltaTime) = 0;
	virtual BIGINT renderMesh()             = 0;

			// Accessors.
    virtual VVOID  SetWorld(Matrix4x4 world) = 0;
	};
}

#endif