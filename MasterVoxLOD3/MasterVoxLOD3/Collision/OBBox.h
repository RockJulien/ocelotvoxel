#ifndef DEF_OBBOX_H
#define DEF_OBBOX_H

#include "..\DataStructures\Array1D.h"
#include "..\Geometry\Line.h"
#include "IIntersectable.h"
#include "IRenderable.h"

namespace Ocelot
{
	class OBBox : public IIntersectable, public IRenderable
	{
	private:

		// Attributes
		BaseDXData     m_baseData;
		Matrix4x4      m_matBoundsWorld;
		Array1D<Line*> m_bounds;

	public:

		// Constructor & Destructor
		OBBox();
		OBBox(BaseDXData base);
		~OBBox();

		// Methods to use once
		BIGINT createInstance();
		VVOID  releaseInstance();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT renderMesh();

		// Intersection forced methods by inheritance.
		BBOOL  intersect(const Vector3& vec);
		BBOOL  intersect(const Ray& fired);
		BBOOL  intersect(const IIntersectable& toCheck, BoundType type);

		// Generic Accessors.
		FFLOAT X(AttributeType toGet) const;
		FFLOAT Y(AttributeType toGet) const;
		FFLOAT Z(AttributeType toGet) const;

		// Accessors
		inline VVOID   SetBaseData(BaseDXData base) { m_baseData = base;}
		inline VVOID   SetWorld(Matrix4x4 world) { m_matBoundsWorld = world;}
	};
}

#endif