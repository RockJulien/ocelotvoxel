#include "BSphere.h"

using namespace Ocelot;

BSphere::BSphere() :
IIntersectable(), IRenderable(), m_baseData(),
m_vCenter(0.0f), m_fRadius(2.0f)
{
	// two circles for a bounding sphere.
	m_bounds.resize(2);

	for(USMALLINT currLine = 0; currLine < 2; currLine++)
		m_bounds[currLine] = NULL;
}

BSphere::BSphere(BaseDXData base, const Vector3& center, const FFLOAT& radius) :
IIntersectable(), IRenderable(), m_baseData(base),
m_vCenter(center), m_fRadius(radius)
{
	// two circles for a bounding sphere.
	m_bounds.resize(2);

	for(USMALLINT currLine = 0; currLine < 2; currLine++)
		m_bounds[currLine] = NULL;
}

BSphere::BSphere(BaseDXData base, const FFLOAT& centerX, const FFLOAT& centerY, const FFLOAT& centerZ, const FFLOAT& radius) :
IIntersectable(), IRenderable(), m_baseData(base),
m_vCenter(centerX, centerY, centerZ), m_fRadius(radius)
{
	// two circles for a bounding sphere.
	m_bounds.resize(2);

	for(USMALLINT currLine = 0; currLine < 2; currLine++)
		m_bounds[currLine] = NULL;
}

BSphere::BSphere(BSphere& toCopy)
{
	m_baseData       = toCopy.m_baseData;
	m_matBoundsWorld = toCopy.m_matBoundsWorld;
	m_vCenter        = toCopy.m_vCenter;
	m_fRadius        = toCopy.m_fRadius;

	m_bounds.map(2);

	if(toCopy.m_bounds.Size() != 0)
		for(USMALLINT currCircle = 0; currCircle < 2; currCircle++)
			m_bounds[currCircle] = toCopy.GetCircle(currCircle);

	toCopy.m_bounds.unmap();
}

BSphere& BSphere::operator= (BSphere& toAssign)
{
	m_baseData       = toAssign.m_baseData;
	m_matBoundsWorld = toAssign.m_matBoundsWorld;
	m_vCenter        = toAssign.m_vCenter;
	m_fRadius        = toAssign.m_fRadius;

	for(USMALLINT currCircle = 0; currCircle < 2; currCircle++)
	{
		// First, release Line of this if any
		if(m_bounds.Size() != 0)
			if(m_bounds[currCircle])
				DeletePointer(m_bounds[currCircle]);

		// Then copy.
		if((m_bounds.Size() != 0) && (toAssign.m_bounds.Size() != 0))
			m_bounds[currCircle] = toAssign.GetCircle(currCircle);
	}

	toAssign.m_bounds.unmap();

	return *this;
}

BSphere::~BSphere()
{
	releaseInstance();
}

BIGINT BSphere::createInstance()
{
	HRESULT hr = S_OK;

	releaseInstance();

	CircleInfo info(m_baseData.Device(), m_baseData.GetCamera(), L"", L".\\Effects\\Line.fx", m_vCenter, (UBIGINT)AutoRoundF(m_fRadius));
	m_bounds[0] = new Circle(info);
	m_bounds[1] = new Circle(info);

	for(UBIGINT currCircle = 0; currCircle < (UBIGINT)m_bounds.Size(); currCircle++)
	{
		HR(m_bounds[currCircle]->createInstance());
	}

	return (BIGINT)hr;
}

VVOID  BSphere::releaseInstance()
{
	if(m_bounds.Size() != 0)
		for(UBIGINT currCircle = 0; currCircle < (UBIGINT)m_bounds.Size(); currCircle++)
		{
			Circle* toDestroy = m_bounds[currCircle];
			DeletePointer(toDestroy);
		}
}

VVOID  BSphere::update(FFLOAT deltaTime)
{
	for(UBIGINT currCircle = 0; currCircle < (UBIGINT)m_bounds.Size(); currCircle++)
	{
		// update the circle's world matrix
		m_bounds[currCircle]->SetWorld(m_matBoundsWorld);

		// Set the proper circle 90� rotation for one of the two.
		if(currCircle == 0)
		{
			Matrix4x4 tempRotat;
			Matrix4x4 invWorld;
			Matrix4x4 newWorld;
			tempRotat.Matrix4x4Identity();
			invWorld.Matrix4x4Identity();
			tempRotat.Matrix4x4RotationY(tempRotat, DegToRad(90.0f));
			m_matBoundsWorld.Matrix4x4Inverse(invWorld);
			invWorld.Matrix4x4Mutliply(invWorld, tempRotat);
			invWorld.Matrix4x4Inverse(newWorld);
			m_bounds[0]->SetWorld(newWorld);
		}

		// update the meshes.
		m_bounds[currCircle]->update(deltaTime);
	}
}

BIGINT BSphere::renderMesh()
{
	HRESULT hr = S_OK;

	for(UBIGINT currCircle = 0; currCircle < (UBIGINT)m_bounds.Size(); currCircle++)
	{
		// render meshes.
		HR(m_bounds[currCircle]->renderMesh());
	}

	return (BIGINT)hr;
}

BBOOL  BSphere::intersect(const Vector3& vec)
{
	return false;
}

BBOOL  BSphere::intersect(const Ray& fired)
{
	//A vector from the centre to the origin of the ray
	Vector3 toOrigin = fired.GetOrigin() - m_vCenter;

	//Distance between this vector and the radius of the sphere.
	//Note a vector "dotted" with itself is equal to its magnitude squared
	FFLOAT relRayOrigin = toOrigin.Vec3DotProduct(toOrigin) - SQUARE(m_fRadius);
	if(relRayOrigin <= 0.0f) //If ray origin is inside sphere, then it intersects regardless of ray direction
	{
		return true;
	}

	//Angle between this vector and direction of the ray
	FFLOAT relRayDirection = toOrigin.Vec3DotProduct(fired.GetDirection());
	if(relRayDirection > 0.0f) //If > 0 then facing *away* from sphere
	{
		return false;
	}

	FFLOAT discriminant = SQUARE(relRayDirection) - relRayOrigin; //Check for real solutions, imaginary solutions => no intersection
	if(discriminant < 0.0f) //If this is negative, the square root (when working out the quadratic to find t) would give complex numbers
	{
		return false;
	}

	return true;
}

BBOOL  BSphere::intersect(const IIntersectable& toCheck, BoundType type)
{
	switch(type)
	{
	case BOUNDTYPE_OBB:
		{

		}
		break;
	case BOUNDTYPE_AABB:
					{
						// First, find the closest point of the AABBox regarding to the sphere.
						Vector3 closest;

						// Clamp posX value between Xmin & Xmax
						FFLOAT posX = m_vCenter.getX();
						Clamp<FFLOAT, FFLOAT, FFLOAT>(toCheck.X(ATTRIBUTETYPE_MINIMUM), posX, toCheck.X(ATTRIBUTETYPE_MAXIMUM));
						// Clamp posY value between Ymin & Ymax
						FFLOAT posY = m_vCenter.getY();
						Clamp<FFLOAT, FFLOAT, FFLOAT>(toCheck.Y(ATTRIBUTETYPE_MINIMUM), posY, toCheck.Y(ATTRIBUTETYPE_MAXIMUM));
						// Clamp posZ value between Zmin & Zmax
						FFLOAT posZ = m_vCenter.getZ();
						Clamp<FFLOAT, FFLOAT, FFLOAT>(toCheck.Z(ATTRIBUTETYPE_MINIMUM), posZ, toCheck.Z(ATTRIBUTETYPE_MAXIMUM));
						// store it.
						closest = Vector3(posX, posY, posZ);

						// then, compute the vector from the closest point and the sphere center.
						Vector3 vec = closest - m_vCenter;

						// and check the distance between the vec length and the sphere radius.
						return vec.Vec3DotProduct(vec) < SQUARE(m_fRadius);
					}
					break;
	case BOUNDTYPE_SPHERE:
					{
						//Very simple intersection test, if the distance between their
						//centers is less than the sum of their radii, they intersect
						Vector3 toCentre = m_vCenter - Vector3(toCheck.X(ATTRIBUTETYPE_CENTER), toCheck.Y(ATTRIBUTETYPE_CENTER), toCheck.Z(ATTRIBUTETYPE_CENTER));

						return toCentre.Vec3Length() < m_fRadius + toCheck.X(ATTRIBUTETYPE_RADIUS);
					}
					break;
	}

	return true;
}

/*std::wofstream& operator<< (std::wofstream& output, BSphere sph)
{
	output << "Rad: " << sph.Radius() << "\n";
	output << "Cen: " << sph.Center().getX() << " , " << sph.Center().getY() << " , " << sph.Center().getZ() << "\n";

	return output;
}*/

BBOOL  BSphere::operator== (const BSphere& toTest) const
{
	return (Equal<Vector3>(this->Center(), toTest.Center()) && Equal<FFLOAT>(this->Radius(), toTest.Radius()));
}

BBOOL  BSphere::operator!= (const BSphere& toTest) const
{
	return !(*this == toTest);
}

FFLOAT BSphere::X(AttributeType toGet) const
{
	FFLOAT toReturn;

	switch(toGet)
	{
	case ATTRIBUTETYPE_CENTER:
							toReturn = m_vCenter.getX();
							break;
	case ATTRIBUTETYPE_RADIUS:
							toReturn = m_fRadius;
							break;
	default:
		toReturn = -0.0f;
	}

	return toReturn;
}

FFLOAT BSphere::Y(AttributeType toGet) const
{
	FFLOAT toReturn;

	switch(toGet)
	{
	case ATTRIBUTETYPE_CENTER:
							toReturn = m_vCenter.getY();
							break;
	case ATTRIBUTETYPE_RADIUS:
							toReturn = m_fRadius;
							break;
	default:
		toReturn = -0.0f;
	}

	return toReturn;
}

FFLOAT BSphere::Z(AttributeType toGet) const
{
	FFLOAT toReturn;

	switch(toGet)
	{
	case ATTRIBUTETYPE_CENTER:
							toReturn = m_vCenter.getZ();
							break;
	case ATTRIBUTETYPE_RADIUS:
							toReturn = m_fRadius;
							break;
	default:
		toReturn = -0.0f;
	}

	return toReturn;
}
