#ifndef DEF_BSPHERE_H
#define DEF_BSPHERE_H

#include "..\DataStructures\Array1D.h"
#include "..\Geometry\Circle.h"
#include "IIntersectable.h"
#include "IRenderable.h"

namespace Ocelot
{
	class BSphere : public IIntersectable, public IRenderable
	{
	private:

		// Attributes
		BaseDXData       m_baseData;
		Matrix4x4        m_matBoundsWorld;
		Array1D<Circle*> m_bounds;
		Vector3          m_vCenter;
		FFLOAT           m_fRadius;

	public:

		// Constructor & Destructor
		BSphere();
		BSphere(BaseDXData base, const Vector3& center, const FFLOAT& radius);
		BSphere(BaseDXData base, const FFLOAT& centerX, const FFLOAT& centerY, const FFLOAT& centerZ, const FFLOAT& radius);
		BSphere(BSphere& toCopy);
		BSphere& operator= (BSphere& toAssign);
		~BSphere();

		// Methods to use once
		BIGINT createInstance();
		VVOID  releaseInstance();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT renderMesh();

		// Intersection forced methods by inheritance.
		BBOOL  intersect(const Vector3& vec);
		BBOOL  intersect(const Ray& fired);
		BBOOL  intersect(const IIntersectable& toCheck, BoundType type);

		// Operator overload
		friend std::wofstream& operator<< (std::wofstream& output, BSphere sph)
		{
			output << "Rad: " << sph.Radius() << "\r\n";
			output << "Cen: " << sph.Center().getX() << " , " << sph.Center().getY() << " , " << sph.Center().getZ() << "\r\n";

			return output;
		}
		BBOOL  operator== (const BSphere& toTest) const;
		BBOOL  operator!= (const BSphere& toTest) const;

		// Generic Accessors.
		FFLOAT X(AttributeType toGet) const;
		FFLOAT Y(AttributeType toGet) const;
		FFLOAT Z(AttributeType toGet) const;

		// Accessors
		inline Vector3 Center() const { return m_vCenter;}
		inline FFLOAT  Radius() const { return m_fRadius;}
		inline Circle* GetCircle(USMALLINT circleIndex) { return m_bounds[circleIndex];}
		inline VVOID   SetBaseData(BaseDXData base) { m_baseData = base;}
		inline VVOID   SetWorld(Matrix4x4 world) { m_matBoundsWorld = world;}
	};
}

#endif