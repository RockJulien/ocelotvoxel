#include "OBBox.h"

using namespace Ocelot;

OBBox::OBBox() : 
IIntersectable(), IRenderable(), m_baseData()
{
	// twelve lines for a bounding box.
	m_bounds.resize(12);
}

OBBox::OBBox(BaseDXData base) : 
IIntersectable(), IRenderable(), m_baseData(base)
{
	// twelve lines for a bounding box.
	m_bounds.resize(12);
}

OBBox::~OBBox()
{
	releaseInstance();
}

BIGINT OBBox::createInstance()
{
	HRESULT hr = S_OK;

	return (BIGINT)hr;
}

VVOID  OBBox::releaseInstance()
{
	for(UBIGINT currLine = 0; currLine < (UBIGINT)m_bounds.Size(); currLine++)
	{
		Line* toDestroy = m_bounds[currLine];
		DeletePointer(toDestroy);
	}
}

VVOID  OBBox::update(FFLOAT deltaTime)
{

}

BIGINT OBBox::renderMesh()
{
	HRESULT hr = S_OK;

	return (BIGINT)hr;
}

BBOOL  OBBox::intersect(const Vector3& vec)
{
	return false;
}

BBOOL  OBBox::intersect(const Ray& fired)
{
	return false;
}

BBOOL  OBBox::intersect(const IIntersectable& toCheck, BoundType type)
{
	return false;
}

FFLOAT OBBox::X(AttributeType toGet) const
{
	FFLOAT toReturn = 0.0f;

	return toReturn;
}

FFLOAT OBBox::Y(AttributeType toGet) const
{
	FFLOAT toReturn = 0.0f;

	return toReturn;
}

FFLOAT OBBox::Z(AttributeType toGet) const
{
	FFLOAT toReturn = 0.0f;

	return toReturn;
}
