#include "ParticleSystem.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

ParticleSystem::ParticleSystem() :
IOcelotParticleSystem(), m_iType(PARTICULESYSTEM_TYPE_DEFAULT), m_info(), m_particleBuffer(NULL), m_particleIndices(NULL),
m_particleDepth(NULL), m_iParticleCount(0), m_fStartTime(0.0f), m_fCurrentTime(0.0f), m_bRunning(false), m_bOnFrustum(true)
{
	
}

ParticleSystem::ParticleSystem(ParticleSystemInfo info) :
IOcelotParticleSystem(), m_iType(PARTICULESYSTEM_TYPE_DEFAULT), m_info(info), m_particleBuffer(NULL), m_particleIndices(NULL),
m_particleDepth(NULL), m_iParticleCount(0), m_fStartTime(0.0f), m_fCurrentTime(0.0f), m_bRunning(false), m_bOnFrustum(true)
{

}

ParticleSystem::~ParticleSystem()
{
	releaseSystem();
}

VVOID ParticleSystem::createSystem(UBIGINT particleCount, OcelotParticle* adressToStart)
{
	// Give the starting adress of the part of the already initialized global
	// particles buffer which will limit the overall count of particles.
	m_particleBuffer = adressToStart;
	m_iParticleCount = particleCount;
}

VVOID ParticleSystem::initialize()
{
	// initialize each components of this particle system.
	for(UBIGINT currAttr = 0; currAttr < m_iParticleCount; currAttr++)
	{
		m_particleBuffer[currAttr].SetPosition(((RandUnity() * m_info.Spread()) * m_info.PosWeights()) + m_info.Position());
		m_particleBuffer[currAttr].SetDirection(Vector3(RandUnity() * m_info.DirWeights().getX(), 
														fabs(RandUnity()) * m_info.DirWeights().getY(), 
														RandUnity() * m_info.DirWeights().getZ()).Vec3Normalise());
		m_particleBuffer[currAttr].SetFade(0.0f);
		m_particleBuffer[currAttr].SetLife(m_fStartTime);
		m_particleBuffer[currAttr].SetRadius(m_info.StartSize());
		m_particleBuffer[currAttr].SetRotAngle(TWOPI * RandUnity());

		// Interpolate from color0 to color1.
		FFLOAT lerp = RandUnity();
		Color tempColor = m_info.Color0() * lerp + m_info.Color1() * (1.0f - lerp);
		// Set the particle's color.
		m_particleBuffer[currAttr].SetColor(((UBIGINT)(tempColor.getA() * 255.0f) << 24) | 
										   (((UBIGINT)(tempColor.getB() * 255.0f) & 255) << 16) | 
										   (((UBIGINT)(tempColor.getG() * 255.0f) & 255) << 8 ) | 
										    ((UBIGINT)(tempColor.getR() * 255.0f) & 255));
	}

	// reset timers and boolean.
	m_fCurrentTime = m_fStartTime;
	m_bRunning     = false;
}

VVOID ParticleSystem::initializeLayout()
{

}

VVOID ParticleSystem::initializeEffect()
{

}

VVOID ParticleSystem::initializeVertices()
{

}

VVOID ParticleSystem::initializeIndices()
{

}

VVOID ParticleSystem::initializeEffectVariables()
{

}

VVOID ParticleSystem::releaseSystem()
{
	// nothing to release
}

VVOID ParticleSystem::update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity)
{
	if(m_fCurrentTime > 0.0f)
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			// Compute the size, speed and fade interpolation values.
			FFLOAT t   = m_particleBuffer[currPart].Life() / m_info.LifeSpan();
			FFLOAT tm1 = t - 1.0f;
			FFLOAT sizeLerp  = 1.0f - pow(tm1, m_info.SizeExponent());
			FFLOAT speedLerp = 1.0f - pow(tm1, m_info.SpeedExponent());
			FFLOAT fadeLerp  = 1.0f - pow(tm1, m_info.FadeExponent());

			// Interpolate the size, speed and fade values for the particle.
			FFLOAT size  = sizeLerp * m_info.EndSize() + (1.0f - sizeLerp) * m_info.StartSize();
			FFLOAT speed = speedLerp * m_info.EndSpeed() + (1.0f - speedLerp) * m_info.StartSpeed();
			FFLOAT fade  = fadeLerp;

			// Compute the particle's velocity.
			Vector3 velocity = m_particleBuffer[currPart].Direction() * speed;

			// and add wind forces to velocity.
			velocity += (windVel * 1.0f);

			// Compute the new position.
			Vector3 tempPos = m_particleBuffer[currPart].Position() + (deltaTime * velocity);
			// Compute the new life.
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			// Compute the new rotation.
			FFLOAT tempRot  = m_particleBuffer[currPart].RotAngle() + 0.0f;

			// reset particle's attributes.
			m_particleBuffer[currPart].SetPosition(tempPos);
			m_particleBuffer[currPart].SetRotAngle(tempRot);
			m_particleBuffer[currPart].SetLife(tempLife);
			m_particleBuffer[currPart].SetRadius(size);
			m_particleBuffer[currPart].SetFade(fade);
			m_particleBuffer[currPart].Visible();
		}

		// If not running, start.
		if(!m_bRunning)
		{
			m_bRunning = true;
		}
	}
	else
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			m_particleBuffer[currPart].NotVisible();
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			m_particleBuffer[currPart].SetLife(tempLife);
		}
	}

	// Increase current time by the elapsed time.
	m_fCurrentTime += deltaTime;
}

VVOID ParticleSystem::updateEffectVariables()
{

}

VVOID ParticleSystem::renderParticles()
{
	// nothing for now.
}

ParticleSystemInfo::ParticleSystemInfo() :
m_iStreamerCount(0), m_fSpread(0.0f), m_fLifeSpan(0.0f), m_fFadeExponent(9.0f), m_fStartSize(0.0f), m_fEndSize(1.0f), 
m_fSizeExponent(1.0f), m_fStartSpeed(0.0f), m_fEndSpeed(0.0f), m_fSpeedExponent(1.0f), m_fSpeedVariance(0.0f), m_fRotAngle(0.0f), 
m_fWindFalloff(0.2f), m_vEmitterPosition(0.0f), m_vPositionWeights(0.0f), m_vEmitterDirection(1.0f), m_vDirectionVariance(0.0f), 
m_vDirectionWeights(0.0f), m_cColor0(GREY), m_cColor1(LIGHTGREY), m_cFlashColor(BLUE)
{

}

ParticleSystemInfo::ParticleSystemInfo(UBIGINT streamerCount, FFLOAT spread, FFLOAT lifeSpan, FFLOAT fadeExponent, FFLOAT startSize, FFLOAT endSize, 
									   FFLOAT sizeExponent, FFLOAT startSpeed, FFLOAT endSpeed, FFLOAT speedExponent, FFLOAT speedVariance, FFLOAT rotAngle, 
									   FFLOAT windFalloff, Vector3 emitterPosition, Vector3 positionWeights, Vector3 emitterDirection, Vector3 directionVariance, 
									   Vector3 directionWeights, Color color0, Color color1, Color flashColor) :
m_iStreamerCount(streamerCount), m_fSpread(spread), m_fLifeSpan(lifeSpan), m_fFadeExponent(fadeExponent), m_fStartSize(startSize), m_fEndSize(endSize), 
m_fSizeExponent(sizeExponent), m_fStartSpeed(startSpeed), m_fEndSpeed(endSpeed), m_fSpeedExponent(speedExponent), m_fSpeedVariance(speedVariance), m_fRotAngle(rotAngle), 
m_fWindFalloff(windFalloff), m_vEmitterPosition(emitterPosition), m_vPositionWeights(positionWeights), m_vEmitterDirection(emitterDirection), m_vDirectionVariance(directionVariance), 
m_vDirectionWeights(directionWeights), m_cColor0(color0), m_cColor1(color1), m_cFlashColor(flashColor)
{

}

ParticleSystemInfo::~ParticleSystemInfo()
{

}
