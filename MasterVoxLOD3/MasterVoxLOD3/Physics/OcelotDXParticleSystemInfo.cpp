#include "OcelotDXParticleSystemInfo.h"

using namespace Ocelot;

OcelotDXParticleSystemInfo::OcelotDXParticleSystemInfo() :
ParticleSystemInfo(), m_baseData(), m_sTexturePath(L""), m_sEffectPath(L""), m_vLightDirection(1.0f), m_bIndependent(true)
{

}

OcelotDXParticleSystemInfo::OcelotDXParticleSystemInfo(DevPtr device, CamPtr camera,  WSTRING texturePath, WSTRING effectPath, Vector3 lightDir, UBIGINT streamerCount, FFLOAT spread, FFLOAT lifeSpan, FFLOAT fadeExponent, 
													   FFLOAT startSize, FFLOAT endSize, FFLOAT sizeExponent, FFLOAT startSpeed, FFLOAT endSpeed, FFLOAT speedExponent, FFLOAT speedVariance, FFLOAT rotAngle, 
													   FFLOAT windFalloff, Vector3 emitterPosition, Vector3 positionWeights, Vector3 emitterDirection, Vector3 directionVariance, Vector3 directionWeights, 
													   Color color0, Color color1, Color flashColor, BBOOL independent) :
ParticleSystemInfo(streamerCount, spread, lifeSpan, fadeExponent, startSize, endSize, 
				   sizeExponent, startSpeed, endSpeed, speedExponent, speedVariance, rotAngle, 
				   windFalloff, emitterPosition, positionWeights, emitterDirection, directionVariance, 
				   directionWeights, color0, color1, flashColor), m_baseData(device, camera), m_sTexturePath(texturePath), m_sEffectPath(effectPath), m_vLightDirection(lightDir), m_bIndependent(independent)
{

}

OcelotDXParticleSystemInfo::~OcelotDXParticleSystemInfo()
{

}
