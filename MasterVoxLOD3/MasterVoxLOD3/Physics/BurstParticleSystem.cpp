#include "BurstParticuleSystem.h"
#include "..\Audio\AudioManager.h"

using namespace Ocelot;
using namespace Audio;

BurstParticleSystem::BurstParticleSystem() :
OcelotDXParticleSystem()
{
	m_iType = PARTICULESYSTEM_TYPE_EXPLOSION_BURST;
}

BurstParticleSystem::BurstParticleSystem(OcelotDXParticleSystemInfo info) :
OcelotDXParticleSystem(info)
{
	m_iType = PARTICULESYSTEM_TYPE_EXPLOSION_BURST;
}

BurstParticleSystem::~BurstParticleSystem()
{
	releaseSystem();
}

VVOID BurstParticleSystem::createSystem(UBIGINT particleCount, OcelotParticle* adressToStart)
{
	OcelotDXParticleSystem::createSystem(particleCount, adressToStart);

	// Initialize
	initialize();
}

VVOID BurstParticleSystem::initialize()
{
	// The particules count will be split in branch for the particle's animation.
	UBIGINT currGlobalPart = 0;
	UBIGINT partPerStream  = (m_iParticleCount / m_info.StreamerCount()) + 1;

	// For each branch.
	for(UBIGINT currBranch = 0; currBranch < m_info.StreamerCount(); currBranch++)
	{
		// Compute the branch's position.
		Vector3 branchPos = Vector3(RandUnity() * m_info.Spread(), RandUnity() * m_info.Spread(), RandUnity() * m_info.Spread());

		// Compute the branch's direction.
		Vector3 branchDir = Vector3(m_info.Direction().getX() + (RandUnity() * m_info.DirVariance().getX()), m_info.Direction().getY() + (RandUnity() * m_info.DirVariance().getY()), m_info.Direction().getZ() + (RandUnity() * m_info.DirVariance().getZ()));
		branchDir.Vec3Normalise();

		for(UBIGINT currPartInBranch = 0; currPartInBranch < partPerStream; currPartInBranch++)
		{
			if(currGlobalPart < m_iParticleCount)
			{
				m_particleBuffer[currGlobalPart].SetPosition(Vector3(branchPos.getX() * m_info.PosWeights().getX(), 
																	 branchPos.getY() * m_info.PosWeights().getY(), 
																	 branchPos.getZ() * m_info.PosWeights().getZ()) + m_info.Position());
				
				// Compute the particle's speed.
				FFLOAT speed = m_info.StartSpeed() + RandUnity() * m_info.SpeedVariance();
				
				m_particleBuffer[currGlobalPart].SetDirection(Vector3(branchDir.getX() * speed * m_info.DirWeights().getX(), 
																	  branchDir.getY() * speed * m_info.DirWeights().getY(), 
																	  branchDir.getZ() * speed * m_info.DirWeights().getZ()));

				// Compute the radius interpolation value.
				FFLOAT radiusLerp = (speed / (m_info.StartSpeed() + m_info.SpeedVariance()));
				// Interpolate the radius to apply for the current particle.
				FFLOAT tempRadius = m_info.StartSize() * radiusLerp + m_info.EndSize() * (1.0f - radiusLerp);

				m_particleBuffer[currGlobalPart].SetFade(0.0f);
				m_particleBuffer[currGlobalPart].SetLife(m_fStartTime);
				m_particleBuffer[currGlobalPart].SetRadius(tempRadius);
				m_particleBuffer[currGlobalPart].SetRotAngle(TWOPI * RandUnity());
				m_particleBuffer[currGlobalPart].SetRotStep(RandUnity() * 1.5f);

				// Interpolate from color0 to color1.
				FFLOAT lerp = RandUnity();
				Color tempColor = m_info.Color0() * lerp + m_info.Color1() * (1.0f - lerp);
				// Set the particle's color.
				m_particleBuffer[currGlobalPart].SetColor(((UBIGINT)(tempColor.getA() * 255.0f) << 24) | 
														 (((UBIGINT)(tempColor.getB() * 255.0f) & 255) << 16) | 
														 (((UBIGINT)(tempColor.getG() * 255.0f) & 255) << 8 ) | 
														  ((UBIGINT)(tempColor.getR() * 255.0f) & 255));

				// Incremente the currGlobalPart index.
				currGlobalPart++;
			}
		}
	}

	// reset timers and boolean.
	m_fCurrentTime = m_fStartTime;
	m_bRunning     = false;

	// Initialize the proper sound
	HR(AudioMng->createSound(L"BombLongExplosion.wav", true, false)); // sound name / 3D sound false / sound looped true.
	AudioMng->stopASound(L"BombLongExplosion.wav");
	IOcelotAudioDevice* lMusic = AudioMng->GetAudioByName(L"BombLongExplosion.wav");
	lMusic->setSoundPosition(this->Info().Position(), 0);
	lMusic->setSoundMaxDistance(1000.0, 0);
}

VVOID BurstParticleSystem::initializeLayout()
{
	OcelotDXParticleSystem::initializeLayout();
}

VVOID BurstParticleSystem::initializeVertices()
{
	OcelotDXParticleSystem::initializeVertices();
}

VVOID BurstParticleSystem::initializeEffectVariables()
{
	OcelotDXParticleSystem::initializeEffectVariables();
}

VVOID BurstParticleSystem::releaseSystem()
{
	OcelotDXParticleSystem::releaseSystem();
}

VVOID BurstParticleSystem::update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity)
{
	if(m_fCurrentTime > 0.0f)
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			// Compute the size, speed and fade interpolation values.
			FFLOAT t   = m_particleBuffer[currPart].Life() / m_info.LifeSpan();
			FFLOAT tm1 = t - 1.0f;
			FFLOAT sizeLerp  = 1.0f - pow(tm1, m_info.SizeExponent());
			FFLOAT speedLerp = pow(tm1, m_info.SpeedExponent());
			FFLOAT fadeLerp  = 1.0f - pow(tm1, m_info.FadeExponent());

			// Compute the particle dir.
			Vector3 partDir  = m_particleBuffer[currPart].Position() - m_info.Position();

			// and wind forces to apply velocity.
			FFLOAT windAm    = max(0.0f, min(1.0f, partDir.getY() / m_info.WindFalloff()));
			Vector3 tempWind = windVel * windAm;
			tempWind.y(0.0f); // no wind force on Y.

			// Compute the particle's velocity.
			Vector3 velocity = m_particleBuffer[currPart].Direction() + tempWind;

			// Compute the new position.
			Vector3 tempPos = m_particleBuffer[currPart].Position() + (deltaTime * velocity);
			// Insure that the Y pos is unsigned.
			if(tempPos.getY() < 0.0f)
				tempPos.y(0.0f);

			// Compute the new direction.
			FFLOAT drag = 4.0f * speedLerp;
			Vector3 tempDir = m_particleBuffer[currPart].Direction() + (gravity * deltaTime);
			tempDir *= 1.0f - drag * deltaTime;

			// Compute the new life.
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			// Compute the new rotation.
			FFLOAT tempRot  = m_particleBuffer[currPart].RotAngle() + (m_particleBuffer[currPart].RotStep() * deltaTime);
			// Compute the new size.
			FFLOAT tempSize = m_particleBuffer[currPart].Radius() + sizeLerp * deltaTime;

			// reset particle's attributes.
			m_particleBuffer[currPart].SetPosition(tempPos);
			m_particleBuffer[currPart].SetDirection(tempDir);
			m_particleBuffer[currPart].SetRotAngle(tempRot);
			m_particleBuffer[currPart].SetLife(tempLife);
			m_particleBuffer[currPart].SetRadius(tempSize);
			m_particleBuffer[currPart].SetFade(fadeLerp);
			m_particleBuffer[currPart].Visible();
		}

		// If not running, start.
		if(!m_bRunning)
		{
			// trigger extra behavior for other classes here.
			// e.g. If the explosion has to influence a close object's shape.
			m_bRunning = true;
			HR(AudioMng->startASound(L"BombLongExplosion.wav"));
			HR(AudioMng->restartASound(L"BombLongExplosion.wav"));
		}
	}
	else
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			// Stop
			m_particleBuffer[currPart].NotVisible();
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			m_particleBuffer[currPart].SetLife(tempLife);
		}
	}

	// Increase current time by the elapsed time.
	m_fCurrentTime += deltaTime;

	if(m_bIndependent)
		// update effect variables.
		updateEffectVariables();
}

VVOID BurstParticleSystem::updateEffectVariables()
{
	OcelotDXParticleSystem::updateEffectVariables();

	Color posIntensity;
	FFLOAT flashLife  = m_info.LifeSpan() * 0.5f;
	FFLOAT lightRaise = 2.0f;
	// Add extra per frame code for the mushroom.
	if(m_fCurrentTime > m_info.LifeSpan())
	{
		// Re-initialize
		initialize();
	}
	// else update the flashing light effect.
	else if((m_fCurrentTime > 0.0f) && (m_fCurrentTime < flashLife))
	{
		FFLOAT intensity   = 1000.0f * ((flashLife - m_fCurrentTime) / flashLife); // 0.5f flash life/ 1000.0f flash intensity.
		posIntensity = Color(m_info.Position().getX() + lightRaise, m_info.Position().getY() + lightRaise, m_info.Position().getZ() + lightRaise, intensity); // 1.0f intensity increase factor.
	}

	// Set up flash variables.
	HR(m_fxPosIntensity->SetRawValue((FFLOAT*)&posIntensity, 0, sizeof(Color)));
	HR(m_fxColor->SetRawValue((FFLOAT*)&m_info.FlashColor(), 0, sizeof(Color)));
	HR(m_fxAttenuation->SetRawValue((FFLOAT*)new Vector3(0.0f, 0.0f, 3.0f), 0, sizeof(Vector3)));
}

VVOID BurstParticleSystem::renderParticles()
{
	OcelotDXParticleSystem::renderParticles();
}
