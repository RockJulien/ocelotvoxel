#include "MineParticleSystem.h"
#include "..\Audio\AudioManager.h"

using namespace Ocelot;
using namespace Audio;

MineParticleSystem::MineParticleSystem() :
OcelotDXParticleSystem()
{
	m_iType = PARTICULESYSTEM_TYPE_EXPLOSION_MINE;
}

MineParticleSystem::MineParticleSystem(OcelotDXParticleSystemInfo info) :
OcelotDXParticleSystem(info)
{
	m_iType = PARTICULESYSTEM_TYPE_EXPLOSION_MINE;

}

MineParticleSystem::~MineParticleSystem()
{
	releaseSystem();
}

VVOID MineParticleSystem::createSystem(UBIGINT particleCount, OcelotParticle* adressToStart)
{
	OcelotDXParticleSystem::createSystem(particleCount, adressToStart);

	// Initialize
	initialize();
}

VVOID MineParticleSystem::initialize()
{
	for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
	{
		// Compute the particle's direction.
		Vector3 partDir = m_info.Direction() + Vector3(RandUnity() * m_info.DirVariance().getX(), 
													   RandUnity() * m_info.DirVariance().getY(), 
													   RandUnity() * m_info.DirVariance().getZ()).Vec3Normalise();

		FFLOAT speed    = m_info.StartSpeed() + RandUnity() * m_info.SpeedVariance();

		// Compute the particle's position.
		Vector3 partPos = Vector3((RandUnity() * m_info.Spread()) * m_info.PosWeights().getX(), 
								  (RandUnity() * m_info.Spread()) * m_info.PosWeights().getY(), 
								  (RandUnity() * m_info.Spread()) * m_info.PosWeights().getZ());

		// Compute the length of the vector position.
		FFLOAT len = partPos.Vec3Length();
		len /= m_info.Spread(); // divide by the spread factor.

		// Compute the speed decreaser thanks to the len value.
		FFLOAT speedFactor = 1.0f - len;

		// add the speed decreased by the speed factor times dirWeights.
		partDir *= speed * speedFactor;
		FFLOAT dirX = partDir.getX() * m_info.DirWeights().getX();
		FFLOAT dirY = partDir.getY() * m_info.DirWeights().getY();
		FFLOAT dirZ = partDir.getZ() * m_info.DirWeights().getZ();
		partDir = Vector3(dirX, dirY, dirZ);

		// and add the emitter position to the particle's position.
		partPos += m_info.Position();

		// Compute the radius interpolation value.
		FFLOAT radiusLerp = speed / (m_info.StartSpeed() + m_info.SpeedVariance());
		// Interpolate the particle's radius.
		FFLOAT tempRad    = m_fStartTime * radiusLerp + m_info.EndSize() * (1.0f - radiusLerp);

		// Compute the particle's rotAngle.
		FFLOAT tempRot  = RandUnity() * TWOPI;

		// Compute the particle's rotStep.
		FFLOAT tempStep = RandUnity() * 1.5f;

		m_particleBuffer[currPart].SetPosition(partPos);
		m_particleBuffer[currPart].SetDirection(partDir);
		m_particleBuffer[currPart].SetLife(m_fStartTime);
		m_particleBuffer[currPart].SetRotAngle(tempRot);
		m_particleBuffer[currPart].SetRotStep(tempStep);
		m_particleBuffer[currPart].SetRadius(tempRad);
		m_particleBuffer[currPart].SetFade(0.0f);

		// Interpolate from color0 to color1.
		FFLOAT lerp = RandUnity();
		Color tempColor = m_info.Color0() * lerp + m_info.Color1() * (1.0f - lerp);
		// Set the particle's color.
		m_particleBuffer[currPart].SetColor(((UBIGINT)(tempColor.getA() * 255.0f) << 24) | 
										   (((UBIGINT)(tempColor.getB() * 255.0f) & 255) << 16) | 
										   (((UBIGINT)(tempColor.getG() * 255.0f) & 255) << 8 ) | 
										    ((UBIGINT)(tempColor.getR() * 255.0f) & 255));
	}

	// reset timers and boolean.
	m_fCurrentTime = m_fStartTime;
	m_bRunning     = false;

	HR(AudioMng->createSound(L"MineExplosion.wav", true, false)); // sound name / 3D sound false / sound looped true.
	AudioMng->stopASound(L"MineExplosion.wav");
	IOcelotAudioDevice* lMusic = AudioMng->GetAudioByName(L"MineExplosion.wav");
	lMusic->setSoundPosition(this->Info().Position(), 0);
	lMusic->setSoundMaxDistance(1000.0, 0);
}

VVOID MineParticleSystem::initializeLayout()
{
	OcelotDXParticleSystem::initializeLayout();
}

VVOID MineParticleSystem::initializeVertices()
{
	OcelotDXParticleSystem::initializeVertices();
}

VVOID MineParticleSystem::initializeEffectVariables()
{
	OcelotDXParticleSystem::initializeEffectVariables();
}

VVOID MineParticleSystem::releaseSystem()
{
	OcelotDXParticleSystem::releaseSystem();
}

VVOID MineParticleSystem::update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity)
{
	if(m_fCurrentTime > 0.0f)
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			// Compute the size, speed and fade interpolation values.
			FFLOAT t   = m_particleBuffer[currPart].Life() / m_info.LifeSpan();
			FFLOAT tm1 = t - 1.0f;
			FFLOAT sizeLerp  = 1.0f - pow(tm1, m_info.SizeExponent());
			FFLOAT speedLerp = pow(tm1, m_info.SpeedExponent());
			FFLOAT fadeLerp  = 1.0f - pow(tm1, m_info.FadeExponent());

			// and add wind forces to apply velocity.
			Vector3 tempWind = windVel;
			tempWind.y(0.0f); // no wind force on Y.

			// Compute the particle's velocity.
			Vector3 velocity = m_particleBuffer[currPart].Direction() + tempWind;

			// Compute the new position.
			Vector3 tempPos = m_particleBuffer[currPart].Position() + deltaTime * velocity;
			// Insure that the Y pos is unsigned.
			if(tempPos.getY() < 0.0f)
				tempPos.y(0.0f);

			// Compute the new direction.
			FFLOAT drag = 8.0f * speedLerp;
			Vector3 tempDir = m_particleBuffer[currPart].Direction() + gravity * (1 - 1.0f) * deltaTime;
			tempDir *= 1.0f - drag * deltaTime;

			// Compute the new life.
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			// Compute the new rotation.
			FFLOAT tempRot  = m_particleBuffer[currPart].RotAngle() + m_particleBuffer[currPart].RotStep() * deltaTime;
			// Compute the new size.
			FFLOAT tempSize = m_particleBuffer[currPart].Radius() + sizeLerp * deltaTime * 1.0f;

			// reset particle's attributes.
			m_particleBuffer[currPart].SetPosition(tempPos);
			m_particleBuffer[currPart].SetDirection(tempDir);
			m_particleBuffer[currPart].SetRotAngle(tempRot);
			m_particleBuffer[currPart].SetLife(tempLife);
			m_particleBuffer[currPart].SetRadius(tempSize);
			m_particleBuffer[currPart].SetFade(fadeLerp);
			m_particleBuffer[currPart].Visible();
		}

		// If not running, start.
		if(!m_bRunning)
		{
			// trigger extra behavior for other classes here.
			// e.g. If the explosion has to influence a close object's shape.
			m_bRunning = true;
			HR(AudioMng->startASound(L"MineExplosion.wav"));
			HR(AudioMng->restartASound(L"MineExplosion.wav"));
		}
	}
	else
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			// Stop
			m_particleBuffer[currPart].NotVisible();
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			m_particleBuffer[currPart].SetLife(tempLife);
		}
	}

	// Increase current time by the elapsed time.
	m_fCurrentTime += deltaTime;

	if(m_bIndependent)
		// update effect variables.
		updateEffectVariables();
}

VVOID MineParticleSystem::updateEffectVariables()
{
	OcelotDXParticleSystem::updateEffectVariables();

	Color posIntensity;
	FFLOAT flashLife  = m_info.LifeSpan() * 0.5f;
	FFLOAT lightRaise = 5.0f;
	// Add extra per frame code for the mushroom.
	if(m_fCurrentTime > m_info.LifeSpan())
	{
		// Re-initialize
		initialize();
	}
	// else update the flashing light effect.
	else if((m_fCurrentTime > 0.0f) && (m_fCurrentTime < flashLife))
	{
		FFLOAT intensity   = 1000.0f * ((flashLife - m_fCurrentTime) / flashLife); // 0.5f flash life/ 1000.0f flash intensity.
		posIntensity = Color(m_info.Position().getX() + lightRaise, m_info.Position().getY() + lightRaise, m_info.Position().getZ() + lightRaise, intensity); // 1.0f intensity increase factor.
	}

	// Set up flash variables.
	HR(m_fxPosIntensity->SetRawValue((FFLOAT*)&posIntensity, 0, sizeof(Color)));
	HR(m_fxColor->SetRawValue((FFLOAT*)&m_info.FlashColor(), 0, sizeof(Color)));
	HR(m_fxAttenuation->SetRawValue((FFLOAT*)new Vector3(0.0f, 0.0f, 3.0f), 0, sizeof(Vector3)));
}

VVOID MineParticleSystem::renderParticles()
{
	OcelotDXParticleSystem::renderParticles();
}
