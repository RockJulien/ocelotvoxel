#ifndef DEF_MINEPARTICLESYSTEM_H
#define DEF_MINEPARTICLESYSTEM_H

#include "OcelotDXParticleSystem.h"

namespace Ocelot
{
	class MineParticleSystem : public OcelotDXParticleSystem
	{
	private:

		// Attributes

	public:

		// Constructor & destructor
		MineParticleSystem();
		MineParticleSystem(OcelotDXParticleSystemInfo info);
		~MineParticleSystem();

		// Methods to use once.
		VVOID createSystem(UBIGINT particleCount, OcelotParticle* adressToStart);
		VVOID initialize();

		// API dependent.
		VVOID initializeLayout();
		VVOID initializeVertices();
		VVOID initializeEffectVariables();
		VVOID releaseSystem();

		// Methods per frame.
		VVOID update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity);
		VVOID updateEffectVariables();
		VVOID renderParticles();

		// Accessors
	};
}

#endif