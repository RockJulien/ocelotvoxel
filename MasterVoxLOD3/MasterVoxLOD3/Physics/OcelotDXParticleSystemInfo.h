#ifndef DEF_OCELOTDXPARTICLESYSTEMINFO_H
#define DEF_OCELOTDXPARTICLESYSTEMINFO_H

#include "ParticleSystem.h"
#include "..\Geometry\BaseDXData.h"
#include "..\DirectXAPI\DXHelperFunctions.h"

namespace Ocelot
{
	class OcelotDXParticleSystemInfo : public ParticleSystemInfo
	{
	private:

		// Attributes
		BaseDXData m_baseData;
		WSTRING    m_sEffectPath;
		WSTRING    m_sTexturePath;
		Vector3    m_vLightDirection;
		BBOOL      m_bIndependent;

	public:

		// Constructor & Destructor
		OcelotDXParticleSystemInfo();
		OcelotDXParticleSystemInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, Vector3 lightDir, UBIGINT streamerCount, FFLOAT spread, FFLOAT lifeSpan, FFLOAT fadeExponent, FFLOAT startSize, FFLOAT endSize, 
								   FFLOAT sizeExponent, FFLOAT startSpeed, FFLOAT endSpeed, FFLOAT speedExponent, FFLOAT speedVariance, FFLOAT rotAngle, 
								   FFLOAT windFalloff, Vector3 emitterPosition, Vector3 positionWeights, Vector3 emitterDirection, Vector3 directionVariance, 
								   Vector3 directionWeights, Color color0, Color color1, Color flashColor, BBOOL independent = true);
		~OcelotDXParticleSystemInfo();

		// Accessors
		inline BaseDXData BaseData() const { return m_baseData;}
		inline WSTRING    EffectPath() const { return m_sEffectPath;}
		inline WSTRING    TexturePath() const { return m_sTexturePath;}
		inline Vector3    LightDir() const { return m_vLightDirection;}
		inline BBOOL      isIndependent() const { return m_bIndependent;}
		inline VVOID      SetBaseData(BaseDXData base) { m_baseData = base;}
		inline VVOID      SetEffectPath(WSTRING effectPath) { m_sEffectPath = effectPath;}
		inline VVOID      SetTexturePath(WSTRING texturePath) { m_sTexturePath = texturePath;}
		inline VVOID      SetLightDir(Vector3 lightDir) { m_vLightDirection = lightDir;}
		inline VVOID      SetIndependent(BBOOL state) { m_bIndependent = state;}
	};
}

#endif