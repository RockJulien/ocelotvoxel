#include "OcelotParticleSystemFactory.h"

using namespace Ocelot;

OcelotParticleSystemFactory::OcelotParticleSystemFactory() :
m_baseData(), m_globalParticleBuffer(0), m_iGlobalParticleIndices(0), m_fGlobalParticleDepth(0), m_sEffectPath(L""), m_sTexturePath(L""), m_vLightDirection(1.0f), m_fTime(0.0f), m_iMaxParticles(0), m_iParticleCountUsed(0),
m_fxWorld(NULL), m_fxView(NULL), m_fxProj(NULL), m_fxInvViewProj(NULL), m_fxTime(NULL), m_fxTechParticleToBuffer(NULL), m_fxTechParticleToScene(NULL), m_fxLightDir(NULL), m_fxEyePosition(NULL), m_fxForward(NULL), m_fxRight(NULL), 
m_fxUp(NULL), m_fxPosIntensity(NULL), m_fxColor(NULL), m_fxAttenuation(NULL), m_particleLayout(NULL), m_screenLayout(NULL), m_effect(NULL), m_particleVBuffer(NULL), m_screenQuadVBuffer(NULL), m_offScreenParticleRTV(NULL), 
m_offScreenParticleColorRTV(NULL), m_offscreenParticleTex(NULL), m_offscreenParticleSRV(NULL), m_offscreenParticleColorTex(NULL), m_offscreenParticleColorSRV(NULL), m_rvParticleTexture(NULL), 
m_fxParticleSRV(NULL), m_fxParticleColorSRV(NULL), m_bInitialized(false)
{
	m_globalParticleBuffer.unmap();
	m_iGlobalParticleIndices.unmap();
	m_fGlobalParticleDepth.unmap();
}

OcelotParticleSystemFactory::~OcelotParticleSystemFactory()
{
	releaseSystems();
}

VVOID                   OcelotParticleSystemFactory::initializeGlobalParticleSystemManager(BaseParticleSystem neededGlob,UBIGINT maxParticles)
{
	m_iParticleCountUsed = 0;
	m_baseData           = neededGlob.m_baseData;
	m_sEffectPath        = neededGlob.m_sEffecPath;
	m_sTexturePath       = neededGlob.m_sTexturePath;
	m_vLightDirection    = neededGlob.m_vLightDir;
	m_iMaxParticles      = maxParticles;
	m_globalParticleBuffer.map(maxParticles);
	m_iGlobalParticleIndices.map(maxParticles);
	m_fGlobalParticleDepth.map(maxParticles);

	m_rvParticleTexture = loadRV(m_baseData.Device(), m_sTexturePath);

	// create effect object
	initializeEffect();

	// initialize effect Variables
	initializeEffectVariables();

	// create the input layout
	initializeLayout();

	// Set singleton to initialized state.
	m_bInitialized = true;
}

VVOID                   OcelotParticleSystemFactory::update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity)
{
	// refresh the time variable.
	m_fTime = time;

	if(m_particleSystems.Size() != 0)
	{
		List2Iterator<OcelotDXParticleSystem*> iter = m_particleSystems.iterator();
		for(iter.begin(); iter.notEnd(); iter.next())
		{
			iter.data()->update(deltaTime, time, right, windVel, gravity);
		}

		// update effect variables for all.
		updateEffectVariables();
	}
}

VVOID                   OcelotParticleSystemFactory::renderParticle()
{
	if(m_particleSystems.Size() != 0)
	{
		renderParticlesIntoBuffer();
		compositeParticlesIntoScene();
	}
}

OcelotDXParticleSystem* OcelotParticleSystemFactory::addSystem(ParticleSystemType type, Vector3 position, UBIGINT particleCount, BaseParticleSystem neededForIndependent, BBOOL independent)
{
	OcelotDXParticleSystem* toReturn = NULL;

	switch(type)
	{
	case PARTICULESYSTEM_TYPE_FIRE:
		break;
	case PARTICULESYSTEM_TYPE_SMOKE:
		break;
	case PARTICULESYSTEM_TYPE_WEATHER_RAIN:
		break;
	case PARTICULESYSTEM_TYPE_WEATHER_SNOW:
		break;
	case PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM:
		{
			// Global mushroom attribute.
			Vector3 emitterDir(0.0f);
			Color   color0(OcelotColor::DARKGREY);
			Color   color1(OcelotColor::BLACK);
			Color flashCol(OcelotColor::REDORANGE);
			
			if(independent) // create independentely and return.
			{
				OcelotDXParticleSystemInfo mushInfo(neededForIndependent.m_baseData.Device(), neededForIndependent.m_baseData.GetCamera(), neededForIndependent.m_sTexturePath, neededForIndependent.m_sEffecPath, neededForIndependent.m_vLightDir, 
												    1, 10.0f, 20.0f, 2.0f, 0.0f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
													254.0f, 20.0f, 4.0f, 16.0f, 0.0f, 0.1f, 15.0f, position, Vector3(0.5f, 1.0f, 0.5f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
													emitterDir, Vector3(0.0f), Vector3(1.0f, 2.0f, 1.0f), color0, color1, flashCol); // emitDir / dirVar / dirWeights / color0 / color1 / flashCol / independent
				toReturn = new MushroomParticleSystem(mushInfo);
				OcelotParticle* part = new OcelotParticle[particleCount];
				toReturn->createSystem(particleCount, part);
			}
			else // add to global memory manager and return NULL.
			{
				// avoid overhead.
				if(m_iParticleCountUsed < m_iMaxParticles) // if still space.
				{
					// check if the new one needed is not to big for the asked size.
					UBIGINT tempRest = m_iMaxParticles - m_iParticleCountUsed;
					if(tempRest < particleCount)
						particleCount = tempRest; // truncate.

					// Then, create.
					OcelotDXParticleSystemInfo mushInfo(m_baseData.Device(), m_baseData.GetCamera(), m_sTexturePath, m_sEffectPath, m_vLightDirection, 
														1, 10.0f, 20.0f, 2.0f, 0.0f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
													   254.0f, 20.0f, 4.0f, 16.0f, 0.0f, 0.1f, 15.0f, position, Vector3(0.5f, 1.0f, 0.5f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
													   emitterDir, Vector3(0.0f), Vector3(1.0f, 2.0f, 1.0f), color0, color1, flashCol, false); // emitDir / dirVar / dirWeights / color0 / color1 / flashCol / independent
					OcelotDXParticleSystem* toAdd = new MushroomParticleSystem(mushInfo);
					toAdd->createSystem(particleCount, &m_globalParticleBuffer[m_iParticleCountUsed]);
					m_iParticleCountUsed += particleCount;
					m_particleSystems.putBACK(toAdd);
				}
			}
		}
		break;
	case PARTICULESYSTEM_TYPE_EXPLOSION_STALK:
		{
			// Global stalk attribute.
			Vector3 emitterDir(0.0f);
			Color   color0(OcelotColor::DARKGREY);
			Color   color1(OcelotColor::BLACK);
			Color flashCol(OcelotColor::REDORANGE);

			if(independent) // create independentely and return.
			{
				OcelotDXParticleSystemInfo stalkInfo(neededForIndependent.m_baseData.Device(), neededForIndependent.m_baseData.GetCamera(), neededForIndependent.m_sTexturePath, neededForIndependent.m_sEffecPath, neededForIndependent.m_vLightDir,  
													 1, 15.0f, 20.0f, 8.0f, 0.0f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
													 128.0f, 50.0f, -1.0f, 16.0f, 0.0f, 0.2f, 15.0f, position, Vector3(2.0f, 0.1f, 2.0f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
													 emitterDir, Vector3(0.0f), Vector3(2.0f, 0.1f, 2.0f), color0, color1, flashCol);
				toReturn = new StalkParticleSystem(stalkInfo);
				OcelotParticle* part = new OcelotParticle[particleCount];
				toReturn->createSystem(particleCount, part);
			}
			else // add to global memory manager and return NULL.
			{
				// avoid overhead.
				if(m_iParticleCountUsed < m_iMaxParticles) // if still space.
				{
					// check if the new one needed is not to big for the asked size.
					UBIGINT tempRest = m_iMaxParticles - m_iParticleCountUsed;
					if(tempRest < particleCount)
						particleCount = tempRest; // truncate.

					// Then, create.
					OcelotDXParticleSystemInfo stalkInfo(m_baseData.Device(), m_baseData.GetCamera(), m_sTexturePath, m_sEffectPath, m_vLightDirection, 
														 1, 15.0f, 20.0f, 8.0f, 0.0f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
														 128.0f, 50.0f, -1.0f, 16.0f, 0.0f, 0.2f, 15.0f, position, Vector3(2.0f, 0.1f, 2.0f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
														 emitterDir, Vector3(0.0f), Vector3(2.0f, 0.1f, 2.0f), color0, color1, flashCol, false);
					OcelotDXParticleSystem* toAdd = new StalkParticleSystem(stalkInfo);
					toAdd->createSystem(particleCount, &m_globalParticleBuffer[m_iParticleCountUsed]);
					m_iParticleCountUsed += particleCount;
					m_particleSystems.putBACK(toAdd);
				}
			}
		}
		break;
	case PARTICULESYSTEM_TYPE_EXPLOSION_BURST:
		{
			// Global burst attribute.
			Vector3 emitterDir(0.0f, 0.5f, 0.0f);
			Color   color0(OcelotColor::DARKGREY);
			Color   color1(OcelotColor::BLACK);
			Color flashCol(OcelotColor::REDORANGE);

			if(independent) // create independentely and return.
			{
				OcelotDXParticleSystemInfo burstInfo(neededForIndependent.m_baseData.Device(), neededForIndependent.m_baseData.GetCamera(), neededForIndependent.m_sTexturePath, neededForIndependent.m_sEffecPath, neededForIndependent.m_vLightDir,
													 20, 1.0f, 9.0f, 4.0f, 0.5f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
													 1.0f, 100.0f, 4.0f, 4.0f, 100.0f, 0.2f, 1.0f, position, Vector3(0.5f, 1.0f, 0.5f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
													 emitterDir, Vector3(1.0f, 0.5f, 1.0f), Vector3(1.0f, 1.5f, 1.0f), color0, color1, flashCol);
				toReturn = new BurstParticleSystem(burstInfo);
				OcelotParticle* part = new OcelotParticle[particleCount];
				toReturn->createSystem(particleCount, part);
			}
			else // add to global memory manager and return NULL.
			{
				// avoid overhead.
				if(m_iParticleCountUsed < m_iMaxParticles) // if still space.
				{
					// check if the new one needed is not to big for the asked size.
					UBIGINT tempRest = m_iMaxParticles - m_iParticleCountUsed;
					if(tempRest < particleCount)
						particleCount = tempRest; // truncate.

					// Then, create.
					OcelotDXParticleSystemInfo burstInfo(m_baseData.Device(), m_baseData.GetCamera(), m_sTexturePath, m_sEffectPath, m_vLightDirection, 
														 20, 1.0f, 9.0f, 4.0f, 0.5f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
														 1.0f, 100.0f, 4.0f, 4.0f, 100.0f, 0.2f, 1.0f, position, Vector3(0.5f, 1.0f, 0.5f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
														 emitterDir, Vector3(1.0f, 0.5f, 1.0f), Vector3(1.0f, 1.5f, 1.0f), color0, color1, flashCol, false);
					OcelotDXParticleSystem* toAdd = new BurstParticleSystem(burstInfo);
					toAdd->createSystem(particleCount, &m_globalParticleBuffer[m_iParticleCountUsed]);
					m_iParticleCountUsed += particleCount;
					m_particleSystems.putBACK(toAdd);
				}
			}
		}
		break;
	case PARTICULESYSTEM_TYPE_EXPLOSION_MINE:
		{
			// Global mine attribute.
			Vector3 emitterDir(0.0f, 0.8f, 0.0f);
			Color   color0(OcelotColor::DARKGREY);
			Color   color1(OcelotColor::BLACK);
			Color flashCol(OcelotColor::REDORANGE);

			if(independent) // create independentely and return.
			{
				OcelotDXParticleSystemInfo mineInfo(neededForIndependent.m_baseData.Device(), neededForIndependent.m_baseData.GetCamera(), neededForIndependent.m_sTexturePath, neededForIndependent.m_sEffecPath, neededForIndependent.m_vLightDir,
													0, 1.0f, 9.0f, 4.0f, 2.0f, 60.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
													1.0f, 250.0f, 4.0f, 2.0f, 80.0f, 0.2f, 4.0f, position, Vector3(1.0f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
													emitterDir, Vector3(0.5f, 0.2f, 0.5f), Vector3(2.0f, 4.0f, 2.0f), color0, color1, flashCol);
				toReturn = new MineParticleSystem(mineInfo);
				OcelotParticle* part = new OcelotParticle[particleCount];
				toReturn->createSystem(particleCount, part);
			}
			else // add to global memory manager and return NULL.
			{
				// avoid overhead.
				if(m_iParticleCountUsed < m_iMaxParticles) // if still space.
				{
					// check if the new one needed is not to big for the asked size.
					UBIGINT tempRest = m_iMaxParticles - m_iParticleCountUsed;
					if(tempRest < particleCount)
						particleCount = tempRest; // truncate.

					// Then, create.
					OcelotDXParticleSystemInfo mineInfo(m_baseData.Device(), m_baseData.GetCamera(), m_sTexturePath, m_sEffectPath, m_vLightDirection, 
														0, 1.0f, 9.0f, 4.0f, 2.0f, 60.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
														1.0f, 250.0f, 4.0f, 2.0f, 80.0f, 0.2f, 4.0f, position, Vector3(1.0f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
														emitterDir, Vector3(0.5f, 0.2f, 0.5f), Vector3(2.0f, 4.0f, 2.0f), color0, color1, flashCol, false);
					OcelotDXParticleSystem* toAdd = new MineParticleSystem(mineInfo);
					toAdd->createSystem(particleCount, &m_globalParticleBuffer[m_iParticleCountUsed]);
					m_iParticleCountUsed += particleCount;
					m_particleSystems.putBACK(toAdd);
				}
			}
		}
		break;
	default:
		toReturn = NULL;
	}

	// initialize vertice's buffers. (need m_iParticleCountUsed != 0 and be refreshed after new add.
	initializeVertices();

	return toReturn;
}

VVOID                   OcelotParticleSystemFactory::modifySystem(OcelotDXParticleSystemInfo newInfo, UBIGINT index)
{
	if(m_particleSystems.Size() != 0)
	{
		List2Iterator<OcelotDXParticleSystem*> iter = m_particleSystems.iterator();
		UBIGINT counter = 0;
		for(iter.begin(); iter.notEnd(); iter.next(), counter++)
		{
			if(counter == index)
			{
				OcelotDXParticleSystem* temp = iter.data();
				temp->SetInfo(newInfo);
			}
		}
	}
}

VVOID                   OcelotParticleSystemFactory::changePosition(Vector3 newPosition, UBIGINT index)
{
	if(m_particleSystems.Size() != 0)
	{
		List2Iterator<OcelotDXParticleSystem*> iter = m_particleSystems.iterator();
		UBIGINT counter = 0;
		for(iter.begin(); iter.notEnd(); iter.next(), counter++)
		{
			if(counter == index)
			{
				OcelotDXParticleSystem* temp = iter.data();
				temp->SetPosition(newPosition);
			}
		}
	}
}

VVOID                   OcelotParticleSystemFactory::releaseSystems()
{
	ReleaseCOM(m_offscreenParticleTex);
	ReleaseCOM(m_offscreenParticleColorTex);
	ReleaseCOM(m_offscreenParticleSRV);
	ReleaseCOM(m_offscreenParticleColorSRV);
	ReleaseCOM(m_offScreenParticleRTV);
	ReleaseCOM(m_offScreenParticleColorRTV);
	ReleaseCOM(m_effect);
	ReleaseCOM(m_particleLayout);
	ReleaseCOM(m_screenLayout);
	ReleaseCOM(m_screenQuadVBuffer);
	ReleaseCOM(m_particleVBuffer);
	ReleaseCOM(m_rvParticleTexture);

	// Destroy Systems.
	if(m_particleSystems.Size() != 0)
		m_particleSystems.clear();

	m_fxWorld                = NULL;
	m_fxView                 = NULL;
	m_fxProj                 = NULL;
	m_fxInvViewProj          = NULL;
	m_fxTime                 = NULL;
	m_fxTechParticleToBuffer = NULL;
	m_fxTechParticleToScene  = NULL;
	m_fxLightDir             = NULL;
	m_fxEyePosition          = NULL;
	m_fxForward              = NULL;
	m_fxRight                = NULL;
	m_fxUp                   = NULL;
	m_fxPosIntensity         = NULL;
	m_fxColor                = NULL;
	m_fxAttenuation          = NULL;
	m_fxParticleSRV          = NULL;
	m_fxParticleColorSRV     = NULL;
}

VVOID					OcelotParticleSystemFactory::initializeEffect()
{
	HR(initializeDXEffect(m_baseData.Device(), m_sEffectPath, &m_effect));
}

VVOID					OcelotParticleSystemFactory::initializeLayout()
{
	// Initialize the particle layout.
	const D3D10_INPUT_ELEMENT_DESC ParticleLayout[] =
    {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D10_INPUT_PER_VERTEX_DATA, 0},
        {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0},
        {"LIFENROT", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 20, D3D10_INPUT_PER_VERTEX_DATA, 0},
        {"COLOR",    0, DXGI_FORMAT_R8G8B8A8_UNORM,  0, 28, D3D10_INPUT_PER_VERTEX_DATA, 0}
    };
	UBIGINT nbElement1 = sizeof(ParticleLayout) / sizeof(ParticleLayout[0]);

    D3D10_PASS_DESC PassDesc;
	HR(m_fxTechParticleToBuffer->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_baseData.Device()->CreateInputLayout(ParticleLayout, nbElement1, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_particleLayout));

	// Initialize the screen layout.
	const D3D10_INPUT_ELEMENT_DESC Screenlayout[] =
    {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
    };
	UBIGINT nbElement2 = sizeof(Screenlayout) / sizeof(Screenlayout[0]);

	HR(m_fxTechParticleToScene->GetPassByIndex(0)->GetDesc(&PassDesc));
    HR(m_baseData.Device()->CreateInputLayout(Screenlayout, nbElement2, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_screenLayout));
}

VVOID					OcelotParticleSystemFactory::initializeVertices()
{
	// Initialize the particle buffer as Dynamic.
	D3D10_BUFFER_DESC BDesc;
	BDesc.ByteWidth      = sizeof(ParticleVertex) * 6 * m_iParticleCountUsed; // * 6 for number of triangle vertices per particle.
    BDesc.Usage          = D3D10_USAGE_DYNAMIC;
    BDesc.BindFlags      = D3D10_BIND_VERTEX_BUFFER;
    BDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
    BDesc.MiscFlags      = 0;
	HR(m_baseData.Device()->CreateBuffer(&BDesc, NULL, &m_particleVBuffer));

	// Initialize the screen quad buffer as Immutable.
	BDesc.ByteWidth      = 4 * sizeof(Vector3);
    BDesc.Usage          = D3D10_USAGE_IMMUTABLE;
    BDesc.BindFlags      = D3D10_BIND_VERTEX_BUFFER;
    BDesc.CPUAccessFlags = 0;
    BDesc.MiscFlags      = 0;

    Vector3 vertices[4] =
    {
        Vector3(-1.0f, -1.0f, 0.5f),
        Vector3(-1.0f,  1.0f, 0.5f),
        Vector3( 1.0f, -1.0f, 0.5f),
        Vector3( 1.0f,  1.0f, 0.5f)
    };

    D3D10_SUBRESOURCE_DATA InitData;
    InitData.pSysMem = vertices;
	HR(m_baseData.Device()->CreateBuffer(&BDesc, &InitData, &m_screenQuadVBuffer));
}

VVOID					OcelotParticleSystemFactory::updateEffectVariables()
{
	// Grab the camera's matrices.
	Matrix4x4 view = m_baseData.GetCamera()->View();
	Matrix4x4 proj = m_baseData.GetCamera()->Proj();

	// Compute the forward, side and up vector.
	Vector3 forward(view.get_13(), view.get_23(), view.get_33());
	Vector3 side(view.get_11(), view.get_21(), view.get_31());
	Vector3 up(view.get_12(), view.get_22(), view.get_32());
	forward.Vec3Normalise();
	side.Vec3Normalise();
	up.Vec3Normalise();

	// Set axes vector
	m_fxForward->SetRawValue((FFLOAT*)&forward, 0, sizeof(Vector3));
	m_fxRight->SetRawValue((FFLOAT*)&side, 0, sizeof(Vector3));
	m_fxUp->SetRawValue((FFLOAT*)&up, 0, sizeof(Vector3));

	// Re-Compute the vertex buffer.
	ParticleVertex* vertices = NULL;
	HR(m_particleVBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (VVOID**)&vertices));

	// Copy particle to vertex buffer.
	copyParticlesToVBuffer(vertices, side, up);

	m_particleVBuffer->Unmap();

	// Send Matrices.
	Matrix4x4 world, invViewProj;
	world.Matrix4x4Identity();
	invViewProj = view * proj;
	invViewProj.Matrix4x4Inverse(invViewProj);

	HR(m_fxWorld->SetMatrix((FFLOAT*)&world));
	HR(m_fxView->SetMatrix((FFLOAT*)&view));
	HR(m_fxProj->SetMatrix((FFLOAT*)&proj));
	HR(m_fxInvViewProj->SetMatrix((FFLOAT*)&invViewProj));
	HR(m_fxTime->SetFloat(m_fTime));
	HR(m_fxEyePosition->SetRawValue((FFLOAT*)&m_baseData.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));
	HR(m_fxLightDir->SetRawValue((FFLOAT*)&m_vLightDirection, 0, sizeof(Vector3)));

	Color posIntensity;
	Color flashColor;
	FFLOAT lightRaise = 2.0f;

	List2Iterator<OcelotDXParticleSystem*> iter = m_particleSystems.iterator();
	for(iter.begin(); iter.notEnd(); iter.next())
	{
		flashColor                  = iter.data()->Info().FlashColor();
		FFLOAT currTime             = iter.data()->CurrentTime();
		FFLOAT lifeSpan             = iter.data()->Info().LifeSpan();
		FFLOAT flashLife            = lifeSpan * 0.5f; // default flash life time equal life of particles divided by 2.
		Vector3 tempPos             = iter.data()->Info().Position();
		ParticleSystemType currType = iter.data()->Type();
		// Add extra per frame code for the mushroom.
		if(currTime > lifeSpan)
		{
			// Re-initialize
			iter.data()->initialize();
		}
		// else update the flashing light effect.
		else if((currTime > 0.0f) && (currTime < flashLife))
		{
			FFLOAT intensity   = 1000.0f * ((flashLife - currTime) / flashLife); // 1000.0f flash intensity.

			switch(currType)
			{
			case PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM:
				posIntensity = Color(tempPos.getX() + (lightRaise * 2.0f), tempPos.getY() + (lightRaise * 2.0f), tempPos.getZ() + (lightRaise * 2.0f), intensity);
				break;
			case PARTICULESYSTEM_TYPE_EXPLOSION_STALK:
				posIntensity = Color(tempPos.getX() + lightRaise, tempPos.getY() + lightRaise, tempPos.getZ() + lightRaise, intensity);
				break;
			case PARTICULESYSTEM_TYPE_EXPLOSION_BURST:
				posIntensity = Color(tempPos.getX() + (lightRaise * 2.0f), tempPos.getY() + (lightRaise * 2.0f), tempPos.getZ() + (lightRaise * 2.0f), intensity);
				break;
			case PARTICULESYSTEM_TYPE_EXPLOSION_MINE:
				posIntensity = Color(tempPos.getX(), tempPos.getY() + lightRaise, tempPos.getZ(), intensity);
				break;
			default:
				posIntensity = Color(tempPos.getX() + lightRaise, tempPos.getY() + lightRaise, tempPos.getZ() + lightRaise, intensity);
				break;
			}
		}
	}

	Vector3 attenuation(0.0f, 0.0f, 3.0f);

	// Set up flash variables.
	HR(m_fxPosIntensity->SetRawValue((FFLOAT*)&posIntensity, 0, sizeof(Color)));
	HR(m_fxColor->SetRawValue((FFLOAT*)&flashColor, 0, sizeof(Color)));
	HR(m_fxAttenuation->SetRawValue((FFLOAT*)&attenuation, 0, sizeof(Vector3)));
}

VVOID					OcelotParticleSystemFactory::initializeEffectVariables()
{
	m_fxTechParticleToBuffer = m_effect->GetTechniqueByName("RenderParticlesToBuffer");
	m_fxTechParticleToScene  = m_effect->GetTechniqueByName("CompositeParticlesToScene");
	m_fxLightDir			 = m_effect->GetVariableByName("gLightDir");
	m_fxEyePosition			 = m_effect->GetVariableByName("gEyePosW");
	m_fxForward				 = m_effect->GetVariableByName("gForward");
	m_fxRight				 = m_effect->GetVariableByName("gSide"); 
	m_fxUp					 = m_effect->GetVariableByName("gUp");
	m_fxTime				 = m_effect->GetVariableByName("gTime")->AsScalar();
	m_fxWorld				 = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView				 = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj				 = m_effect->GetVariableByName("gProj")->AsMatrix();
	m_fxInvViewProj			 = m_effect->GetVariableByName("gInvViewProj")->AsMatrix();
	m_fxPosIntensity		 = m_effect->GetVariableByName("gPosIntensity");
	m_fxColor				 = m_effect->GetVariableByName("gColor");
	m_fxAttenuation			 = m_effect->GetVariableByName("gAttenuation");
	m_fxParticleSRV			 = m_effect->GetVariableByName("gTextureParticle")->AsShaderResource();
	m_fxParticleColorSRV	 = m_effect->GetVariableByName("gColorParticle")->AsShaderResource();
}

VVOID					OcelotParticleSystemFactory::renderParticlesIntoBuffer()
{
	// Clear the new render target view.
	FFLOAT color[4] = { 0, 0, 0, 0 };

	m_baseData.Device()->ClearRenderTargetView(m_offScreenParticleRTV, color);
	m_baseData.Device()->ClearRenderTargetView(m_offScreenParticleColorRTV, color);

	// Get previous render targets view(s)
	ID3D10RenderTargetView* prevRTV;
	ID3D10DepthStencilView* prevDSV;
	m_baseData.Device()->OMGetRenderTargets(1, &prevRTV, &prevDSV);

	// Set the new render targets view(s).
	ID3D10RenderTargetView* newViews[2];
	newViews[0] = m_offScreenParticleRTV;
	newViews[1] = m_offScreenParticleColorRTV;
	m_baseData.Device()->OMSetRenderTargets(2, newViews, prevDSV);

	// Then, render particles.
	// set the input layout and buffers
	m_baseData.Device()->IASetInputLayout(m_particleLayout);

	// vertex buffer
	UBIGINT stride[1];
    UBIGINT offset[1];
    ID3D10Buffer* tempVertexBuffer[1];
	tempVertexBuffer[0] = m_particleVBuffer;
	stride[0] = sizeof(ParticleVertex);
    offset[0] = 0;

	m_baseData.Device()->IASetVertexBuffers(0, 1, tempVertexBuffer, stride, offset);

	// index buffer
	m_baseData.Device()->IASetIndexBuffer(NULL, DXGI_FORMAT_R16_UINT, 0);

	// primitive topology
	m_baseData.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Set the resource texture loaded from file.
	m_fxParticleSRV->SetResource(m_rvParticleTexture);

	// Render Particles.
	D3D10_TECHNIQUE_DESC techDesc;
	HR(m_fxTechParticleToBuffer->GetDesc(&techDesc));

	// for every pass in the shader file.
	for(UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
	{
		HR(m_fxTechParticleToBuffer->GetPassByIndex(p)->Apply(0));
		m_baseData.Device()->Draw(6 * m_iParticleCountUsed, 0);
	}

	// And restore original render targets view(s) for not disturbing other rendered things.
	newViews[0] = prevRTV;
	newViews[1] = NULL;
	m_baseData.Device()->OMSetRenderTargets(2, newViews, prevDSV);
	ReleaseCOM(prevRTV);
	ReleaseCOM(prevDSV);
}

VVOID					OcelotParticleSystemFactory::compositeParticlesIntoScene()
{
	// set the input layout and buffers
	m_baseData.Device()->IASetInputLayout(m_screenLayout);

	// vertex buffer 
	UBIGINT stride[1];
    UBIGINT offset[1];
    ID3D10Buffer* tempVertexBuffer[1];
	tempVertexBuffer[0] = m_screenQuadVBuffer;
    stride[0] = sizeof(Vector3);
    offset[0] = 0;

	m_baseData.Device()->IASetVertexBuffers(0, 1, tempVertexBuffer, stride, offset);

	// index buffer
	m_baseData.Device()->IASetIndexBuffer(NULL, DXGI_FORMAT_R16_UINT, 0);

	// primitive topology
	m_baseData.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Set texture created from render target views.
	m_fxParticleSRV->SetResource(m_offscreenParticleSRV);
	m_fxParticleColorSRV->SetResource(m_offscreenParticleColorSRV);

	// Render Particles.
	D3D10_TECHNIQUE_DESC techDesc;
	HR(m_fxTechParticleToScene->GetDesc(&techDesc));

	// for every pass in the shader file.
	for(UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
	{
		HR(m_fxTechParticleToScene->GetPassByIndex(p)->Apply(0));
		m_baseData.Device()->Draw(4, 0);
	}

	// Un-set the OM linked shader resource texture.
	m_fxParticleColorSRV->SetResource(NULL);
	for(UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
	{
		HR(m_fxTechParticleToScene->GetPassByIndex(p)->Apply(0));
	}
}

VVOID					OcelotParticleSystemFactory::copyParticlesToVBuffer(ParticleVertex* vertBuffer, Vector3 side, Vector3 up)
{
	for(UBIGINT currPart = 0; currPart < m_iParticleCountUsed; currPart++)
	{
		// Simply initialize the index buffer in a growing order.
		m_iGlobalParticleIndices[currPart] = currPart;
		Vector3 toEye = m_baseData.GetCamera()->Info().Eye() - m_globalParticleBuffer[currPart].Position();
		// Then, initialize the depth buffer with the length of toEye.
		m_fGlobalParticleDepth[currPart] = toEye.Vec3LengthSquared();
	}

	// Sort particle regarding to the depth.
	DQuickSort(m_iGlobalParticleIndices.Array(), m_fGlobalParticleDepth.Array(), 0, m_iParticleCountUsed - 1);

	// Create a temporary quad for convenience.
	Vector2 quad[4] = 
	{
		Vector2(-1.0f, -1.0f),
		Vector2( 1.0f, -1.0f),
		Vector2( 1.0f,  1.0f),
		Vector2(-1.0f,  1.0f)
	};

	UBIGINT VBCounter = 0;
	// For each particle.
	for(BIGINT currPart = m_iParticleCountUsed - 1; currPart >= 0; currPart--)
	{
		// Grab the index corresponding to the currPart.
		UBIGINT index = m_iGlobalParticleIndices[currPart];

		// Grab the particle's info corresponding to the above index.
		Vector3 partPos  = m_globalParticleBuffer[index].Position();
		FFLOAT  partRad  = m_globalParticleBuffer[index].Radius();
		FFLOAT  partFade = m_globalParticleBuffer[index].Fade();
		FFLOAT  partRot  = m_globalParticleBuffer[index].RotAngle();
		UBIGINT partCol  = m_globalParticleBuffer[index].Color();

		// Check if the particle is visible else skip the rest.
		if(!m_globalParticleBuffer[index].IsVisible())
			continue;

		// Rotate the screen quad.
		Vector2 newQuad[4];
		// Compute rotation to apply on X & Y.
		FFLOAT sinAngle = sinf(partRot);
		FFLOAT cosAngle = cosf(partRot);

		for(UBIGINT currCorner = 0; currCorner < 4; currCorner++)
		{
			FFLOAT tempX = cosAngle * quad[currCorner].getX() - sinAngle * quad[currCorner].getY();
			FFLOAT tempY = sinAngle * quad[currCorner].getX() + cosAngle * quad[currCorner].getY();
			tempX *= partRad;
			tempY *= partRad; // scale to the particle's radius.

			newQuad[currCorner].x(tempX);
			newQuad[currCorner].y(tempY);
		}

		// Compute and store the first triangle containing the first three particle vertices.
		// Triangle (0, 1, 3), indices corresponding to value of newQuad.
		vertBuffer[VBCounter + 2].Position = partPos + side * newQuad[0].getX() + up * newQuad[0].getY();
		vertBuffer[VBCounter + 2].TexCoord = Vector2(0.0f, 1.0f);
		vertBuffer[VBCounter + 2].LifeNRot = Vector2(partFade, partRot);
		vertBuffer[VBCounter + 2].Color    = partCol;
		vertBuffer[VBCounter + 1].Position = partPos + side * newQuad[1].getX() + up * newQuad[1].getY();
		vertBuffer[VBCounter + 1].TexCoord = Vector2(1.0f, 1.0f);
		vertBuffer[VBCounter + 1].LifeNRot = Vector2(partFade, partRot);
		vertBuffer[VBCounter + 1].Color    = partCol;
		vertBuffer[VBCounter].Position     = partPos + side * newQuad[3].getX() + up * newQuad[3].getY();
		vertBuffer[VBCounter].TexCoord     = Vector2(0.0f, 0.0f);
		vertBuffer[VBCounter].LifeNRot     = Vector2(partFade, partRot);
		vertBuffer[VBCounter].Color        = partCol;

		// Compute and store the second triangle containing the three last particle vertices.
		// Triangle (3, 1, 2), indices corresponding to value of newQuad.
		vertBuffer[VBCounter + 5].Position = partPos + side * newQuad[3].getX() + up * newQuad[3].getY();
		vertBuffer[VBCounter + 5].TexCoord = Vector2(0.0f, 0.0f);
		vertBuffer[VBCounter + 5].LifeNRot = Vector2(partFade, partRot);
		vertBuffer[VBCounter + 5].Color    = partCol;
		vertBuffer[VBCounter + 4].Position = partPos + side * newQuad[1].getX() + up * newQuad[1].getY();
		vertBuffer[VBCounter + 4].TexCoord = Vector2(1.0f, 1.0f);
		vertBuffer[VBCounter + 4].LifeNRot = Vector2(partFade, partRot);
		vertBuffer[VBCounter + 4].Color    = partCol;
		vertBuffer[VBCounter + 3].Position = partPos + side * newQuad[2].getX() + up * newQuad[2].getY();
		vertBuffer[VBCounter + 3].TexCoord = Vector2(1.0f, 0.0f);
		vertBuffer[VBCounter + 3].LifeNRot = Vector2(partFade, partRot);
		vertBuffer[VBCounter + 3].Color    = partCol;

		// Incremente the VBCounter.
		VBCounter += 6;
	}
}

VVOID					OcelotParticleSystemFactory::resetRenderTargetView(UBIGINT width, UBIGINT height)
{
	/*********************************** Will be used each time the back buffer is resized  ************************************/

	// Create the offscreen particle buffer to the proper size (window's size).
	D3D10_TEXTURE2D_DESC Desc;
    Desc.Width              = width;
    Desc.Height             = height;
    Desc.MipLevels          = 1;
    Desc.ArraySize          = 1;
    Desc.Format             = DXGI_FORMAT_R16G16B16A16_FLOAT;
    Desc.SampleDesc.Count   = 1;
    Desc.SampleDesc.Quality = 0;
    Desc.Usage				= D3D10_USAGE_DEFAULT;
    Desc.BindFlags			= D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE;
    Desc.CPUAccessFlags		= 0;
    Desc.MiscFlags			= 0;

	// Create 2D texture which will eb used for the RTV creation.
	HR(m_baseData.Device()->CreateTexture2D(&Desc, NULL, &m_offscreenParticleTex));
	Desc.Format             = DXGI_FORMAT_R8G8B8A8_UNORM;
	HR(m_baseData.Device()->CreateTexture2D(&Desc, NULL, &m_offscreenParticleColorTex));

	D3D10_RENDER_TARGET_VIEW_DESC RTVDesc;
    RTVDesc.Format             = DXGI_FORMAT_R16G16B16A16_FLOAT;
    RTVDesc.ViewDimension      = D3D10_RTV_DIMENSION_TEXTURE2D;
    RTVDesc.Texture2D.MipSlice = 0;

	// Create the Render Target Views.
	HR(m_baseData.Device()->CreateRenderTargetView(m_offscreenParticleTex, &RTVDesc, &m_offScreenParticleRTV));
	RTVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	HR(m_baseData.Device()->CreateRenderTargetView(m_offscreenParticleColorTex, &RTVDesc, &m_offScreenParticleColorRTV));

	// Create the shader resource views
	D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
    SRVDesc.Format                    = DXGI_FORMAT_R16G16B16A16_FLOAT;
    SRVDesc.ViewDimension             = D3D10_SRV_DIMENSION_TEXTURE2D;
    SRVDesc.Texture2D.MostDetailedMip = 0;
    SRVDesc.Texture2D.MipLevels       = Desc.MipLevels;

	HR(m_baseData.Device()->CreateShaderResourceView(m_offscreenParticleTex, &SRVDesc, &m_offscreenParticleSRV));
	SRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	HR(m_baseData.Device()->CreateShaderResourceView(m_offscreenParticleColorTex, &SRVDesc, &m_offscreenParticleColorSRV));
}
