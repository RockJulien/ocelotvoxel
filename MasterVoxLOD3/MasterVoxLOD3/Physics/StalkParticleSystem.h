#ifndef DEF_STALKPARTICLESYSTEM_H
#define DEF_STALKPARTICLESYSTEM_H

#include "OcelotDXParticleSystem.h"

namespace Ocelot
{
	class StalkParticleSystem : public OcelotDXParticleSystem
	{
	private:

		// Attribute

	public:

		// Constructor & Destructor
		StalkParticleSystem();
		StalkParticleSystem(OcelotDXParticleSystemInfo info);
		~StalkParticleSystem();

		// Methods to use once.
		VVOID createSystem(UBIGINT particleCount, OcelotParticle* adressToStart);
		VVOID initialize();

		// API dependent.
		VVOID initializeLayout();
		VVOID initializeVertices();
		VVOID initializeEffectVariables();
		VVOID releaseSystem();

		// Methods per frame.
		VVOID update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity);
		VVOID updateEffectVariables();
		VVOID renderParticles();

		// Accessors
	};
}

#endif