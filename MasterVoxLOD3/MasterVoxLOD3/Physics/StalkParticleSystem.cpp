#include "StalkParticleSystem.h"

using namespace Ocelot;

StalkParticleSystem::StalkParticleSystem() :
OcelotDXParticleSystem()
{
	m_iType = PARTICULESYSTEM_TYPE_EXPLOSION_STALK;
}

StalkParticleSystem::StalkParticleSystem(OcelotDXParticleSystemInfo info) :
OcelotDXParticleSystem(info)
{
	m_iType = PARTICULESYSTEM_TYPE_EXPLOSION_STALK;
}

StalkParticleSystem::~StalkParticleSystem()
{
	releaseSystem();
}

VVOID StalkParticleSystem::createSystem(UBIGINT particleCount, OcelotParticle* adressToStart)
{
	OcelotDXParticleSystem::createSystem(particleCount, adressToStart);

	// initialize
	initialize();
}

VVOID StalkParticleSystem::initialize()
{
	OcelotDXParticleSystem::initialize();
}

VVOID StalkParticleSystem::initializeLayout()
{
	OcelotDXParticleSystem::initializeLayout();
}

VVOID StalkParticleSystem::initializeVertices()
{
	OcelotDXParticleSystem::initializeVertices();
}

VVOID StalkParticleSystem::initializeEffectVariables()
{
	OcelotDXParticleSystem::initializeEffectVariables();
}

VVOID StalkParticleSystem::releaseSystem()
{
	OcelotDXParticleSystem::releaseSystem();
}

VVOID StalkParticleSystem::update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity)
{
	if(m_fCurrentTime > 0.0f)
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			// Compute the size, speed and fade interpolation values.
			FFLOAT t   = m_particleBuffer[currPart].Life() / m_info.LifeSpan();
			FFLOAT tm1 = t - 1.0f;
			FFLOAT sizeLerp  = 1.0f - pow(tm1, m_info.SizeExponent());
			FFLOAT speedLerp = 1.0f - pow(tm1, m_info.SpeedExponent());
			FFLOAT fadeLerp  = 1.0f - pow(tm1, m_info.FadeExponent());

			// Interpolate the size, speed and fade values for the particle.
			FFLOAT size  = sizeLerp * m_info.EndSize() + (1.0f - sizeLerp) * m_info.StartSize();
			FFLOAT speed = speedLerp * m_info.EndSpeed() + (1.0f - speedLerp) * m_info.StartSpeed();
			FFLOAT fade  = fadeLerp;

			// Compute the angle between the particle dir & the right vector.
			Vector3 partDir  = m_particleBuffer[currPart].Position() - m_info.Position();
			FFLOAT currAngle = partDir.Vec3DotProduct(right);

			// Compute the new angle variation to apply (stalk particularity: +currAngle => no inverse rotation of the smoke).
			FFLOAT newAngle  = currAngle * m_info.RotAngle() * deltaTime * (1.0f - t);

			// Compute the particle's velocity.
			Vector3 velocity = m_particleBuffer[currPart].Direction() * speed;

			// Constrain the velocity to the ground (stalk particularity: constrained to the ground (Y)).
			Vector3 partDirConstrained = Vector3(partDir.getX(), m_info.Position().getY(), partDir.getZ());
			FFLOAT lenSqr = partDirConstrained.Vec3LengthSquared() / m_info.Spread();  // divided by the spread factor.
			FFLOAT constrainedExpensionFactor = min(windVel.getY(), (1.0f / lenSqr));
			FFLOAT velX = velocity.getX() - 1.0f * partDirConstrained.getX();
			FFLOAT velY = constrainedExpensionFactor;
			FFLOAT velZ = velocity.getZ() - 1.0f * partDirConstrained.getZ();
			velocity.x(velX);
			velocity.y(velY);
			velocity.z(velZ);

			// and add wind forces to velocity.
			FFLOAT windAm = max(0.0f, min(1.0f, partDir.getY() / m_info.WindFalloff()));
			velocity += (windVel * windAm);

			// Compute the new position.
			Vector3 tempPos = m_particleBuffer[currPart].Position() + (deltaTime * velocity);
			// Compute the new life.
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			// Compute the new rotation.
			FFLOAT tempRot  = m_particleBuffer[currPart].RotAngle() + newAngle;

			// reset particle's attributes.
			m_particleBuffer[currPart].SetPosition(tempPos);
			m_particleBuffer[currPart].SetRotAngle(tempRot);
			m_particleBuffer[currPart].SetLife(tempLife);
			m_particleBuffer[currPart].SetRadius(size);
			m_particleBuffer[currPart].SetFade(fade);
			m_particleBuffer[currPart].Visible();
		}

		// If not running, start.
		if(!m_bRunning)
		{
			// trigger extra behavior for other classes here.
			// e.g. If the explosion has to influence a close object's shape.
			m_bRunning = true;
		}
	}
	else
	{
		// for each particles
		for(UBIGINT currPart = 0; currPart < m_iParticleCount; currPart++)
		{
			m_particleBuffer[currPart].NotVisible();
			FFLOAT tempLife = m_particleBuffer[currPart].Life() + deltaTime;
			m_particleBuffer[currPart].SetLife(tempLife);
		}
	}

	// Increase current time by the elapsed time.
	m_fCurrentTime += deltaTime;

	if(m_bIndependent)
		// update effect variables.
		updateEffectVariables();
}

VVOID StalkParticleSystem::updateEffectVariables()
{
	OcelotDXParticleSystem::updateEffectVariables();

	Color posIntensity;
	FFLOAT flashLife  = m_info.LifeSpan() * 0.5f;
	FFLOAT lightRaise = 2.0f;
	// Add extra per frame code for the mushroom.
	if(m_fCurrentTime > m_info.LifeSpan())
	{
		// Re-initialize
		initialize();
	}
	// else update the flashing light effect.
	else if((m_fCurrentTime > 0.0f) && (m_fCurrentTime < flashLife))
	{
		FFLOAT intensity   = 1000.0f * ((flashLife - m_fCurrentTime) / flashLife); // 0.5f flash life/ 1000.0f flash intensity.
		posIntensity = Color(m_info.Position().getX() + lightRaise, m_info.Position().getY() + lightRaise, m_info.Position().getZ() + lightRaise, intensity); // 1.0f intensity increase factor.
	}

	// Set up flash variables.
	HR(m_fxPosIntensity->SetRawValue((FFLOAT*)&posIntensity, 0, sizeof(Color)));
	HR(m_fxColor->SetRawValue((FFLOAT*)&m_info.FlashColor(), 0, sizeof(Color)));
	HR(m_fxAttenuation->SetRawValue((FFLOAT*)new Vector3(0.0f, 0.0f, 3.0f), 0, sizeof(Vector3)));
}

VVOID StalkParticleSystem::renderParticles()
{
	OcelotDXParticleSystem::renderParticles();
}
