#ifndef DEF_MUSHROOMPARTICLESYSTEM_H
#define DEF_MUSHROOMPARTICLESYSTEM_H

#include "OcelotDXParticleSystem.h"

namespace Ocelot
{
	class MushroomParticleSystem : public OcelotDXParticleSystem
	{
	private:

		// Attributes.
		

	public:

		// Constructor & Destructor
		MushroomParticleSystem();
		MushroomParticleSystem(OcelotDXParticleSystemInfo info);
		~MushroomParticleSystem();

		// Methods to use once.
		VVOID createSystem(UBIGINT particleCount, OcelotParticle* adressToStart);
		VVOID initialize();

		// API dependent.
		VVOID initializeLayout();
		VVOID initializeVertices();
		VVOID initializeEffectVariables();
		VVOID releaseSystem();

		// Methods per frame.
		VVOID update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity);
		VVOID updateEffectVariables();
		VVOID renderParticles();

		// Accessors.
	};
}

#endif