#include "OcelotParticle.h"

using namespace Ocelot;

OcelotParticle::OcelotParticle() :
m_vPosition(0.0f), m_vDirection(1.0f), m_vMass(1.0f), m_iColor(0), m_fRadius(1.0f), 
m_fLife(9.0f), m_fFade(0.0f), m_fRotAngle(0.5f), m_fRotStep(0.1f), m_bVisible(true)
{

}

OcelotParticle::OcelotParticle(Vector3 position, Vector3 direction, Vector3 mass, UBIGINT color, FFLOAT radius, FFLOAT life, FFLOAT rotAngle, FFLOAT rotStep, FFLOAT fade, BBOOL visibility) :
m_vPosition(position), m_vDirection(direction), m_vMass(mass), m_iColor(color), m_fRadius(radius), 
m_fLife(life), m_fFade(fade), m_fRotAngle(rotAngle), m_fRotStep(rotStep), m_bVisible(visibility)
{

}

OcelotParticle::~OcelotParticle()
{

}
