#ifndef DEF_OCELOTDXPARTICLESYSTEM_H
#define DEF_OCELOTDXPARTICLESYSTEM_H

#include "OcelotDXParticleSystemInfo.h"
#include "..\Geometry\VertexTypes.h"

namespace Ocelot
{
#define MAX_FLASH 10

	class OcelotDXParticleSystem : public ParticleSystem
	{
	protected:

			// Attribute
			BaseDXData                   m_baseData;
			WSTRING                      m_sEffectPath;
			WSTRING                      m_sTexturePath;
			Vector3                      m_vLightDirection;
			FFLOAT                       m_fTime;
			BBOOL                        m_bIndependent;

			// Effect variables.
			ID3D10EffectMatrixVariable*  m_fxWorld;                // effect's matrix
			ID3D10EffectMatrixVariable*  m_fxView;                 // effect's matrix
			ID3D10EffectMatrixVariable*  m_fxProj;                 // effect's matrix
			ID3D10EffectMatrixVariable*  m_fxInvViewProj;          // effect's matrix
			ID3D10EffectScalarVariable*  m_fxTime;
			ID3D10EffectTechnique*       m_fxTechParticleToBuffer; // effect's technique.
			ID3D10EffectTechnique*       m_fxTechParticleToScene;  // effect's technique.
			ID3D10EffectVariable*        m_fxLightDir;             // effect's vector
			ID3D10EffectVariable*        m_fxEyePosition;          // effect's vector
			ID3D10EffectVariable*		 m_fxForward;              // effect's vector
			ID3D10EffectVariable*        m_fxRight;                // effect's vector
			ID3D10EffectVariable*        m_fxUp;                   // effect's vector

			// Glow light variables.
			ID3D10EffectVariable*        m_fxPosIntensity;
			ID3D10EffectVariable*        m_fxColor;
			ID3D10EffectVariable*        m_fxAttenuation;

			// Geometry variables.
			ID3D10InputLayout*			 m_particleLayout;     // the geometry layout.
			ID3D10InputLayout*			 m_screenLayout;      
			ID3D10Effect*				 m_effect;             // unless effect :-) which is the effect itself (the shader file).
			ID3D10Buffer*				 m_particleVBuffer;    // the geometry particle buffer.
			ID3D10Buffer*				 m_screenQuadVBuffer;  // the geometry screen quad buffer.

			// Render target view variable.
			ID3D10RenderTargetView*      m_offScreenParticleRTV;
			ID3D10RenderTargetView*      m_offScreenParticleColorRTV;

			// Texture & Shader Resource View.
			ID3D10Texture2D*             m_offscreenParticleTex;
			ID3D10ShaderResourceView*    m_offscreenParticleSRV;
			ID3D10Texture2D*             m_offscreenParticleColorTex;
			ID3D10ShaderResourceView*    m_offscreenParticleColorSRV;
			ID3D10ShaderResourceView*    m_rvParticleTexture;

			// Effect Shader Resource variables.
			ID3D10EffectShaderResourceVariable* m_fxParticleSRV;
			ID3D10EffectShaderResourceVariable* m_fxParticleColorSRV;
		
			// Private Methods.
			VVOID renderParticlesIntoBuffer();
			VVOID compositeParticlesIntoScene();
			VVOID copyParticlesToVBuffer(ParticleVertex* vertBuffer, Vector3 side, Vector3 up);

	public:

			// Constructor & Destructor
			OcelotDXParticleSystem();
			OcelotDXParticleSystem(OcelotDXParticleSystemInfo info);
	virtual ~OcelotDXParticleSystem();

			// Methods to use once.
	virtual VVOID createSystem(UBIGINT particleCount, OcelotParticle* adressToStart);
	virtual VVOID initialize();

			// API dependent.
	virtual VVOID initializeLayout();
	virtual VVOID initializeEffect();
	virtual VVOID initializeVertices();
	virtual VVOID initializeIndices();
	virtual VVOID initializeEffectVariables();
			VVOID resetRenderTargetView(UBIGINT width, UBIGINT height);
	virtual VVOID releaseSystem();

			// Methods per frame.
	virtual VVOID update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity);
	virtual VVOID updateEffectVariables();
	virtual VVOID renderParticles();

			// Accessors.
	inline  BaseDXData    Base() const { return m_baseData;}
	inline  ID3D10Buffer* ParticleVB() const { return m_particleVBuffer;}
	inline  VVOID         SetBase(BaseDXData base) { m_baseData = base;}
	inline  VVOID         SetEffectPath(WSTRING effectPath) { m_sEffectPath = effectPath;}
	inline  VVOID         SetTexturePath(WSTRING texturePath) { m_sTexturePath = texturePath;}
	inline  VVOID         SetLightDir(Vector3 lightDir) { m_vLightDirection = lightDir;}
	inline  VVOID         SetParticleVB(ID3D10Buffer* partBuffer) { m_particleVBuffer = partBuffer;}
	};
}

#endif