#ifndef DEF_OCELOTPARTICLE_H
#define DEF_OCELOTPARTICLE_H

#include "..\DataStructures\DataStructureUtil.h"
#include "..\Maths\Vector3.h"

namespace Ocelot
{
	class OcelotParticle
	{
	private:

		// Attributes
		Vector3 m_vPosition;
		Vector3 m_vDirection;
		Vector3 m_vMass;
		UBIGINT m_iColor;
		FFLOAT  m_fRadius;
		FFLOAT  m_fLife;
		FFLOAT  m_fFade;
		FFLOAT  m_fRotAngle;
		FFLOAT  m_fRotStep;
		BBOOL   m_bVisible;

	public:

		// Constructor & Destructor
		OcelotParticle();
		OcelotParticle(Vector3 position, Vector3 direction, Vector3 mass, UBIGINT color, FFLOAT radius, FFLOAT life, FFLOAT rotAngle, FFLOAT rotStep, FFLOAT fade = 0.0f, BBOOL visibility = true);
		~OcelotParticle();

		// Methods


		// Accessors
		inline Vector3 Position() const { return m_vPosition;}
		inline Vector3 Direction() const { return m_vDirection;}
		inline Vector3 Mass() const { return m_vMass;}
		inline UBIGINT Color() const { return m_iColor;}
		inline FFLOAT  Radius() const { return m_fRadius;}
		inline FFLOAT  Life() const { return m_fLife;}
		inline FFLOAT  Fade() const { return m_fFade;}
		inline FFLOAT  RotAngle() const { return m_fRotAngle;}
		inline FFLOAT  RotStep() const { return m_fRotStep;}
		inline BBOOL   IsVisible() const { return m_bVisible;}
		inline VVOID   SetPosition(Vector3 position) { m_vPosition = position;}
		inline VVOID   SetDirection(Vector3 direction) { m_vDirection = direction;}
		inline VVOID   SetMass(Vector3 mass) { m_vMass = mass;}
		inline VVOID   SetColor(UBIGINT color) { m_iColor = color;}
		inline VVOID   SetRadius(FFLOAT radius) { m_fRadius = radius;}
		inline VVOID   SetLife(FFLOAT life) { m_fLife = life;}
		inline VVOID   SetFade(FFLOAT fade) { m_fFade = fade;}
		inline VVOID   SetRotAngle(FFLOAT rotAngle) { m_fRotAngle = rotAngle;}
		inline VVOID   SetRotStep(FFLOAT rotStep) { m_fRotStep = rotStep;}
		inline VVOID   SetVisible(BBOOL state) { m_bVisible = state;}
		inline VVOID   Visible() { m_bVisible = true;}
		inline VVOID   NotVisible() { m_bVisible = false;}
	};
}

#endif