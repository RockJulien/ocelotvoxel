#ifndef DEF_IOCELOTPARTICLESYSTEM_H
#define DEF_IOCELOTPARTICLESYSTEM_H

#include "OcelotParticle.h"

namespace Ocelot
{
				enum ParticleSystemType
				{
					PARTICULESYSTEM_TYPE_DEFAULT,
					PARTICULESYSTEM_TYPE_FIRE,
					PARTICULESYSTEM_TYPE_SMOKE,
					PARTICULESYSTEM_TYPE_WEATHER_RAIN,
					PARTICULESYSTEM_TYPE_WEATHER_SNOW,
					PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM,
					PARTICULESYSTEM_TYPE_EXPLOSION_STALK,
					PARTICULESYSTEM_TYPE_EXPLOSION_BURST,
					PARTICULESYSTEM_TYPE_EXPLOSION_MINE
				};

	class IOcelotParticleSystem
	{
	public:

			// Constructor & Destructor
			IOcelotParticleSystem(){}
	virtual ~IOcelotParticleSystem(){}

			// Methods to use once.
	virtual VVOID createSystem(UBIGINT particleCount, OcelotParticle* adressToStart)  = 0;
	virtual VVOID initialize()                = 0;

			// API dependent.
	virtual VVOID initializeLayout()          = 0;
	virtual VVOID initializeEffect()          = 0;
	virtual VVOID initializeVertices()        = 0;
	virtual VVOID initializeIndices()         = 0;
	virtual VVOID initializeEffectVariables() = 0;
	virtual VVOID releaseSystem()             = 0;

			// Methods per frame.
	virtual VVOID update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity) = 0;
	virtual VVOID updateEffectVariables()     = 0;
	virtual VVOID renderParticles()           = 0;
	};
}

#endif