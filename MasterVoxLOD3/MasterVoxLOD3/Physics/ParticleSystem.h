#ifndef DEF_PARTICLESYSTEM_H
#define DEF_PARTICLESYSTEM_H

#include "IOcelotParticleSystem.h"
#include "..\Maths\Color.h"

namespace Ocelot
{

						class ParticleSystemInfo
						{
						protected:

								// Attributes
								UBIGINT			   m_iStreamerCount;
								FFLOAT             m_fSpread;
								FFLOAT             m_fLifeSpan;
								FFLOAT             m_fFadeExponent;
								FFLOAT             m_fStartSize;
								FFLOAT             m_fEndSize;
								FFLOAT             m_fSizeExponent;
								FFLOAT             m_fStartSpeed;
								FFLOAT             m_fEndSpeed;
								FFLOAT             m_fSpeedExponent;
								FFLOAT             m_fSpeedVariance;
								FFLOAT             m_fRotAngle;    // roll amount.
								FFLOAT             m_fWindFalloff;

								Vector3            m_vEmitterPosition;
								Vector3            m_vPositionWeights;
								Vector3            m_vEmitterDirection;
								Vector3            m_vDirectionVariance;
								Vector3            m_vDirectionWeights;

								Color              m_cColor0;
								Color              m_cColor1;
								Color              m_cFlashColor;

						public:

								// Constructor & Destructor
								ParticleSystemInfo();
								ParticleSystemInfo(UBIGINT streamerCount, FFLOAT spread, FFLOAT lifeSpan, FFLOAT fadeExponent, FFLOAT startSize, FFLOAT endSize, 
												   FFLOAT sizeExponent, FFLOAT startSpeed, FFLOAT endSpeed, FFLOAT speedExponent, FFLOAT speedVariance, FFLOAT rotAngle, 
												   FFLOAT windFalloff, Vector3 emitterPosition, Vector3 positionWeights, Vector3 emitterDirection, Vector3 directionVariance, 
												   Vector3 directionWeights, Color color0, Color color1, Color flashColor);
						virtual ~ParticleSystemInfo();

								// Accessors
						inline  UBIGINT	 StreamerCount() const { return m_iStreamerCount;}
						inline  FFLOAT   Spread() const { return m_fSpread;}
						inline  FFLOAT   LifeSpan() const { return m_fLifeSpan;}
						inline  FFLOAT   FadeExponent() const { return m_fFadeExponent;}
						inline  FFLOAT   StartSize() const { return m_fStartSize;}
						inline  FFLOAT   EndSize() const { return m_fEndSize;}
						inline  FFLOAT   SizeExponent() const { return m_fSizeExponent;}
						inline  FFLOAT   StartSpeed() const { return m_fStartSpeed;}
						inline  FFLOAT   EndSpeed() const { return m_fEndSpeed;}
						inline  FFLOAT   SpeedExponent() const { return m_fSpeedExponent;}
						inline  FFLOAT   SpeedVariance() const { return m_fSpeedVariance;}
						inline  FFLOAT   RotAngle() const { return m_fRotAngle;}
						inline  FFLOAT   WindFalloff() const { return m_fWindFalloff;}
						inline  Vector3  Position() const { return m_vEmitterPosition;}
						inline  Vector3  PosWeights() const { return m_vPositionWeights;}
						inline  Vector3  Direction() const { return m_vEmitterDirection;}
						inline  Vector3  DirVariance() const { return m_vDirectionVariance;}
						inline  Vector3  DirWeights() const { return m_vDirectionWeights;}
						inline  Color    Color0() const { return m_cColor0;}
						inline  Color    Color1() const { return m_cColor1;}
						inline  Color    FlashColor() const { return m_cFlashColor;}
						inline  VVOID	 SetStartSpeed(FFLOAT startSpeed) { m_fStartSpeed = startSpeed;}
						inline  VVOID	 SetEndSpeed(FFLOAT endSpeed) { m_fEndSpeed = endSpeed;}
						inline  VVOID	 SetSpeedExponent(FFLOAT speedExponent) { m_fSpeedExponent = speedExponent;}
						inline  VVOID	 SetSpeedVariance(FFLOAT speedVariance) { m_fSpeedVariance = speedVariance;}
						inline  VVOID	 SetRotAngle(FFLOAT rotAngle) { m_fRotAngle = rotAngle;}
						inline  VVOID	 SetWindFalloff(FFLOAT windFalloff) { m_fWindFalloff = windFalloff;}
						inline  VVOID	 SetPosition(Vector3 position) { m_vEmitterPosition = position;}
						inline  VVOID	 SetPosWeights(Vector3 weights) { m_vPositionWeights = weights;}
						inline  VVOID	 SetDirection(Vector3 direction) { m_vEmitterDirection = direction;}
						inline  VVOID	 SetDirVariance(Vector3 variance) { m_vDirectionVariance = variance;}
						inline  VVOID	 SetDirWeights(Vector3 weights) { m_vDirectionWeights = weights;}
						inline  VVOID	 SetColor0(Color color) { m_cColor0 = color;}
						inline  VVOID	 SetColor1(Color color) { m_cColor1 = color;}
						inline  VVOID    SetFlashColor(Color flashColor) { m_cFlashColor = flashColor;}
						};

	class ParticleSystem : public IOcelotParticleSystem
	{
	protected:

			// Attributes
			ParticleSystemType m_iType;
			ParticleSystemInfo m_info;
			OcelotParticle*    m_particleBuffer;
			UBIGINT*           m_particleIndices;
			FFLOAT*            m_particleDepth;
			UBIGINT            m_iParticleCount;
			FFLOAT             m_fStartTime;
			FFLOAT             m_fCurrentTime;
			BBOOL              m_bOnFrustum;
			BBOOL              m_bRunning;

	public:

			// Constructor & Destructor
			ParticleSystem();
			ParticleSystem(ParticleSystemInfo info);
	virtual ~ParticleSystem();

			// Methods to use once.
	virtual VVOID createSystem(UBIGINT particleCount, OcelotParticle* adressToStart);
	virtual VVOID initialize();

			// API dependent.
	virtual VVOID initializeLayout();
	virtual VVOID initializeEffect();
	virtual VVOID initializeVertices();
	virtual VVOID initializeIndices();
	virtual VVOID initializeEffectVariables();
	virtual VVOID releaseSystem();

			// Methods per frame.
	virtual VVOID update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity);
	virtual VVOID updateEffectVariables();
	virtual VVOID renderParticles();

			// Accessors
	inline  ParticleSystemInfo Info() const { return m_info;}
	inline  FFLOAT             CurrentTime() const { return m_fCurrentTime;}
	inline  UBIGINT            ParticleCount() const { return m_iParticleCount;}
	inline  BBOOL              IsInFrustum() const { return m_bOnFrustum;}
	inline  ParticleSystemType Type() const { return m_iType;}
	inline  VVOID              SetInfo(ParticleSystemInfo info) { m_info = info;}
	inline  VVOID              SetStartTime(FFLOAT startTime) { m_fStartTime = startTime;}
	inline  VVOID              SetStartSpeed(FFLOAT startSpeed) { m_info.SetStartSpeed(startSpeed);}
	inline  VVOID              SetFlashColor(Color flashColor) { m_info.SetFlashColor(flashColor);}
	inline  VVOID              SetPosition(Vector3 position) { m_info.SetPosition(position);}
	inline  VVOID              SetInFructum(BBOOL state) { m_bOnFrustum = state;}
	inline  VVOID              InFrustum() { m_bOnFrustum = true;}
	inline  VVOID              NotInFrustum() { m_bOnFrustum = false;}
	};
}

#endif