#ifndef DEF_OCELOTPARTICLESYSTEMFACTORY_H
#define DEF_OCELOTPARTICLESYSTEMFACTORY_H

#include "MushroomParticleSystem.h"
#include "StalkParticleSystem.h"
#include "BurstParticuleSystem.h"
#include "MineParticleSystem.h"
#include "..\DataStructures\Linked2List.h"

namespace Ocelot
{

								struct BaseParticleSystem
								{
									BaseParticleSystem() : 
									m_baseData(), m_sTexturePath(L""), m_sEffecPath(L""), m_vLightDir(1.0f) {}

									BaseDXData m_baseData;
									WSTRING    m_sTexturePath;
									WSTRING    m_sEffecPath;
									Vector3    m_vLightDir;
								};

	class OcelotParticleSystemFactory
	{
	private:

			// Attributes
			BaseDXData							 m_baseData;
			WSTRING								 m_sEffectPath;
			WSTRING								 m_sTexturePath;
			Vector3								 m_vLightDirection;
			FFLOAT                               m_fTime;
			UBIGINT								 m_iMaxParticles;
			UBIGINT                              m_iParticleCountUsed;
			Array1D<OcelotParticle>				 m_globalParticleBuffer;
			Array1D<UBIGINT>					 m_iGlobalParticleIndices;
			Array1D<FFLOAT>						 m_fGlobalParticleDepth;
			Linked2List<OcelotDXParticleSystem*> m_particleSystems;
			BBOOL                                m_bInitialized;

			// Global Effect variables.
			ID3D10EffectMatrixVariable*			 m_fxWorld;
			ID3D10EffectMatrixVariable*			 m_fxView;
			ID3D10EffectMatrixVariable*			 m_fxProj;
			ID3D10EffectMatrixVariable*			 m_fxInvViewProj;
			ID3D10EffectScalarVariable*			 m_fxTime;
			ID3D10EffectTechnique*				 m_fxTechParticleToBuffer;
			ID3D10EffectTechnique*				 m_fxTechParticleToScene;
			ID3D10EffectVariable*				 m_fxLightDir;
			ID3D10EffectVariable*				 m_fxEyePosition;
			ID3D10EffectVariable*				 m_fxForward;
			ID3D10EffectVariable*				 m_fxRight;
			ID3D10EffectVariable*				 m_fxUp;

			// Global Glow light variables.
			ID3D10EffectVariable*				 m_fxPosIntensity;
			ID3D10EffectVariable*				 m_fxColor;
			ID3D10EffectVariable*				 m_fxAttenuation;

			// Global Geometry variables.
			ID3D10InputLayout*					 m_particleLayout;
			ID3D10InputLayout*					 m_screenLayout;      
			ID3D10Effect*						 m_effect;
			ID3D10Buffer*						 m_particleVBuffer;
			ID3D10Buffer*						 m_screenQuadVBuffer;

			// Global Render target view variable.
			ID3D10RenderTargetView*				 m_offScreenParticleRTV;
			ID3D10RenderTargetView*				 m_offScreenParticleColorRTV;

			// Global Texture & Shader Resource View.
			ID3D10Texture2D*					 m_offscreenParticleTex;
			ID3D10ShaderResourceView*			 m_offscreenParticleSRV;
			ID3D10Texture2D*					 m_offscreenParticleColorTex;
			ID3D10ShaderResourceView*			 m_offscreenParticleColorSRV;
			ID3D10ShaderResourceView*			 m_rvParticleTexture;

			// Global Effect Shader Resource variables.
			ID3D10EffectShaderResourceVariable*  m_fxParticleSRV;
			ID3D10EffectShaderResourceVariable*  m_fxParticleColorSRV;

			// Private Methods
			VVOID initializeEffect();			  // Global
			VVOID initializeLayout();			  // Global
			VVOID initializeVertices();			  // Global
			VVOID updateEffectVariables();		  // Global
			VVOID initializeEffectVariables();    // Global
			VVOID renderParticlesIntoBuffer();    // Global
			VVOID compositeParticlesIntoScene();  // Global
			VVOID copyParticlesToVBuffer(ParticleVertex* vertBuffer, Vector3 side, Vector3 up); // Global

			// Private Constructor, copy & Assgmt (not allowed).
			OcelotParticleSystemFactory(const OcelotParticleSystemFactory&);
			OcelotParticleSystemFactory& operator = (const OcelotParticleSystemFactory&);

	public:

			// Destructor
			OcelotParticleSystemFactory();
			~OcelotParticleSystemFactory();

			// Methods
			VVOID                   initializeGlobalParticleSystemManager(BaseParticleSystem neededGlob,UBIGINT maxParticles);
			VVOID                   update(FFLOAT deltaTime, FFLOAT time, Vector3 right, Vector3 windVel, Vector3 gravity);
			VVOID                   renderParticle();
			OcelotDXParticleSystem* addSystem(ParticleSystemType type, Vector3 position, UBIGINT particleCount, BaseParticleSystem neededForIndependent = BaseParticleSystem(), BBOOL independent = false);
			VVOID                   modifySystem(OcelotDXParticleSystemInfo newInfo, UBIGINT index);
			VVOID                   changePosition(Vector3 newPosition, UBIGINT index);
			VVOID					resetRenderTargetView(UBIGINT width, UBIGINT height);
			VVOID                   releaseSystems();

			// Accessors
			inline  BBOOL isInitialized() const { return m_bInitialized;}
			inline  VVOID SetLightDir(Vector3 lightDir) { m_vLightDirection = lightDir;}
	};
}

#endif