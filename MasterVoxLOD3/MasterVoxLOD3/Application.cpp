#include "Application.h"

using namespace Ocelot;

BIGINT WINAPI WinMain( HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPSTR     lpCmdLine,
					   BIGINT    nShowCmd )
{
	Application newApplication;

	newApplication.initApp(hInstance, SW_HIDE, L"MasterVoxLOD: ");

#if defined (DEBUG) || defined (_DEBUG)
	// Tells to send mem leaks info to a separate window.
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
#endif

	return newApplication.run();
}

Application::Application() :
DXOcelotApp(), m_wireFrameMode(NULL), m_camera(NULL), m_controller(NULL), m_vOldMousePos(0.0f), m_fElapsedTime(0.0f), m_bBounds(false), 
m_bWireFrame(false), m_visitorLight(NULL), m_terrainInfo(NULL), m_cameraInfo(NULL), m_bRefreshScale(false), m_bRefreshPosition(false), m_bRefreshDirection(false),
m_particleBuilder(NULL), m_terrain(NULL), m_terrainMaterial(NULL), m_terrMatPalette(NULL), m_test(NULL)
{
	
}

Application::~Application()
{
	// Destroy meshes.
	// Function added to LinkedList class especially for
	// releasing pointer object entirely even with multiple inheritance
	// no matter which class of this cascaded amount of inherited
	// class you store in it. (NOTE: a division occurs when storing 
	// an object thanks to a mid-inherited class, and looping
	// and deleting each element by accessing them like ABOVE will let 
	// Base class element under the class stored and corrupt the VTable
	// causing mem leaks.
	m_meshes.clear();

	// Destroy particle Systems.
	/*if(m_particles.Size() != 0)
	{
		List2Iterator<OcelotDXParticleSystem*> iter2 = m_particles.iterator();
		for(iter2.begin(); iter2.notEnd(); iter2.next())
		{
			OcelotDXParticleSystem* temp = iter2.data();
			DeletePointer(temp);
		}
	}*/

	if(AudioMng)
		AudioMng->release();

#if defined (DEBUG) || defined (_DEBUG)
	// Check for memory leaks when destroying the application.
	_CrtDumpMemoryLeaks();
#endif

}

BIGINT  Application::run()
{
	// the main loop
	MSG msg = {0};

	m_timer.resetTimer();

	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))      
		{												  

			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc
			DispatchMessage(&msg);
		}
		else
		{
			if(m_bPaused)
			{
				Sleep(50);
			}
			else
			{
				m_timer.tick();
				update(m_timer.getDeltaTime());
				renderFrame();
			}
		}
	}

	// clean up DirectX COM
	clean3DDevice();

	return msg.wParam;
}

VVOID   Application::update(FFLOAT deltaTime)
{
	// update the base class.
	DXOcelotApp::update(deltaTime);
	m_fElapsedTime = deltaTime;

	// update the camera.
	if(m_controller)
		m_controller->update(deltaTime);

	// update light if needed.
	// if point light??
	/*FFLOAT radius = 50.0f;
	Vector3 moves;
	moves.x(radius * cosf(m_timer.getGameTime()));
	moves.y(80.0f);
	moves.z(radius * sinf(m_timer.getGameTime()));
	m_light->m_vPosition = moves;*/

	// if spot light??
	if(m_visitorLight)
	{
		m_visitorLight->m_vPosition  = m_camera->Info().Eye();
		m_visitorLight->m_vDirection = m_camera->Info().LookAt();
	}

	// handle inputs.
	handleInput();

	// Update the audio component
	HR(AudioMng->update(deltaTime));

	for(int lCounter = 0; lCounter < AudioMng->Device().size(); lCounter++)
	{
		AudioMng->Device()[lCounter]->setListener(m_camera->Info().Eye(), Vector3(0.0f), Vector3(0.0f), Vector3(0.0f));
	}

	// update meshes if any after inputs
	// which will influence meshes pos.
	List2Iterator<IOcelotMesh*> iter = m_meshes.iterator();
	for(iter.begin(); iter.notEnd(); iter.next())
		iter.data()->update(deltaTime);

	// Update the terrain.
	if(m_terrain)
		m_terrain->update(deltaTime);

	if(m_test)
		m_test->update(deltaTime);

	// Grab the camera's viewmatrix.
	Matrix4x4 view = m_camera->View();

	// Compute the side vector.
	Vector3 side(view.get_11(), view.get_21(), view.get_31());
	side.Vec3Normalise();

	if(m_particleBuilder)
		m_particleBuilder->update(deltaTime, 0.0f, side, Vector3(-2.0f, 10.0f, 0.0f), Vector3(0.0f, -9.8f, 0.0f));
}

VVOID   Application::renderFrame()
{
	// render base elements.
	DXOcelotApp::renderFrame();

	// if wireframe mode is active for terrain.
	if (m_bWireFrame)
		m_device->RSSetState(m_wireFrameMode);

	// Reset the Depth Stencil and Blend State.
	m_device->OMSetDepthStencilState(0, 0);
	FFLOAT blendFactor[] = {0.0f, 0.0f, 0.0f, 0.0f};
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	// render meshes if any.
	List2Iterator<IOcelotMesh*> iter = m_meshes.iterator();
	for(iter.begin(); iter.notEnd(); iter.next())
		HR(iter.data()->renderMesh());

	// Reset the Blend State.
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	// Render the Terrain.
	if(m_terrain)
		m_terrain->render();

	// swap with the back buffer and sync at the monitors refresh rate (60)
	if(m_particleBuilder)
		m_particleBuilder->renderParticle();

	// Reset the Blend State.
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	if(m_test)
		m_test->renderMesh();

	refreshTerrainInfo();
	refreshCameraInfo();

	// Render Texts
	// Terrain Info At the higher left corner (shift of 10(X)/10(Y) from borders).
	if(m_terrainInfo)
		m_terrainInfo->Render(10, 10, m_iWindowWidth, m_iWindowHeight);

	// Camera Info At the lower right corner (400 = size of my longest line (Adjust it)).
	// windowHeight as topPos in this OcelotText DrawMulti mode using Sprite will wrap
	// text regarding to the number of line and the window's height.
	if(m_cameraInfo)
		m_cameraInfo->Render(m_iWindowWidth - 400, m_iWindowHeight, m_iWindowWidth, m_iWindowHeight);

	m_swapChain->Present(1, 0);
}

VVOID   Application::initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPWSTR windowName)
{
#if defined (DEBUG) || defined (_DEBUG)
	//_CrtSetBreakAlloc(17698);
#endif

	// Init the Application Base.
	DXOcelotApp::initApp(hInstance, nShowCmd, windowName);

	// Init the Rasterizer for wireframe render mode.
	D3D10_RASTERIZER_DESC rsDesc;
	ZeroMemory(&rsDesc, sizeof(D3D10_RASTERIZER_DESC));
	rsDesc.FillMode = D3D10_FILL_WIREFRAME;
	rsDesc.CullMode = D3D10_CULL_BACK;
    rsDesc.FrontCounterClockwise = false;
	m_device->CreateRasterizerState(&rsDesc, &m_wireFrameMode);

	// Initialize the camera.
	OcelotCameraInfo camInfo(Vector3(0.0f, 110.0f, -300.0f), Vector3(0.0f, 0.0f, 1.0f), (FFLOAT)m_iWindowWidth/m_iWindowHeight, 2000.0f);
	m_camera    = new OcelotCamera(camInfo);

	// And create the controller
	m_controller = ControlFact->createController(m_camera, CAMERA_CONTROLLER_FLYER, 10.0f);

	// init the audio device.
	// for now, add whatever song you wnat here. just the short name
	// by putting them first in the Resources folder.
	AudioMng->initialize(m_hAppInstance);

	// Initialize meshes if any.
	CubeMapInfo CMInfo(m_device, m_camera, 10000, L".\\Resources\\MyHaloMap.dds", L".\\Effects\\CubeMap.fx");
	m_meshes.putBACK(new CubeMap(CMInfo));
	
	// Create the light                               // dir                      // atten                    // ambient              // diffuse              // spec         // range // spotPow // specPow
	m_light = new OcelotLight(m_camera->Info().Eye(), Vector3(0.0f, 0.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f), OcelotColor::DARKGREY, OcelotColor::LIGHTGREY, OcelotColor::GREY, 1000.0f, 64.0f, 1.0f);        // Global point light
	m_visitorLight = new OcelotLight(m_camera->Info().Eye(), Vector3(0.0f, 0.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f), OcelotColor::DARKGREY, OcelotColor::LIGHTGREY, OcelotColor::GREY, 100.0f, 100.0f, 1.0f); // Player spot light (maglight :-)).

	// Create the fog.
	m_fog = new OcelotFog(5.0f, 140.0f);

	// Just some bump mapped examples...
	/*CubeInfo cubeInfo(m_device, m_camera, L".\\Resources\\StealWoodBox.jpg", L".\\Effects\\BumpMapping.fx", m_light, m_fog, Vector3(0.0f, 80.0f, 10.0f), Vector3(20.0f));
	m_meshes.putBACK(new Cube(cubeInfo));

	CylinderInfo CI(m_device, m_camera, L".\\Resources\\WoodBoxDiag.jpg", L".\\Effects\\BumpMapping.fx", m_light, m_fog, Vector3(30.0f, 80.0f, 10.0f), 4, 30, 20, 20);
	m_meshes.putBACK(new Cylinder(CI));

	PyraCylinderInfo PCI(m_device, m_camera, L".\\Resources\\BrickWall.jpg", L".\\Effects\\BumpMapping.fx", m_light, m_fog, Vector3(-35.0f, 80.0f, 10.0f), 2, 10, 30, 20, 20);
	m_meshes.putBACK(new PyraCylinder(PCI));

	SphereInfo SI(m_device, m_camera, L".\\Resources\\MetallicWall.jpg", L".\\Effects\\BumpMapping.fx", m_light, m_fog, Vector3(0.0f, 102.0f, -10.0f));
	m_meshes.putBACK(new Sphere(SI));*/

	List2Iterator<IOcelotMesh*> iter = m_meshes.iterator();
	for(iter.begin(); iter.notEnd(); iter.next())
		HR(iter.data()->createInstance());

	// Initialize audio components if any.

	// Initialize particle system(s).
	/*Vector3 emitterPos(0.0f, 80.0f, -10.0f);
	Vector3 emitterDir(0.0f);
	Color   color0(OcelotColor::DARKGREY);
	Color   color1(OcelotColor::BLACK);
	Color flashCol(OcelotColor::REDORANGE);
	/*OcelotDXParticleSystemInfo partSysInfo(m_device, m_camera, L".\\Resources\\DeferredParticle.dds", L".\\Effects\\DeferredParticleSystem.fx", m_light->m_vDirection, 
										   1, 10.0f, 20.0f, 2.0f, 0.0f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
										   254.0f, 20.0f, 4.0f, 16.0f, 0.0f, 0.1f, 15.0f, emitterPos, Vector3(0.5f, 1.0f, 0.5f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
										   emitterDir, Vector3(0.0f), Vector3(1.0f, 2.0f, 1.0f), color0, color1, flashCol); // emitDir / dirVar / dirWeights / color0 / color1 / flashCol.
	OcelotDXParticleSystem* myMushroom = new MushroomParticleSystem(partSysInfo);
	OcelotParticle* part = new OcelotParticle[200];
	myMushroom->createSystem(200, part);
	m_particles.putBACK(myMushroom);

	OcelotDXParticleSystemInfo stalkInfo(m_device, m_camera, L".\\Resources\\DeferredParticle.dds", L".\\Effects\\DeferredParticleSystem.fx", m_light->m_vDirection, 
										 1, 15.0f, 20.0f, 8.0f, 0.0f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
										 128.0f, 50.0f, -1.0f, 16.0f, 0.0f, 0.2f, 15.0f, emitterPos, Vector3(2.0f, 0.1f, 2.0f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
										 emitterDir, Vector3(0.0f), Vector3(2.0f, 0.1f, 2.0f), color0, color1, flashCol);
	OcelotDXParticleSystem* myStalk    = new StalkParticleSystem(stalkInfo);
	OcelotParticle* part2 = new OcelotParticle[200];
	myStalk->createSystem(200, part2);
	m_particles.putBACK(myStalk);

	emitterPos = Vector3(50.0f, 80.0f, 10.0f);
	emitterDir = Vector3(0.0f, 0.5f, 0.0f);
	OcelotDXParticleSystemInfo burstInfo(m_device, m_camera, L".\\Resources\\DeferredParticle.dds", L".\\Effects\\DeferredParticleSystem.fx", m_light->m_vDirection, 
										 20, 1.0f, 9.0f, 4.0f, 0.5f, 15.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
										 1.0f, 100.0f, 4.0f, 4.0f, 100.0f, 0.2f, 1.0f, emitterPos, Vector3(0.5f, 1.0f, 0.5f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
										 emitterDir, Vector3(1.0f, 0.5f, 1.0f), Vector3(1.0f, 1.5f, 1.0f), color0, color1, flashCol);
	OcelotDXParticleSystem* myBurst = new BurstParticleSystem(burstInfo);
	OcelotParticle* part3 = new OcelotParticle[200];
	myBurst->createSystem(200, part3);
	m_particles.putBACK(myBurst);

	/*emitterPos = Vector3(-50.0f, 80.0f, 10.0f);
	emitterDir = Vector3(0.0f, 0.8f, 0.0f);
	OcelotDXParticleSystemInfo mineInfo(m_device, m_camera, L".\\Resources\\DeferredParticle.dds", L".\\Effects\\DeferredParticleSystem.fx", m_light->m_vDirection, 
										0, 1.0f, 9.0f, 4.0f, 2.0f, 60.0f, // streamCount / spread / lifeSpan / fadeExp / startSize / endSize
										1.0f, 250.0f, 4.0f, 2.0f, 80.0f, 0.2f, 4.0f, emitterPos, Vector3(1.0f, 1.0f, 1.0f), // sizeExp / startSpeed / endSpeed / speedExp / speedVar / rotAngle / windFalloff / emitPos / posWeights
										emitterDir, Vector3(0.5f, 0.2f, 0.5f), Vector3(2.0f, 4.0f, 2.0f), color0, color1, flashCol);
	OcelotDXParticleSystem* myMine = new MineParticleSystem(mineInfo);
	OcelotParticle* part4 = new OcelotParticle[200];
	myMine->createSystem(200, part4);
	m_particles.putBACK(myMine);*/

	// Show the window once everything is inilialized.
	BaseParticleSystem base;
	base.m_baseData.SetDevice(m_device);
	base.m_baseData.SetCamera(m_camera);
	base.m_sEffecPath   = L".\\Effects\\DeferredParticleSystem.fx";
	base.m_sTexturePath = L".\\Resources\\DeferredParticle.dds";
	base.m_vLightDir    = m_light->m_vDirection;
	m_particleBuilder   = new OcelotParticleSystemFactory();

	UBIGINT maxParticle = 500;
	m_particleBuilder->initializeGlobalParticleSystemManager(base, maxParticle);
	UBIGINT partPerSys = maxParticle / 4;
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(-5.0f, 120.0f, -180.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(-5.0f, 120.0f, -180.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(70.0f, 120.0f, -180.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-70.0f, 120.0f, -180.0f), partPerSys);
	/*m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(0.0f, 80.0f, 20.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(0.0f, 70.0f, 20.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(50.0f, 80.0f, 20.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-50.0f, 80.0f, 20.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(70.0f, 80.0f, -30.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(70.0f, 70.0f, -30.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(90.0f, 80.0f, -30.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-90.0f, 80.0f, -30.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(100.0f, 80.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(100.0f, 70.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(150.0f, 80.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-150.0f, 80.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(0.0f, 80.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(0.0f, 70.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(50.0f, 80.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-50.0f, 80.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(100.0f, 80.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(100.0f, 70.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(150.0f, 80.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-150.0f, 80.0f, 150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(300.0f, 80.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(300.0f, 70.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(350.0f, 80.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-350.0f, 80.0f, -10.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(300.0f, 80.0f, -150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(300.0f, 70.0f, -150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(350.0f, 80.0f, -150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-350.0f, 80.0f, -150.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(0.0f, 80.0f, -300.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(0.0f, 70.0f, -300.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(50.0f, 80.0f, -300.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-50.0f, 80.0f, -300.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MUSHROOM, Vector3(0.0f, 80.0f, -400.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_STALK, Vector3(0.0f, 70.0f, -400.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_BURST, Vector3(50.0f, 80.0f, -400.0f), partPerSys);
	m_particleBuilder->addSystem(PARTICULESYSTEM_TYPE_EXPLOSION_MINE, Vector3(-50.0f, 80.0f, -400.0f), partPerSys);*/

	// Voxel Terrain Test.
	WSTRING defTextPath[3];
	defTextPath[0] = L".\\Resources\\Dirt.jpg";
	defTextPath[1] = L".\\Resources\\Grass.jpg";
	defTextPath[2] = L".\\Resources\\Rock.jpg";
	// Create common textures for saving memory (will be the same).
	m_terrainMaterial = new TriPlanarBumpMaterial();
	//m_terrMatPalette = new TriPlanarTexturePalette();
	//m_terrMatPalette->SetPalette(loadRV(m_device, L".\\Resources\\4096x4096.jpg"));
	OcelotLight* lightTab[2];
	lightTab[0] = m_light;
	lightTab[1] = m_visitorLight;
	loadTriPlanarMaterial(m_device, defTextPath, *m_terrainMaterial);
	DiscreteVoxelMeshLODInfo LODInfo(16, 16, 16, 1.0f);   // a 16x16x16 block size and a scale of 1.0f.
	OcelotVoxelMeshInfo TerrainInfo(LODInfo, m_device, m_camera, m_terrainMaterial, m_terrMatPalette, L".\\Effects\\TriPlanarBumpMapping.fx", lightTab/*m_light*/, m_fog);
	m_terrain = new OcelotTerrainManager(TerrainInfo, 3, 30.0f); // LOD count = 3. (put whatever count you want here).
	m_terrain->loadEdits("TerrainEdits.vox");

	//DiscreteVoxelMeshLODInfo LODInfo2(16, 16, 16, 1.0f);
	//SimleLODMeshInfo simpleInfo(LODInfo2, m_device, m_camera, m_terrainMaterial, m_terrMatPalette, L".\\Effects\\TriPlanarBumpMapping.fx", lightTab/*m_light*/, m_fog);
	//m_test = new SimpleLODExample(simpleInfo, 3);

	// Text Information.
	OcelotDXTextInfo textInfo(m_device, m_camera, OcelotColor::CHOCOLATE, 20);
	/*m_terrainInfo = new OcelotDXText(textInfo, TEXT_TYPE_DRAW_SINGLE);
	WSTRING lodCount;
	WSTRING editScale;
	toWString(m_terrain->GetLODCount(), lodCount);
	toWString(m_terrain->GetEditScale(), editScale);
	m_terrainInfo->addLine(L" ------------- Terrain Information ------------- ");
	m_terrainInfo->addLine(L"LODs Count: " + lodCount);
	m_terrainInfo->addLine(L"Edit Scale: " + editScale);*/
	// Change color for camera's text block.
	textInfo.SetColor(OcelotColor::LIME);
	m_cameraInfo = new OcelotDXText(textInfo, TEXT_TYPE_DRAW_MULTI); // NOTE: Draw Multi will use Sprite for drawing lines and let lines independent to each other (Means that color could change).
	WSTRING camPosit;
	WSTRING camDirec;
	toWString(m_camera->Info().Eye(), camPosit);
	toWString(m_camera->Info().LookAt(), camDirec);
	m_cameraInfo->addLine(L" ---------------- Camera Information ---------------- ");
	m_cameraInfo->addLine(L"Position: " + camPosit);
	m_cameraInfo->addLine(L"Direction: " + camDirec);

	ShowWindow(m_mainWindowInstance, SW_SHOW);

	HR(AudioMng->createSound(L"BackGroundSoundtrack.wav", false, true)); // sound name / 3D sound false / sound looped true.
	HR(AudioMng->createSound(L"TopFairy.wav", false, true)); // sound name / 3D sound false / sound looped true.
	AudioMng->stopASound(L"BackGroundSoundtrack.wav");
	AudioMng->stopASound(L"TopFairy.wav");
}

VVOID   Application::handleInput()
{
	// Inputs handled in the application.
	if(InMng->isPressedAndLocked(OCELOT_W))
		m_bWireFrame = !m_bWireFrame;

	if(InMng->isPressedAndLocked(OCELOT_NPPLUS) || InMng->isPressedAndLocked(OCELOT_NPMINUS))
		RefreshScale();

	if(InMng->isPressed(Keyboard, OCELOT_UP) || InMng->isPressed(Keyboard, OCELOT_DOWN) || InMng->isPressed(Keyboard, OCELOT_RIGHT) || InMng->isPressed(Keyboard, OCELOT_LEFT))
		RefreshPosition();

	// Change music.
	if(InMng->isPressedAndLocked(OCELOT_1))
	{
		HR(AudioMng->startASound(L"BackGroundSoundtrack.wav"));
		HR(AudioMng->restartASound(L"BackGroundSoundtrack.wav"));
		AudioMng->stopASound(L"TopFairy.wav");
	}

	// Change music
	if(InMng->isPressedAndLocked(OCELOT_2))
	{
		HR(AudioMng->startASound(L"TopFairy.wav"));
		HR(AudioMng->restartASound(L"TopFairy.wav"));
		AudioMng->stopASound(L"BackGroundSoundtrack.wav");
	}

	// Stop every music.
	if(InMng->isPressedAndLocked(OCELOT_3))
	{
		AudioMng->stopASound(L"TopFairy.wav");
		AudioMng->stopASound(L"BackGroundSoundtrack.wav");
	}

}

VVOID   Application::clean3DDevice()
{
	// Destroy every DirectX components.
	DXOcelotApp::clean3DDevice();
	DeletePointer(m_camera);
	DeletePointer(m_controller);
	DeletePointer(m_light);
	DeletePointer(m_visitorLight);
	DeletePointer(m_fog);
	DeletePointer(m_particleBuilder);
	DeletePointer(m_terrain);
	if(m_terrainMaterial)
		m_terrainMaterial->releaseCOM();
	DeletePointer(m_terrainMaterial);
	if(m_terrMatPalette)
		m_terrMatPalette->releaseCOM();
	DeletePointer(m_terrMatPalette);
	DeletePointer(m_test);
	DeletePointer(m_terrainInfo);
	DeletePointer(m_cameraInfo);
}

VVOID   Application::resize()
{
	// Resize the window and back buffer(s).
	DXOcelotApp::resize();

	// Reset with new size the Camera's aspect Ratio.
	if(m_camera)
	{
		OcelotCameraInfo tempInfo = m_camera->Info();
		FFLOAT aspectRatio = static_cast<FFLOAT>(m_iWindowWidth)/m_iWindowHeight;
		tempInfo.SetAspect(aspectRatio);
		m_camera->SetInfo(tempInfo);
	}

	/*List2Iterator<OcelotDXParticleSystem*> iter = m_particles.iterator();
	for(iter.begin(); iter.notEnd(); iter.next())
		iter.data()->resetRenderTargetView(m_iWindowWidth, m_iWindowHeight);*/
	if(m_particleBuilder)
		m_particleBuilder->resetRenderTargetView(m_iWindowWidth, m_iWindowHeight);
}

LRESULT Application::AppProc(UBIGINT message,WPARAM wParam,LPARAM lParam)
{
	// add extra button behaviour here
	Vector2 mousePosition;

	float dx = 0.0f;
	float dy = 0.0f;

	switch(message)
	{
		case WM_LBUTTONDOWN:
			if(wParam & MK_LBUTTON)
			{
				SetCapture(m_mainWindowInstance);

				m_vOldMousePos.x((float)LOWORD(lParam));
				m_vOldMousePos.y((float)HIWORD(lParam));
			}
			return 0;
			break;

		case WM_LBUTTONUP:
			ReleaseCapture();
			return 0;
			break;

		case WM_MOUSEMOVE:
			if(wParam & MK_LBUTTON)
			{
				mousePosition.x((float)LOWORD(lParam)); 
				mousePosition.y((float)HIWORD(lParam)); 

				dx = mousePosition.getX() - m_vOldMousePos.getX();
				dy = mousePosition.getY() - m_vOldMousePos.getY();

				// add extra camera controler stuff here
				m_controller->pitch( dy * 0.0087266f );
				m_controller->yaw( dx * 0.0087266f );

				// refresh camera direction info.
				RefreshDirection();
			
				m_vOldMousePos = mousePosition;
			}
			return 0;
	}

	// then pass to the basic Proc.
	return DXOcelotApp::AppProc(message, wParam, lParam);
}

VVOID   Application::refreshCameraInfo()
{
	if(m_bRefreshPosition)
	{
		WSTRING camPosit;
		toWString(m_camera->Info().Eye(), camPosit);
		m_cameraInfo->refresh(L"Position: " + camPosit, 1);
		DontRefreshPosition();
	}

	if(m_bRefreshDirection)
	{
		WSTRING camDirec;
		toWString(m_camera->Info().LookAt(), camDirec);
		m_cameraInfo->refresh(L"Direction: " + camDirec, 2);
		DontRefreshDirection();
	}
}

VVOID   Application::refreshTerrainInfo()
{
	if(m_bRefreshScale && m_terrain)
	{
		WSTRING editScale;
		toWString(m_terrain->GetEditScale(), editScale);
		m_terrainInfo->refresh(L"Edit Scale: " + editScale, 2);
		DontRefreshScale();
	}
}
