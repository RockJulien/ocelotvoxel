#ifndef DEF_STRINGHELPERS_H
#define DEF_STRINGHELPERS_H

/************************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   STRINGHelpers.h/.cpp
/
/ Description: This file provide a way to create convert mathematical structs
/              or scalar values into strings and strings into mathematical 
/              structs or scalar values. Can be used for extracting info from
/              .txt files or .xml for example.
/
/ Information: Portions of the code are:
/			   Copyright (C) 2005-2007 Feeling Software Inc.
/			   under MIT License: http://www.opensource.org/licenses/mit-license.php
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        10/09/2012
/************************************************************************************/

#include "STRINGUtil.h"
#include "..\Maths\Vector2.h"
#include "..\Maths\Matrix4x4.h"

#ifndef UINTMAX
#define UINTMAX 4294967295
#endif

// Some useful function using strings.

// Split Helpers
inline VVOID splitWithBlanks(const STRING& toSplit, LSSTRING& result);
inline VVOID splitWithBlanks(const WSTRING& toSplit, LWSTRING& result);
inline VVOID splitWithDelimiter(const STRING& toSplit, LSSTRING& result, const CCHAR& delimiter);
inline VVOID splitWithDelimiter(const WSTRING& toSplit, LWSTRING& result, const CCHAR& delimiter);
inline VVOID eraseBlanks(STRING& result);

//--------------------------- Convert Helpers from string to scalar----------------------------
/********************** from ASCII UTF-8 string to UNICODE string ***************************/
inline VVOID toUString(const CCHAR* toConvert, WSTRING& result);
inline VVOID toUString(const STRING toConvert, WSTRING& result);

/********************** from UNICODE UTF-8 string to ASCII 8-BIT string ***************************/
inline VVOID toAString(const WCHAR* toConvert, STRING& result);
inline VVOID toAString(const WSTRING toConvert, STRING& result);

/********************** from whatever string to boolean *******************************************/
template<class charType>
inline BBOOL toBool(const charType* toConvert);
inline BBOOL toBool(const STRING& toConvert);
inline BBOOL toBool(const WSTRING& toConvert);

/********************** from whatever string to float *******************************************/
template<class charType>
inline FFLOAT toFloat(const charType** toConvert);  // extra string pointer.
template<class charType>
inline FFLOAT toFloat(const charType* toConvert);
inline FFLOAT toFloat(const STRING& toConvert);
inline FFLOAT toFloat(const WSTRING& toConvert);

/********************** from whatever string to Sint *******************************************/
template<class charType>
inline BIGINT toSInt(const charType** toConvert);  // extra string pointer.
template<class charType>
inline BIGINT toSInt(const charType* toConvert);
inline BIGINT toSInt(const STRING& toConvert);
inline BIGINT toSInt(const WSTRING& toConvert);

/********************** from whatever string to Uint *******************************************/
template<class charType>
inline UBIGINT toUInt(const charType** toConvert);  // extra string pointer.
template<class charType>
inline UBIGINT toUInt(const charType* toConvert);
inline UBIGINT toUInt(const STRING& toConvert);
inline UBIGINT toUInt(const WSTRING& toConvert);

/********************** from whatever hexa to Uint *******************************************/
template<class charType>
inline UBIGINT Hex2UInt(const charType** toConvert, UBIGINT nbChar = UINTMAX);  // extra string pointer.
template<class charType>
inline UBIGINT Hex2UInt(const charType* toConvert, UBIGINT nbChar = UINTMAX);
inline UBIGINT Hex2UInt(const STRING& toConvert, UBIGINT nbChar = UINTMAX);
inline UBIGINT Hex2UInt(const WSTRING& toConvert, UBIGINT nbChar = UINTMAX);

/********************** from whatever string to Vector2D *******************************************/
template<class charType>
inline VVOID toVector2D(const charType** toConvert, Vector2& result);
template<class charType>
inline VVOID toVector2D(const charType* toConvert, Vector2& result);
inline VVOID toVector2D(const STRING& toConvert, Vector2& result);
inline VVOID toVector2D(const WSTRING& toConvert, Vector2& result);

/********************** from whatever string to Vector3D *******************************************/
template<class charType>
inline VVOID toVector3D(const charType** toConvert, Vector3& result);
template<class charType>
inline VVOID toVector3D(const charType* toConvert, Vector3& result);
inline VVOID toVector3D(const STRING& toConvert, Vector3& result);
inline VVOID toVector3D(const WSTRING& toConvert, Vector3& result);

/********************** from whatever string to Vector4D *******************************************/
template<class charType>
inline VVOID toVector4D(const charType** toConvert, Vector4& result);
template<class charType>
inline VVOID toVector4D(const charType* toConvert, Vector4& result);
inline VVOID toVector4D(const STRING& toConvert, Vector4& result);
inline VVOID toVector4D(const WSTRING& toConvert, Vector4& result);

/********************** from whatever string to Matrix4x4 ******************************************/
template<class charType>
inline VVOID toMatrix4x4CM(const charType** toConvert, Matrix4x4& result);  // Column Major
template<class charType>
inline VVOID toMatrix4x4CM(const charType* toConvert, Matrix4x4& result);   // Column Major
inline VVOID toMatrix4x4CM(const STRING& toConvert, Matrix4x4& result);     // Column Major
inline VVOID toMatrix4x4CM(const WSTRING& toConvert, Matrix4x4& result);    // Column Major

template<class charType>
inline VVOID toMatrix4x4RM(const charType** toConvert, Matrix4x4& result);  // Row Major
template<class charType>
inline VVOID toMatrix4x4RM(const charType* toConvert, Matrix4x4& result);   // Row Major
inline VVOID toMatrix4x4RM(const STRING& toConvert, Matrix4x4& result);     // Row Major
inline VVOID toMatrix4x4RM(const WSTRING& toConvert, Matrix4x4& result);    // Row Major

/********************** from whatever string to Date Struct ****************************************/


/********************** from whatever string to a bool list ******************************************/
template<class charType>
inline VVOID toBoolList(const charType* toConvert, Array1D<BBOOL>& result);
inline VVOID toBoolList(const STRING& toConvert, Array1D<BBOOL>& result);
inline VVOID toBoolList(const WSTRING& toConvert, Array1D<BBOOL>& result);

/********************** from whatever string to a float list *****************************************/
template<class charType>
inline VVOID toFloatList(const charType* toConvert, Array1D<FFLOAT>& result);
inline VVOID toFloatList(const STRING& toConvert, Array1D<FFLOAT>& result);
inline VVOID toFloatList(const WSTRING& toConvert, Array1D<FFLOAT>& result);

/********************** from whatever string to a Sint list *****************************************/
template<class charType>
inline VVOID toSIntList(const charType* toConvert, Array1D<BIGINT>& result);
inline VVOID toSIntList(const STRING& toConvert, Array1D<BIGINT>& result);
inline VVOID toSIntList(const WSTRING& toConvert, Array1D<BIGINT>& result);

/********************** from whatever string to a Uint list *****************************************/
template<class charType>
inline VVOID toUIntList(const charType* toConvert, Array1D<UBIGINT>& result);
inline VVOID toUIntList(const STRING& toConvert, Array1D<UBIGINT>& result);
inline VVOID toUIntList(const WSTRING& toConvert, Array1D<UBIGINT>& result);

/********************** from whatever string to a Vector2D list *****************************************/
template<class charType>
inline VVOID toVector2DList(const charType* toConvert, Array1D<Vector2>& result);
inline VVOID toVector2DList(const STRING& toConvert, Array1D<Vector2>& result);
inline VVOID toVector2DList(const WSTRING& toConvert, Array1D<Vector2>& result);

/********************** from whatever string to a Vector3D list *****************************************/
template<class charType>
inline VVOID toVector3DList(const charType* toConvert, Array1D<Vector3>& result);
inline VVOID toVector3DList(const STRING& toConvert, Array1D<Vector3>& result);
inline VVOID toVector3DList(const WSTRING& toConvert, Array1D<Vector3>& result);

/********************** from whatever string to a Vector4D list *****************************************/
template<class charType>
inline VVOID toVector4DList(const charType* toConvert, Array1D<Vector4>& result);
inline VVOID toVector4DList(const STRING& toConvert, Array1D<Vector4>& result);
inline VVOID toVector4DList(const WSTRING& toConvert, Array1D<Vector4>& result);

/********************** from whatever string to a Matrix4x4 list ****************************************/
template<class charType>
inline VVOID toMatrix4x4CMList(const charType* toConvert, Array1D<Matrix4x4>& result);   // Column Major List
inline VVOID toMatrix4x4CMList(const STRING& toConvert, Array1D<Matrix4x4>& result);     // Column Major List
inline VVOID toMatrix4x4CMList(const WSTRING& toConvert, Array1D<Matrix4x4>& result);    // Column Major List

template<class charType>
inline VVOID toMatrix4x4RMList(const charType* toConvert, Array1D<Matrix4x4>& result);   // Row Major List
inline VVOID toMatrix4x4RMList(const STRING& toConvert, Array1D<Matrix4x4>& result);     // Row Major List
inline VVOID toMatrix4x4RMList(const WSTRING& toConvert, Array1D<Matrix4x4>& result);    // Row Major List

//--------------------------------- Inverse operation from scalar to string -----------------------------
/********************** from a float list to whatever string *******************************************/
inline VVOID toString(const Array1D<FFLOAT>& toConvert, WSTRING& result);

/********************** from a Sint list to whatever string ********************************************/
inline VVOID toString(const Array1D<BIGINT>& toConvert, WSTRING& result);

/********************** from a Uint list to whatever string ********************************************/
inline VVOID toString(const Array1D<UBIGINT>& toConvert, WSTRING& result);
inline VVOID toString(const UBIGINT* toConvert, UBIGINT nbInt, WSTRING& result);

/********************** from a Vector2D to whatever string *********************************************/
inline VVOID toString(const Vector2& toConvert, STRING& result);
inline VVOID toWString(const Vector2& toConvert, WSTRING& result);

/********************** from a Vector3D to whatever string *********************************************/
inline VVOID toString(const Vector3& toConvert, STRING& result);
inline VVOID toWString(const Vector3& toConvert, WSTRING& result);

/********************** from a Vector4D to whatever string *********************************************/
inline VVOID toString(const Vector4& toConvert, STRING& result);
inline VVOID toWString(const Vector4& toConvert, WSTRING& result);

/********************** from a Matrix4x4 to whatever string ********************************************/
inline VVOID toStringCM(const Matrix4x4& toConvert, STRING& result);
inline VVOID toWStringCM(const Matrix4x4& toConvert, WSTRING& result);
inline VVOID toStringRM(const Matrix4x4& toConvert, STRING& result);
inline VVOID toWStringRM(const Matrix4x4& toConvert, WSTRING& result);

/********************** from a Date struct to whatever string ******************************************/


/********************** from whatever scalar to whatever string ****************************************/
template<class scalarType>
inline VVOID toString(const scalarType& toConvert, STRING& result);
template<class scalarType> // only for UInt & Int.
inline VVOID toWString(const scalarType& toConvert, WSTRING& result);
inline VVOID toString(const FFLOAT& toConvert, STRING& result);
inline VVOID toWString(const FFLOAT& toConvert, WSTRING& result);

// char counter helper
template<class charType>
inline BIGINT count(const charType* toCount);
inline UBIGINT xmlStrLen(const UCCHAR* toCount);

// Cyclic redundancy checksum CRC
template<class charType>
inline UBIGINT computeCRC(const charType* toCount);
inline UBIGINT computeCRC(const STRING& toCount);
inline UBIGINT computeCRC(const WSTRING& toCount);

// string info checker
template<class charType>
inline BBOOL isInt(const charType* toCheck);
template<class charType>
inline BBOOL isFloat(const charType* toCheck);
inline BBOOL isInt(const STRING& toCheck);
inline BBOOL isInt(const WSTRING& toCheck);
inline BBOOL isFloat(const STRING& toCheck);
inline BBOOL isFloat(const WSTRING& toCheck);

/********************** Extraneous methods using strings ***********************************************/
inline STRING fileExtension(const STRING& file);
inline VVOID  splitExtension(STRING& file);
inline UCCHAR* xmlStrndUp(const UCCHAR* str, UBIGINT len);
inline UCCHAR* xmlStrndUp(const CCHAR* str, UBIGINT len);
inline UCCHAR* xmlStrdUp(const UCCHAR* str);
inline UCCHAR* xmlStrdUp(const CCHAR* str);
inline UCCHAR* xmlStrnCat(UCCHAR* char1, const UCCHAR* toAdd, UBIGINT len);
inline UCCHAR* xmlStrCat(UCCHAR* str, const UCCHAR* toAdd);
inline UCCHAR* xmlStrnCatNew(const UCCHAR* str1, const UCCHAR* str2, UBIGINT len);

// case-sensitive
inline BBOOL  areEqual(const CCHAR* toComp1, const CCHAR* toComp2);
inline BBOOL  areEqual(const STRING& toComp1, const CCHAR* toComp2);
inline BBOOL  areEqual(const CCHAR* toComp1, const STRING& toComp2);
inline BBOOL  areEqual(const STRING& toComp1, const STRING& toComp2);
inline BBOOL  areEqual(const WCHAR* toComp1, const WCHAR* toComp2);
inline BBOOL  areEqual(const WSTRING& toComp1, const WCHAR* toComp2);
inline BBOOL  areEqual(const WCHAR* toComp1, const WSTRING& toComp2);
inline BBOOL  areEqual(const WSTRING& toComp1, const WSTRING& toComp2);
inline BBOOL  operator==(const STRING& toComp1, const CCHAR* toComp2);
inline BBOOL  operator==(const CCHAR* toComp1, const STRING& toComp2);
inline BBOOL  operator==(const WSTRING& toComp1, const WCHAR* toComp2);
inline BBOOL  operator==(const WCHAR* toComp1, const WSTRING& toComp2);
inline BBOOL  areEqual(const UCCHAR* xmlChar, const CCHAR* toComp);

// case-insensitive
inline BBOOL  areIEqual(const CCHAR* toComp1, const CCHAR* toComp2);
inline BBOOL  areIEqual(const STRING& toComp1, const CCHAR* toComp2);
inline BBOOL  areIEqual(const CCHAR* toComp1, const STRING& toComp2);
inline BBOOL  areIEqual(const STRING& toComp1, const STRING& toComp2);
inline BBOOL  areIEqual(const WCHAR* toComp1, const WCHAR* toComp2);
inline BBOOL  areIEqual(const WSTRING& toComp1, const WCHAR* toComp2);
inline BBOOL  areIEqual(const WCHAR* toComp1, const WSTRING& toComp2);
inline BBOOL  areIEqual(const WSTRING& toComp1, const WSTRING& toComp2);

inline VVOID  xmlToString(const CCHAR* xmlChar, STRING& result);
inline VVOID  xmlToWString(const WCHAR* xmlWChar, WSTRING& result);

#endif