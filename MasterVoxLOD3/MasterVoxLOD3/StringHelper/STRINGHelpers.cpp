#include "STRINGHelpers.h"

using namespace std;
#pragma warning (disable : 4154)

// define macro
#define BLANKandRETURN ((tok != ' ') && (tok != '\t') && (tok != '\n') && (tok != '\r'))
#define BLANKorRETURN ((tok == ' ') || (tok == '\t') || (tok == '\n') || (tok == '\r'))
#define LBLANKandRETURN ((tok != ' ') && (tok != '\t') && (tok != '\n'))
#define LBLANKorRETURN ((tok == ' ') || (tok == '\t') || (tok == '\n'))
#define ADDtoSTRING(x, y) y.append(x)
#define FLOATtoCHAR(x, y) sprintf_s(y, "%f", x)
#define FLOATtoWCHAR(x, y) swprintf_s(y, L"%f", x)
#define INTtoCHAR(x, y) sprintf_s(y, "%d", x)
#define INTtoWCHAR(x, y) swprintf_s(y, L"%d", x)

// Look up table for CRC computation.
static const BIGINT kCRCTable[256] = 
{
	0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
	0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988, 0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
	0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
	0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
	0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
	0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59, 
	0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
	0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924, 0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 
	0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433, 
	0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
	0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 
	0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65, 
	0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB, 
	0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9, 
	0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F, 
	0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD, 
	0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683, 
	0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1, 
	0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7, 
	0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5, 
	0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B, 
	0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79, 
	0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236, 0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 
	0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D, 
	0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713, 
	0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38, 0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 
	0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
	0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45, 
	0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB, 
	0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9, 
	0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF, 
	0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};

VVOID splitWithBlanks(const STRING& toSplit, LSSTRING& result)
{
	splitWithDelimiter(toSplit, result, ' ');
}

VVOID splitWithBlanks(const WSTRING& toSplit, LWSTRING& result)
{
	splitWithDelimiter(toSplit, result, ' ');
}

VVOID splitWithDelimiter(const STRING& toSplit, LSSTRING& result, const CCHAR& delimiter)
{
	STRING::size_type start = 0;
	STRING::size_type stop  = toSplit.find(delimiter);

	while(stop != STRING::npos)
	{
		result.putBACK(toSplit.substr(start, (stop - start)));

		start = ++stop;
		stop  = toSplit.find(delimiter, stop);

		if(stop == STRING::npos)
			result.putBACK(toSplit.substr(start, toSplit.length()));
	}
}

VVOID splitWithDelimiter(const WSTRING& toSplit, LWSTRING& result, const CCHAR& delimiter)
{
	WSTRING::size_type start = 0;
	WSTRING::size_type stop  = toSplit.find(delimiter);

	while(stop != WSTRING::npos)
	{
		result.putBACK(toSplit.substr(start, (stop - start)));

		start = ++stop;
		stop  = toSplit.find(delimiter, stop);

		if(stop == WSTRING::npos)
			result.putBACK(toSplit.substr(start, toSplit.length()));
	}
}

VVOID eraseBlanks(STRING& result)
{
	result.erase(remove(result.begin(), result.end(), ' '), result.end());
}

VVOID toUString(const CCHAR* toConvert, WSTRING& result)
{
	result.clear();
	if(toConvert != NULL)
	{
		UBIGINT len = (UBIGINT) strlen(toConvert);
		const size_t newSize = 10;
		result.reserve(len + 1);

		for(UBIGINT currChar = 0; currChar < len; currChar++)
		{
			WCHAR temp[newSize];
			CCHAR temp2 = toConvert[currChar];
			size_t converted = 0;
			mbstowcs_s(&converted, temp, &temp2, 1);
			result.append(temp);
		}
	}
}

VVOID toUString(const STRING toConvert, WSTRING& result)
{
	toUString(toConvert.c_str(), result);
}

VVOID toAString(const WCHAR* toConvert, STRING& result)
{
	result.clear();
	if(toConvert != NULL)
	{
		UBIGINT len = (UBIGINT)wcslen(toConvert);
		const size_t newSize = 10;
		result.reserve(len + 1);

		for(UBIGINT currWChar = 0; currWChar < len; currWChar++)
		{
			if((toConvert[currWChar] < 0xFF) || (toConvert[currWChar] & (~0xFF)) >= 32)
			{
				CCHAR temp[newSize];
				WCHAR temp2 = toConvert[currWChar];
				size_t converted = 0;
				wcstombs_s(&converted, temp, &temp2, 1);
				result.append(temp);
			}
			else
			{
				const char* temp = "_";
				result.append(temp);
			}
		}
	}
}

VVOID toAString(const WSTRING toConvert, STRING& result)
{
	toAString(toConvert.c_str(), result);
}

template<class charType>
BBOOL toBool(const charType* toConvert)
{
	if(toConvert == NULL)
		return false;

	if((*toConvert != 0) && (*toConvert != '0') && (*toConvert != 'f') &&(*toConvert != 'F'))
		return true;

	return false;
}

BBOOL toBool(const STRING& toConvert)
{
	return toBool(toConvert.c_str());
}

BBOOL toBool(const WSTRING& toConvert)
{
	return toBool(toConvert.c_str());
}

template<class charType>
FFLOAT toFloat(const charType** toConvert)
{
	const charType* temp = *toConvert;

	if((temp == NULL) || (*temp == 0))
		return 0.0f;

	// iterate through string until a char which is not a space.
	charType tok;
	while (((tok = *temp) != 0) && BLANKorRETURN/*((tok == SPACE) || (tok == '\t') || (tok == '\n') || (tok == '\r'))*/)
		tok++;

	float val  = 0.0f;
	float sign = 1.0f;

	// check if negative value
	if(*temp == '-')
	{
		tok++;
		sign = -1.0f;
	}

	float decimals  = 0.0f;
	BIGINT exponent = 0;
	BBOOL infinity  = false;
	BBOOL nonValidFound = false;

	// determine the float
	while(((tok = *temp) != 0) && (nonValidFound == false))
	{
		switch(tok)
		{
			case '.': decimals = 1.0f; 
					  break;
			case '0': val *= 10.0f; 
					  decimals *= 10.0f; 
					  break;
			case '1': val = val * 10.0f + 1.0f; 
					  decimals *= 10.0f; 
					  break;
			case '2': val = val * 10.0f + 2.0f; 
					  decimals *= 10.0f; 
					  break;
			case '3': val = val * 10.0f + 3.0f; 
					  decimals *= 10.0f; 
					  break;
			case '4': val = val * 10.0f + 4.0f; 
					  decimals *= 10.0f; 
					  break;
			case '5': val = val * 10.0f + 5.0f; 
					  decimals *= 10.0f; 
					  break;
			case '6': val = val * 10.0f + 6.0f; 
					  decimals *= 10.0f; 
					  break;
			case '7': val = val * 10.0f + 7.0f; 
					  decimals *= 10.0f; 
					  break;
			case '8': val = val * 10.0f + 8.0f; 
					  decimals *= 10.0f; 
					  break;
			case '9': val = val * 10.0f + 9.0f; 
					  decimals *= 10.0f; 
					  break;
			case 'e':
			case 'E': temp++; 
					  exponent = toSInt(&temp); 
					  temp -= 2; 
					  nonValidFound = true; 
					  break;
			case 'I': infinity = true; // intentional pass-through.
			default: nonValidFound = true; 
					 temp--; 
					 break;
		}
		temp++;
	}

	float result = 0.0f;
	if(infinity)
	{
		infinity = false;
		tok = *(temp++);

		if(!(tok == 'I'))
			infinity = false;
		else
		{
			tok = *(temp++);

			if(!(tok == 'N'))
				infinity = false;
			else
			{
				tok = *(temp++);

				if(!(tok == 'F'))
					infinity = false;
				else
					result = INFINITAS * sign;
			}
		}
	}

	if(!infinity)
	{
		if(decimals == 0.0f)
			decimals = 1.0f;

		result = (val * sign) / decimals;

		if(!(exponent == 0))
			result *= powf(10.0f, (FFLOAT)exponent);
	}

	// Find the next blanks and skip them even the end ones.
	while(((tok = *temp) != 0) && BLANKandRETURN)
		temp++;
	while(((tok = *temp) != 0) && BLANKorRETURN)
		temp++;

	*toConvert = temp;
	return result;
}

template<class charType>
FFLOAT toFloat(const charType* toConvert)
{
	return toFloat(&toConvert);
}

FFLOAT toFloat(const STRING& toConvert)
{
	return toFloat(toConvert.c_str());
}

FFLOAT toFloat(const WSTRING& toConvert)
{
	return toFloat(toConvert.c_str());
}

/********************** from whatever string to Sint *******************************************/
template<class charType>
BIGINT toSInt(const charType** toConvert)
{
	// check if NULL
	if(*toConvert == NULL)
		return 0;

	// skip first white spaces.
	const charType* temp = *toConvert;
	charType tok;
	while(((tok = *temp) != 0) && BLANKorRETURN)
		temp++;

	BIGINT result = 0;
	BIGINT sign   = 1; // the sign to multiply then
	
	// determine the sign
	if(*temp == '-')
	{
		temp++;
		sign = -1;
	}

	while((tok = *temp) != 0)
	{
		if((tok >= '0') && (tok <= '9'))
			result = (result * 10) + tok - '0';
		else
			break;
		temp++;
	}

	// add the sign.
	result *= sign;

	// skip last white spaces.
	while(((tok = *temp) != '\0') && LBLANKandRETURN)
		temp++;
	while(((tok = *temp) != '\0') && LBLANKorRETURN)
		temp++;

	*toConvert = temp;
	return result;
}

template<class charType>
BIGINT toSInt(const charType* toConvert)
{
	return toSInt(&toConvert);
}

BIGINT toSInt(const STRING& toConvert)
{
	return toSInt(toConvert.c_str());
}

BIGINT toSInt(const WSTRING& toConvert)
{
	return toSInt(toConvert.c_str());
}

/********************** from whatever string to Uint *******************************************/
template<class charType>
UBIGINT toUInt(const charType** toConvert)
{
	// check if NULL
	if((toConvert == NULL) || (*toConvert == NULL) || ((**toConvert) == 0))
		return 0;

	const charType* temp = *toConvert;
	charType tok;

	while(((tok = *temp) != 0) && BLANKorRETURN)
		temp++;

	UBIGINT result = 0;
	while((tok = *temp) != 0)
	{
		if((tok >= '0') && (tok <= '9'))
			result = result * 10 + tok - '0';
		else
			break;
		temp++;
	}

	while(((tok = *temp) != '\0') && LBLANKandRETURN)
		temp++;
	while(((tok = *temp) != '\0') && LBLANKorRETURN)
		temp++;

	*toConvert = temp;
	return result;
}

template<class charType>
UBIGINT toUInt(const charType* toConvert)
{
	return toUInt(&toConvert);
}

UBIGINT toUInt(const STRING& toConvert)
{
	return toUInt(toConvert.c_str());
}

UBIGINT toUInt(const WSTRING& toConvert)
{
	return toUInt(toConvert.c_str());
}

/********************** from whatever hexa to Uint *******************************************/
template<class charType>
UBIGINT Hex2UInt(const charType** toConvert, UBIGINT nbChar)
{
	// check if NULL
	if((toConvert == NULL) || (*toConvert == NULL) || ((**toConvert) == 0))
		return 0;

	const charType* temp = *toConvert;
	charType tok;

	// skip the hexa prefix
	if(*temp == '0' && (((*(temp+1)) == 'x') || ((*(temp+1)) == 'X')))
		temp += 2;

	UBIGINT result = 0;
	for(UBIGINT i = 0; i < nbChar && ((tok = *temp) != 0); i++)
	{
		if((tok >= '0') && (tok <= '9'))
			result = result * 16 + tok - '0';
		else if((tok >= 'A') && (tok <= 'F'))
			result = result * 16 + tok + 10 - 'A';
		else if((tok >= 'a') && (tok <= 'f'))
			result = result * 16 + tok + 10 - 'a';
		else
			break;
		temp++;
	}

	*toConvert = temp;
	return result;
}

template<class charType>
UBIGINT Hex2UInt(const charType* toConvert, UBIGINT nbChar)
{
	return Hex2UInt(&toConvert, nbChar);
}

UBIGINT Hex2UInt(const STRING& toConvert, UBIGINT nbChar)
{
	return Hex2UInt(toConvert.c_str(), nbChar);
}

UBIGINT Hex2UInt(const WSTRING& toConvert, UBIGINT nbChar)
{
	return Hex2UInt(toConvert.c_str(), nbChar);
}

/********************** from whatever string to Vector2D *******************************************/
template<class charType>
VVOID toVector2D(const charType** toConvert, Vector2& result)
{
	if((toConvert != NULL) && (*toConvert != NULL) && ((**toConvert) != 0))
	{
		result.x(toFloat(toConvert));
		result.y(toFloat(toConvert));
	}
}

template<class charType>
VVOID toVector2D(const charType* toConvert, Vector2& result)
{
	toVector2D(&toConvert, result);
}

VVOID toVector2D(const STRING& toConvert, Vector2& result)
{
	toVector2D(toConvert.c_str(), result);
}

VVOID toVector2D(const WSTRING& toConvert, Vector2& result)
{
	toVector2D(toConvert.c_str(), result);
}

/********************** from whatever string to Vector3D *******************************************/
template<class charType>
VVOID toVector3D(const charType** toConvert, Vector3& result)
{
	if((toConvert != NULL) && (*toConvert != NULL) && ((**toConvert) != 0))
	{
		result.x(toFloat(toConvert));
		result.y(toFloat(toConvert));
		result.z(toFloat(toConvert));
	}
}

template<class charType>
VVOID toVector3D(const charType* toConvert, Vector3& result)
{
	toVector3D(&toConvert, result);
}

VVOID toVector3D(const STRING& toConvert, Vector3& result)
{
	toVector3D(toConvert.c_str(), result);
}

VVOID toVector3D(const WSTRING& toConvert, Vector3& result)
{
	toVector3D(toConvert.c_str(), result);
}

/********************** from whatever string to Vector4D *******************************************/
template<class charType>
VVOID toVector4D(const charType** toConvert, Vector4& result)
{
	if((toConvert != NULL) && (*toConvert != NULL) && ((**toConvert) != 0))
	{
		result.x(toFloat(toConvert));
		result.y(toFloat(toConvert));
		result.z(toFloat(toConvert));
		
		if((*toConvert != NULL) && ((**toConvert) != 0))
			result.w(toFloat(toConvert));
		else
			result.w(1.0f); // default value if not provided.
	}
}

template<class charType>
VVOID toVector4D(const charType* toConvert, Vector4& result)
{
	toVector4D(&toConvert, result);
}

VVOID toVector4D(const STRING& toConvert, Vector4& result)
{
	toVector4D(toConvert.c_str(), result);
}

VVOID toVector4D(const WSTRING& toConvert, Vector4& result)
{
	toVector4D(toConvert.c_str(), result);
}

/********************** from whatever string to Matrix4x4 ******************************************/
template<class charType>
VVOID toMatrix4x4CM(const charType** toConvert, Matrix4x4& result)
{
	if((toConvert != NULL) && (*toConvert != NULL) && ((**toConvert) != 0))
	{
		// COLLADA is column major.
		result.set_11(toFloat(toConvert)); result.set_21(toFloat(toConvert)); result.set_31(toFloat(toConvert)); result.set_41(toFloat(toConvert));
		result.set_12(toFloat(toConvert)); result.set_22(toFloat(toConvert)); result.set_32(toFloat(toConvert)); result.set_42(toFloat(toConvert));
		result.set_13(toFloat(toConvert)); result.set_23(toFloat(toConvert)); result.set_33(toFloat(toConvert)); result.set_43(toFloat(toConvert));
		result.set_14(toFloat(toConvert)); result.set_24(toFloat(toConvert)); result.set_34(toFloat(toConvert)); result.set_44(toFloat(toConvert));
	}
}

template<class charType>
VVOID toMatrix4x4CM(const charType* toConvert, Matrix4x4& result)
{
	toMatrix4x4CM(&toConvert, result);
}

VVOID toMatrix4x4CM(const STRING& toConvert, Matrix4x4& result)
{
	toMatrix4x4CM(toConvert.c_str(), result);
}

VVOID toMatrix4x4CM(const WSTRING& toConvert, Matrix4x4& result)
{
	toMatrix4x4CM(toConvert.c_str(), result);
}

template<class charType>
VVOID toMatrix4x4RM(const charType** toConvert, Matrix4x4& result)
{
	if((toConvert != NULL) && (*toConvert != NULL) && ((**toConvert) != 0))
	{
		// row major version
		result.set_11(toFloat(toConvert)); result.set_12(toFloat(toConvert)); result.set_13(toFloat(toConvert)); result.set_14(toFloat(toConvert));
		result.set_21(toFloat(toConvert)); result.set_22(toFloat(toConvert)); result.set_23(toFloat(toConvert)); result.set_24(toFloat(toConvert));
		result.set_31(toFloat(toConvert)); result.set_32(toFloat(toConvert)); result.set_33(toFloat(toConvert)); result.set_34(toFloat(toConvert));
		result.set_41(toFloat(toConvert)); result.set_42(toFloat(toConvert)); result.set_43(toFloat(toConvert)); result.set_44(toFloat(toConvert));
	}
}

template<class charType>
VVOID toMatrix4x4RM(const charType* toConvert, Matrix4x4& result)
{
	toMatrix4x4RM(&toConvert, result);
}

VVOID toMatrix4x4RM(const STRING& toConvert, Matrix4x4& result)
{
	toMatrix4x4RM(toConvert.c_str(), result);
}

VVOID toMatrix4x4RM(const WSTRING& toConvert, Matrix4x4& result)
{
	toMatrix4x4RM(toConvert.c_str(), result);
}

/********************** from whatever string to Date Struct ****************************************/


/********************** from whatever string to a bool list ******************************************/
template<class charType>
VVOID toBoolList(const charType* toConvert, Array1D<BBOOL>& result)
{
	const charType* temp = toConvert;
	charType tok  = *temp;
	BIGINT nbSlot = count(toConvert);
	result.resize(nbSlot);
	BIGINT indx = 0;

	// skip white spaces.
	while(((tok = *temp) != 0) && BLANKorRETURN)
		temp++;

	while(*temp != 0)
	{
		result.insert(toBool(temp), indx);
		indx++;

		while(((tok = *temp) != 0) && BLANKandRETURN)
			temp++;

		while(((tok = *temp) != 0) && BLANKorRETURN)
			temp++;
	}
	
	if(indx != result.Size()) // aVVOID useless memory consumption.
		result.resize(indx);
}

VVOID toBoolList(const STRING& toConvert, Array1D<BBOOL>& result)
{
	toBoolList(toConvert.c_str(), result);
}

VVOID toBoolList(const WSTRING& toConvert, Array1D<BBOOL>& result)
{
	toBoolList(toConvert.c_str(), result);
}

/********************** from whatever string to a float list *****************************************/
template<class charType>
VVOID toFloatList(const charType* toConvert, Array1D<FFLOAT>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbChar = count(toConvert);
		result.resize(nbChar);

		while(*toConvert != 0)
		{
			result.insert(toFloat(&toConvert), (BIGINT)indx);
			indx++;
		}

		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toFloatList(const STRING& toConvert, Array1D<FFLOAT>& result)
{
	toFloatList(toConvert.c_str(), result);
}

VVOID toFloatList(const WSTRING& toConvert, Array1D<FFLOAT>& result)
{
	toFloatList(toConvert.c_str(), result);
}

/********************** from whatever string to a Sint list *****************************************/
template<class charType>
VVOID toSIntList(const charType* toConvert, Array1D<BIGINT>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbSlot = count(toConvert);
		result.resize(nbSlot);

		while(*toConvert != 0)
		{
			result.insert(toSInt(&toConvert), (BIGINT)indx);
			indx++;
		}

		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toSIntList(const STRING& toConvert, Array1D<BIGINT>& result)
{
	toSIntList(toConvert.c_str(), result);
}

VVOID toSIntList(const WSTRING& toConvert, Array1D<BIGINT>& result)
{
	toSIntList(toConvert.c_str(), result);
}

/********************** from whatever string to a Uint list *****************************************/
template<class charType>
VVOID toUIntList(const charType* toConvert, Array1D<UBIGINT>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbSlot = count(toConvert);
		result.resize(nbSlot);

		while(*toConvert != 0)
		{
			result.insert(toUInt(&toConvert), (BIGINT)indx);
			indx++;
		}

		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toUIntList(const STRING& toConvert, Array1D<UBIGINT>& result)
{
	toUIntList(toConvert.c_str(), result);
}

VVOID toUIntList(const WSTRING& toConvert, Array1D<UBIGINT>& result)
{
	toUIntList(toConvert.c_str(), result);
}

/********************** from whatever string to a Vector2D list *****************************************/
template<class charType>
VVOID toVector2DList(const charType* toConvert, Array1D<Vector2>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbSlot = count(toConvert);
		// knowing that every scalar value will be counted one by one
		// and that a vector2D contains 2 of them, so divide the count by 2.
		FFLOAT nbVec2D = (FFLOAT)nbSlot * 0.5f;
		result.resize((BIGINT)nbVec2D);

		while(*toConvert != 0)
		{
			Vector2 temp(0.0f);
			toVector2D(&toConvert, temp);
			result.insert(temp, (BIGINT)indx);
			indx++;
		}

		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toVector2DList(const STRING& toConvert, Array1D<Vector2>& result)
{
	toVector2DList(toConvert.c_str(), result);
}

VVOID toVector2DList(const WSTRING& toConvert, Array1D<Vector2>& result)
{
	toVector2DList(toConvert.c_str(), result);
}

/********************** from whatever string to a Vector3D list *****************************************/
template<class charType>
VVOID toVector3DList(const charType* toConvert, Array1D<Vector3>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbSlot = count(toConvert);
		nbSlot /= 3;
		result.resize(nbSlot);

		while(*toConvert != 0)
		{
			Vector3 temp(0.0f);
			toVector3D(&toConvert, temp);
			result.insert(temp, (BIGINT)indx);
			indx++;
		}

		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toVector3DList(const STRING& toConvert, Array1D<Vector3>& result)
{
	toVector3DList(toConvert.c_str(), result);
}

VVOID toVector3DList(const WSTRING& toConvert, Array1D<Vector3>& result)
{
	toVector3DList(toConvert.c_str(), result);
}

/********************** from whatever string to a Vector4D list *****************************************/
template<class charType>
VVOID toVector4DList(const charType* toConvert, Array1D<Vector4>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbSlot = count(toConvert);
		nbSlot /= 3; // the w param may be not provided so at least divide by 3 like for vec3 (limit the potential resize cost at the end).
		result.resize(nbSlot);

		while(*toConvert != 0)
		{
			Vector4 temp(0.0f, 1.0f);
			toVector4D(&toConvert, temp);
			result.insert(temp, (BIGINT)indx);
			indx++;
		}

		// aVVOID useless memory location because of impossible
		// accurate allocation because of random w param presence.
		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toVector4DList(const STRING& toConvert, Array1D<Vector4>& result)
{
	toVector4DList(toConvert.c_str(), result);
}

VVOID toVector4DList(const WSTRING& toConvert, Array1D<Vector4>& result)
{
	toVector4DList(toConvert.c_str(), result);
}

/********************** from whatever string to a Matrix4x4 list ****************************************/
template<class charType>
VVOID toMatrix4x4CMList(const charType* toConvert, Array1D<Matrix4x4>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbSlot = count(toConvert);
		nbSlot /= 16; // nb elements in a 4x4 matrix.
		result.resize(nbSlot);

		while(*toConvert != 0)
		{
			Matrix4x4 temp;
			toMatrix4x4CM(&toConvert, temp);
			result.insert(temp, (BIGINT)indx);
			indx++;
		}

		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toMatrix4x4CMList(const STRING& toConvert, Array1D<Matrix4x4>& result)
{
	toMatrix4x4CMList(toConvert.c_str(), result);
}

VVOID toMatrix4x4CMList(const WSTRING& toConvert, Array1D<Matrix4x4>& result)
{
	toMatrix4x4CMList(toConvert.c_str(), result);
}

template<class charType>
VVOID toMatrix4x4RMList(const charType* toConvert, Array1D<Matrix4x4>& result)
{
	UBIGINT indx = 0;

	if((toConvert != NULL) && (*toConvert != 0))
	{
		BIGINT nbSlot = count(toConvert);
		nbSlot /= 16; // nb elements in a 4x4 matrix.
		result.resize(nbSlot);

		while(*toConvert != 0)
		{
			Matrix4x4 temp;
			toMatrix4x4RM(&toConvert, temp);
			result.insert(temp, (BIGINT)indx);
			indx++;
		}

		if(result.Size() != indx)
			result.resize(indx);
	}
}

VVOID toMatrix4x4RMList(const STRING& toConvert, Array1D<Matrix4x4>& result)
{
	toMatrix4x4RMList(toConvert.c_str(), result);
}

VVOID toMatrix4x4RMList(const WSTRING& toConvert, Array1D<Matrix4x4>& result)
{
	toMatrix4x4RMList(toConvert.c_str(), result);
}

//--------------------------------- Inverse operation from scalar to string -----------------------------
/********************** from a float list to whatever string *******************************************/
VVOID toString(const Array1D<FFLOAT>& toConvert, WSTRING& result)
{
	UBIGINT nbSlot = (UBIGINT)toConvert.Size();

	if(nbSlot == 0)
		return;

	const WCHAR* SPACe = L" ";
	if(!result.empty()) result.append(SPACe);

	WCHAR first[15];
	FLOATtoWCHAR(toConvert.Array()[0], first);
	result.append(first);

	for(UBIGINT currSlot = 1; currSlot < nbSlot; currSlot++)
	{
		WCHAR temp[15];
		FLOATtoWCHAR(toConvert.Array()[currSlot], temp);
		result.append(SPACe);
		result.append(temp);
	}
}

/********************** from a Sint list to whatever string ********************************************/
VVOID toString(const Array1D<BIGINT>& toConvert, WSTRING& result)
{
	UBIGINT nbSlot = (UBIGINT)toConvert.Size();

	if(nbSlot == 0)
		return;

	const WCHAR* SPACe = L" ";
	if(!result.empty()) result.append(SPACe);

	WCHAR first[15];
	INTtoWCHAR(toConvert.Array()[0], first);
	result.append(first);

	for(UBIGINT currSlot = 1; currSlot < nbSlot; currSlot++)
	{
		WCHAR temp[15];
		INTtoWCHAR(toConvert.Array()[currSlot], temp);
		result.append(SPACe);
		result.append(temp);
	}
}

/********************** from a Uint list to whatever string ********************************************/
VVOID toString(const Array1D<UBIGINT>& toConvert, WSTRING& result)
{
	UBIGINT nbSlot = (UBIGINT)toConvert.Size();

	if(nbSlot == 0)
		return;

	const WCHAR* SPACe = L" ";
	if(!result.empty()) result.append(SPACe);

	WCHAR first[15];
	INTtoWCHAR(toConvert.Array()[0], first);
	result.append(first);

	for(UBIGINT currSlot = 1; currSlot < nbSlot; currSlot++)
	{
		WCHAR temp[15];
		INTtoWCHAR(toConvert.Array()[currSlot], temp);
		result.append(SPACe);
		result.append(temp);
	}
}

VVOID toString(const UBIGINT* toConvert, UBIGINT nbInt, WSTRING& result)
{
	if(nbInt > 0)
	{
		if(result.empty())
		{
			WCHAR first[15];
			UBIGINT toConv = *toConvert;
			INTtoWCHAR(toConv, first);
			*toConvert++;
			result.append(first);
			nbInt--;
		}

		const WCHAR* SPACe = L" ";

		while(((intptr_t) nbInt) > 0)
		{
			WCHAR temp[15];
			UBIGINT toConver = *toConvert;
			INTtoWCHAR(toConver, temp);
			*toConvert++;
			result.append(SPACe);
			result.append(temp);
			nbInt--;
		}
		/*else if(T != WC)
		{
			if(result.empty())
			{
				CCHAR first[15];
				UBIGINT toConv = *toConvert;
				INTtoCHAR(toConv, first);
				*toConvert++;
				result.append(first);
				nbInt--;
			}

			const CCHAR* SPACe = " ";

			while(((intptr_t) nbInt) > 0)
			{
				CCHAR temp[15];
				UBIGINT toConver = *toConvert;
				INTtoCHAR(toConver, temp);
				*toConvert++;
				result.append(SPACe);
				result.append(temp);
				nbInt--;
			}
		}*/
	}
}

/********************** from a Vector2D to whatever string *********************************************/
VVOID toString(const Vector2& toConvert, STRING& result)
{
	FFLOAT temp  = toConvert.getX();
	FFLOAT temp1 = toConvert.getY();
	CCHAR pre[15];
	CCHAR post[15];
	CCHAR* SPACe = " ";
	FLOATtoCHAR(temp, pre);
	FLOATtoCHAR(temp1, post);
	result.append(pre);
	result.append(SPACe);
	result.append(post);
	
}

VVOID toWString(const Vector2& toConvert, WSTRING& result)
{
	FFLOAT temp  = toConvert.getX();
	FFLOAT temp1 = toConvert.getY();
	WCHAR pre[15];
	WCHAR post[15];
	WCHAR* SPACe = L" ";
	FLOATtoWCHAR(temp, pre);
	FLOATtoWCHAR(temp1, post);
	result.append(pre);
	result.append(SPACe);
	result.append(post);
}

/********************** from a Vector3D to whatever string *********************************************/
VVOID toString(const Vector3& toConvert, STRING& result)
{
	FFLOAT temp  = toConvert.getX();
	FFLOAT temp1 = toConvert.getY();
	FFLOAT temp2 = toConvert.getZ();
	CCHAR pre[15];
	CCHAR mid[15];
	CCHAR post[15];
	CCHAR* SPACe = " ";
	FLOATtoCHAR(temp, pre);
	FLOATtoCHAR(temp1, mid);
	FLOATtoCHAR(temp2, post);
	result.append(pre);
	result.append(SPACe);
	result.append(mid);
	result.append(SPACe);
	result.append(post);
}

VVOID toWString(const Vector3& toConvert, WSTRING& result)
{
	FFLOAT temp  = toConvert.getX();
	FFLOAT temp1 = toConvert.getY();
	FFLOAT temp2 = toConvert.getZ();
	WCHAR pre[15];
	WCHAR mid[15];
	WCHAR post[15];
	WCHAR* SPACe = L" ";
	FLOATtoWCHAR(temp, pre);
	FLOATtoWCHAR(temp1, mid);
	FLOATtoWCHAR(temp2, post);
	result.append(pre);
	result.append(SPACe);
	result.append(mid);
	result.append(SPACe);
	result.append(post);
}

/********************** from a Vector4D to whatever string *********************************************/
VVOID toString(const Vector4& toConvert, STRING& result)
{
	FFLOAT temp  = toConvert.getX();
	FFLOAT temp1 = toConvert.getY();
	FFLOAT temp2 = toConvert.getZ();
	FFLOAT temp3 = toConvert.getW();
	CCHAR quar[15];
	CCHAR tier[15];
	CCHAR demi[15];
	CCHAR fini[15];
	CCHAR* SPACe = " ";
	FLOATtoCHAR(temp, quar);
	FLOATtoCHAR(temp1, tier);
	FLOATtoCHAR(temp2, demi);
	FLOATtoCHAR(temp3, fini);

	result.append(quar); result.append(SPACe); result.append(tier); result.append(SPACe);
	result.append(demi); result.append(SPACe); result.append(fini);
}

VVOID toWString(const Vector4& toConvert, WSTRING& result)
{
	FFLOAT temp  = toConvert.getX();
	FFLOAT temp1 = toConvert.getY();
	FFLOAT temp2 = toConvert.getZ();
	FFLOAT temp3 = toConvert.getW();
	WCHAR quar[15];
	WCHAR tier[15];
	WCHAR demi[15];
	WCHAR fini[15];
	WCHAR* SPACe = L" ";
	FLOATtoWCHAR(temp, quar);
	FLOATtoWCHAR(temp1, tier);
	FLOATtoWCHAR(temp2, demi);
	FLOATtoWCHAR(temp3, fini);

	result.append(quar); result.append(SPACe); result.append(tier); result.append(SPACe);
	result.append(demi); result.append(SPACe); result.append(fini);
}

/********************** from a Matrix4x4 to whatever string ********************************************/
VVOID toStringCM(const Matrix4x4& toConvert, STRING& result)
{
	CCHAR* SPACe = " ";
	CCHAR temp[15]; CCHAR temp1[15]; CCHAR temp2[15]; CCHAR temp3[15];
	CCHAR temp4[15]; CCHAR temp5[15]; CCHAR temp6[15]; CCHAR temp7[15];
	CCHAR temp8[15]; CCHAR temp9[15]; CCHAR temp10[15]; CCHAR temp11[15];
	CCHAR temp12[15]; CCHAR temp13[15]; CCHAR temp14[15]; CCHAR temp15[15];

	FLOATtoCHAR(toConvert.get_11(), temp); FLOATtoCHAR(toConvert.get_21(), temp1); FLOATtoCHAR(toConvert.get_31(), temp2); FLOATtoCHAR(toConvert.get_41(), temp3);
	FLOATtoCHAR(toConvert.get_12(), temp4); FLOATtoCHAR(toConvert.get_22(), temp5); FLOATtoCHAR(toConvert.get_32(), temp6); FLOATtoCHAR(toConvert.get_42(), temp7);
	FLOATtoCHAR(toConvert.get_13(), temp8); FLOATtoCHAR(toConvert.get_23(), temp9); FLOATtoCHAR(toConvert.get_33(), temp10); FLOATtoCHAR(toConvert.get_43(), temp11);
	FLOATtoCHAR(toConvert.get_14(), temp12); FLOATtoCHAR(toConvert.get_24(), temp13); FLOATtoCHAR(toConvert.get_34(), temp14); FLOATtoCHAR(toConvert.get_44(), temp15);

	result.append(temp); result.append(SPACe); result.append(temp1); result.append(SPACe); result.append(temp2); result.append(SPACe); result.append(temp3); result.append(SPACe);
	result.append(temp4); result.append(SPACe); result.append(temp5); result.append(SPACe); result.append(temp6); result.append(SPACe); result.append(temp7); result.append(SPACe);
	result.append(temp8); result.append(SPACe); result.append(temp9); result.append(SPACe); result.append(temp10); result.append(SPACe); result.append(temp11); result.append(SPACe);
	result.append(temp12); result.append(SPACe); result.append(temp13); result.append(SPACe); result.append(temp14); result.append(SPACe); result.append(temp15);
}

VVOID toWStringCM(const Matrix4x4& toConvert, WSTRING& result)
{
	WCHAR* SPACe = L" ";
	WCHAR temp[15]; WCHAR temp1[15]; WCHAR temp2[15]; WCHAR temp3[15];
	WCHAR temp4[15]; WCHAR temp5[15]; WCHAR temp6[15]; WCHAR temp7[15];
	WCHAR temp8[15]; WCHAR temp9[15]; WCHAR temp10[15]; WCHAR temp11[15];
	WCHAR temp12[15]; WCHAR temp13[15]; WCHAR temp14[15]; WCHAR temp15[15];

	FLOATtoWCHAR(toConvert.get_11(), temp); FLOATtoWCHAR(toConvert.get_21(), temp1); FLOATtoWCHAR(toConvert.get_31(), temp2); FLOATtoWCHAR(toConvert.get_41(), temp3);
	FLOATtoWCHAR(toConvert.get_12(), temp4); FLOATtoWCHAR(toConvert.get_22(), temp5); FLOATtoWCHAR(toConvert.get_32(), temp6); FLOATtoWCHAR(toConvert.get_42(), temp7);
	FLOATtoWCHAR(toConvert.get_13(), temp8); FLOATtoWCHAR(toConvert.get_23(), temp9); FLOATtoWCHAR(toConvert.get_33(), temp10); FLOATtoWCHAR(toConvert.get_43(), temp11);
	FLOATtoWCHAR(toConvert.get_14(), temp12); FLOATtoWCHAR(toConvert.get_24(), temp13); FLOATtoWCHAR(toConvert.get_34(), temp14); FLOATtoWCHAR(toConvert.get_44(), temp15);

	result.append(temp); result.append(SPACe); result.append(temp1); result.append(SPACe); result.append(temp2); result.append(SPACe); result.append(temp3); result.append(SPACe);
	result.append(temp4); result.append(SPACe); result.append(temp5); result.append(SPACe); result.append(temp6); result.append(SPACe); result.append(temp7); result.append(SPACe);
	result.append(temp8); result.append(SPACe); result.append(temp9); result.append(SPACe); result.append(temp10); result.append(SPACe); result.append(temp11); result.append(SPACe);
	result.append(temp12); result.append(SPACe); result.append(temp13); result.append(SPACe); result.append(temp14); result.append(SPACe); result.append(temp15);
}

VVOID toStringRM(const Matrix4x4& toConvert, STRING& result)
{
	CCHAR* SPACe = " ";
	CCHAR temp[15]; CCHAR temp1[15]; CCHAR temp2[15]; CCHAR temp3[15];
	CCHAR temp4[15]; CCHAR temp5[15]; CCHAR temp6[15]; CCHAR temp7[15];
	CCHAR temp8[15]; CCHAR temp9[15]; CCHAR temp10[15]; CCHAR temp11[15];
	CCHAR temp12[15]; CCHAR temp13[15]; CCHAR temp14[15]; CCHAR temp15[15];

	FLOATtoCHAR(toConvert.get_11(), temp); FLOATtoCHAR(toConvert.get_12(), temp1); FLOATtoCHAR(toConvert.get_13(), temp2); FLOATtoCHAR(toConvert.get_14(), temp3);
	FLOATtoCHAR(toConvert.get_21(), temp4); FLOATtoCHAR(toConvert.get_22(), temp5); FLOATtoCHAR(toConvert.get_23(), temp6); FLOATtoCHAR(toConvert.get_24(), temp7);
	FLOATtoCHAR(toConvert.get_31(), temp8); FLOATtoCHAR(toConvert.get_32(), temp9); FLOATtoCHAR(toConvert.get_33(), temp10); FLOATtoCHAR(toConvert.get_34(), temp11);
	FLOATtoCHAR(toConvert.get_41(), temp12); FLOATtoCHAR(toConvert.get_42(), temp13); FLOATtoCHAR(toConvert.get_43(), temp14); FLOATtoCHAR(toConvert.get_44(), temp15);

	result.append(temp); result.append(SPACe); result.append(temp1); result.append(SPACe); result.append(temp2); result.append(SPACe); result.append(temp3); result.append(SPACe);
	result.append(temp4); result.append(SPACe); result.append(temp5); result.append(SPACe); result.append(temp6); result.append(SPACe); result.append(temp7); result.append(SPACe);
	result.append(temp8); result.append(SPACe); result.append(temp9); result.append(SPACe); result.append(temp10); result.append(SPACe); result.append(temp11); result.append(SPACe);
	result.append(temp12); result.append(SPACe); result.append(temp13); result.append(SPACe); result.append(temp14); result.append(SPACe); result.append(temp15);
}

VVOID toWStringRM(const Matrix4x4& toConvert, WSTRING& result)
{
	WCHAR* SPACe = L" ";
	WCHAR temp[15]; WCHAR temp1[15]; WCHAR temp2[15]; WCHAR temp3[15];
	WCHAR temp4[15]; WCHAR temp5[15]; WCHAR temp6[15]; WCHAR temp7[15];
	WCHAR temp8[15]; WCHAR temp9[15]; WCHAR temp10[15]; WCHAR temp11[15];
	WCHAR temp12[15]; WCHAR temp13[15]; WCHAR temp14[15]; WCHAR temp15[15];

	FLOATtoWCHAR(toConvert.get_11(), temp); FLOATtoWCHAR(toConvert.get_12(), temp1); FLOATtoWCHAR(toConvert.get_13(), temp2); FLOATtoWCHAR(toConvert.get_14(), temp3);
	FLOATtoWCHAR(toConvert.get_21(), temp4); FLOATtoWCHAR(toConvert.get_22(), temp5); FLOATtoWCHAR(toConvert.get_23(), temp6); FLOATtoWCHAR(toConvert.get_24(), temp7);
	FLOATtoWCHAR(toConvert.get_31(), temp8); FLOATtoWCHAR(toConvert.get_32(), temp9); FLOATtoWCHAR(toConvert.get_33(), temp10); FLOATtoWCHAR(toConvert.get_34(), temp11);
	FLOATtoWCHAR(toConvert.get_41(), temp12); FLOATtoWCHAR(toConvert.get_42(), temp13); FLOATtoWCHAR(toConvert.get_43(), temp14); FLOATtoWCHAR(toConvert.get_44(), temp15);

	result.append(temp); result.append(SPACe); result.append(temp1); result.append(SPACe); result.append(temp2); result.append(SPACe); result.append(temp3); result.append(SPACe);
	result.append(temp4); result.append(SPACe); result.append(temp5); result.append(SPACe); result.append(temp6); result.append(SPACe); result.append(temp7); result.append(SPACe);
	result.append(temp8); result.append(SPACe); result.append(temp9); result.append(SPACe); result.append(temp10); result.append(SPACe); result.append(temp11); result.append(SPACe);
	result.append(temp12); result.append(SPACe); result.append(temp13); result.append(SPACe); result.append(temp14); result.append(SPACe); result.append(temp15);
}

/********************** from a Date struct to whatever string ******************************************/


template<class scalarType>
VVOID toString(const scalarType& toConvert, STRING& result)
{
	CCHAR temp[15];
	INTtoCHAR(toConvert, temp);
	result.append(temp);
}

template<class scalarType>
VVOID toWString(const scalarType& toConvert, WSTRING& result)
{
	WCHAR temp[15];
	INTtoWCHAR(toConvert, temp);
	result.append(temp);
}

inline VVOID toString(const FFLOAT& toConvert, STRING& result)
{
	CCHAR temp[15];
	FLOATtoCHAR(toConvert, temp);
	result.append(temp);
}

inline VVOID toWString(const FFLOAT& toConvert, WSTRING& result)
{
	WCHAR temp[15];
	FLOATtoWCHAR(toConvert, temp);
	result.append(temp);
}

template<class charType>
BIGINT count(const charType* toCount)
{
	BIGINT number = 0;

	if((toCount != NULL) && (*toCount > 0))
	{
		while(*toCount != 0)
		{
			while((*toCount != 0) && ((*toCount == ' ') || (*toCount == '\t') || (*toCount == '\r') || (*toCount == '\n')))
				toCount++;

			if(*toCount == 0)
				break;

			// new portion of char found between two blanks
			number++;

			while((*toCount != 0) && (*toCount != ' ') && (*toCount != '\t') && (*toCount != '\r') && (*toCount != '\n'))
				toCount++;
		}
	}

	return number;
}

UBIGINT xmlStrLen(const UCCHAR* toCount)
{
	UBIGINT counter = 0;

	if(!toCount)
		return 0;

	while(*toCount != 0)
	{
		toCount++;
		counter++;
	}

	return counter;
}

template<class charType>
UBIGINT computeCRC(const charType* toCount)
{
	UBIGINT crc = 0xffffffff; // 8 * 4 bits (quartet).
	
	while(*toCount != 0)
	{
		crc = (crc >> 8) ^ kCRCTable[(crc & 0xFF) ^ (UCCHAR)(*toCount++)];
	}

	return (UBIGINT)(crc ^ 0xffffffff);
}

UBIGINT computeCRC(const STRING& toCount)
{
	return computeCRC(toCount.c_str());
}

UBIGINT computeCRC(const WSTRING& toCount)
{
	return computeCRC(toCount.c_str());
}

template<class charType>
BBOOL isInt(const charType* toCheck)
{
	return true;
}

template<class charType>
BBOOL isFloat(const charType* toCheck)
{
	return true;
}

BBOOL isInt(const STRING& toCheck)
{
	return isInt(toCheck.c_str());
}

BBOOL isInt(const WSTRING& toCheck)
{
	return isInt(toCheck.c_str());
}

BBOOL isFloat(const STRING& toCheck)
{
	return isFloat(toCheck.c_str());
}

BBOOL isFloat(const WSTRING& toCheck)
{
	return isFloat(toCheck.c_str());
}

STRING fileExtension(const STRING& file)
{
	STRING temp = "";

	if(file.empty())
		return temp;

	LSSTRING res;
	splitWithDelimiter(file, res, '.');

	// set the extension for being return.
	temp = res.Last()->Data();

	return temp;
}

VVOID  splitExtension(STRING& file)
{
	if(file.empty())
		return;

	LSSTRING res;
	splitWithDelimiter(file, res, '.');

	// set the path without extension in.
	file = res.First()->Data();
}

UCCHAR* xmlStrndUp(const UCCHAR* str, UBIGINT len)
{
	UCCHAR* result;

	if(!str)
		return NULL;

	result = (UCCHAR*)malloc((len + 1) * sizeof(UCCHAR));

	if(!result)
		return NULL;

	memcpy(result, str, len * sizeof(UCCHAR));
	result[len] = 0;

	return result;
}

UCCHAR* xmlStrndUp(const CCHAR* str, UBIGINT len)
{
	UCCHAR* result;

	if(!str)
		return NULL;

	result = (UCCHAR*)malloc((len + 1) * sizeof(UCCHAR));

	if(!result)
		return NULL;

	for(UBIGINT indx = 0; indx < len; indx++)
	{
		result[indx] = (UCCHAR)str[indx];

		if(result[indx] == 0)
			return result;
	}

	result[len] = 0;

	return result;
}

UCCHAR* xmlStrdUp(const UCCHAR* str)
{
	if(!str)
		return NULL;

	const UCCHAR* tok = str;

	while(*tok != 0)
		tok++;

	return xmlStrndUp(str, (tok - str));
}

UCCHAR* xmlStrdUp(const CCHAR* str)
{
	const CCHAR* tok = str;

	if(!str)
		return NULL;

	while(*tok != '\0')
		tok++;

	return xmlStrndUp(str, (tok - str));
}

UCCHAR* xmlStrnCat(UCCHAR* char1, const UCCHAR* toAdd, UBIGINT len)
{
	UBIGINT size;
	UCCHAR* result;

	if((char1 == NULL) && (toAdd != NULL))
		memcpy(result, toAdd, xmlStrLen(toAdd));

	if((toAdd == NULL) || (len == 0))
		return char1;

	size = xmlStrLen(char1);
	result = (UCCHAR*)realloc(char1, (size + len + 1) * sizeof(UCCHAR));

	if(!result)
		return char1;

	memcpy(&result[size], toAdd, len * sizeof(UCCHAR));
	result[size + len] = 0;

	return result;
}

UCCHAR* xmlStrCat(UCCHAR* str, const UCCHAR* toAdd)
{
	if((str == NULL) && (toAdd != NULL))
		return xmlStrdUp(toAdd);

	if(!toAdd)
		return str;

	const UCCHAR* tok = toAdd;

	while(*tok != 0)
		tok++;

	return xmlStrnCat(str, toAdd, (tok - toAdd));
}

UCCHAR* xmlStrnCatNew(const UCCHAR* str1, const UCCHAR* str2, UBIGINT len)
{
	UBIGINT size = 0;
	UCCHAR* result;

	if((str1 == NULL) && (str2 != NULL))
		return xmlStrndUp(str2, len);

	if((str2 == NULL) || (len == 0))
		return xmlStrdUp(str1);

	size = xmlStrLen(str1);
	result = (UCCHAR*)malloc((size + len + 1) * sizeof(UCCHAR));

	if(!result)
		return xmlStrndUp(str1, size);

	memcpy(result, str1, size * sizeof(UCCHAR));
	memcpy(&result[size], str2, len * sizeof(UCCHAR));
	result[size + len] = 0;

	return result;
}

// case-sensitive.
BBOOL  areEqual(const CCHAR* toComp1, const CCHAR* toComp2)
{
	if(strcmp(toComp1, toComp2) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const STRING& toComp1, const CCHAR* toComp2)
{
	if(strcmp(toComp1.c_str(), toComp2) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const CCHAR* toComp1, const STRING& toComp2)
{
	if(strcmp(toComp1, toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const STRING& toComp1, const STRING& toComp2)
{
	if(strcmp(toComp1.c_str(), toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const WCHAR* toComp1, const WCHAR* toComp2)
{
	if(wcscmp(toComp1, toComp2) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const WSTRING& toComp1, const WCHAR* toComp2)
{
	if(wcscmp(toComp1.c_str(), toComp2) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const WCHAR* toComp1, const WSTRING& toComp2)
{
	if(wcscmp(toComp1, toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const WSTRING& toComp1, const WSTRING& toComp2)
{
	if(wcscmp(toComp1.c_str(), toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  operator==(const STRING& toComp1, const CCHAR* toComp2)
{
	if(strcmp(toComp1.c_str(), toComp2) != 0)
		return false;

	return true;
}

BBOOL  operator==(const CCHAR* toComp1, const STRING& toComp2)
{
	if(strcmp(toComp1, toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  operator==(const WSTRING& toComp1, const WCHAR* toComp2)
{
	if(wcscmp(toComp1.c_str(), toComp2) != 0)
		return false;

	return true;
}

BBOOL  operator==(const WCHAR* toComp1, const WSTRING& toComp2)
{
	if(wcscmp(toComp1, toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  areEqual(const UCCHAR* xmlChar, const CCHAR* toComp)
{
	return areEqual((const CCHAR*)xmlChar, toComp);
}

// case-insensitive
BBOOL  areIEqual(const CCHAR* toComp1, const CCHAR* toComp2)
{
	if(_stricmp(toComp1, toComp2) != 0)
		return false;

	return true;
}

BBOOL  areIEqual(const STRING& toComp1, const CCHAR* toComp2)
{
	if(_stricmp(toComp1.c_str(), toComp2) != 0)
		return false;

	return true;
}

BBOOL  areIEqual(const CCHAR* toComp1, const STRING& toComp2)
{
	if(_stricmp(toComp1, toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  areIEqual(const STRING& toComp1, const STRING& toComp2)
{
	if(_stricmp(toComp1.c_str(), toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  areIEqual(const WCHAR* toComp1, const WCHAR* toComp2)
{
	if(_wcsicmp(toComp1, toComp2) != 0)
		return false;

	return true;
}

BBOOL  areIEqual(const WSTRING& toComp1, const WCHAR* toComp2)
{
	if(_wcsicmp(toComp1.c_str(), toComp2) != 0)
		return false;

	return true;
}

BBOOL  areIEqual(const WCHAR* toComp1, const WSTRING& toComp2)
{
	if(_wcsicmp(toComp1, toComp2.c_str()) != 0)
		return false;

	return true;
}

BBOOL  areIEqual(const WSTRING& toComp1, const WSTRING& toComp2)
{
	if(_wcsicmp(toComp1.c_str(), toComp2.c_str()) != 0)
		return false;

	return true;
}

VVOID  xmlToString(const CCHAR* xmlChar, STRING& result)
{
	// reset the string if was not empty
	result.clear();

	CCHAR tok;
	while((tok = *xmlChar) != 0)
	{
		if(tok == '%')
		{
			// skip the % char
			xmlChar++;
			UBIGINT newVal = Hex2UInt(&xmlChar, 2);
			CCHAR temp[15];
			INTtoCHAR(newVal, temp);
			result.append(temp);
		}
		else
		{
			CCHAR temp2 = tok;
			result.append(&temp2);
			xmlChar++;
		}
	}
}

VVOID  xmlToWString(const WCHAR* xmlWChar, WSTRING& result)
{
	// reset the string if was not empty
	result.clear();

	WCHAR tok;
	while((tok = *xmlWChar) != 0)
	{
		if(tok == (WCHAR)'%')
		{
			// skip the % char
			xmlWChar++;
			UBIGINT newVal = Hex2UInt(&xmlWChar, 2);
			WCHAR temp[15];
			INTtoWCHAR(newVal, temp);
			result.append(temp);
		}
		else
		{
			WCHAR temp2 = tok;
			result.append(&temp2);
			xmlWChar++;
		}
	}
}
