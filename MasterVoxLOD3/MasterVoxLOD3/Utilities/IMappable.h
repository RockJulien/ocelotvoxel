#ifndef DEF_IMAPPABLE_H
#define DEF_IMAPPABLE_H

#include "..\DataStructures\DataStructureUtil.h"

namespace Ocelot
{
	class IMappable
	{
	public:

			// Constructor & Destructor
			IMappable(){}
	virtual ~IMappable(){}

			// Methods
	virtual VVOID map()             = 0;
	virtual VVOID unmap()           = 0;
	};
}

#endif