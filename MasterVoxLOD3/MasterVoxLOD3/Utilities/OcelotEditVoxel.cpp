#include "OcelotEditVoxel.h"

using namespace Ocelot;

OcelotEditVoxel::OcelotEditVoxel() :
IEditable(), m_bSplit(false)
{

}

OcelotEditVoxel::OcelotEditVoxel(BBOOL split) :
IEditable(), m_bSplit(split)
{

}

OcelotEditVoxel::~OcelotEditVoxel()
{

}

VVOID  OcelotEditVoxel::buildVoxel(const FFLOAT scale, const BIGINT startPosX, const BIGINT startPosY, const BIGINT startPosZ, const BIGINT endPosX, const BIGINT endPosY, const BIGINT endPosZ, BaseBlockMap* sampleBlockMap)
{
	// This process build the voxel thanks to the AABBox of the block
	// and will iterate through the block creating samples one by one
	// needed for each voxel in the block, this process is obviously
	// slower than grabbing samples from a loaded from file set of samples,
	// but is cheaper in memory.
	AABBox bBox = scaleAABBox(scale);

	BIGINT startSamplePosX = bBox.Minimum().getX() - 1;
	BIGINT startSamplePosY = bBox.Minimum().getY() - 1;
	BIGINT startSamplePosZ = bBox.Minimum().getZ() - 1;
	BIGINT endSamplePosX   = bBox.Maximum().getX() + 1;
	BIGINT endSamplePosY   = bBox.Maximum().getY() + 1;
	BIGINT endSamplePosZ   = bBox.Maximum().getZ() + 1;

	// Clamp position values in the range constrained by 
	// correspondent values passed as parameters.
	if(startSamplePosX < startPosX)
		startSamplePosX = startPosX;

	if(startSamplePosY < startPosY)
		startSamplePosY = startPosY;

	if(startSamplePosZ < startPosZ)
		startSamplePosZ = startPosZ;

	if(endSamplePosX > endPosX)
		endSamplePosX = endPosX;

	if(endSamplePosY > endPosY)
		endSamplePosY = endPosY;

	if(endSamplePosZ > endPosZ)
		endSamplePosZ = endPosZ;

	// For each axis, compute the samlpe block position in the map
	// as well as the sample value itself and store it in the BlockSampleMap.
	BIGINT startSamplePosMapX = (startSamplePosX - startPosX) - sampleBlockMap->Info().m_iSampleOffsetX;

	for(BIGINT currSamplePosX = startSamplePosX; currSamplePosX < endSamplePosX; currSamplePosX++, startSamplePosMapX++)
	{
		BIGINT startSamplePosMapY = (startSamplePosY - startPosY) - sampleBlockMap->Info().m_iSampleOffsetY;
		
		for(BIGINT currSamplePosY = startSamplePosY; currSamplePosY < endSamplePosY; currSamplePosY++, startSamplePosMapY++)
		{
			BIGINT startSamplePosMapZ = (startSamplePosZ - startPosZ) - sampleBlockMap->Info().m_iSampleOffsetZ;

			for(BIGINT currSamplePosZ = startSamplePosZ; currSamplePosZ < endSamplePosZ; currSamplePosZ++, startSamplePosMapZ++)
			{
				// Here samples are generated for composing the shape described by the edit shape
				// described by their implicit name. AABBox will be a cube from -1.0f to 1.0f.
				// Then, float make sens for a sphere where floating value will be needed between
				// this -1.0f/1.0f range for creating the spheric shape but will not exceed this 
				// range in any case.
				FFLOAT res = computeVoxel(scale, Vector3((FFLOAT)currSamplePosX, (FFLOAT)currSamplePosY, (FFLOAT)currSamplePosZ));

				// Clamp to -1 for negative values
				// and do nothing if higher.
				if(res < -1.0f)
					continue;

				// clamp to 1.0f for positive values.
				if(res > 1.0f)
					res = 1.0f;

				//CCHAR sample = (CCHAR)res;
				OcelotVoxelCorner newCorner;

				if(!m_bSplit)
				{
					newCorner.SetSample(res);
					// Store the new corner sample.
					sampleBlockMap->SetSample(newCorner, startSamplePosMapX, startSamplePosMapY, startSamplePosMapZ, m_bSplit);
			
				}
				else
				{
					newCorner.SetSample(-res);
					// Store the new corner sample.
					sampleBlockMap->SetSample(newCorner, startSamplePosMapX, startSamplePosMapY, startSamplePosMapZ, m_bSplit);
				}
			}
		}
	}
}
