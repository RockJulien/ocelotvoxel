#ifndef DEF_ILOOKUPTRIANGULATION_H
#define DEF_ILOOKUPTRIANGULATION_H

/************************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   ILookUpTriangulation.h
/
/ Description: This file provide an Interface for data structure holding
/              voxel's triangulation extracted from Look-up tables #2 and #5.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        25/09/2012
/************************************************************************************/

#include "..\DataStructures\DataStructureUtil.h"

#define DEFINDICE 255

class ILookUpTriangulation
{
public:

		// Constructor & Destructor
		ILookUpTriangulation(){}
virtual ~ILookUpTriangulation(){}

		// Methods
virtual USMALLINT GetVertexCount() const = 0;
virtual USMALLINT GetTriangleCount() const = 0;
};

#endif