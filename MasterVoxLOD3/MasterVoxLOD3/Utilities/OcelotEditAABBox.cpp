#include "OcelotEditAABBox.h"

using namespace Ocelot;

OcelotEditAABBox::OcelotEditAABBox() :
OcelotEditVoxel(), m_boundingBox()
{
	m_editShape = "AABBOX";
}

OcelotEditAABBox::OcelotEditAABBox(AABBox boundingBox, BBOOL split) :
OcelotEditVoxel(split), m_boundingBox(boundingBox)
{
	m_editShape = "AABBOX";
}

OcelotEditAABBox::~OcelotEditAABBox()
{

}

FFLOAT OcelotEditAABBox::computeVoxel(const FFLOAT& scale, const Vector3& position)
{
	// Scale the BBox regarding to the scale.
	AABBox tempBBox(BaseDXData(), (m_boundingBox.Minimum() / scale), (m_boundingBox.Maximum() / scale), AABB_GENERATOR_MINMAX);

	// If we are in the box, generate samples. Here cube shaped,
	// so create the cube by setting samples to 1.0f.
	if(tempBBox.contains(position))
		return 1.0f;

	// Else, this is like no sample to generate (Empty Space).
	// thus, not not on the surface where iso-surfaces have to be generated.
	return -2.0f;
}

AABBox OcelotEditAABBox::scaleAABBox(FFLOAT scale) const
{
	// Scale the BBox regarding to the scale.
	AABBox toReturn(BaseDXData(), (m_boundingBox.Minimum() / scale) - Vector3(1.0f), (m_boundingBox.Maximum() / scale) + Vector3(1.0f), AABB_GENERATOR_MINMAX);

	return toReturn;
}
