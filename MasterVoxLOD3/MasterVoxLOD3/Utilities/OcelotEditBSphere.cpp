#include "OcelotEditBSphere.h"

using namespace Ocelot;

OcelotEditBSphere::OcelotEditBSphere() :
OcelotEditVoxel(), m_boundingSphere()
{
	m_editShape = "BSPHERE";
}

OcelotEditBSphere::OcelotEditBSphere(BSphere boundingSphere, BBOOL split) :
OcelotEditVoxel(split), m_boundingSphere(boundingSphere)
{
	m_editShape = "BSPHERE";
}

OcelotEditBSphere::~OcelotEditBSphere()
{

}

FFLOAT OcelotEditBSphere::computeVoxel(const FFLOAT& scale, const Vector3& position)
{
	// Scale the sphere to the scale.
	BSphere tempS(BaseDXData(), (m_boundingSphere.Center() / scale), (m_boundingSphere.Radius() / scale));

	Vector3 center  = tempS.Center();
	FFLOAT radius   = tempS.Radius();
	FFLOAT toReturn = radius - center.Vec3DistanceBetween(position);//(position - tempS.Center()).Vec3Length();

	return toReturn;
}

AABBox OcelotEditBSphere::scaleAABBox(FFLOAT scale) const
{
	// First, scale the sphere to the scale.
	BSphere tempS(BaseDXData(), (m_boundingSphere.Center() / scale), (m_boundingSphere.Radius() / scale));

	// Then, pass the scaled sphere to a AABBox for
	// corresponding to a voxel world.
	Vector3 center = tempS.Center();
	FFLOAT  radius = tempS.Radius();
	AABBox toReturn(BaseDXData(), center.getX() - (FFLOAT)((BIGINT)radius + 1), 
								  center.getY() - (FFLOAT)((BIGINT)radius + 1), 
								  center.getZ() - (FFLOAT)((BIGINT)radius + 1),
								  center.getX() + (FFLOAT)((BIGINT)radius + 1), 
								  center.getY() + (FFLOAT)((BIGINT)radius + 1), 
								  center.getZ() + (FFLOAT)((BIGINT)radius + 1), 
								  AABB_GENERATOR_MINMAX);

	return toReturn;
}
