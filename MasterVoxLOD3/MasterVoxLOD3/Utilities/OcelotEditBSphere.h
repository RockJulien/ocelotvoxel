#ifndef DEF_OCELOTEDITBSPHERE_H
#define DEF_OCELOTEDITBSPHERE_H

#include "OcelotEditVoxel.h"
#include "..\Collision\BSphere.h"

namespace Ocelot
{
	class OcelotEditBSphere : public OcelotEditVoxel
	{
	private:

		// Attributes
		BSphere m_boundingSphere;

	public:

		// Constructor & Destructor
		OcelotEditBSphere();
		OcelotEditBSphere(BSphere boundingSphere, BBOOL split);
		~OcelotEditBSphere();

		// Methods
		FFLOAT computeVoxel(const FFLOAT& scale, const Vector3& position);
		AABBox scaleAABBox(FFLOAT scale) const;

		// Accessors
		inline BSphere BSPhere() /*const*/ { return m_boundingSphere;}
	};
}

#endif