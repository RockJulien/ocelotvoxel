#include "BaseBlockMap.h"

using namespace Ocelot;

BaseBlockMap::BaseBlockMap() : 
IMappable(), m_blockSamples(NULL), m_info(), m_iEmptySpaceCounter(0)
{
	
}

BaseBlockMap::BaseBlockMap(BaseBlockMapInfo info) :
IMappable(), m_blockSamples(NULL), m_info(info), m_iEmptySpaceCounter(0)
{
	
}

BaseBlockMap::~BaseBlockMap()
{
	unmap();
}

UBIGINT BaseBlockMap::GetMapPos(BIGINT posX, BIGINT posY, BIGINT posZ)
{
	UBIGINT index = 0;

	// First, add offset to axes index for going to the first minimum sample
	// of owned ones.
	posX += m_info.m_iSampleOffsetX;
	posY += m_info.m_iSampleOffsetY;
	posZ += m_info.m_iSampleOffsetZ;

	index = posX + posY * m_info.m_iSizeInSampleOnX + posZ * m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY;

	return index;
}

VVOID BaseBlockMap::map()
{
	// Will unmap only if still something in
	// the array, so user forgot to unmap, 
	// before to map (Avoid mem Leaks).
	unmap();

	// Alloc memory for the block
	UBIGINT normalMemBlockSize = m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY * m_info.m_iSizeInSampleOnZ;
	//m_blockSamples = new OcelotVoxelCorner[normalMemBlockSize];

	// Set sample values to negative by default.
	for(UBIGINT currCorner = 0; currCorner < normalMemBlockSize; currCorner++)
		//m_blockSamples[currCorner].SetSample(-1);
		//m_blockSamples[currCorner].SetSample(-1.0f);
		m_blockSamples.push_back(OcelotVoxelCorner(-1.0f));

	// Reset the counter of cell in empty space.
	m_iEmptySpaceCounter = 0;
}

/*CCHAR BaseBlockMap::GetCornerSample(BIGINT indX, BIGINT indY, BIGINT indZ)
{
	// First, add offset to axes index for going to the first minimum sample
	// of owned ones.
	indX += m_info.m_iSampleOffsetX;
	indY += m_info.m_iSampleOffsetY;
	indZ += m_info.m_iSampleOffsetZ;

	// Compute the index of a 1D array storing 3D based information.
	UBIGINT index = (UBIGINT)((indZ * m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY) + (indY * m_info.m_iSizeInSampleOnX) + indX);

	// Be sure that the index do not go over the array size.
	if(index >= (UBIGINT)(m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY * m_info.m_iSizeInSampleOnZ))
		return 127;

	return m_blockSamples[index].Sample();
}*/

FFLOAT BaseBlockMap::GetCornerSample(BIGINT indX, BIGINT indY, BIGINT indZ)
{
	// First, add offset to axes index for going to the first minimum sample
	// of owned ones.
	//indX += m_info.m_iSampleOffsetX;
	//indY += m_info.m_iSampleOffsetY;
	//indZ += m_info.m_iSampleOffsetZ;

	// Compute the index of a 1D array storing 3D based information.
	//UBIGINT index = (UBIGINT)((indZ * m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY) + (indY * m_info.m_iSizeInSampleOnX) + indX);

	// Be sure that the index do not go over the array size.
	UBIGINT index = GetMapPos(indX, indY, indZ);

	assert(index < (m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY * m_info.m_iSizeInSampleOnZ));

	return m_blockSamples[index].Sample();
}

VVOID BaseBlockMap::updateEmptySpaceCounter(const OcelotVoxelCorner& newSample, UBIGINT index)
{
	// If the sample value is in Empty space and the one beside
	// at the index position in the block sample map
	// is in Solid space, incremente the counter, else dicremente.
	if(newSample.isPositive() && m_blockSamples[index].isNegative())
		m_iEmptySpaceCounter++;
	// Because it is either one case or the other
	// else if is better than a simple if :-).
	else if(newSample.isNegative() && m_blockSamples[index].isPositive())
		m_iEmptySpaceCounter--;

	// Obviously, if this counter is homogeneously inhabited
	// by empty space and solid space number of samples,
	// it will mean that the block is either entirely in 
	// empty space or solid space, so not needed in the overall
	// mesh anymore.
}

VVOID BaseBlockMap::unmap()
{
	// Release allocated memory.
	//if(m_blockSamples)
		//DeleteArray1D(m_blockSamples);

	m_blockSamples.clear();
}

VVOID BaseBlockMap::SetSample(OcelotVoxelCorner newSample, BIGINT indX, BIGINT indY, BIGINT indZ, BBOOL split)
{
	//indX += m_info.m_iSampleOffsetX;
	//indY += m_info.m_iSampleOffsetY;
	//indZ += m_info.m_iSampleOffsetZ;

	// Compute the index of a 1D array storing 3D based information.
	//UBIGINT index = (UBIGINT)((indZ * m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY) + (indY * m_info.m_iSizeInSampleOnX) + indX);

	UBIGINT index = GetMapPos(indX, indY, indZ);

	// Check that the index is in the array range.
	assert(index < (m_info.m_iSizeInSampleOnX * m_info.m_iSizeInSampleOnY * m_info.m_iSizeInSampleOnZ));

	if(!split)
	{
		// Either in empty space if positive
		// or in solid space if strictly negative.
		if(m_blockSamples[index].Sample() < 0.0f)
		{
			// First, grab the old sample for keeping a track.
			FFLOAT oldSample = m_blockSamples[index].Sample();
			// Update the sample sign count.
			updateEmptySpaceCounter(newSample, index);
			// Store the new sample.
			m_blockSamples[index] = newSample;

			// Makes no sens, but ...
			if(oldSample >= 0.0f)
				// If Equal to zero, add the old sample and divide by 2 (Center shape).
				m_blockSamples[index].SetSample((oldSample + newSample.Sample()) / 2.0f);
		}
	}
	else 
	{
		if(m_blockSamples[index].isPositive())
		{
			// Update the sample sign count.
			updateEmptySpaceCounter(newSample, index);
			// Store the new sample.
			m_blockSamples[index] = newSample;
		}
	}
}
