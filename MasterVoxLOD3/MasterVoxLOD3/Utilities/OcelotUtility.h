#ifndef DEF_OCELOTUTILITY_H
#define DEF_OCELOTUTILITY_H

#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include "..\Maths\Utility.h"

namespace Ocelot
{
	inline VVOID splitWithDelimiter(std::vector<std::wstring>& result, const std::wstring& toSplit, const CCHAR& delimiter)
	{
		std::wstring::size_type start = 0;
		std::wstring::size_type stop  = toSplit.find(delimiter);

		while(stop != std::wstring::npos)
		{
			result.push_back(toSplit.substr(start, (stop - start)));
			start = ++stop;
			stop  = toSplit.find(delimiter, stop);

			if(stop == std::wstring::npos)
				result.push_back(toSplit.substr(start, toSplit.length()));
		}
	}

	enum Axes
	{
		XAxis,
		YAxis,
		ZAxis
	};
}

#endif