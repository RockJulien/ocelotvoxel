#ifndef DEF_BASEBLOCKMAP_H
#define DEF_BASEBLOCKMAP_H

#include <vector>
#include "IMappable.h"
#include "..\Maths\Utility.h"
#include "..\DataStructures\Array1D.h"
#include "..\Marching Cubes\OcelotVoxelCorner.h"

namespace Ocelot
{
#define MAX_SAMPLE_LAYER_NEEDED 3
#define POSITIVE_SAMPLE_LAYER   2
#define NEGATIVE_SAMPLE_LAYER   1

								struct BaseBlockMapInfo
								{
									BaseBlockMapInfo() {}
									BaseBlockMapInfo(UBIGINT sizeInSampleX, UBIGINT sizeInSampleY, UBIGINT sizeInSampleZ, 
													 BIGINT sampleOffsetX, BIGINT sampleOffsetY, BIGINT sampleOffsetZ) :
									m_iSizeInSampleOnX(sizeInSampleX), m_iSizeInSampleOnY(sizeInSampleY), m_iSizeInSampleOnZ(sizeInSampleZ),
									m_iSampleOffsetX(sampleOffsetX), m_iSampleOffsetY(sampleOffsetY), m_iSampleOffsetZ(sampleOffsetZ){}

									// Size on each axis of the block in sample as unit.
									// This size will be different on each axis for transition 
									// blocks but the same for a regular block of regular cell.
									UBIGINT                   m_iSizeInSampleOnX;
									UBIGINT                   m_iSizeInSampleOnY;
									UBIGINT                   m_iSizeInSampleOnZ;

									// Offsets on each axis working as a starting point
									// offset for separating samples owned by the block 
									// from those owned by neighboring blocks and loaded 
									// in memory.
									// Usually skip the preceding layer of sample of the
									// block on each axis, but will be different for transition
									// blocks.
									BIGINT                    m_iSampleOffsetX;
									BIGINT                    m_iSampleOffsetY;
									BIGINT                    m_iSampleOffsetZ;
								};

	class BaseBlockMap : public IMappable
	{
	protected:

				// Attributes
				// Block's container of the corner's sample.
				std::vector<OcelotVoxelCorner> m_blockSamples;

				// Block's base information.
				BaseBlockMapInfo   m_info;
		
				// Counter for samples with negative sign, so
				// in Solid space.
				UBIGINT            m_iEmptySpaceCounter;

				// Private Methods
				UBIGINT GetMapPos(BIGINT posX, BIGINT posY, BIGINT posZ);

	public:

				// Constructor & Destructor
				BaseBlockMap();
				BaseBlockMap(BaseBlockMapInfo info);
		virtual ~BaseBlockMap();

				// Methods
				VVOID  map();
				//CCHAR GetCornerSample(BIGINT indX, BIGINT indY, BIGINT indZ);
				FFLOAT GetCornerSample(BIGINT indX, BIGINT indY, BIGINT indZ);
				VVOID  updateEmptySpaceCounter(const OcelotVoxelCorner& newSample, UBIGINT index);
				VVOID  unmap();

				// Accessors
		inline  std::vector<OcelotVoxelCorner>* Samples() { return &m_blockSamples;}
		inline  BaseBlockMapInfo                Info() const { return m_info;}
		inline  UBIGINT                         EmptySpaceCounter() const { return m_iEmptySpaceCounter;}
		inline  BBOOL							NoNeedBlock() const { if(m_iEmptySpaceCounter == 0) return true; return false;}
		        VVOID							SetSample(OcelotVoxelCorner newSample, BIGINT indX, BIGINT indY, BIGINT indZ, BBOOL split);
		inline  VVOID							SetInfo(BaseBlockMapInfo info) { m_info = info;}
	};
}

#endif