#ifndef DEF_OCELOTEDITVOXEL_H
#define DEF_OCELOTEDITVOXEL_H

#include "IEditable.h"
#include "BaseBlockMap.h"

namespace Ocelot
{
	class OcelotEditVoxel : public IEditable
	{
	protected:

			// Attributes
			BBOOL  m_bSplit;
			STRING m_editShape;

	public:

			// Constructor & Destructor
			OcelotEditVoxel();
			OcelotEditVoxel(BBOOL split);
	virtual ~OcelotEditVoxel();

			// Methods
			VVOID  buildVoxel(const FFLOAT scale, const BIGINT startPosX, const BIGINT startPosY, const BIGINT startPosZ, const BIGINT endPosX, const BIGINT endPosY, const BIGINT endPosZ/*Vector3 startPosMap, Vector3 endPosMap*/, BaseBlockMap* sampleBlockMap);
	virtual FFLOAT computeVoxel(const FFLOAT& scale, const Vector3& position){ return 0.0f;}
	virtual AABBox scaleAABBox(FFLOAT scale) const { return AABBox();}

			// Accessors
	inline  BBOOL  IsSplit() const { return m_bSplit;}
	inline  STRING Shape() const { return m_editShape;}
	inline  VVOID  SetSplit(BBOOL state) { m_bSplit = state;}
	inline  VVOID  Split() { m_bSplit = true;}
	inline  VVOID  NotSplit() { m_bSplit = false;}
	};
}

#endif