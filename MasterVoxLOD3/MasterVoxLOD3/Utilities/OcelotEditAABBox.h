#ifndef DEF_OCELOTEDITAABBOX_H
#define DEF_OCELOTEDITAABBOX_H

#include "OcelotEditVoxel.h"

namespace Ocelot
{
	class OcelotEditAABBox : public OcelotEditVoxel
	{
	private:

		// Attributes
		AABBox m_boundingBox;

	public:

		// Constructor & Destructor
		OcelotEditAABBox();
		OcelotEditAABBox(AABBox boundingBox, BBOOL split);
		~OcelotEditAABBox();

		// Methods
		FFLOAT computeVoxel(const FFLOAT& scale, const Vector3& position);
		AABBox scaleAABBox(FFLOAT scale) const;

		// Accessors
		inline AABBox BBox() /*const*/ { return m_boundingBox;}
	};
}

#endif