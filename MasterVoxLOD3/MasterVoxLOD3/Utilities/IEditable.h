#ifndef DEF_IEDITABLE_H
#define DEF_IEDITABLE_H

#include "..\Collision\AABBox.h"

namespace Ocelot
{
	class IEditable
	{
	public:

			// Constructor & Destructor
			IEditable() {}
	virtual ~IEditable() {}

			// Methods
	virtual FFLOAT computeVoxel(const FFLOAT& scale, const Vector3& position) = 0;
	virtual AABBox scaleAABBox(FFLOAT scale) const = 0;
	};
}

#endif