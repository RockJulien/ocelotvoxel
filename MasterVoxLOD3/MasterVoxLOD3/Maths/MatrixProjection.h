#ifndef DEF_MATRIXPROJECTION_H
#define DEF_MATRIXPROJECTION_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   MatrixProjection.h/.cpp
/
/ Description: This file provide a way to create a 4 by 4 matrix projection
/              
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Matrix4x4.h"

class MatrixProjection : public Matrix4x4
{
private:

	// attributes of a projection matrix
	float m_fWidth;
	float m_fHeight;
	float m_fNearPlaneDist;
	float m_fFarPlaneDist;

	// extras for an off center projection
	// creating the the view frustrum.
	float m_fLeftPlaneDist;
	float m_fTopPlaneDist;
	float m_fRightPlaneDist;
	float m_fBottomPlaneDist;

	// extras for a FOV determined projection
	float m_fFieldOfView;
	float m_fRatio;

public:

	// Constructor & Destructor
	MatrixProjection();
	~MatrixProjection();

	// Methods
	/***********************Orthographic projection*********************/   // (2D Games)
	MatrixProjection MatrixProjectionOrthoLH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionOrthoRH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionOrthoExCentredLH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionOrthoExCentredRH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist);
	/***********************Perspective projection**********************/   // (3D Games)
	MatrixProjection MatrixProjectionPerspectiveLH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveRH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveFOVLH(const float& fieldOfView, const float& ratio, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveFOVRH(const float& fieldOfView, const float& ratio, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveExCentredLH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveExCentredRH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist);

	// Accessors
	float getWidth() const { return m_fWidth; }
	float getHeight() const { return m_fHeight; }
	float getNearPlaneDist() const { return m_fNearPlaneDist; }
	float getFarPlaneDist() const { return m_fFarPlaneDist; }
	float getLeftPlaneDist() const { return m_fLeftPlaneDist; }
	float getTopPlaneDist() const { return m_fTopPlaneDist; }
	float getRightPlaneDist() const { return m_fRightPlaneDist; }
	float getBottomPlaneDist() const { return m_fBottomPlaneDist; }
	float getFOV() const { return m_fFieldOfView; }
	float getRatio() const { return m_fRatio; }
	/*void  setWidth(float width);
	void  setHeight(float height);
	void  setNearPlaneDist(float dist);
	void  setFarPlaneDist(float dist);
	void  setLeftPlaneDist(float pos);
	void  setTopPlaneDist(float pos);
	void  setRightPlaneDist(float pos);     // not needed
	void  setBottomPlaneDist(float pos);
	void  setFOV(float angle);
	void  setRatio(float rat);*/
};

#endif