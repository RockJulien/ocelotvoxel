#ifndef DEF_MATRIX4X4_H
#define DEF_MATRIX4X4_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Matrix4x4.h/.cpp
/
/ Description: This file provide a way to create a 4 by 4 matrix with its
/              basic contents and extras for applying rotation or whatever
/              transformation expected for 3D Games as well.
/              It will serve for Rotation, Translation, Scaling a object in
/              a 3D world and could eb used as simple matrix.
/              The possibility to use quaternion for applying rotations has 
/              been added.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

//#include "Utility.h"
#include "Quaternion.h"
#include "Vector3.h"
#include "Vector4.h"
//#include "Matrix3x3.h"

class Vector3;
class Quaternion;

class Matrix4x4
{
protected:

	// attributes representing the matrix
	float m_f11, m_f12, m_f13, m_f14;
	float m_f21, m_f22, m_f23, m_f24;
	float m_f31, m_f32, m_f33, m_f34;
	float m_f41, m_f42, m_f43, m_f44;

public:

	// Constructors & Destructor
	Matrix4x4();											  // default cst
	Matrix4x4(const Matrix4x4& mat);                          // copy cst
	Matrix4x4& operator = (const Matrix4x4&);                 // assgm cst
	Matrix4x4(float val);                                     // overload cst
	Matrix4x4(double val);						              // overload cst
	Matrix4x4(float _11, float _12, float _13, float _14,     // overload cst
			  float _21, float _22, float _23, float _24,
			  float _31, float _32, float _33, float _34,
			  float _41, float _42, float _43, float _44);
	Matrix4x4(double _11, double _12, double _13, double _14, // overload cst
		      double _21, double _22, double _23, double _24,
			  double _31, double _32, double _33, double _34,
			  double _41, double _42, double _43, double _44);
	~Matrix4x4();

	// Methods
	void      Matrix4x4Identity();					// create an identity matrix
	bool      Matrix4x4IsIdentity();				// check if the matrix is an identity one
	float     Matrix4x4Determinant();				// compute the determinant of the matrix
	void      Matrix4x4Transpose(Matrix4x4& out);	// transpose the matrix
	void      Matrix4x4Inverse(Matrix4x4& out);		// compute the inverse of the matrix
	void      Matrix4x4Mutliply(Matrix4x4& out, const Matrix4x4& mat);
	void      Matrix4x4Mutliply(Matrix4x4& out, const Matrix4x4& mat1, const Matrix4x4& mat2);
	void      Matrix4x4MultiplyTranspose(Matrix4x4& out, const Matrix4x4& mat);
	void      Matrix4x4MultiplyTranspose(Matrix4x4& out, const Matrix4x4& mat1, const Matrix4x4& mat2);
	void      Matrix4x4Translation(Matrix4x4& out, const float& Txyz);
	void      Matrix4x4Translation(Matrix4x4& out, const double& Txyz);
	void      Matrix4x4Translation(Matrix4x4& out, const float& Tx, const float& Ty, const float& Tz);
	void      Matrix4x4Translation(Matrix4x4& out, const double& Tx, const double& Ty, const double& Tz);
	void      Matrix4x4Scaling(Matrix4x4& out, const float& Sxyz);
	void      Matrix4x4Scaling(Matrix4x4& out, const double& Sxyz);
	void      Matrix4x4Scaling(Matrix4x4& out, const float& Sx, const float& Sy, const float& Sz);
	void      Matrix4x4Scaling(Matrix4x4& out, const double& Sx, const double& Sy, const double& Sz);
	void      Matrix4x4RotationX(Matrix4x4& out, const float& Rx);
	void      Matrix4x4RotationX(Matrix4x4& out, const double& Rx);
	void      Matrix4x4RotationY(Matrix4x4& out, const float& Ry);
	void      Matrix4x4RotationY(Matrix4x4& out, const double& Ry);
	void      Matrix4x4RotationZ(Matrix4x4& out, const float& Rz);
	void      Matrix4x4RotationZ(Matrix4x4& out, const double& Rz);
	void      Matrix4x4RotationAxis(Matrix4x4& out, Vector3 axis, float angle);
	void      Matrix4x4RotationGeneralized(Matrix4x4& out, const float& Rxyz);
	void      Matrix4x4RotationGeneralized(Matrix4x4& out, const double& Rxyz);
	void      Matrix4x4RotationGeneralized(Matrix4x4& out, const float& Rx, const float& Ry, const float& Rz);
	void      Matrix4x4RotationGeneralized(Matrix4x4& out, const double& Rx, const double& Ry, const double& Rz);
	void      Matrix4x4RotationQuaternion(Matrix4x4& out, Quaternion q);
	Vector4	  Transform(const Vector4& v);

	// Accessors
	inline float get_11() const { return m_f11; }
	inline float get_12() const { return m_f12; }
	inline float get_13() const { return m_f13; }
	inline float get_14() const { return m_f14; }
	inline float get_21() const { return m_f21; }
	inline float get_22() const { return m_f22; }
	inline float get_23() const { return m_f23; }
	inline float get_24() const { return m_f24; }
	inline float get_31() const { return m_f31; }
	inline float get_32() const { return m_f32; }
	inline float get_33() const { return m_f33; }
	inline float get_34() const { return m_f34; }
	inline float get_41() const { return m_f41; }
	inline float get_42() const { return m_f42; }
	inline float get_43() const { return m_f43; }
	inline float get_44() const { return m_f44; }
	inline void  set_11(float _11) { m_f11 = _11; }
	inline void  set_12(float _12) { m_f12 = _12; }
	inline void  set_13(float _13) { m_f13 = _13; }
	inline void  set_14(float _14) { m_f14 = _14; }
	inline void  set_21(float _21) { m_f21 = _21; }
	inline void  set_22(float _22) { m_f22 = _22; }
	inline void  set_23(float _23) { m_f23 = _23; }
	inline void  set_24(float _24) { m_f24 = _24; }
	inline void  set_31(float _31) { m_f31 = _31; }
	inline void  set_32(float _32) { m_f32 = _32; }
	inline void  set_33(float _33) { m_f33 = _33; }
	inline void  set_34(float _34) { m_f34 = _34; }
	inline void  set_41(float _41) { m_f41 = _41; }
	inline void  set_42(float _42) { m_f42 = _42; }
	inline void  set_43(float _43) { m_f43 = _43; }
	inline void  set_44(float _44) { m_f44 = _44; }

	// Operators overload
	/*********************Access Operats**************************/
	float& operator () (int line, int col);
	float  operator () (int line, int col) const;
	/*********************Cast Operators**************************/
	operator float* ();              // for being able to pass it through the shader as a (float*)
	operator const float* () const;  // constant version
	/*********************Unary Operators*************************/
	Matrix4x4 operator + () const;
	Matrix4x4 operator - () const;
	/*********************Assgm Operators*************************/
	Matrix4x4& operator += (const Matrix4x4&);
	Matrix4x4& operator -= (const Matrix4x4&);
	Matrix4x4& operator *= (const Matrix4x4&);
	Matrix4x4& operator *= (const float& val);
	Matrix4x4& operator /= (const float& val);
	/*********************Binary Operators************************/
	Matrix4x4 operator + (const Matrix4x4&) const;
	Matrix4x4 operator - (const Matrix4x4&) const;
	Matrix4x4 operator * (const Matrix4x4&) const;
	Matrix4x4 operator * (const float& val) const;
	Matrix4x4 operator / (const float& val) const;
	bool operator == (const Matrix4x4&) const;
	bool operator != (const Matrix4x4&) const;
	friend Matrix4x4 operator * (float, const Matrix4x4&);
};

#endif