#ifndef DEF_MATRIX3X3_H
#define DEF_MATRIX3X3_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Matrix3x3.h/.cpp
/
/ Description: This file provide a way to create a 3 by 3 matrix with its
/              basic contents and extras for applying rotation or whatever
/              transformation expected for 2D Games as well.
/              It will serve for determinant computation for inverse 
/              calculation of a 4 by 4 matrix (by subdivision in 3 by 3 ones) 
/              and could be used as simple matrix as well.
/              The possibility to use quaternion for applying rotations has 
/              been added even if rotation is not really possible in every
/              direction.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

//#include "Utility.h"
#include "Matrix2x2.h"
#include "Quaternion.h"

class Quaternion;
class Vector3;

class Matrix3x3
{
private:

	// variables which represent the matrix
	float m_f11, m_f12, m_f13;
	float m_f21, m_f22, m_f23;
	float m_f31, m_f32, m_f33;

public:

	// Constructors & Destructor
	Matrix3x3();                                   // default cst
	Matrix3x3(const Matrix3x3& mat);               // copy cst
	Matrix3x3& operator = (const Matrix3x3&);      // assgm cst
	Matrix3x3(float val);                          // overload cst
	Matrix3x3(double val);						   // overload cst
	Matrix3x3(float _11, float _12, float _13,     // overload cst
			  float _21, float _22, float _23,
			  float _31, float _32, float _33);
	Matrix3x3(double _11, double _12, double _13,  // overload cst
		      double _21, double _22, double _23,
			  double _31, double _32, double _33);
	~Matrix3x3();

	// Methods
	Vector3	  Matrix3x3Transform(const Vector3 &v) const;
	void      Matrix3x3Identity();					// create an identity matrix
	bool      Matrix3x3IsIdentity();				// check if the matrix is an identity one
	float     Matrix3x3Determinant();				// compute the determinant of the matrix
	void	  Matrix3x3Transpose(Matrix3x3 &mat);	// transpose the matrix
	void	  Matrix3x3Inverse(Matrix3x3 &mat);		// compute the inverse of the matrix
	void      Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat);
	void      Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2);
	void      Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat);
	void      Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2);
	void      Matrix3x3Translation(Matrix3x3& out, const float& Txy);
	void      Matrix3x3Translation(Matrix3x3& out, const double& Txy);
	void      Matrix3x3Translation(Matrix3x3& out, const float& Tx, const float& Ty);
	void      Matrix3x3Translation(Matrix3x3& out, const double& Tx, const double& Ty);
	void      Matrix3x3Scaling(Matrix3x3& out, const float& Sxy);
	void      Matrix3x3Scaling(Matrix3x3& out, const double& Sxy);
	void      Matrix3x3Scaling(Matrix3x3& out, const float& Sx, const float& Sy);
	void      Matrix3x3Scaling(Matrix3x3& out, const double& Sx, const double& Sy);
	void      Matrix3x3RotationX(Matrix3x3& out, const float& Rx);
	void      Matrix3x3RotationX(Matrix3x3& out, const double& Rx);
	void      Matrix3x3RotationY(Matrix3x3& out, const float& Ry);
	void      Matrix3x3RotationY(Matrix3x3& out, const double& Ry);
	void      Matrix3x3RotationZ(Matrix3x3& out, const float& Rz);
	void      Matrix3x3RotationZ(Matrix3x3& out, const double& Rz);
	void      Matrix3x3RotationGeneralized(Matrix3x3& out, const float& Rxyz);
	void      Matrix3x3RotationGeneralized(Matrix3x3& out, const double& Rxyz);
	void      Matrix3x3RotationGeneralized(Matrix3x3& out, const float& Rx, const float& Ry, const float& Rz);
	void      Matrix3x3RotationGeneralized(Matrix3x3& out, const double& Rx, const double& Ry, const double& Rz);
	void      Matrix3x3RotationQuaternion(Matrix3x3& out, Quaternion q);
	void	  Matrix3x3SetOrientation(Quaternion& q);
	void	  Matrix3x3SetBasis(const Vector3& one, const Vector3& two, const Vector3& three);
	Vector3	  Matrix3x3TransformTranspose(const Vector3& v);
	void	  Matrix3x3SetSkewSymetric(const Vector3& v);
	// Accessors
	inline float get_11() const { return m_f11; }
	inline float get_12() const { return m_f12; }
	inline float get_13() const { return m_f13; }
	inline float get_21() const { return m_f21; }
	inline float get_22() const { return m_f22; }
	inline float get_23() const { return m_f23; }
	inline float get_31() const { return m_f31; }
	inline float get_32() const { return m_f32; }
	inline float get_33() const { return m_f33; }
	inline void  set_11(float _11) { m_f11 = _11; }
	inline void  set_12(float _12) { m_f12 = _12; }
	inline void  set_13(float _13) { m_f13 = _13; }
	inline void  set_21(float _21) { m_f21 = _21; }
	inline void  set_22(float _22) { m_f22 = _22; }
	inline void  set_23(float _23) { m_f23 = _23; }
	inline void  set_31(float _31) { m_f31 = _31; }
	inline void  set_32(float _32) { m_f32 = _32; }
	inline void  set_33(float _33) { m_f33 = _33; }

	// Operators overload
	/*********************Access Operats**************************/
	float& operator () (int line, int col);
	float  operator () (int line, int col) const;
	/*********************Cast Operators**************************/
	operator float* ();              // for being able to pass it through the shader as a (float*)
	operator const float* () const;  // constant version
	/*********************Unary Operators*************************/
	Matrix3x3 operator + () const;
	Matrix3x3 operator - () const;
	/*********************Assgm Operators*************************/
	Matrix3x3& operator += (const Matrix3x3&);
	Matrix3x3& operator -= (const Matrix3x3&);
	Matrix3x3& operator *= (const Matrix3x3&);
	Matrix3x3& operator *= (const float& val);
	Matrix3x3& operator /= (const float& val);
	/*********************Binary Operators************************/
	Matrix3x3 operator + (const Matrix3x3&) const;
	Matrix3x3 operator - (const Matrix3x3&) const;
	Matrix3x3 operator * (const Matrix3x3&) const;
	Matrix3x3 operator * (const float& val) const;
	Matrix3x3 operator / (const float& val) const;
	bool operator == (const Matrix3x3&) const;
	bool operator != (const Matrix3x3&) const;
	friend Matrix3x3 operator * (float, const Matrix3x3&);
};

#endif