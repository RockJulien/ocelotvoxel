#include "Quaternion.h"

Quaternion::Quaternion() :
m_fX(0.0f), m_fY(0.0f), m_fZ(0.0f), m_fW(0.0f)
{
	QuaternionIdentity();
}

Quaternion::Quaternion(const Quaternion& q) :
m_fX(q.m_fX), m_fY(q.m_fY), m_fZ(q.m_fZ), m_fW(q.m_fW)
{

}

Quaternion& Quaternion::operator = (const Quaternion& q)
{
	m_fX = q.m_fX;
	m_fY = q.m_fY;
	m_fZ = q.m_fZ;
	m_fW = q.m_fW;

	return *this;
}

Quaternion::Quaternion(float xyzw) :
m_fX(xyzw), m_fY(xyzw), m_fZ(xyzw), m_fW(xyzw)
{

}

Quaternion::Quaternion(double xyzw) :
m_fX((float)xyzw), m_fY((float)xyzw), m_fZ((float)xyzw), m_fW((float)xyzw)
{

}

Quaternion::Quaternion(float x, float y, float z, float w) :
m_fX(x), m_fY(y), m_fZ(z), m_fW(w)
{

}

Quaternion::Quaternion(double x, double y, double z, double w) :
m_fX((float)x), m_fY((float)y), m_fZ((float)z), m_fW((float)w)
{

}

// constructors for constructing a quaternion from euler angles
Quaternion::Quaternion(float headingAngle, float attitudeAngle, float bankAngle)
{
	// compute cosine and sine result in radian.
	float angle = headingAngle * 0.5f;
	float CosRow   = cos(angle);
	float SinRow   = sin(angle);

	angle = attitudeAngle * 0.5f;
	float CosPitch = cos(angle);
	float SinPitch = sin(angle);

	angle = bankAngle * 0.5f;
	float CosYaw   = cos(angle);
	float SinYaw   = sin(angle);

	// limit number of calculation just for the CPU
	float CosRowPitch = CosRow * CosPitch;
	float SinRowPitch = SinRow * SinPitch;

	// then create the quaternion
	m_fX = (CosRowPitch * SinYaw) + (SinRowPitch * CosYaw);
	m_fY = (SinRow * CosPitch * CosYaw) + (CosRow * SinPitch * SinYaw);
	m_fZ = (CosRow * SinPitch * CosYaw) - (SinRow * CosPitch * SinYaw);
	m_fW = (CosRowPitch * CosYaw) - (SinRowPitch * SinYaw);
}

Quaternion::Quaternion(double headingAngle, double attitudeAngle, double bankAngle)
{
	// compute cosine and sine result in radian.
	float angle = (float)headingAngle * 0.5f;
	float CosRow   = cos(angle);
	float SinRow   = sin(angle);

	angle = (float)attitudeAngle * 0.5f;
	float CosPitch = cos(angle);
	float SinPitch = sin(angle);

	angle = (float)bankAngle * 0.5f;
	float CosYaw   = cos(angle);
	float SinYaw   = sin(angle);

	// limit number of calculation just for the CPU
	float CosRowPitch = CosRow * CosPitch;
	float SinRowPitch = SinRow * SinPitch;

	// then create the quaternion
	m_fX = (CosRowPitch * SinYaw) + (SinRowPitch * CosYaw);
	m_fY = (SinRow * CosPitch * CosYaw) + (CosRow * SinPitch * SinYaw);
	m_fZ = (CosRow * SinPitch * CosYaw) - (SinRow * CosPitch * SinYaw);
	m_fW = (CosRowPitch * CosYaw) - (SinRowPitch * SinYaw);
}

Quaternion::Quaternion(const Vector3& vec)
{
	// constructor which convert euler angle to quaternion
	// by simply take back X, Y and Z components from the 
	// vector and assign them to same named components of 
	// a quaternion.

	// compute cosine and sine result in radian.
	float angle = vec.getX() * 0.5f;
	float CosRow   = cos(angle);
	float SinRow   = sin(angle);

	angle = vec.getY() * 0.5f;
	float CosPitch = cos(angle);
	float SinPitch = sin(angle);

	angle = vec.getZ() * 0.5f;
	float CosYaw   = cos(angle);
	float SinYaw   = sin(angle);

	// limit number of calculation just for the CPU
	float CosRowPitch = CosRow * CosPitch;
	float SinRowPitch = SinRow * SinPitch;

	// then create the quaternion
	m_fX = (CosRowPitch * SinYaw) + (SinRowPitch * CosYaw);
	m_fY = (SinRow * CosPitch * CosYaw) + (CosRow * SinPitch * SinYaw);
	m_fZ = (CosRow * SinPitch * CosYaw) - (SinRow * CosPitch * SinYaw);
	m_fW = (CosRowPitch * CosYaw) - (SinRowPitch * SinYaw);
}

Quaternion::Quaternion(const Matrix4x4& mat)
{
	float sumDiag = mat.get_11() + mat.get_22() + mat.get_33();

	if(sumDiag > 0.0f)
	{
		// Scale = 4 * m_fW of the quaternion
		float scale = sqrt(sumDiag + 1.0f) * 2.0f;

		m_fX = (mat.get_32() - mat.get_23()) / scale;
		m_fY = (mat.get_13() - mat.get_31()) / scale;
		m_fZ = (mat.get_21() - mat.get_12()) / scale;
		m_fW = 0.25f * scale;
	}
	else if(mat.get_11() > mat.get_22() && mat.get_11() > mat.get_33())
	{
		// scale = 4 * m_fX of the quaternion
		float scale = sqrt(mat.get_11() - mat.get_22() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = 0.25f * scale;
		m_fY = (mat.get_12() + mat.get_21()) / scale;
		m_fZ = (mat.get_13() + mat.get_31()) / scale;
		m_fW = (mat.get_32() - mat.get_23()) / scale;
	}
	else if(mat.get_22() > mat.get_33())
	{
		// Scale = 4 * m_fY of the quaternion
		float scale = sqrt(mat.get_22() - mat.get_11() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = (mat.get_12() + mat.get_21()) / scale;
		m_fY = 0.25f * scale;
		m_fZ = (mat.get_23() + mat.get_32()) / scale;
		m_fW = (mat.get_13() - mat.get_31()) / scale;
	}
	else
	{
		// Scale = 4 * m_fZ of the quaternion
		float scale = sqrt(mat.get_33() - mat.get_11() - mat.get_22() + 1.0f) * 2.0f;

		m_fX = (mat.get_13() + mat.get_31()) / scale;
		m_fY = (mat.get_23() + mat.get_32()) / scale;
		m_fZ = 0.25f * scale;
		m_fW = (mat.get_21() - mat.get_12()) / scale;
	}

	// don't forget to normalize if I didn't add the function here.
}

Quaternion::Quaternion(const Matrix3x3& mat)
{
	float sumDiag = mat.get_11() + mat.get_22() + mat.get_33();

	if(sumDiag > 0.0f)
	{
		// Scale = 4 * m_fW of the quaternion
		float scale = sqrt(sumDiag + 1.0f) * 2.0f;

		m_fX = (mat.get_32() - mat.get_23()) / scale;
		m_fY = (mat.get_13() - mat.get_31()) / scale;
		m_fZ = (mat.get_21() - mat.get_12()) / scale;
		m_fW = 0.25f * scale;
	}
	else if(mat.get_11() > mat.get_22() && mat.get_11() > mat.get_33())
	{
		// scale = 4 * m_fX of the quaternion
		float scale = sqrt(mat.get_11() - mat.get_22() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = 0.25f * scale;
		m_fY = (mat.get_12() + mat.get_21()) / scale;
		m_fZ = (mat.get_13() + mat.get_31()) / scale;
		m_fW = (mat.get_32() - mat.get_23()) / scale;
	}
	else if(mat.get_22() > mat.get_33())
	{
		// Scale = 4 * m_fY of the quaternion
		float scale = sqrt(mat.get_22() - mat.get_11() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = (mat.get_12() + mat.get_21()) / scale;
		m_fY = 0.25f * scale;
		m_fZ = (mat.get_23() + mat.get_32()) / scale;
		m_fW = (mat.get_13() - mat.get_31()) / scale;
	}
	else
	{
		// Scale = 4 * m_fZ of the quaternion
		float scale = sqrt(mat.get_33() - mat.get_11() - mat.get_22() + 1.0f) * 2.0f;

		m_fX = (mat.get_13() + mat.get_31()) / scale;
		m_fY = (mat.get_23() + mat.get_32()) / scale;
		m_fZ = 0.25f * scale;
		m_fW = (mat.get_21() - mat.get_12()) / scale;
	}

	// don't forget to normalize if I didn't add the function here.
}

Quaternion::~Quaternion()
{

}

Quaternion  Quaternion::QuaternionIdentity()
{
	// set the real part of the quaternion to 1.
	m_fX = 0.0f;
	m_fY = 0.0f;
	m_fZ = 0.0f;
	m_fW = 1.0f;

	return *this;
}

float      Quaternion::QuaternionLength()
{
	// compute the length of the quaternion
	return sqrt((SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ)) + (SQUARE(m_fW)));
}

float      Quaternion::QuaternionLengthSqrt()
{
	// compute the length square of the quaternion
	return (SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ)) + (SQUARE(m_fW));
}

Quaternion Quaternion::QuaternionNormalize()
{
	// Normalize the quaternion (same as vectors)
	float magnitude = QuaternionLength();

	if(fabsf(magnitude) > FLT_EPSILON)  // prevent from dividing by 0
	{
		magnitude = sqrt(magnitude);
		this->m_fX /= magnitude;
		this->m_fY /= magnitude;
		this->m_fZ /= magnitude;
		this->m_fW /= magnitude;
	}

	return Quaternion(m_fX, m_fY, m_fZ, m_fW);
}

Quaternion Quaternion::QuaternionInverse()
{
	// compute the inverse of the quaternion
	Quaternion temp(*this);

	float magnitudeSqrt = temp.QuaternionLengthSqrt();

	// same as the conjuguate but by dividing each 
	// component by the magnitude square.
	m_fX = -temp.m_fX / magnitudeSqrt;
	m_fY = -temp.m_fY / magnitudeSqrt;
	m_fZ = -temp.m_fZ / magnitudeSqrt;
	m_fW = temp.m_fW / magnitudeSqrt;

	return *this;
}

Quaternion Quaternion::QuaternionConjuguate()
{
	// compute the conjuguate of the quaternion
	// just insert opposite of the imaginary part.
	return Quaternion(-m_fX, -m_fY, -m_fZ, m_fW);
}

Quaternion Quaternion::QuaternionExponantial()
{
	// compute the exponential of the quaternion
	// First compute the absolute of the imaginary part
	float absImagPart = sqrt(SQUARE(m_fX) + SQUARE(m_fY) + SQUARE(m_fZ));

	// then, compute the real and imaginary part of the new quaternion
	float realPart  = exp(m_fW) * cos(absImagPart);
	float imagPartX = exp(m_fW) * ((m_fX/absImagPart) * sin(absImagPart));
	float imagPartY = exp(m_fW) * ((m_fY/absImagPart) * sin(absImagPart));
	float imagPartZ = exp(m_fW) * ((m_fZ/absImagPart) * sin(absImagPart));

	return Quaternion(imagPartX, imagPartY, imagPartZ, realPart);
}

Quaternion Quaternion::QuaternionLogarithm()
{
	// compute the logarithm of the quaternion.
	// First compute the absolute of the imaginary part
	float absImagPart = sqrt(SQUARE(m_fX) + SQUARE(m_fY) + SQUARE(m_fZ));

	// then, compute the real and imaginary part of the new quaternion
	float realPart  = log(QuaternionLength());
	float imagPartX = ((m_fX/absImagPart) * acos(m_fW/QuaternionLength()));
	float imagPartY = ((m_fY/absImagPart) * acos(m_fW/QuaternionLength()));
	float imagPartZ = ((m_fZ/absImagPart) * acos(m_fW/QuaternionLength()));

	return Quaternion(imagPartX, imagPartY, imagPartZ, realPart);
}

Quaternion Quaternion::QuaternionPowerScalar(const float& power)
{
	// compute the quaternion at the scalar power in parameter.
	return (QuaternionLogarithm() * power).QuaternionExponantial();
}

Quaternion Quaternion::QuaternionPowerQuaternion(const Quaternion& q)
{
	// compute the quaternion at the quaternion power in parameter.
	return (QuaternionLogarithm() * q).QuaternionExponantial();
}

float      Quaternion::QuaternionDotProduct(const Quaternion& q)
{
	// apply a dot product to the quaternion
	return ((m_fX * q.m_fX) + (m_fY * q.m_fY) + (m_fZ * q.m_fZ) + (m_fW * q.m_fW));
}

Vector3    Quaternion::QuaternionToVectorTransformAxis(const Vector3& axis)
{
	Vector3 newAxis;
	Matrix4x4 rotat;

	// create a matrix from the quaternion
	rotat = QuaternionToMatrix4Normalized();

	// then, transform the axis by the matrix
	newAxis.x((axis.getX() * rotat.get_11()) + (axis.getY() * rotat.get_21()) + (axis.getZ() * rotat.get_31()) + rotat.get_41());
	newAxis.y((axis.getX() * rotat.get_12()) + (axis.getY() * rotat.get_22()) + (axis.getZ() * rotat.get_32()) + rotat.get_42());
	newAxis.z((axis.getX() * rotat.get_13()) + (axis.getY() * rotat.get_23()) + (axis.getZ() * rotat.get_33()) + rotat.get_43());

	return newAxis;
}

Vector3    Quaternion::QuaternionToEulerNoNormalized()
{
	// for lazy programmer which were sleeping before the TV 
	// and so forgot to normalize the quaternion.
	// It will work thanks to this function.
	float unit    = QuaternionLengthSqrt();
	float checker = (m_fX * m_fY) + (m_fZ * m_fW);  // will test singularity points.

	if(checker > (0.499f * unit))        // singularity at north pole of the hyper sphere.
	{
		float Heading  = 2.0f * atan2(m_fX, m_fW);
		float Attitude = PI * 0.5f;
		float Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}
	else if(checker < (-0.499f * unit))  // singularity at the south of the hyper sphere
	{
		float Heading  = -2.0f * atan2(m_fX, m_fW);
		float Attitude = -PI * 0.5f;
		float Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}

	// else
	float Xsqrt = SQUARE(m_fX);
	float Ysqrt = SQUARE(m_fY);
	float Zsqrt = SQUARE(m_fZ);
	float Wsqrt = SQUARE(m_fW);

	float Heading  = atan2((2.0f * m_fY * m_fW) - (2.0f * m_fX * m_fZ), Xsqrt - Ysqrt - Zsqrt + Wsqrt);
	float Attitude = asin(2.0f * checker/unit);
	float Bank     = atan2((2.0f * m_fX * m_fW) - (2.0f * m_fY * m_fZ), -Xsqrt + Ysqrt - Zsqrt + Wsqrt);

	return Vector3(Heading, Attitude, Bank);
}

Vector3    Quaternion::QuaternionToEulerNormalized()
{
	// we assume in this one that the quaternion is normalized.
	float checker = (m_fX * m_fY) + (m_fZ * m_fW);  // will test singularity points.

	if(checker > 0.499f)        // singularity at north pole of the hyper sphere.
	{
		float Heading  = 2.0f * atan2(m_fX, m_fW);
		float Attitude = PI * 0.5f;
		float Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}
	else if(checker < -0.499f)  // singularity at the south of the hyper sphere
	{
		float Heading  = -2.0f * atan2(m_fX, m_fW);
		float Attitude = -PI * 0.5f;
		float Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}

	// else
	float Xsqrt = SQUARE(m_fX);
	float Ysqrt = SQUARE(m_fY);
	float Zsqrt = SQUARE(m_fZ);

	float Heading  = atan2((2.0f * m_fY * m_fW) - (2.0f * m_fX * m_fZ), 1.0f - (2.0f * Ysqrt) - (2.0f * Zsqrt));
	float Attitude = asin(2.0f * checker);
	float Bank     = atan2((2.0f * m_fX * m_fW) - (2.0f * m_fY * m_fZ), 1.0f - (2.0f * Xsqrt) - (2.0f * Zsqrt));

	return Vector3(Heading, Attitude, Bank);
}

Matrix3x3  Quaternion::QuaternionToMatrix3NoNormalized()
{
	// case where the quaternion is not normalized
	// convertion of a quaternion to a matrix 3x3.
	float Xsqrt = SQUARE(m_fX);
	float Ysqrt = SQUARE(m_fY);
	float Zsqrt = SQUARE(m_fZ);
	float Wsqrt = SQUARE(m_fW);

	// compute the inverse square length because of the non normalization
	// of the quaternion.
	// this case looks better because the sqrt root is not computed
	// (save process time)
	float invSqrtLen = 1.0f / QuaternionLengthSqrt();

	// then create the matrix
	Matrix3x3 QuatRotMatrix;

	// do not need to create an identity one because particular
	// value will be in the main diagonal. (save process time)
	QuatRotMatrix.set_11((Xsqrt - Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_22((-Xsqrt + Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_33((-Xsqrt - Ysqrt + Zsqrt + Wsqrt) * invSqrtLen);

	float mulXY = m_fX * m_fY;
	float mulZW = m_fZ * m_fW;
	QuatRotMatrix.set_21((2.0f * (mulXY + mulZW)) * invSqrtLen);
	QuatRotMatrix.set_12((2.0f * (mulXY - mulZW)) * invSqrtLen);

	float mulXZ = m_fX * m_fZ;
	float mulYW = m_fY * m_fW;
	QuatRotMatrix.set_31((2.0f * (mulXZ - mulYW)) * invSqrtLen);
	QuatRotMatrix.set_13((2.0f * (mulXZ + mulYW)) * invSqrtLen);

	float mulYZ = m_fY * m_fZ;
	float mulXW = m_fX * m_fW;
	QuatRotMatrix.set_32((2.0f * (mulYZ + mulXW)) * invSqrtLen);
	QuatRotMatrix.set_23((2.0f * (mulYZ - mulXW)) * invSqrtLen);

	return QuatRotMatrix;
}

Matrix3x3  Quaternion::QuaternionToMatrix3Normalized()
{
	// we assume that the quaternion is normalized
	Matrix3x3 QuatRotMatrix;

	// useful precalculation (save process time)
	float Xsqrt = SQUARE(m_fX);
	float Ysqrt = SQUARE(m_fY);
	float Zsqrt = SQUARE(m_fZ);
	float Wsqrt = SQUARE(m_fW);
	float mulXY = m_fX * m_fY;
	float mulXZ = m_fX * m_fZ;
	float mulXW = m_fX * m_fW;
	float mulYZ = m_fY * m_fZ;
	float mulYW = m_fY * m_fW;
	float mulZW = m_fZ * m_fW;

	// now let's fill the matrix
	/*******************First row*************************/
	QuatRotMatrix.set_11(1.0f - (2.0f * (Ysqrt + Zsqrt)));
	QuatRotMatrix.set_12(2.0f * (mulXY - mulZW));
	QuatRotMatrix.set_13(2.0f * (mulXZ + mulYW));
	/*******************Second Row************************/
	QuatRotMatrix.set_21(2.0f * (mulXY + mulZW));
	QuatRotMatrix.set_22(1.0f - (2.0f * (Xsqrt + Zsqrt)));
	QuatRotMatrix.set_23(2.0f * (mulYZ - mulXW));
	/*******************Third row*************************/
	QuatRotMatrix.set_31(2.0f * (mulXZ - mulYW));
	QuatRotMatrix.set_32(2.0f * (mulYZ + mulXW));
	QuatRotMatrix.set_33(1.0f - (2.0f * (Xsqrt + Ysqrt)));

	return QuatRotMatrix;
}

Matrix4x4  Quaternion::QuaternionToMatrix4NoNormalized()
{
	// case where the quaternion is not normalized
	// convertion of a quaternion to a matrix 4x4.
	float Xsqrt = SQUARE(m_fX);
	float Ysqrt = SQUARE(m_fY);
	float Zsqrt = SQUARE(m_fZ);
	float Wsqrt = SQUARE(m_fW);

	// compute the inverse square length because of the non normalization
	// of the quaternion.
	// this case looks better because the sqrt root is not computed
	// (save process time)
	float invSqrtLen = 1.0f / QuaternionLengthSqrt();

	// then create the matrix
	Matrix4x4 QuatRotMatrix;

	// do not need to create an identity one because particular
	// value will be in the main diagonal. (save process time)
	QuatRotMatrix.set_11((Xsqrt - Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_22((-Xsqrt + Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_33((-Xsqrt - Ysqrt + Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_44(1.0f);

	float mulXY = m_fX * m_fY;
	float mulZW = m_fZ * m_fW;
	QuatRotMatrix.set_21((2.0f * (mulXY + mulZW)) * invSqrtLen);
	QuatRotMatrix.set_12((2.0f * (mulXY - mulZW)) * invSqrtLen);

	float mulXZ = m_fX * m_fZ;
	float mulYW = m_fY * m_fW;
	QuatRotMatrix.set_31((2.0f * (mulXZ - mulYW)) * invSqrtLen);
	QuatRotMatrix.set_13((2.0f * (mulXZ + mulYW)) * invSqrtLen);

	float mulYZ = m_fY * m_fZ;
	float mulXW = m_fX * m_fW;
	QuatRotMatrix.set_32((2.0f * (mulYZ + mulXW)) * invSqrtLen);
	QuatRotMatrix.set_23((2.0f * (mulYZ - mulXW)) * invSqrtLen);

	// fill the rest of the 4x4 matrix with 0.0f
	/*QuatRotMatrix.set_14(0.0f);
	QuatRotMatrix.set_24(0.0f);
	QuatRotMatrix.set_34(0.0f);
	QuatRotMatrix.set_41(0.0f);   // do not need because
	QuatRotMatrix.set_42(0.0f);   // every slot are already
	QuatRotMatrix.set_43(0.0f);*/ // set to zero

	return QuatRotMatrix;
}

Matrix4x4  Quaternion::QuaternionToMatrix4Normalized()
{
	// we assume that the quaternion is normalized
	Matrix4x4 QuatRotMatrix;

	// useful precalculation (save process time)
	float Xsqrt = SQUARE(m_fX);
	float Ysqrt = SQUARE(m_fY);
	float Zsqrt = SQUARE(m_fZ);
	float Wsqrt = SQUARE(m_fW);
	float mulXY = m_fX * m_fY;
	float mulXZ = m_fX * m_fZ;
	float mulXW = m_fX * m_fW;
	float mulYZ = m_fY * m_fZ;
	float mulYW = m_fY * m_fW;
	float mulZW = m_fZ * m_fW;

	// now let's fill the matrix
	/*******************First row*************************/
	QuatRotMatrix.set_11(1.0f - (2.0f * (Ysqrt + Zsqrt)));
	QuatRotMatrix.set_12(2.0f * (mulXY - mulZW));
	QuatRotMatrix.set_13(2.0f * (mulXZ + mulYW));
	//QuatRotMatrix.set_14(0.0f);
	/*******************Second Row************************/
	QuatRotMatrix.set_21(2.0f * (mulXY + mulZW));
	QuatRotMatrix.set_22(1.0f - (2.0f * (Xsqrt + Zsqrt)));
	QuatRotMatrix.set_23(2.0f * (mulYZ - mulXW));
	//QuatRotMatrix.set_24(0.0f);
	/*******************Third row*************************/
	QuatRotMatrix.set_31(2.0f * (mulXZ - mulYW));
	QuatRotMatrix.set_32(2.0f * (mulYZ + mulXW));
	QuatRotMatrix.set_33(1.0f - (2.0f * (Xsqrt + Ysqrt)));
	//QuatRotMatrix.set_34(0.0f);
	/*******************Fourth row************************/
	/*QuatRotMatrix.set_41(0.0f);
	QuatRotMatrix.set_42(0.0f);    // already set to zero
	QuatRotMatrix.set_43(0.0f);*/  // when matrix is created.
	QuatRotMatrix.set_44(1.0f);

	return QuatRotMatrix;
}

Quaternion Quaternion::QuaternionLinearInterpolation(Quaternion to, float var)
{
	// Simple Linear Interpolation of two quaternions.
	Quaternion from(*this);
	Quaternion q;

	// very quickly done thanks to good operator overload :-)
	return q = from + (var * (to - from));
}

Quaternion Quaternion::QuaternionSLinearInterpolation(Quaternion to, float var, bool shortestPath)
{
	Quaternion from(*this);
	Quaternion q;
	// this method compute the spherical linear interpolation
	// between two quaternions.
	float angleBetween = from.QuaternionDotProduct(to);

	// change the sign of a quaternion for avoiding
	// extra spinning caused by interpolated rotations
	// because of the fact that the sortest path is not
	// going to be taken.
	if(angleBetween < 0.0f) // if the angle is obtuse (not acute)
	{
		// let the choice to the user.
		// because we need a non inverted
		// method for the Cubic Lerp.
		if(shortestPath)
		{
			// just insure that the quaternion becomes
			// its opposite. Not only the conjuguate.
			to *= -1.0f;  // or to = -to;

			// just fix if the angle need to be changed
			// as well in a test.
		}
	}

	// perform the spherical linear interpolation
	// calculation.
	float thetaAngle   = acos(angleBetween);
	float invSinTheta  = 1.0f / sin(thetaAngle);
	float leftOperand  = sin((1.0f - var) * thetaAngle) * invSinTheta;
	float rightOperand = sin(var * thetaAngle) * invSinTheta;

	return q = (from * leftOperand) + (to * rightOperand);
}

Quaternion Quaternion::QuaternionCLinearInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, float var, bool shortestPath)
{
	// cubic interpolation of four quaternions thanks to the
	// Spherical Linear Interpolation almost like 
	// the CastleJau method.
	Quaternion from(*this);

	// r1 = c(b1,b2)  b1 = c(from, to) & b2 = c(ctrlFrom, ctrlTo)
	Quaternion b1 = from.QuaternionSLinearInterpolation(to, var, shortestPath);
	Quaternion b2 = ctrlFrom.QuaternionSLinearInterpolation(ctrlTo, var, shortestPath);

	return b1.QuaternionSLinearInterpolation(b2, (2.0f * var) * (1.0f - var), shortestPath);
}

Quaternion Quaternion::QuaternionCastleJauInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, float var)
{
	// de CastleJau Interpolation which uses
	// Simple Linear Interpolation.
	Quaternion from(*this);

	// First interpolation (base of the pyramid)
	Quaternion q0 = from.QuaternionLinearInterpolation(ctrlFrom, var);
	Quaternion q1 = ctrlFrom.QuaternionLinearInterpolation(ctrlTo, var);
	Quaternion q2 = ctrlTo.QuaternionLinearInterpolation(to, var);
	// Second round (middle of the pyramid)
	Quaternion r0 = q0.QuaternionLinearInterpolation(q1, var);
	Quaternion r1 = q1.QuaternionLinearInterpolation(q2, var);

	// and return directly the final quaternion (hat of the pyramid)
	return r0.QuaternionLinearInterpolation(r1, var);
}

Quaternion Quaternion::QuaternionBézierInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, float var, bool shortestPath)
{
	// Bézier Interpolation just by using Spherical Linear 
	// Interpolation instead of Simple Linear Interpolation.
	Quaternion from(*this);

	// First interpolation (base of the pyramid)
	Quaternion q0 = from.QuaternionSLinearInterpolation(ctrlFrom, var, shortestPath);
	Quaternion q1 = ctrlFrom.QuaternionSLinearInterpolation(ctrlTo, var, shortestPath);
	Quaternion q2 = ctrlTo.QuaternionSLinearInterpolation(to, var, shortestPath);
	// Second round (middle of the pyramid)
	Quaternion r0 = q0.QuaternionSLinearInterpolation(q1, var, shortestPath);
	Quaternion r1 = q1.QuaternionSLinearInterpolation(q2, var, shortestPath);

	// and return directly the final quaternion (hat of the pyramid)
	return r0.QuaternionSLinearInterpolation(r1, var, shortestPath);
}

void Quaternion::RotateByVector(const Vector3& v)
{
	Quaternion q(0.0f, v.getX(), v.getY(), v.getZ());
	(*this) *= q;
}

void Quaternion::AddScaledVector(const Vector3& v, const float scale)
{
	Quaternion q(0.0f, v.getX() * scale, v.getY() * scale, v.getZ() * scale);
	q *= *this;

	m_fW += q.getW() * 0.5f;
	m_fX += q.getX() * 0.5f;
	m_fY += q.getY() * 0.5f;
	m_fZ += q.getZ() * 0.5f;
}

void Quaternion::QuaternionRotateFromAxis(Quaternion& result, const Vector3& axis, float angle)
{
	float sinVal = sinf(angle * 0.5f);

	result.x(axis.getX() * sinVal);
	result.y(axis.getY() * sinVal);
	result.z(axis.getZ() * sinVal);
	result.w(cosf(angle * 0.5f));
}

Quaternion Quaternion::operator + () const
{
	return Quaternion(+this->m_fX, +this->m_fY, +this->m_fZ, +this->m_fW);
}

Quaternion Quaternion::operator - () const
{
	return Quaternion(-this->m_fX, -this->m_fY, -this->m_fZ, -this->m_fW);
}

Quaternion& Quaternion::operator += (const Quaternion& q)
{
	m_fX += q.m_fX;
	m_fY += q.m_fY;
	m_fZ += q.m_fZ;
	m_fW += q.m_fW;

	return *this;
}

Quaternion& Quaternion::operator -= (const Quaternion& q)
{
	m_fX -= q.m_fX;
	m_fY -= q.m_fY;
	m_fZ -= q.m_fZ;
	m_fW -= q.m_fW;

	return *this;
}

void Quaternion::operator *= (const Quaternion& q)
{
	Quaternion t = *this;

	m_fW = t.getW() * q.getW() - t.getX() * q.getX() - t.getY() * q.getY() - t.getZ() * q.getZ();
	m_fX = t.getW() * q.getX() + t.getX() * q.getW() + t.getY() * q.getZ() - t.getZ() * q.getY();
	m_fY = t.getW() * q.getY() + t.getY() * q.getW() + t.getZ() * q.getX() - t.getX() * q.getZ();
	m_fZ = t.getW() * q.getZ() + t.getZ() * q.getW() + t.getX() * q.getY() - t.getY() * q.getX();
}

Quaternion& Quaternion::operator *= (const float& val)
{
	m_fX *= val;
	m_fY *= val;
	m_fZ *= val;
	m_fW *= val;

	return *this;
}

Quaternion& Quaternion::operator /= (const float& val)
{
	m_fX /= val;
	m_fY /= val;
	m_fZ /= val;
	m_fW /= val;

	return *this;
}

Quaternion Quaternion::operator + (const Quaternion& q) const
{
	Quaternion temp(*this);
	temp += q;

	return temp;
}

Quaternion Quaternion::operator - (const Quaternion& q) const
{
	Quaternion temp(*this);
	temp -= q;

	return temp;
}

Quaternion Quaternion::operator * (const Quaternion& q) const
{
	float newW = (q.m_fW * m_fW) - (q.m_fX * m_fX) - (q.m_fY * m_fY) - (q.m_fZ * m_fZ);
	float newX = (q.m_fW * m_fX) + (q.m_fX * m_fW) - (q.m_fY * m_fZ) + (q.m_fZ * m_fY);
	float newY = (q.m_fW * m_fY) + (q.m_fX * m_fZ) + (q.m_fY * m_fW) - (q.m_fZ * m_fX);
	float newZ = (q.m_fW * m_fZ) - (q.m_fX * m_fY) + (q.m_fY * m_fX) + (q.m_fZ * m_fW);

	return Quaternion(newX, newY, newZ, newW);
}

Quaternion Quaternion::operator / (const Quaternion& q) const
{
	// allow to divide two quaternions
	float magnitudeSqrt = SQUARE(q.m_fX) + SQUARE(q.m_fY) + SQUARE(q.m_fY) + SQUARE(q.m_fW);

	float newW = ((q.m_fW * m_fW) + (q.m_fX * m_fX) + (q.m_fY * m_fY) + (q.m_fZ * m_fZ)) / magnitudeSqrt;
	float newX = ((q.m_fW * m_fX) - (q.m_fX * m_fW) - (q.m_fY * m_fZ) + (q.m_fZ * m_fY)) / magnitudeSqrt;
	float newY = ((q.m_fW * m_fY) + (q.m_fX * m_fZ) - (q.m_fY * m_fW) - (q.m_fZ * m_fX)) / magnitudeSqrt;
	float newZ = ((q.m_fW * m_fZ) - (q.m_fX * m_fY) + (q.m_fY * m_fX) - (q.m_fZ * m_fW)) / magnitudeSqrt;

	return Quaternion(newX, newY, newZ, newW);
}

Quaternion Quaternion::operator * (const float& val) const
{
	Quaternion temp(*this);
	temp *= val;

	return temp;
}

Quaternion Quaternion::operator / (const float& val) const
{
	Quaternion temp(*this);
	temp /= val;

	return temp;
}

bool Quaternion::operator == (const Quaternion& q) const
{
	return (Equal<float>(m_fX, q.m_fX) && Equal<float>(m_fY, q.m_fY) && Equal<float>(m_fZ, q.m_fZ) && Equal<float>(m_fW, q.m_fW));
}

bool Quaternion::operator != (const Quaternion& q) const
{
	return (Different<float>(m_fX, q.m_fX) || Different<float>(m_fY, q.m_fY) || Different<float>(m_fZ, q.m_fZ) || Different<float>(m_fW, q.m_fW));
}

Quaternion operator * (float val, const Quaternion& q)
{
	Quaternion temp(q);
	temp *= val;

	return temp;
}
