#ifndef DEF_COLOR_H
#define DEF_COLOR_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Color.h/.cpp
/   
/  Description: This component represent a simple color and 
/               could be pass to DXColor thanks to the DXUtility.h
/               file (OcelotColToDXCol() function).
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include "Utility.h"

namespace Ocelot
{
	class Color
	{
	private:

		// Attributes
		float m_fRed;
		float m_fGreen;
		float m_fBlue;
		float m_fAlpha; // transparency

	public:

		// Constructors & Destructor
		Color();
		Color(float r, float g, float b);
		Color(float r, float g, float b, float a);
		~Color();

		// Methods


		// Accessors
		inline float getR() const { return m_fRed;}
		inline float getG() const { return m_fGreen;}
		inline float getB() const { return m_fBlue;}
		inline float getA() const { return m_fAlpha;}
		inline void  R(float red) { m_fRed = red;}
		inline void  G(float green){ m_fGreen = green;}
		inline void  B(float blue) { m_fBlue = blue;}
		inline void  A(float alpha) { m_fAlpha = alpha;}

		// Operators overload
		/*********************Cast Operators**************************/
		operator float* ();
		operator const float* () const;
		/*********************** unary operators **********************/
		Color  operator + () const;
		Color  operator - () const;
		/*********************** assgm operators **********************/
		Color& operator += (const Color&);
		Color& operator -= (const Color&);
		Color& operator *= (const float& val);
		Color& operator /= (const float& val);
		/*********************** binary operators *********************/
		Color  operator +  (const Color&) const;
		Color  operator -  (const Color&) const;
		Color  operator *  (const float& val) const;
		Color  operator /  (const float& val) const;
		bool   operator == (const Color&) const;
		bool   operator != (const Color&) const;
		friend Color operator * (float, const Color&);
	};

	namespace OcelotColor
	{
		const Color WHITE(1.0f, 1.0f, 1.0f, 1.0f);
		const Color BLACK(0.0f, 0.0f, 0.0f, 1.0f);
		const Color GREY(0.5f, 0.5f, 0.5f, 1.0f);
		const Color SILVER(0.75f, 0.75f, 0.75f, 1.0f);
		const Color RED(1.0f, 0.0f, 0.0f, 1.0f);
		const Color MAROON(0.5f, 0.0f, 0.0f, 1.0f);
		const Color GREEN(0.0f, 0.5f, 0.0f, 1.0f);
		const Color LIME(0.0f, 1.0f, 0.0f, 1.0f);
		const Color SKYBLUE(0.0f, 1.0f, 1.0f, 1.0f);
		const Color LIGHTBLUE(0.0f, 0.5f, 1.0f, 1.0f);
		const Color BLUE(0.0f, 0.0f, 1.0f, 1.0f);
		const Color NAVYBLUE(0.0f, 0.0f, 0.5f, 1.0f);
		const Color VIOLET(0.5f, 0.0f, 1.0f, 1.0f);
		const Color YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
		const Color ORANGE(1.0f, 0.65f, 0.0f, 1.0f);
		const Color REDORANGE(1.0f, 0.27f, 0.0f, 1.0f);
		const Color CYAN(0.0f, 1.0f, 1.0f, 1.0f);
		const Color MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);
		const Color CHOCOLATE(0.82f, 0.41f, 0.12f, 1.0f);
		const Color BROWN(0.65f, 0.16f, 0.16f, 1.0f);

		const Color BEACH_SAND(1.0f, 0.96f, 0.62f, 1.0f);
		const Color LIGHT_YELLOW_GREEN(0.48f, 0.77f, 0.46f, 1.0f);
		const Color DARK_YELLOW_GREEN(0.1f, 0.48f, 0.19f, 1.0f);
		const Color DARKBROWN(0.45f, 0.39f, 0.34f, 1.0f);
		const Color LIGHTGREY(0.8f, 0.8f, 0.8f, 1.0f);
		const Color DARKGREY(0.05f, 0.05f, 0.05f, 1.0f);
		const Color DARKORANGE(1.0f, 0.55f, 0.0f, 1.0f);
	}
}

#endif