#ifndef RAY_H
#define RAY_H

/*
	I've only added the bare minimum needed to do the intersection test.
	You'll have to add whatever else you need to the class.
*/

#include "Vector3.h"

namespace Ocelot
{
	class Ray
	{
	public:
		Ray();
		Ray(const Vector3 &o, const Vector3 &d);
		Ray(const float oX, const float oY, const float oZ, const float dX, const float dY, const float dZ);

		inline Vector3 GetOrigin() const { return origin; }
		inline Vector3 GetDirection() const { return direction; }
		inline Vector3 GetIntersectionPoint() const { return intersectionPoint; }

		inline void SetOrigin(const Vector3 &o) { origin = o; }
		inline void SetDirection(const Vector3 &d) { direction = d; }
		inline void SetIntersectionPoint(const Vector3 &p) { intersectionPoint = p; }

		Vector3 operator() (const float t);
		float& operator[] (const int index);
	private:
		Vector3 origin;
		Vector3 direction;
		Vector3 intersectionPoint;
	};
}

#endif