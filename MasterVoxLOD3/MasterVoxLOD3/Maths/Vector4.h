#ifndef DEF_VECTOR4_H
#define DEF_VECTOR4_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Vector4.h/.cpp
/
/ Description: This file provide a way to create a 4D vector with its
/              basic contents Dot, Cross products and extras such as
/              four interpolation function between vectors and product
/              with matrixes as well.
/              It will serve especially to pass in homogeneous coordinates
/              for calculating world coordinates by applying to the 4 by 4
/              world matrix for example.
/              Note: The "w" parameter which allow transformation or not
/              will be 1 for a point and 0 for a vector allowing a point
/              to be translated and not a vector representing a direction
/              for example.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"
#include "Matrix4x4.h"

class Matrix4x4;

class Vector4
{
private:

	// attributes
	float m_fX;
	float m_fY;
	float m_fZ;
	float m_fW;

public:

	// Constructors & Destructor
	Vector4();                                        // default cst
	Vector4(const Vector4&);                          // copy cst
	Vector4& operator = (const Vector4&);             // assignment cst
	Vector4(float xyz, float w);                      // overload cst
	Vector4(double xyz, double w);                    // overload cst
	Vector4(float  x, float  y, float  z, float  w);  // overload cst
	Vector4(double x, double y, double z, double w);  // overload cst
	~Vector4();

	// Methods
	float   Vec4Length();
	float   Vec4LengthSqrt();
	float   Vec4DistanceBetween(const Vector4& target);
	Vector4 Vec4Normalise();
	float   Vec4DotProduct(const Vector4& vec);
	Vector4 Vec4CrossProduct(const Vector4& adjVec1, const Vector4& adjVec2);
	Vector4 Vec4Addition(const Vector4& vecToAdd);
	Vector4 Vec4Subtraction(const Vector4& vecToSub);
	Vector4 Vec4LinearInterpolation(const Vector4& vecToJoin, float var);
	Vector4 Vec4HermiteSplineInterpolation(const Vector4& tangStartPoint, const Vector4& vecToJoin, const Vector4& tangVecToJoin, float var);
	Vector4 Vec4CatmullSplineInterpolation(const Vector4& vecControl1, const Vector4& vecToJoin, const Vector4& vecControl2, float var);
	Vector4 Vec4BarycentricInterpolation(const Vector4& vecAtBar, const Vector4& vecToJoin, float baryVar1, float baryVar2);
	Vector4 Vec4VecMatrixProduct(const Matrix4x4& mat);

	// Accessors
	inline float getX() const { return m_fX; }
	inline float getY() const { return m_fY; }
	inline float getZ() const { return m_fZ; }
	inline float getW() const { return m_fW; }
	inline void  x(const float X) { m_fX = X; }
	inline void  y(const float Y) { m_fY = Y; }
	inline void  z(const float Z) { m_fZ = Z; }
	inline void  w(const float W) { m_fW = W; }

	// operators overload
	/*********************Cast Operators**************************/
	operator float* ();              // for being able to pass it through the shader as a (float*)
	operator const float* () const;  // constant version
	/*********************** unary operators **********************/
	Vector4  operator + () const;
	Vector4  operator - () const;
	/*********************** assgm operators **********************/
	Vector4& operator += (const Vector4&);
	Vector4& operator -= (const Vector4&);
	Vector4& operator *= (const float& val);
	Vector4& operator /= (const float& val);
	/*********************** binary operators *********************/
	Vector4  operator +  (const Vector4&) const;
	Vector4  operator -  (const Vector4&) const;
	Vector4  operator *  (const float& val) const;
	Vector4  operator /  (const float& val) const;
	bool     operator == (const Vector4&) const;
	bool     operator != (const Vector4&) const;
	friend Vector4 operator * (float, const Vector4&);
};

#endif