#ifndef DEF_QUATERNION_H
#define DEF_QUATERNION_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Quaternion.h/.cpp
/
/ Description: This file provide a way to create Quaternion with its
/              basic contents and extras for applying rotation to a 3D 
/              object through a matrix on every axis we would like.
/              A way to pass from Euler to Quaternion angles, Quaternion
/              to matrices 3 by 3 or 4 by 4 has been added, so that Log,
/              Exp, and Pow function calculation of a quaternion and five
/              interpolation processes.
/              Note: for avoiding square root calculation when normalizing
/              NoNormalized or Normalized methods have been implemented.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"
#include "Vector3.h"
#include "Matrix3x3.h"
#include "Matrix4x4.h"

class Matrix3x3;
class Matrix4x4;
class Vector3;

class Quaternion
{
private:

	// attributes of a quaternion which are like 
	// a vector4 with common methods like Dot prod
	// but will behave more like a matrix4x1.
	float m_fX;   // imaginary part
	float m_fY;   // imaginary part
	float m_fZ;   // imaginary part
	float m_fW;   // real part

public:

	// Constructors & Destructor
	Quaternion();										     // default cst
	Quaternion(const Quaternion& q);					     // copy cst
	Quaternion& operator = (const Quaternion&);		         // assgm cst
	Quaternion(float xyzw);						             // overload cst
	Quaternion(double xyzw);						         // overload cst
	Quaternion(float x, float y, float z, float w);	         // overload cst
	Quaternion(double x, double y, double z, double w);      // overload cst
	Quaternion(float headingAngle, float attitudeAngle, float bankAngle);    // overload cst
	Quaternion(double headingAngle, double attitudeAngle, double bankAngle); // overload cst
	Quaternion(const Vector3&);							     // overload cst
	Quaternion(const Matrix4x4&);						     // overload cst (could be a Matrix3x3 ? : minimum)
	Quaternion(const Matrix3x3&);						     // overload cst
	~Quaternion();

	// Methods
	Quaternion QuaternionIdentity();                         // set the quaternion with a 1 on the real part.
	float      QuaternionLength();                           // length of the quaternion
	float      QuaternionLengthSqrt();					     // length square of the quaternion
	Quaternion QuaternionNormalize();					     // normalize the quaternion
	Quaternion QuaternionInverse();                          // compute the inverse of the quaternion
	Quaternion QuaternionConjuguate();                       // compute the conjuguate of the quaternion.
	Quaternion QuaternionExponantial();                      // compute the exponential of the quaternion.
	Quaternion QuaternionLogarithm();                        // compute the logarithm of the quaternion.
	Quaternion QuaternionPowerScalar(const float& power);    // compute the quaternion at the power scalar enterred as parameter.
	Quaternion QuaternionPowerQuaternion(const Quaternion&); // compute the quaternion at the power quaternion enterred as parameter.
	float      QuaternionDotProduct(const Quaternion& q);    // apply a dot product to the quaternion.
	Vector3    QuaternionToVectorTransformAxis(const Vector3& axis);
	Vector3    QuaternionToEulerNoNormalized();			     // convert a quaternion non normalized into euler space (return a vector)
	Vector3    QuaternionToEulerNormalized();                // convert a quaternion normalized into euler space
	Matrix3x3  QuaternionToMatrix3NoNormalized();		     // return a matrix3x3 with a non normalized quaternion
	Matrix3x3  QuaternionToMatrix3Normalized();              // return a matrix3x3 with a normalized quaternion
	Matrix4x4  QuaternionToMatrix4NoNormalized();            // same but return a matrix4x4
	Matrix4x4  QuaternionToMatrix4Normalized();              // same but return a matrix4x4
	Quaternion QuaternionLinearInterpolation(Quaternion to, float var);  // Simple Linear Interpolation
	Quaternion QuaternionSLinearInterpolation(Quaternion to, float var, bool shortestPath = true);	 // Spherical Linear Interpolation
	Quaternion QuaternionCLinearInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, float var, bool shortestPath = false);  // Cubic Linear Interpolation
	Quaternion QuaternionCastleJauInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, float var);  // de CastleJau algorithm for interpolating
	Quaternion QuaternionBézierInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, float var, bool shortestPath = true);  // Bézier Interpolation
	void	   RotateByVector(const Vector3& v);
	void	   AddScaledVector(const Vector3& v, const float scale);
	void       QuaternionRotateFromAxis(Quaternion& result, const Vector3& axis, float angle);

	// Accessors
	float getX() const { return m_fX; }
	float getY() const { return m_fY; }
	float getZ() const { return m_fZ; }
	float getW() const { return m_fW; }
	void  x(const float X) { m_fX = X; }
	void  y(const float Y) { m_fY = Y; }
	void  z(const float Z) { m_fZ = Z; }
	void  w(const float W) { m_fW = W; }

	// Operators overload
	/*********************Unary Operators*************************/
	Quaternion operator + () const;
	Quaternion operator - () const;
	/*********************Assgm Operators*************************/
	void		operator *= (const Quaternion&);
	Quaternion& operator += (const Quaternion&);
	Quaternion& operator -= (const Quaternion&);
	Quaternion& operator *= (const float& val);
	Quaternion& operator /= (const float& val);
	/*********************Binary Operators************************/
	Quaternion operator + (const Quaternion&) const;
	Quaternion operator - (const Quaternion&) const;
	Quaternion operator * (const Quaternion&) const;
	Quaternion operator / (const Quaternion&) const;
	Quaternion operator * (const float& val) const;
	Quaternion operator / (const float& val) const;
	bool operator == (const Quaternion&) const;
	bool operator != (const Quaternion&) const;
	friend Quaternion operator * (float, const Quaternion&);
};

#endif