#include "Matrix2x2.h"

Matrix2x2::Matrix2x2() :
m_f11(0.0f), m_f12(0.0f),
m_f21(0.0f), m_f22(0.0f)
{
	
}

Matrix2x2::Matrix2x2(const Matrix2x2& mat) :
m_f11(mat.m_f11), m_f12(mat.m_f12),
m_f21(mat.m_f21), m_f22(mat.m_f22)
{

}

Matrix2x2& Matrix2x2::operator = (const Matrix2x2& mat)
{
	m_f11 = mat.m_f11;
	m_f12 = mat.m_f12;
	m_f21 = mat.m_f21;
	m_f22 = mat.m_f22;

	return *this;
}

Matrix2x2::Matrix2x2(float val) :
m_f11(val), m_f12(val),
m_f21(val), m_f22(val)
{

}

Matrix2x2::Matrix2x2(double val) :
m_f11((float)val), m_f12((float)val),
m_f21((float)val), m_f22((float)val)
{

}

Matrix2x2::Matrix2x2(float _11, float _12,	
					 float _21, float _22) :
m_f11(_11), m_f12(_12),
m_f21(_21), m_f22(_22)
{

}

Matrix2x2::Matrix2x2(double _11, double _12,
					 double _21, double _22) :
m_f11((float)_11), m_f12((float)_12),
m_f21((float)_21), m_f22((float)_22)
{

}

Matrix2x2::~Matrix2x2()
{

}

void      Matrix2x2::Matrix2x2Identity()
{
	// initialize as an identity matrix with 1 along the main diagonal.
	m_f11 = 1.0f; m_f12 = 0.0f;
	m_f21 = 0.0f; m_f22 = 1.0f;
}

bool      Matrix2x2::Matrix2x2IsIdentity()
{
	// check if the current matrix is an identity one
	Matrix2x2 temp(*this);
	Matrix2x2 temp2;
	temp2.Matrix2x2Identity();

	if(Equal<Matrix2x2>(temp, temp2))
		return true;
	return false;
}

void      Matrix2x2::Matrix2x2Transpose(Matrix2x2& out)
{
	// transpose the matrix by changing rows with columns
	out.m_f11 = m_f11; out.m_f12 = m_f21;
	out.m_f21 = m_f12; out.m_f22 = m_f22;
}

float     Matrix2x2::Matrix2x2Determinant()
{
	// compute the determinant of the matrix
	// it could be useful for inversing the matrix
	// or in the case of cofactors calcul.
	float result = 0.0f;

	return result = (m_f11 * m_f22) -
					(m_f12 * m_f21);
}

void      Matrix2x2::Matrix2x2Inverse(Matrix2x2& out)
{
	// first compute the determinant
	float determinant = this->Matrix2x2Determinant();

	// if the determinant is not equal to zero,
	// the matrix inverse is possible.
	if(Different<float>(determinant, 0.0f))
	{
		// swap components on the main diagonal
		// and give the opposite of two others
		// at their same place. Then, divide all
		// by the determinant.
		out.m_f11 = m_f22 / determinant;    // swap and divide
		out.m_f22 = m_f11 / determinant;    // swap and divide
		out.m_f12 = (-m_f12) / determinant; // opposite and divide
		out.m_f21 = (-m_f21) / determinant; // opposite and divide
	}
}

void      Matrix2x2::Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat)
{
	// Multiplication between two matrices.
	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	Matrix2x2 temp(*this);

	/*****************first row****************************/
	out.m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21);
	out.m_f12 = (temp.m_f11 * mat.m_f21) + (temp.m_f12 * mat.m_f22);
	/*****************second row***************************/
	out.m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21);
	out.m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22);
}

void      Matrix2x2::Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat1, const Matrix2x2& mat2)
{
	Matrix2x2Mutliply(out, mat1);

	out.Matrix2x2Mutliply(out, mat2);
}

float& Matrix2x2::operator () (int line, int col)
{
	Matrix2x2 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

float  Matrix2x2::operator () (int line, int col) const
{
	Matrix2x2 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

Matrix2x2::operator float* ()
{
	float* mat2x2[4];
	mat2x2[0] = &m_f11;
	mat2x2[1] = &m_f12;
	mat2x2[2] = &m_f21;
	mat2x2[3] = &m_f22;

	return mat2x2[0];
}

Matrix2x2::operator const float* () const
{
	const float* mat2x2[4];
	mat2x2[0] = &m_f11;
	mat2x2[1] = &m_f12;
	mat2x2[2] = &m_f21;
	mat2x2[3] = &m_f22;

	return mat2x2[0];
}

Matrix2x2 Matrix2x2::operator + () const
{
	return Matrix2x2(+this->m_f11, +this->m_f12,
		             +this->m_f21, +this->m_f22);
}

Matrix2x2 Matrix2x2::operator - () const
{
	return Matrix2x2(-this->m_f11, -this->m_f12,
		             -this->m_f21, -this->m_f22);
}

Matrix2x2& Matrix2x2::operator += (const Matrix2x2& mat)
{
	m_f11 += mat.m_f11;
	m_f12 += mat.m_f12;
	m_f21 += mat.m_f21;
	m_f22 += mat.m_f22;

	return *this;
}

Matrix2x2& Matrix2x2::operator -= (const Matrix2x2& mat)
{
	m_f11 -= mat.m_f11;
	m_f12 -= mat.m_f12;
	m_f21 -= mat.m_f21;
	m_f22 -= mat.m_f22;

	return *this;
}

Matrix2x2& Matrix2x2::operator *= (const Matrix2x2& mat)
{
	// Multiplication between two matrices.
	Matrix2x2 temp(*this);

	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	/*****************first row****************************/
	m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21);
	m_f12 = (temp.m_f11 * mat.m_f21) + (temp.m_f12 * mat.m_f22);
	/*****************second row***************************/
	m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21);
	m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22);

	return *this;
}

Matrix2x2& Matrix2x2::operator *= (const float& val)
{
	m_f11 *= val;
	m_f12 *= val;
	m_f21 *= val;
	m_f22 *= val;

	return *this;
}

Matrix2x2& Matrix2x2::operator /= (const float& val)
{
	m_f11 /= val;
	m_f12 /= val;
	m_f21 /= val;
	m_f22 /= val;

	return *this;
}

Matrix2x2 Matrix2x2::operator + (const Matrix2x2& mat) const
{
	Matrix2x2 temp(*this);
	temp += mat;

	return temp;
}

Matrix2x2 Matrix2x2::operator - (const Matrix2x2& mat) const
{
	Matrix2x2 temp(*this);
	temp -= mat;

	return temp;
}

Matrix2x2 Matrix2x2::operator * (const Matrix2x2& mat) const
{
	Matrix2x2 temp(*this);
	temp *= mat;

	return temp;
}

Matrix2x2 Matrix2x2::operator * (const float& val) const
{
	Matrix2x2 temp(*this);
	temp *= val;

	return temp;
}

Matrix2x2 Matrix2x2::operator / (const float& val) const
{
	Matrix2x2 temp(*this);
	temp /= val;

	return temp;
}

bool Matrix2x2::operator == (const Matrix2x2& mat) const
{
	return (Equal<float>(m_f11, mat.m_f11) && Equal<float>(m_f12, mat.m_f12) &&
			Equal<float>(m_f21, mat.m_f21) && Equal<float>(m_f22, mat.m_f22));
}

bool Matrix2x2::operator != (const Matrix2x2& mat) const
{
	return (Different<float>(m_f11, mat.m_f11) || Different<float>(m_f12, mat.m_f12) ||
			Different<float>(m_f21, mat.m_f21) || Different<float>(m_f22, mat.m_f22));
}

Matrix2x2 operator * (float val, const Matrix2x2& mat)
{
	Matrix2x2 temp(mat);
	temp *= val;

	return temp;
}
