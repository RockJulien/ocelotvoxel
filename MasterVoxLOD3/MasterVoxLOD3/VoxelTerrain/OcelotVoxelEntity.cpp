#include "OcelotVoxelEntity.h"

using namespace Ocelot;

OcelotVoxelEntity::OcelotVoxelEntity() :
IDXVoxelEntity(), m_info()
{
	m_baseData = BaseDXData();
	m_bounds   = NULL;
	/*WSTRING defTextPath[3];
	defTextPath[0] = L".\\Resources\\Dirt.jpg";
	defTextPath[1] = L".\\Resources\\Grass.jpg";
	defTextPath[2] = L".\\Resources\\Rock.jpg";
	m_info.SetTexturePath(defTextPath);
	TriPlanarBumpMaterial material;
	loadTriPlanarMaterial(m_baseData.Device(), m_info.TexturePath(), material);*/
}

OcelotVoxelEntity::OcelotVoxelEntity(BaseDXData dxData, OcelotVoxelEntityInfo info) :
IDXVoxelEntity(), m_info(info)
{
	m_baseData = dxData;
	m_bounds   = NULL;
	m_matWorld.Matrix4x4Translation(m_matWorld, m_info.Position().getX(), m_info.Position().getY(), m_info.Position().getZ());

	/*Matrix4x4 localTransl;
	localTransl.Matrix4x4Identity();

	// apply a translation
	localTransl.Matrix4x4Translation(localTransl,
									 m_info.Position().getX(), 
									 m_info.Position().getY(), 
									 m_info.Position().getZ());

	Matrix4x4 invWorld;
	invWorld.Matrix4x4Identity();
	m_matWorld.Matrix4x4Inverse(invWorld);

	invWorld.Matrix4x4Mutliply(invWorld, localTransl);
	invWorld.Matrix4x4Inverse(m_matWorld);

	TriPlanarBumpMaterial material;
	loadTriPlanarMaterial(m_baseData.Device(), m_info.TexturePath(), material);*/
}

OcelotVoxelEntity::~OcelotVoxelEntity()
{
	releaseInstance();
}

VVOID OcelotVoxelEntity::update(FFLOAT deltaTime)
{
	// update effect variables
	updateEffectVariables();

	// update bounds
	if(m_bounds && m_bRenderBounds)
	{
		Matrix4x4 id;
		id.Matrix4x4Identity();
		m_bounds->SetWorld(id);
		m_bounds->update(deltaTime);
	}
}

VVOID OcelotVoxelEntity::updateEffectVariables()
{
	// Compute the world inverse transpose matrix.
	Matrix4x4 matV, matP, matWorldInvTrans;
	matV = m_baseData.GetCamera()->View();
	matP = m_baseData.GetCamera()->Proj();
	m_matWorld.Matrix4x4Inverse(matWorldInvTrans);
	matWorldInvTrans.Matrix4x4Transpose(matWorldInvTrans);

	// set mesh's attributes to the shader's attributes.
	HR(m_fxLight->SetRawValue(m_info.Light(0), 0, sizeof(OcelotLight)));
	HR(m_fxLight2->SetRawValue(m_info.Light(1), 0, sizeof(OcelotLight)));
	HR(m_fxTorchOn->SetBool(m_bTorchIsOn));
	HR(m_fxFog->SetRawValue(m_info.Fog(), 0, sizeof(OcelotFog)));
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&matV));
	HR(m_fxProj->SetMatrix((FFLOAT*)&matP));
	HR(m_fxWorldInvTrans->SetMatrix((FFLOAT*)&matWorldInvTrans));
	HR(m_fxEyePosition->SetRawValue(&m_baseData.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));
	HR(m_fxTextureArray->SetResourceArray(m_info.Material()->TextureArray(), 0, 9));
	//HR(m_fxTextureArray->SetResource(m_info.Palette()->Palette()));
}

VVOID OcelotVoxelEntity::renderMesh()
{
	// set the input layout and buffers
	m_baseData.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UINT stride = sizeof(VoxelVertex);
	UINT offset = 0;
	m_baseData.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_baseData.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_baseData.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if(m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for(UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			m_baseData.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}

		// render bounds
		if(m_bounds && m_bRenderBounds)
		{
			HR(m_bounds->renderMesh());
		}
	}
}

VVOID OcelotVoxelEntity::createInstance(VoxelVertex* vert, UBIGINT vertCount, UBIGINT* ind, UBIGINT indCount)
{
	/**************************************Initialize D3D*****************************************/
	// create effect object
	initializeEffect();

	// initialize effect Variables
	initializeEffectVariables();

	/**************************************Initialize World***************************************/
	// create the input layout
	initializeLayout();

	if(vert)
	{
		// compute vertices
		initializeVBuffers(vert, vertCount);
	}

	if(ind)
	{
		// compute indices
		initializeIBuffers(ind, indCount);
	}
}

VVOID OcelotVoxelEntity::initializeLayout()
{
	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 }
	};
	UBIGINT nbElements = sizeof( layout ) / sizeof( layout[0] );

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_baseData.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

}

VVOID OcelotVoxelEntity::initializeEffect()
{
	// Initialize the shader file.
	HR(initializeDXEffect(m_baseData.Device(), m_info.EffectPath(), &m_effect));
}

VVOID OcelotVoxelEntity::initializeVBuffers(VoxelVertex* vert, UBIGINT vertCount)
{
	m_iNbVertices = vertCount;

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(VoxelVertex) * m_iNbVertices;   
	buffDesc.BindFlags		= D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= vert;

	// Create vertex buffer
	HR(m_baseData.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));
}

VVOID OcelotVoxelEntity::initializeIBuffers(UBIGINT* ind, UBIGINT indCount)
{
	m_iNbIndices = indCount;

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(UBIGINT) * m_iNbIndices;   
	buffDesc.BindFlags		= D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= ind;

	// Create indice buffer
	HR(m_baseData.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));
}

VVOID OcelotVoxelEntity::initializeEffectVariables()
{
	m_fxTechnique     = m_effect->GetTechniqueByName("TriPlanarTexturing");
	m_fxWorld         = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView          = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj          = m_effect->GetVariableByName("gProjection")->AsMatrix();
	m_fxWorldInvTrans = m_effect->GetVariableByName("gWorldInvTrans")->AsMatrix();
	m_fxLight         = m_effect->GetVariableByName("gLight");
	m_fxLight2        = m_effect->GetVariableByName("gTorchLight");
	m_fxTorchOn       = m_effect->GetVariableByName("gTorcheOn")->AsScalar();
	m_fxFog           = m_effect->GetVariableByName("gFog");
	m_fxEyePosition   = m_effect->GetVariableByName("gEyePosW");
	m_fxTextureArray  = m_effect->GetVariableByName("gTexture")->AsShaderResource();
}

VVOID OcelotVoxelEntity::releaseInstance()
{
	// release every COM of DirectX
	ReleaseCOM(m_effect);
	ReleaseCOM(m_inputLayout);
	ReleaseCOM(m_vertexBuffer);
	ReleaseCOM(m_indiceBuffer);

	m_fxTechnique    = NULL;
	m_fxWorld        = NULL; 
	m_fxView         = NULL; 
	m_fxProj         = NULL; 
	m_fxWorldInvTrans = NULL; 
	m_fxLight        = NULL;
	m_fxLight2       = NULL;
	m_fxFog          = NULL; 
	m_fxEyePosition  = NULL; 
	m_fxTextureArray = NULL;

	//m_info.Material()->releaseCOM();
	//WSTRING* text = m_info.TexturePath();
	//DeleteArray1D(text);

	DeletePointer(m_bounds);
}

// Ocelot Voxel Entity base information class
OcelotVoxelEntityInfo::OcelotVoxelEntityInfo() :
m_fog(NULL), m_material(NULL), m_matPalette(NULL), m_sEffectPath(L".\\Effects\\VoxelTriPlanarTexturing.fx")/*, m_sTexturePath(NULL)*/, m_vPosition(0.0f)
{
	
}

OcelotVoxelEntityInfo::OcelotVoxelEntityInfo(/*WSTRING texturePath[3], */WSTRING effectPath, OcelotLight* light[2], OcelotFog* fog, TriPlanarBumpMaterial* mater, TriPlanarTexturePalette* palette, Vector3 position) :
m_fog(fog), m_material(mater), m_matPalette(palette), m_sEffectPath(effectPath)/*, m_sTexturePath(texturePath)*/, m_vPosition(position)
{
	m_light[0] = light[0];
	m_light[1] = light[1];
}

OcelotVoxelEntityInfo::~OcelotVoxelEntityInfo()
{

}
