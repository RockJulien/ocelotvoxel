#ifndef DEF_IDXVOXELENTITY_H
#define DEF_IDXVOXELENTITY_H

#include "..\DirectXAPI\DXUtility.h"
#include "..\Geometry\BaseDXData.h"
#include "..\Collision\AABBox.h"
#include "..\Collision\BSphere.h"
#include "..\Collision\OBBox.h"
#include "..\Geometry\OcelotBaseMesh.h"
#include "..\Geometry\VertexTypes.h"

namespace Ocelot
{
	class IDXVoxelEntity : public OcelotBaseMesh
	{
	protected:

			typedef ID3D10Effect* EffPtr;
			typedef ID3D10Buffer* BufPtr;
			typedef ID3D10InputLayout* LayPtr;
			typedef ID3D10EffectTechnique* TecPtr;
			typedef ID3D10EffectVariable* VarPtr;
			typedef ID3D10EffectMatrixVariable* MatPtr;
			typedef ID3D10EffectScalarVariable* ScaPtr;
			typedef ID3D10ShaderResourceView* ResViewPtr;
			typedef ID3D10EffectShaderResourceVariable* TexPtr;

			// Attributes.
			BaseDXData          m_baseData;
			IRenderable*        m_bounds;
			BBOOL               m_bTorchIsOn;

			// D3DX10 Components.
			EffPtr				m_effect;
			TecPtr				m_fxTechnique;
			LayPtr				m_inputLayout;
			BufPtr				m_vertexBuffer;
			BufPtr				m_indiceBuffer;
			MatPtr				m_fxWorld;
			MatPtr				m_fxView;
			MatPtr				m_fxProj;
			MatPtr				m_fxWorldInvTrans;
			VarPtr				m_fxLight;
			VarPtr              m_fxLight2;
			ScaPtr              m_fxTorchOn;
			VarPtr				m_fxFog;
			VarPtr				m_fxEyePosition;

			// Texture resources.
			TexPtr				m_fxTextureArray;

	public:

			// Constructor & Destructor
			IDXVoxelEntity()  : OcelotBaseMesh(), m_bounds(NULL), m_bTorchIsOn(false), m_effect(NULL), m_fxTechnique(NULL), m_inputLayout(NULL), m_vertexBuffer(NULL), m_indiceBuffer(NULL),
							    m_fxWorld(NULL), m_fxView(NULL), m_fxProj(NULL), m_fxWorldInvTrans(NULL), m_fxLight(NULL), m_fxLight2(NULL), m_fxTorchOn(NULL), m_fxFog(NULL), m_fxEyePosition(NULL), m_fxTextureArray(NULL){}
	virtual ~IDXVoxelEntity() {}

			// Methods per frame
	virtual VVOID update(FFLOAT deltaTime)                                 = 0;
	virtual VVOID updateEffectVariables()                                  = 0;
	virtual VVOID renderMesh()                                             = 0;

			// Methods to use once.
	virtual VVOID createInstance(VoxelVertex* vert, UBIGINT vertCount, UBIGINT* ind, UBIGINT indCount) = 0;
	virtual VVOID initializeLayout()                                       = 0;
	virtual VVOID initializeEffect()                                       = 0;
	virtual VVOID initializeVBuffers(VoxelVertex* vert, UBIGINT vertCount) = 0;
	virtual VVOID initializeIBuffers(UBIGINT* ind, UBIGINT indCount)       = 0;
	virtual VVOID initializeEffectVariables()                              = 0;
	virtual VVOID releaseInstance()                                        = 0;

			// Accessors.
	inline  DevPtr       Dev() const { return m_baseData.Device();}
	inline  CamPtr       Cam() const { return m_baseData.GetCamera();}
	inline  IRenderable* Bounds() const { return m_bounds;}
	inline  VVOID        SetBounds(IRenderable* bounds) { if(m_bounds) m_bounds->releaseInstance(); m_bounds = bounds; m_bounds->createInstance();}
	inline  BBOOL        IsTorchOn() const { return m_bTorchIsOn;}
	inline  VVOID        SetTorchOn(BBOOL state) { m_bTorchIsOn = state;}
	inline  VVOID        TorchOn() { m_bTorchIsOn = true;}
	inline  VVOID        TorchOff() { m_bTorchIsOn = false;}
	};
}

#endif