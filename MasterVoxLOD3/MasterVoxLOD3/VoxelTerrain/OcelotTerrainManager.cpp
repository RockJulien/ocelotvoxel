#include "OcelotTerrainManager.h"

using namespace Ocelot;

OcelotTerrainManager::OcelotTerrainManager(OcelotVoxelMeshInfo info, USMALLINT iLODs, FFLOAT lODDistance) :
m_fLODDistance(lODDistance), m_iEditScale(5), m_bTorchOn(false)
{
	// Initialize the mesh
	m_terrainMesh = new OcelotVoxelMesh(info, iLODs, lODDistance);
}

OcelotTerrainManager::~OcelotTerrainManager()
{
	releaseMesh();
	saveEdits();
	releaseEdits();
}

VVOID   OcelotTerrainManager::releaseEdits()
{
	for(UBIGINT currEdit = 0; currEdit < (UBIGINT)m_edits.size(); currEdit++)
		DeletePointer(m_edits[currEdit]);
	m_edits.clear();
}

Vector3 OcelotTerrainManager::sign(Vector3 camDir)
{
	Vector3 toReturn(1.0f);

	if(camDir.getX() < 0.0f)
		toReturn.x(-1.0f);
	if(camDir.getY() < 0.0f)
		toReturn.y(-1.0f);
	if(camDir.getZ() < 0.0f)
		toReturn.z(-1.0f);

	return toReturn;
}

VVOID OcelotTerrainManager::update(FFLOAT deltaTime)
{
	if(InMng->isPressedAndLocked(OCELOT_T))
	{
		m_bTorchOn = !m_bTorchOn;
		m_terrainMesh->SetTorchOn(m_bTorchOn);
	}

	// Update visibility and entities.
	m_terrainMesh->update(deltaTime);

	// Handle user's inputs
	handleInput();

	// Update LOD drawn (Discrete) regarding 
	// to the camera and the LOD distance.
	//Vector3 camPos = GetCamPos();

	/*for(USMALLINT currLod = 0; currLod < m_terrainMesh->LodCount(); currLod++)
	{
		map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLod)->Meshes()->begin();
		for( ; iter != m_terrainMesh->LOD(currLod)->Meshes()->end(); iter++)
		{
			m_terrainMesh->updateOctree(iter->first, currLod, false);
			m_terrainMesh->updateNodeToDraw(iter->first, currLod);
		}
	}*/
}

VVOID OcelotTerrainManager::handleInput()
{
	// INCREASE SCALE FOR EDITING
	if(InMng->isPressedAndLocked(OCELOT_NPPLUS))
		increaseScale();

	// DECREASE SCALE FOR EDITING
	if(InMng->isPressedAndLocked(OCELOT_NPMINUS))
		decreaseScale();

	// ADD BOX
	if(InMng->isPressedAndLocked(OCELOT_I))
		addAABBox();
											  // | I |  | O |
	// ADD SPHERE
	if(InMng->isPressedAndLocked(OCELOT_O))
		addSphere();

	// REMOVE BOX
	if(InMng->isPressedAndLocked(OCELOT_K))
		remAABBox();
											  // | K |  | L |
	// REMOVE SPHERE
	if(InMng->isPressedAndLocked(OCELOT_L))
		remSphere();

}

VVOID OcelotTerrainManager::render()
{
	m_terrainMesh->renderMesh();
}

VVOID OcelotTerrainManager::addAABBox()
{
	// Create a new one at the LOD 0 just in Front of the camera.
	FFLOAT camOffset = 1.0f;
	Vector3 camPos = GetCamPos();
	Vector3 camDir = m_terrainMesh->Info()->m_data.GetCamera()->Info().LookAt();
	camDir.Vec3Normalise();
	Vector3 minBound;
	FFLOAT halfScale = (FFLOAT)(m_iEditScale/2);
	minBound.x((camPos.getX()  - halfScale) + (camOffset * camDir.getX()));
	minBound.y((camPos.getY()  - halfScale) + (camOffset * camDir.getY()));
	minBound.z((camPos.getZ()  - halfScale) + (camOffset * camDir.getZ()));
	Vector3 maxBounds = minBound + Vector3((FFLOAT)(m_iEditScale));
	AABBox newOneBounds(m_terrainMesh->Info()->m_data, minBound, maxBounds, AABB_GENERATOR_MINMAX);
	OcelotEditAABBox* newOne = new OcelotEditAABBox(newOneBounds, false);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	// And update octree nodes
	for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
	{
		for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
			iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
			iter++)
		{
			m_terrainMesh->updateOctree(iter->first, currLOD, false);
			m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
			//m_terrainMesh->updateOctree2(iter->first, currLOD, false);
		}
	}
}

VVOID OcelotTerrainManager::addSphere()
{
	// Create a new one at the LOD 0 just in Front of the camera.
	FFLOAT camOffset = 1.0f;
	Vector3 camPos = GetCamPos();
	Vector3 camDir = m_terrainMesh->Info()->m_data.GetCamera()->Info().LookAt();
	camDir.Vec3Normalise();
	Vector3 centerBound;
	FFLOAT halfScale = (FFLOAT)(m_iEditScale/2);
	centerBound.x(camPos.getX() + (camOffset * camDir.getX()));
	centerBound.y(camPos.getY() + (camOffset * camDir.getY()));
	centerBound.z(camPos.getZ() + (camOffset * camDir.getZ()));
	BSphere newOneBounds(m_terrainMesh->Info()->m_data, centerBound, halfScale);
	OcelotEditBSphere* newOne = new OcelotEditBSphere(newOneBounds, false);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	// And update nodes to draw and octree nodes by giving the higher LOD.
	for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
	{
		for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
			iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
			iter++)
		{
			m_terrainMesh->updateOctree(iter->first, currLOD, false);
			m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
		}
	}
}

VVOID OcelotTerrainManager::remAABBox()
{
	// Create a new one at the LOD 0 just in Front of the camera.
	FFLOAT camOffset = 1.0f;
	Vector3 camPos = GetCamPos();
	Vector3 camDir = m_terrainMesh->Info()->m_data.GetCamera()->Info().LookAt();
	camDir.Vec3Normalise();
	Vector3 minBound;
	FFLOAT halfScale = (FFLOAT)(m_iEditScale/2);
	minBound.x((camPos.getX()  - halfScale) + (camOffset * camDir.getX()));
	minBound.y((camPos.getY()  - halfScale) + (camOffset * camDir.getY()));
	minBound.z((camPos.getZ()  - halfScale) + (camOffset * camDir.getZ()));
	Vector3 maxBounds = minBound + Vector3((FFLOAT)(m_iEditScale));
	AABBox newOneBounds(m_terrainMesh->Info()->m_data, minBound, maxBounds, AABB_GENERATOR_MINMAX);
	OcelotEditAABBox* newOne = new OcelotEditAABBox(newOneBounds, true);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	// And update nodes to draw and octree nodes //by giving the higher LOD// store every LODs
	for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
	{
		for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
			iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
			iter++)
		{
			m_terrainMesh->updateOctree(iter->first, currLOD, false);
			m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
		}
	}
}

VVOID OcelotTerrainManager::remSphere()
{
	// Create a new one at the LOD 0 just in Front of the camera.
	FFLOAT camOffset = 1.0f;
	Vector3 camPos = GetCamPos();
	Vector3 camDir = m_terrainMesh->Info()->m_data.GetCamera()->Info().LookAt();
	camDir.Vec3Normalise();
	Vector3 centerBound;
	FFLOAT halfScale = (FFLOAT)(m_iEditScale/2);
	centerBound.x(camPos.getX() + (camOffset * camDir.getX()));
	centerBound.y(camPos.getY() + (camOffset * camDir.getY()));
	centerBound.z(camPos.getZ() + (camOffset * camDir.getZ()));
	BSphere newOneBounds(m_terrainMesh->Info()->m_data, centerBound, halfScale);
	OcelotEditBSphere* newOne = new OcelotEditBSphere(newOneBounds, true);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	// And update nodes to draw and octree nodes by giving the higher LOD.
	for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
	{
		for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
			iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
			iter++)
		{
			m_terrainMesh->updateOctree(iter->first, currLOD, false);
			m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
		}
	}
}

VVOID OcelotTerrainManager::increaseScale()
{
	m_iEditScale += 1;
}

VVOID OcelotTerrainManager::decreaseScale()
{
	m_iEditScale -= 1;
}

VVOID OcelotTerrainManager::addAABBox(AABBox area, BBOOL refresh)
{
	OcelotEditAABBox* newOne = new OcelotEditAABBox(area, false);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.
	
	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	if(refresh)
	{
		// And update nodes to draw and octree nodes by giving the higher LOD.
		for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
		{
			for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
				iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
				iter++)
			{
				m_terrainMesh->updateOctree(iter->first, currLOD, false);
				m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
			}
		}
	}
}

VVOID OcelotTerrainManager::addSphere(BSphere area, BBOOL refresh)
{
	OcelotEditBSphere* newOne = new OcelotEditBSphere(area, false);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	if(refresh)
	{
		// And update nodes to draw and octree nodes by giving the higher LOD.
		for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
		{
			for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
				iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
				iter++)
			{
				m_terrainMesh->updateOctree(iter->first, currLOD, false);
				m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
			}
		}
	}
}

VVOID OcelotTerrainManager::remAABBox(AABBox area, BBOOL refresh, BBOOL remove)
{
	OcelotEditAABBox* newOne = new OcelotEditAABBox(area, true);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	if(refresh)
	{
		// And update nodes to draw and octree nodes by giving the higher LOD.
		for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
		{
			for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
				iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
				iter++)
			{
				m_terrainMesh->updateOctree(iter->first, currLOD, remove);
				m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
			}
		}
	}
}

VVOID OcelotTerrainManager::remSphere(BSphere area, BBOOL refresh, BBOOL remove)
{
	OcelotEditBSphere* newOne = new OcelotEditBSphere(area, true);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	m_terrainMesh->addToEdit(newOne, true);

	if(refresh)
	{
		// And update nodes to draw and octree nodes by giving the higher LOD.
		for(USMALLINT currLOD = 0; currLOD < m_terrainMesh->LodCount(); currLOD++)
		{
			for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_terrainMesh->LOD(currLOD)->Meshes()->begin();
				iter != m_terrainMesh->LOD(currLOD)->Meshes()->end(); 
				iter++)
			{
				m_terrainMesh->updateOctree(iter->first, currLOD, remove);
				m_terrainMesh->updateNodeToDraw(iter->first, currLOD);
			}
		}
	}
}

VVOID OcelotTerrainManager::releaseMesh()
{
	if(m_terrainMesh)
		DeletePointer(m_terrainMesh);
}

VVOID OcelotTerrainManager::loadEdits(STRING path)
{
	// Create a stream supporting wstring.
	std::wifstream input;
	input.open(path, ios::in); // read mode.

	if(input) // if file is open properly.
	{
		WSTRING notNeeded; // buffer for not needed stuff in file.
		WSTRING editCount;
		input >> editCount;
		UBIGINT edCount = toUInt(editCount);
		BBOOL   refresh = false;
		for(UBIGINT currEd = 0; currEd < edCount; currEd++)
		{
			if(currEd == (edCount - 1))
				refresh = true;

			// next line is the object type.
			WSTRING edType;
			input >> edType;
			if(edType == L"AABBOX")
			{
				// Grab elements
				Vector3 vBoxMin;
				Vector3 vBoxMax;
				FFLOAT  xVal;
				FFLOAT  yVal;
				FFLOAT  zVal;
				BBOOL   bBoxState;
				input >> bBoxState >> notNeeded; // Min: not needed.
				input >> xVal >> notNeeded;      // coma not needed
				vBoxMin.x(xVal);
				input >> yVal >> notNeeded;      // coma not needed
				vBoxMin.y(yVal);
				input >> zVal >> notNeeded;		 // Max: not needed
				vBoxMin.z(zVal);
				input >> xVal >> notNeeded;		 // coma not needed
				vBoxMax.x(xVal);
				input >> yVal >> notNeeded;		 // coma not needed
				vBoxMax.y(yVal);
				input >> zVal;
				vBoxMax.z(zVal);
				// Create a box.
				AABBox newEdit(m_terrainMesh->Info()->m_data, vBoxMin, vBoxMax, AABB_GENERATOR_MINMAX);
				// add the edit, but two cases: box which split or not isosurfaces.
				if(!bBoxState)
				{
					addAABBox(newEdit, refresh);
				}
				else if(bBoxState)
				{
					remAABBox(newEdit, refresh, false);
				}
			}
			else if(edType == L"BSPHERE")
			{
				// Grab elements
				Vector3 vSphereCen;
				FFLOAT  fSphereRad;
				FFLOAT  xVal;
				FFLOAT  yVal;
				FFLOAT  zVal;
				BBOOL   bSphereState;
				input >> bSphereState >> notNeeded; // Rad: not needed.
				input >> fSphereRad >> notNeeded;   // Cen: not needed.
				input >> xVal >> notNeeded;         // coma not needed.
				vSphereCen.x(xVal);
				input >> yVal >> notNeeded;         // coma not needed.
				vSphereCen.y(yVal);
				input >> zVal;                      // end.
				vSphereCen.z(zVal);
				// Create a sphere.
				BSphere newEdit(m_terrainMesh->Info()->m_data, vSphereCen, fSphereRad);

				if(!bSphereState)
				{
					addSphere(newEdit, refresh);
				}
				else if(bSphereState)
				{
					remSphere(newEdit, refresh, false);
				}
			}
		}  // end for
	} // end if

	input.close();
}

VVOID OcelotTerrainManager::saveEdits()
{
	std::wofstream output;
	output.open("TerrainEdits.vox", 'w'); // write mode.
	output << m_edits.size() << "\r\n";   // save the edit count for loading them after in a loop.

	for(UBIGINT currEdit = 0; currEdit < m_edits.size(); currEdit++)
	{
		// We do not know which shape we're gonna grab.
		OcelotEditAABBox* boxToSave     = NULL;
		OcelotEditBSphere* sphereToSave = NULL;

		// Grab the current editing shape
		if(m_edits[currEdit]->Shape() == "AABBOX")
		{
			boxToSave    = static_cast<OcelotEditAABBox*>(m_edits[currEdit]);
		}
		else if(m_edits[currEdit]->Shape() == "BSPHERE")
		{
			sphereToSave = static_cast<OcelotEditBSphere*>(m_edits[currEdit]);
		}

		// Save bounds information.
		if(boxToSave)
		{
			output << "AABBOX\r\n";
			output << m_edits[currEdit]->IsSplit() << "\r\n"; // Save the sample's state.
			output << boxToSave->BBox();                      // operator << overloaded.
		}
		else if(sphereToSave)
		{
			output << "BSPHERE\r\n";
			output << m_edits[currEdit]->IsSplit() << "\r\n"; // Save the sample's state.
			output << sphereToSave->BSPhere();
		}
	}

	output.close();
}
