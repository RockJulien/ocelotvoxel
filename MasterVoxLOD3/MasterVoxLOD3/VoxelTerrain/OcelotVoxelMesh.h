#ifndef DEF_OCELOTVOXELMESH_H
#define DEF_OCELOTVOXELMESH_H

#include "BaseVoxelMesh.h"
#include "OcelotVoxelEntity.h"
#include "..\Geometry\OcelotFog.h"
#include "..\Geometry\OcelotLight.h"
#include "..\DataStructures\OcelotAOctree.h"
#include "..\DataStructures\BasicOctree.h"

namespace Ocelot
{
#define LOD_BASE_DISTANCE 100.0f

							struct VoxelNodeToDraw
							{
								VoxelNodeToDraw() :
								m_iLod(0), m_iKeyID(0), m_bToLoad(false), m_bounds(), m_entity(0), m_bVisible(false) { for(USMALLINT currChild = 0; currChild < 8; currChild++) m_bChildVisible[currChild] = true; m_entity.unmap();}
								VoxelNodeToDraw(USMALLINT lod, BIGINT keyID, AABBox bounds, BBOOL visible = false) :
								m_iLod(lod), m_iKeyID(keyID), m_bToLoad(false), m_bounds(bounds), m_entity(0), m_bVisible(visible) { for(USMALLINT currChild = 0; currChild < 8; currChild++) m_bChildVisible[currChild] = true; m_entity.unmap();}

								USMALLINT                m_iLod;
								BIGINT                   m_iKeyID;
								BBOOL                    m_bToLoad; // Needed for for waiting till the rendering stuff are loaded before to call render.
								AABBox                   m_bounds;
								Array1D<IDXVoxelEntity*> m_entity;
								BBOOL                    m_bVisible;
								BBOOL                    m_bChildVisible[8];
								SMALLINT                 m_iTransitionIndex[6];
							};

							struct OcelotVoxelMeshInfo
							{
								OcelotVoxelMeshInfo(DiscreteVoxelMeshLODInfo lodInfo, DevPtr device, CamPtr camera, TriPlanarBumpMaterial* mater/*WSTRING texturePath[3]*/, TriPlanarTexturePalette* palette, WSTRING effectPath, OcelotLight* light[2], OcelotFog* fog = NULL) :
								m_voxLodInfo(lodInfo), m_data(device, camera), m_sEffectPath(effectPath), m_material(mater), m_matPalette(palette), m_fog(fog) { /*for(USMALLINT i = 0; i < 3; i++) m_sTexturePath[i] = texturePath[i];*/ m_light[0] = light[0]; m_light[1] = light[1];}

								DiscreteVoxelMeshLODInfo m_voxLodInfo;
								BaseDXData               m_data;
								OcelotLight*             m_light[2];
								OcelotFog*               m_fog;
								WSTRING                  m_sEffectPath;
								// Three names, normal map and specular one have the same name with _NRM & _SPEC.
								//WSTRING                  m_sTexturePath[3]; // Removed.
								TriPlanarBumpMaterial*   m_material;       // Common textures.
								TriPlanarTexturePalette* m_matPalette;     // Texture Palette. // added for choosing which kind of texture you want to use.
							};

	class OcelotVoxelMesh : public BaseVoxelMesh
	{
	private:

		//typedef OcelotAOctree<VoxelNodeToDraw*, AABBox> Node;
		typedef BasicOctree<VoxelNodeToDraw*> Node;

		// Attributes
		OcelotVoxelMeshInfo m_info;
		Node                m_root;
		FFLOAT*             m_fLODDistances;
		BBOOL               m_bTorchOn;

		// Private Methods
		BBOOL    frustumTest(const AABBox& bounds);
		VVOID    releaseNodeToDraw(VoxelNodeToDraw* toRelease);
		VVOID    releaseNodeGeometry(VoxelNodeToDraw* toOffload);
static  SMALLINT determineChildInParentBox(AABBox parentBox, Vector3 position);
static  Node*    FindNode(AABBox bounds, Node* toRecurse);
		AABBox   computeChildBounds(const AABBox& bounds);
		BBOOL    boundsVisible(const AABBox& bounds, const Vector3& camPos, const USMALLINT lod, BBOOL* isFar);
		UBIGINT  GetVertexCount(Node* toRecurse);

	public:

		// Constructor & Destructor
		OcelotVoxelMesh(OcelotVoxelMeshInfo info, USMALLINT iLODs, FFLOAT lodDistance = LOD_BASE_DISTANCE);
		~OcelotVoxelMesh();

		// Methods per frame
		VVOID update(FFLOAT deltaTime);
		BBOOL updateVisibility(Node* toRecurse);
		VVOID renderMesh();
		VVOID releaseInstance();

		VVOID updateNodeToDraw(BIGINT keyID, USMALLINT lod);
		VVOID updateOctree(BIGINT keyID, USMALLINT lod, BBOOL remove);

		// new stuff
		BBOOL updateVisibility2(Node* toRecurse);
		VVOID updateOctree2(BIGINT keyID, USMALLINT lod, BBOOL remove);

		// Accessors
		inline OcelotVoxelMeshInfo* Info() { return &m_info;}
		inline UBIGINT              GetVertexCount() { return GetVertexCount(&m_root);}
		inline BBOOL                IsTorchOn() const { return m_bTorchOn;}
		inline VVOID                SetTorchOn(BBOOL state) { m_bTorchOn = state;}
	};
}

#endif