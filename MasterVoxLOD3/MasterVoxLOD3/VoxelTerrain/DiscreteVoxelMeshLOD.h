#ifndef DEF_DISCRETEVOXELMESHLOD_H
#define DEF_DISCRETEVOXELMESHLOD_H

#include "..\Marching Cubes\OcelotVoxelGeometry.h"
using namespace std;

namespace Ocelot
{
						struct DiscreteVoxelMeshLODInfo
						{
							DiscreteVoxelMeshLODInfo() :
							m_iMeshSizeX(0), m_iMeshSizeY(0), m_iMeshSizeZ(0),
							m_fMeshScale(1.0f), m_bTransitionNeeded(false){}
							DiscreteVoxelMeshLODInfo(BIGINT meshSizeX, BIGINT meshSizeY, BIGINT meshSizeZ, FFLOAT meshScale, BBOOL transitionNeeded = false) :
							m_iMeshSizeX(meshSizeX), m_iMeshSizeY(meshSizeY), m_iMeshSizeZ(meshSizeZ), m_fMeshScale(meshScale), m_bTransitionNeeded(transitionNeeded) {}

							BIGINT m_iMeshSizeX;
							BIGINT m_iMeshSizeY;
							BIGINT m_iMeshSizeZ;
							FFLOAT m_fMeshScale;
							BBOOL  m_bTransitionNeeded;
						};

	class DiscreteVoxelMeshLOD
	{
	private:

		// Attributes
		// Structure holding different blocks constituting
		// the overall mesh.
		map<BIGINT, OcelotVoxelGeometry*> m_meshes;

		// Voxel mesh base info.
		DiscreteVoxelMeshLODInfo          m_info;

		// Private Methods
		VVOID                remBlock(BIGINT keyID);
		VVOID                releaseMap();

	public:

		// Constructor & Destructor
		DiscreteVoxelMeshLOD();
		DiscreteVoxelMeshLOD(DiscreteVoxelMeshLODInfo info);
		~DiscreteVoxelMeshLOD();

		// Methods
		VVOID  addToEdit(OcelotEditVoxel* toEdit, BBOOL refreshGeo);
		BIGINT addBlock(BIGINT blockPosX, BIGINT blockPosY, BIGINT blockPosZ);
		VVOID  remBlock(BIGINT blockPosX, BIGINT blockPosY, BIGINT blockPosZ);
		VVOID  blockPartOverlappedByBox(const AABBox& box, BIGINT* firstX, BIGINT* firstY, BIGINT* firstZ, BIGINT* lastX, BIGINT* lastY, BIGINT* lastZ);
		VVOID  refreshGeometry(BIGINT keyID);

		// Accessors
		inline map<BIGINT, OcelotVoxelGeometry*>* Meshes() { return &m_meshes;}
		inline OcelotVoxelGeometry*               Block(BIGINT blockPosX, BIGINT blockPosY, BIGINT blockPosZ) { return GetBlock(GetKeyID(blockPosX, blockPosY, blockPosZ));}
			   OcelotVoxelGeometry*               GetBlock(BIGINT keyID);
		static BIGINT  			                  GetKeyID(BIGINT blockPosX, BIGINT blockPosY, BIGINT blockPosZ);
		inline DiscreteVoxelMeshLODInfo           Info() const { return m_info;}
		inline VVOID                              SetInfo(DiscreteVoxelMeshLODInfo info) { m_info = info;}
		inline VVOID                              SetScale(FFLOAT newScale) { m_info.m_fMeshScale = newScale;}
	};
}

#endif