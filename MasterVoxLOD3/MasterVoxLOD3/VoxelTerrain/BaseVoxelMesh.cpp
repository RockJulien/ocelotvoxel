#include "BaseVoxelMesh.h"

using namespace Ocelot;

BaseVoxelMesh::BaseVoxelMesh()
{
	//m_LODs.unmap();

	FFLOAT defaultParentScale = 1.0f;
	USMALLINT defaultLodCount = 3;

	//m_LODs.map(defaultLodCount);

	// Create Discrete LODs of blocks.
	for(USMALLINT currLod = 0; currLod < defaultLodCount; currLod++)
	{
		DiscreteVoxelMeshLODInfo info(16, 16, 16, defaultParentScale, (currLod > 0));
		//m_LODs[currLod] = new DiscreteVoxelMeshLOD(info);
		m_LODs.push_back(new DiscreteVoxelMeshLOD(info));

		// Children are twice bigger.
		defaultParentScale *= 2;
	}
}

BaseVoxelMesh::BaseVoxelMesh(DiscreteVoxelMeshLODInfo info, USMALLINT iLODs)
{
	//m_LODs.unmap();

	//m_LODs.map(iLODs);

	// Create Discrete LODs of blocks.
	for(USMALLINT currLod = 0; currLod < iLODs; currLod++)
	{
		if(currLod > 0)
			info.m_bTransitionNeeded = true;
		else
			info.m_bTransitionNeeded = false;

		//m_LODs[currLod] = new DiscreteVoxelMeshLOD(info);
		m_LODs.push_back(new DiscreteVoxelMeshLOD(info));

		// Lower LODs are twice bigger.
		info.m_fMeshScale *= 2;
	}

}

BaseVoxelMesh::~BaseVoxelMesh()
{
	for(USMALLINT currLod = 0; currLod < (USMALLINT)m_LODs.size(); currLod++)
	{
		DeletePointer(m_LODs[currLod]);
	}
}

VVOID BaseVoxelMesh::addToEdit(OcelotEditVoxel* toEdit, BBOOL refreshGeo)
{
	// add to every discrete LODs the part to edit.
	for(USMALLINT currLod = 0; currLod < (USMALLINT)m_LODs.size(); currLod++)
		m_LODs[currLod]->addToEdit(toEdit, refreshGeo);
}
