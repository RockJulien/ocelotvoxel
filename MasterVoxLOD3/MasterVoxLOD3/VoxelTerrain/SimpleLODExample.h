#ifndef DEF_SIMPLELODEXAMPLE_H
#define DEF_SIMPLELODEXAMPLE_H

#include "BaseVoxelMesh.h"
#include "OcelotVoxelEntity.h"
#include "..\Geometry\OcelotFog.h"
#include "..\Geometry\OcelotLight.h"
#include "..\Utilities\OcelotEditAABBox.h"
#include "..\Utilities\OcelotEditBSphere.h"

namespace Ocelot
{
	struct SimleLODMeshInfo
	{
		SimleLODMeshInfo(DiscreteVoxelMeshLODInfo lodInfo, DevPtr device, CamPtr camera, TriPlanarBumpMaterial* mater/*WSTRING texturePath[3]*/, TriPlanarTexturePalette* palette, WSTRING effectPath, OcelotLight* light[2], OcelotFog* fog = NULL) :
		m_voxLodInfo(lodInfo), m_data(device, camera), m_material(mater), m_matPalette(palette), m_sEffectPath(effectPath), m_fog(fog) { /*for(USMALLINT i = 0; i < 3; i++) m_sTexturePath[i] = texturePath[i];*/ m_light[0] = light[0]; m_light[1] = light[1];}

		DiscreteVoxelMeshLODInfo m_voxLodInfo;
		BaseDXData               m_data;
		OcelotLight*             m_light[2];
		OcelotFog*               m_fog;
		WSTRING                  m_sEffectPath;
		// Three names, normal map and specular one have the same name with _NRM & _SPEC.
		//WSTRING                  m_sTexturePath[3];
		TriPlanarBumpMaterial*   m_material;
		TriPlanarTexturePalette* m_matPalette;
	};

	class SimpleLODExample : public BaseVoxelMesh
	{
	private:

		SimleLODMeshInfo                m_info;
		Linked2List<OcelotVoxelEntity*> m_shapes;
		std::vector<OcelotEditVoxel*>   m_edits;

		VVOID addExample();
		VVOID prepareToRender();

	public:

		SimpleLODExample(SimleLODMeshInfo info, USMALLINT iLODs);
		~SimpleLODExample();

		VVOID update(FFLOAT deltaTime);
		VVOID renderMesh();
		VVOID handleInput();

	};
}

#endif