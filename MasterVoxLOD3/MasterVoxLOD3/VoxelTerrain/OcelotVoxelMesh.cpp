#include "OcelotVoxelMesh.h"

using namespace Ocelot;

// Sign to apply to TCell Bounds creation
// for setting the Box to the proper face
// and thus at the right position as well.
static const SMALLINT tCellBoundsSign[6][3] =
{
	{ 0,  0, -1}, // Neg Z face
	{ 0,  0,  1}, // Pos Z face
	{ 0, -1,  0}, // so on.
	{ 0,  1,  0},
	{-1,  0,  0},
	{ 1,  0,  0}
};

// Octree Specific Search Method (OSSM).
ATreeNode<VoxelNodeToDraw*, AABBox>* SearchMethod(ATreeNode<VoxelNodeToDraw*, AABBox>* toTest, AABBox toSearch)
{
	// Will automatically recursively do this for all the octree nodes.
	if(!toTest)
		return NULL;

	// Check for equality for founding the right one.
	if(Equal<AABBox>(toTest->Data()->m_bounds, toSearch))
		return toTest;

	// Children should be twice bigger, so if smaller Oupss return NULL.
	else if(toTest->Data()->m_bounds.Scale().getX() < toSearch.Scale().getX())
		return NULL;

	// Not matching.
	return NULL;
}

// Octree Specific Update Method (OSUM).
VVOID UpdateMethod(ATreeNode<VoxelNodeToDraw*, AABBox>* toUpdate, FFLOAT deltaTime)
{
	for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toUpdate->Data()->m_entity.Size(); currEnt++)
		toUpdate->Data()->m_entity[currEnt]->update(deltaTime);
}

VVOID UpdateMethod(BasicOctree<VoxelNodeToDraw*>* toUpdate, FFLOAT deltaTime)
{
	for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toUpdate->Data()->m_entity.Size(); currEnt++)
	{
		toUpdate->Data()->m_entity[currEnt]->update(deltaTime);
	}
}

// Octree Specific toApply Method (OSAM).
VVOID RenderMethod(ATreeNode<VoxelNodeToDraw*, AABBox>* toRender)
{
	for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toRender->Data()->m_entity.Size(); currEnt++)
		toRender->Data()->m_entity[currEnt]->renderMesh();
}

VVOID RenderMethod(BasicOctree<VoxelNodeToDraw*>* toRender)
{
	for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toRender->Data()->m_entity.Size(); currEnt++)
	{
		toRender->Data()->m_entity[currEnt]->renderMesh();
	}
}

// Octree Specific toApply Method (OSAM). (for releasing node to draw in each octree node).
VVOID ReleaseNode(ATreeNode<VoxelNodeToDraw*, AABBox>* toOffload)
{
	// Check node validity.
	if(!toOffload)
		return;

	// Grab the node pointer to release in the octree node.
	VoxelNodeToDraw* toRelease = toOffload->Data();

	// Check node validity.
	if(!toRelease)
		return;

	// Release Components specific to a node to draw.
	if(toRelease->m_entity.Size() != 0)
		for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toRelease->m_entity.Size(); currEnt++)
		{
			IDXVoxelEntity* toDelete = toRelease->m_entity[currEnt];
			DeletePointer(toDelete);
		}

	toRelease->m_entity.unmap();

	// Delete the pointer once pointers (if any) in this node to draw
	// are freed.
	DeletePointer(toRelease);
}

VVOID ReleaseNode(BasicOctree<VoxelNodeToDraw*>* toOffload)
{
	// Check node validity.
	if(!toOffload)
		return;

	// Grab the node pointer to release in the octree node.
	VoxelNodeToDraw* toRelease = toOffload->Data();

	// Check node validity.
	if(!toRelease)
		return;

	// Release Components specific to a node to draw.
	if(toRelease->m_entity.Size() != 0)
	{
		for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toRelease->m_entity.Size(); currEnt++)
		{
			//IDXVoxelEntity* toDelete = toRelease->m_entity[currEnt];
			DeletePointer(toRelease->m_entity[currEnt]);
		}
	}

	toRelease->m_entity.unmap();

	// Delete the pointer once pointers (if any) in this node to draw
	// are freed.
	DeletePointer(toRelease);
}

OcelotVoxelMesh::OcelotVoxelMesh(OcelotVoxelMeshInfo info, USMALLINT iLODs, FFLOAT lodDistance) :
BaseVoxelMesh(info.m_voxLodInfo, iLODs), m_info(info), m_bTorchOn(false)
{
	// Create LOD distance triggers
	m_fLODDistances = new FFLOAT[iLODs];
	/*for(USMALLINT currDist = 1; currDist <= iLODs; currDist++)
		m_fLODDistances[currDist - 1] = lodDistance * currDist;*/
	m_fLODDistances[0] = 80.0f;
	m_fLODDistances[1] = 200.0f;
	m_fLODDistances[2] = 400.0f;

	m_root.SetData(new VoxelNodeToDraw(0, -1, AABBox(m_info.m_data, Vector3(-1000000.0f), Vector3(1000000.0f), AABB_GENERATOR_MINMAX)));
}

OcelotVoxelMesh::~OcelotVoxelMesh()
{
	releaseInstance();

	DeleteArray1D(m_fLODDistances);
}

BasicOctree<VoxelNodeToDraw*>* OcelotVoxelMesh::FindNode(AABBox bounds, Node* toRecurse)
{
	// If Invalid
	if(!toRecurse)
		return NULL;

	// If Equal, that is it
	if(toRecurse->Data()->m_bounds == bounds)
		return toRecurse;

	// If the scale if smaller than the one researched, skip.
	if(toRecurse->Data()->m_bounds.Scale().getX() < bounds.Scale().getX())
		return NULL;

	// Then, decrease the number of branches in which recurse
	// by targeting the right one among 8 thanks to BBox.
	SMALLINT target = determineChildInParentBox(toRecurse->Data()->m_bounds, bounds.Center());

	// Return negative if invalid.
	if(target < 0)
		return NULL;

	// Then, recurse.
	return FindNode(bounds, *toRecurse->Child(target));
}

AABBox   OcelotVoxelMesh::computeChildBounds(const AABBox& bounds)
{
	Vector3 minPos = bounds.Minimum();
	Vector3 scale  = bounds.Scale();

	// twice bigger.
	scale *= 2.0f;
	Vector3 delta((FFLOAT)std::abs(((BIGINT)minPos.getX())%(BIGINT)scale.getX()),
				  (FFLOAT)std::abs(((BIGINT)minPos.getY())%(BIGINT)scale.getY()),
				  (FFLOAT)std::abs(((BIGINT)minPos.getZ())%(BIGINT)scale.getZ()));

	Vector3 childPos = minPos - delta;
	return AABBox(BaseDXData(), childPos, childPos + scale, AABB_GENERATOR_MINMAX);
}

BBOOL    OcelotVoxelMesh::boundsVisible(const AABBox& bounds, const Vector3& camPos, const USMALLINT lod, BBOOL* isFar)
{
	AABBox childBounds = computeChildBounds(bounds);

	BBOOL isClose   = false;
	BBOOL tempIsFar = false;

	FFLOAT dist  = (bounds.Center() - camPos).Vec3LengthSquared();
	// If TCell.
	if(lod > 0)
	{
		isClose = dist <= SQUARE(m_fLODDistances[lod - 1]);
	}

	FFLOAT dist2 = (childBounds.Center() - camPos).Vec3LengthSquared();
	tempIsFar = dist2 >= SQUARE(m_fLODDistances[lod]);

	// If second value needed.
	if(isFar)
		*isFar = tempIsFar;

	return !isClose && !tempIsFar;
}

BBOOL    OcelotVoxelMesh::frustumTest(const AABBox& bounds)
{
	// refresh planes of the frustum.
	m_info.m_data.GetCamera()->computeFrustumPlane();

	// Then, determine if block's box is visible.
	Vector3 boundsCenter = bounds.Center();
	Vector3 boundsScale  = bounds.Scale();
	return m_info.m_data.GetCamera()->cullMesh(boundsCenter.getX(), boundsCenter.getY(), boundsCenter.getZ(), Higher3Val<FFLOAT>(boundsScale.getX(), boundsScale.getY(), boundsScale.getZ()));
}

VVOID    OcelotVoxelMesh::updateNodeToDraw(BIGINT keyID, USMALLINT lod)
{
	// Grab the block for which tree nodes have to be updated.
	OcelotVoxelGeometry* block = Block(keyID, lod);

	if(!block)
		return;

	// Grab the node in the octree associated with this 
	// geometry block.
	//Node* octreeNode = static_cast<Node*>(m_root.search(SearchMethod, block->GetAABBox()));
	Node* octreeNode = FindNode(block->GetAABBox(), &m_root);

	if(!octreeNode)
		return;

	// Grab the node to draw in the octree node.
	VoxelNodeToDraw* nodeToDraw = octreeNode->Data();

	if(!nodeToDraw)
		return;

	//nodeToDraw->m_bToLoad = false;
	USMALLINT entityCount = 1; // Main block by default. 
	
	// (NOTE: + 6 Transition block if higher LOD than 0).
	if(lod > 0)
		entityCount = 7;

	// Release previous entity of the node if any.
	if(nodeToDraw->m_entity.Size() != 0)
	{
		for(USMALLINT currEnt = 0; currEnt < (USMALLINT)nodeToDraw->m_entity.Size(); currEnt++)
		{
			//IDXVoxelEntity* toDelete = nodeToDraw->m_entity[currEnt];
			DeletePointer(nodeToDraw->m_entity[currEnt]);
		}
	}

	nodeToDraw->m_entity.unmap();
	nodeToDraw->m_entity.map(entityCount);

	// Create new entity(ies).
	VoxelVertex* vertices  = block->VBuffer();
	UBIGINT      vertCount = block->VCount();
	AABBox blockBounds(block->GetAABBox());
	blockBounds.SetBaseData(m_info.m_data);

	for(USMALLINT currEnt = 0; currEnt < entityCount; currEnt++)
	{
		OcelotVoxelEntityInfo entInfo(m_info.m_sEffectPath, m_info.m_light, m_info.m_fog, m_info.m_material, m_info.m_matPalette, blockBounds.Minimum());
		nodeToDraw->m_entity[currEnt] = new OcelotVoxelEntity(m_info.m_data, entInfo);
		nodeToDraw->m_entity[currEnt]->SetBounds(new AABBox(blockBounds));
	}

	// Set TCell index to not existing by default.
	for(USMALLINT currEnt = 0; currEnt < entityCount - 1; currEnt++)
		nodeToDraw->m_iTransitionIndex[currEnt] = -1;

	USMALLINT entityCounter = 0;
	for(USMALLINT currEnt = 0; currEnt < entityCount; currEnt++)
	{
		// Check if no indices.
		UBIGINT  indCount = block->ICount(currEnt);
		if(indCount == 0)
		{
			// And is a TCell if any, set to invalid.
			if(currEnt > 0)
			{
				nodeToDraw->m_iTransitionIndex[currEnt - 1] = -1;
			}

			// create even if no indices or rendering issue will appear while iterating through octree
			// the node contains no indices but will be set to visible and thus to render, so render nothing
			// but do not bug.
			nodeToDraw->m_entity[currEnt]->createInstance(vertices, vertCount, NULL, indCount);

			// Else, pass to the next entity.
			continue;
		}

		// Grab indices for the curr entity (main block and TCells if any).
		UBIGINT* indices  = block->IBuffer(currEnt);

		// If one of the TCells
		if(currEnt > 0)
		{
			nodeToDraw->m_iTransitionIndex[currEnt - 1] = entityCounter;
		}
		entityCounter++;

		// Initialize entity(ies).
		nodeToDraw->m_entity[currEnt]->createInstance(vertices, vertCount, indices, indCount);
	}

	// Save memory, release block geometry now it is
	// in the node to draw.
	block->releaseGeometry();

	// If TCells (LOD > 0) and entity for TCells exist,
	// Set entity to not visible for now, because the 
	// node could not be seen.
	if(lod > 0 && nodeToDraw->m_entity.Size() > 1)
	{
		for(USMALLINT currTrans = 0; currTrans < 6; currTrans++)
		{
			SMALLINT transIndex = nodeToDraw->m_iTransitionIndex[currTrans];

			if(transIndex > -1)
			{
				nodeToDraw->m_entity[transIndex]->NotVisible();
			}
		}
	}
}

VVOID    OcelotVoxelMesh::updateOctree(BIGINT keyID, USMALLINT lod, BBOOL remove)
{
	// Function which will trigger modifications
	// on the octree content regarding to what
	// has been edited.
	// First, grab the voxel block.
	OcelotVoxelGeometry* block = Block(keyID, lod);

	// Check the validity.
	if(!block)
		return;

	// Then, two cases, either remove it if determined
	// as needed no more, or add a new one.
	if(remove)
	{
		//Node* res = static_cast<Node*>(m_root.search(SearchMethod, block->GetAABBox()));
		Node* result = FindNode(block->GetAABBox(), &m_root);
		if(!result)
		{
			// Already removed.
			return;
		}
		releaseNodeToDraw(result->Data());
	}
	else
	{
		// Add a new one.
		Node* root = &m_root;
		AABBox rootBounds = root->Data()->m_bounds;
		AABBox newBounds  = block->GetAABBox();

		// Create the new VoxelNode.
		VoxelNodeToDraw* newOne = new VoxelNodeToDraw(lod, keyID, newBounds);

		// Infinite loop waiting for break call if matching.
		for( ; ; )
		{
			// compute the child index for reaching its position in the tree.
			SMALLINT childIndex = determineChildInParentBox(rootBounds, newBounds.Center());

			if(childIndex == -1)
				return;

			// Then, reach the child. (NOTE: casting only because of inherited class and use of base class methods)
			Node** tempChild = root->Child(childIndex);

			// if node
			if(*tempChild)
			{
				// Else, grab the child box.
				AABBox childBounds = (**tempChild).Data()->m_bounds;

				// If the child bounds are equal to the new block's bounds to process,
				// replace the previous node by the new one and break.
				if(Equal<AABBox>(childBounds, newBounds))
				{
					// But check if previous node had geometry components and if it is the
					// case, it will mean that edits have been done, so that we have to re
					// compute geometry for the current LOD for the passed block keyID.
					if((**tempChild).Data()->m_entity.Size() != 0)
					{
						newOne->m_entity  = (**tempChild).Data()->m_entity;
						newOne->m_entity.SetCopy(false); // Become the original
						(**tempChild).Data()->m_entity.SetCopy(true); // Become the copy and will not be destroy when releasing node to draw.
						//newOne->m_bToLoad = true;
						LOD(lod)->refreshGeometry(newOne->m_iKeyID);
					}

					// To Do: Verify if memory leaks if previous
					// voxel node to draw here has to be deleted
					// before to insert the new one. Modified for mem leaks correction on 15th November 2012.
					delete (**tempChild).Data();
					(**tempChild).SetData(NULL);
					(**tempChild).SetData(newOne);
					break;
				}
				else
				{
					// Else, go one level down and check for children existing.
					// If children are present, set the current root and its bounds
					// with the child's pointer as current root and its bounds as
					// current root's bounds (going one level down in the process)
					// and restart to the start of the loop.
					if((determineChildInParentBox(childBounds, newBounds.Center()) != -1) && (childBounds.Scale().getX() > newBounds.Scale().getX()))
					{
						// set child visible.
						root->Data()->m_bChildVisible[childIndex] = true;

						// Reset current info for going one level down.
						root       = *tempChild;
						rootBounds = childBounds;

						// Restart the loop.
						continue;
					}
					else
					{
						// Else, no twice bigger children for a lower LOD needed, so create 
						// the octree part containing them between the current root and 
						// its current child, then restart the loop.
						Node* toInsertAfter = *tempChild;
						*tempChild          = new Node();

						// Compute the new children's bounds twice bigger.
						Vector3 oldChildBoundScale = childBounds.Scale();
						UBIGINT childBoundScaleX   = (UBIGINT)oldChildBoundScale.getX();
						UBIGINT childBoundScaleY   = (UBIGINT)oldChildBoundScale.getY();
						UBIGINT childBoundScaleZ   = (UBIGINT)oldChildBoundScale.getZ();

						Vector3 oldChildBoundMinimum = childBounds.Minimum();
						Vector3 childBoundMinimum(oldChildBoundMinimum - Vector3((FFLOAT)(((BIGINT)oldChildBoundMinimum.getX())%(childBoundScaleX * 2)), (FFLOAT)(((BIGINT)oldChildBoundMinimum.getY())%(childBoundScaleY * 2)), (FFLOAT)(((BIGINT)oldChildBoundMinimum.getZ())%(childBoundScaleZ * 2))));

						AABBox  newChildBounds(m_info.m_data, childBoundMinimum, childBoundMinimum + oldChildBoundScale * 2.0f, AABB_GENERATOR_MINMAX);

						VoxelNodeToDraw* newData = new VoxelNodeToDraw(0, -1, newChildBounds);
						(*tempChild)->SetData(newData);

						// Check if previous current child bounds are in the newly created parent Box, that is the new current
						// root's child bounds which will contain previous current children of the root as its own 
						// children is matching.
						SMALLINT newChildIndex = determineChildInParentBox(newChildBounds, childBounds.Center());
						if(newChildIndex == -1)
							return; // shouldn't happen.

						(*tempChild)->SetChild(newChildIndex, toInsertAfter);

						// Restart the loop.
						continue;
					}
				}
			}
			// Node empty, Insert Here
			else
			{
				*tempChild = new Node(newOne);
				//root->SetChild(childIndex, tempChild);
				break;
			}
		} // end for infinite
	} // end else of if(remove).
}

SMALLINT OcelotVoxelMesh::determineChildInParentBox(AABBox parentBox, Vector3 position)
{
	// Check if the position is not in the box first.
	// avoid next computation if not in bounds.
	if(!parentBox.intersect(position))
		return -1;

	// Then, check the position regarding to the center 
	// of the box and determine in which child node we are.
	// Note: use getters once for not calling them each time.
	FFLOAT parentCenterX = parentBox.Center().getX();
	FFLOAT parentCenterY = parentBox.Center().getY();
	FFLOAT parentCenterZ = parentBox.Center().getZ();
	FFLOAT positionX     = position.getX();
	FFLOAT positionY     = position.getY();
	FFLOAT positionZ     = position.getZ();

	// Note: use else if for once having reach the case, it 
	// avoid to test other tests.
	if(parentCenterX < positionX && parentCenterY < positionY && parentCenterZ < positionZ)
		return 0; // if pos is higher than center in every axes, higher back left child 0.
	if(parentCenterX < positionX && parentCenterY < positionY && parentCenterZ > positionZ)
		return 1; // if pos is higher than center in every axes unless on Z, higher front left child 1.
	if(parentCenterX < positionX && parentCenterY > positionY && parentCenterZ < positionZ)
		return 2; // if pos is higher than center in every axes unless on Y, lower back left child 2.
	if(parentCenterX < positionX && parentCenterY > positionY && parentCenterZ > positionZ)
		return 3; // if pos is higher than center in X but not on Y and Z, lower front left child 3.
	if(parentCenterX > positionX && parentCenterY < positionY && parentCenterZ < positionZ)
		return 4; // if pos is higher than center in every axes unless on X, higher back right child 4.
	if(parentCenterX > positionX && parentCenterY < positionY && parentCenterZ > positionZ)
		return 5; // if pos is higher than center in Y but not on X and Z, higher front right child 5.
	if(parentCenterX > positionX && parentCenterY > positionY && parentCenterZ < positionZ)
		return 6; // if pos is higher than center in Z but not on X and Y, lower back right child 6.
	if(parentCenterX > positionX && parentCenterY > positionY && parentCenterZ > positionZ)
		return 7; // if pos is smaller than center in every axis, lower front right child 7.

	// Default value if not matching 
	return -1;
}

VVOID    OcelotVoxelMesh::releaseNodeToDraw(VoxelNodeToDraw* toRelease)
{
	// Check node validity.
	if(!toRelease)
		return;

	// Check if entity present
	// delete it.
	releaseNodeGeometry(toRelease);

	// Set other attributes to default value.
	toRelease->m_iKeyID  = -1;
	toRelease->m_iLod    =  0;
	//toRelease->m_bToLoad = false;
}

VVOID    OcelotVoxelMesh::releaseNodeGeometry(VoxelNodeToDraw* toOffload)
{
	// Check if entity present
	// delete it.
	if(toOffload->m_entity.Size() != 0)
	{
		for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toOffload->m_entity.Size(); currEnt++)
		{
			//IDXVoxelEntity* toDelete = toOffload->m_entity[currEnt];
			DeletePointer(toOffload->m_entity[currEnt]);
		}
	}

	toOffload->m_entity.unmap();
	//toOffload->m_bToLoad = false;
}

VVOID    OcelotVoxelMesh::update(FFLOAT deltaTime)
{
	// update block's visibility.
	//updateVisibility(&m_root);
	updateVisibility2(&m_root);

	// Update the Octree entities.
	m_root.updateTree(UpdateMethod, deltaTime);
}

VVOID    OcelotVoxelMesh::renderMesh()
{
	// Render the octree entities.
	m_root.applyToTree(RenderMethod);
}

BBOOL    OcelotVoxelMesh::updateVisibility(Node* toRecurse)
{
	if(!toRecurse)
		return false;

	// Grab node's useful information.
	VoxelNodeToDraw* nodeToDraw = toRecurse->Data();
	USMALLINT lodCount          = LodCount();
	USMALLINT nodeLOD           = nodeToDraw->m_iLod;
	AABBox nodeBounds           = nodeToDraw->m_bounds;
	Vector3   eyePos            = m_info.m_data.GetCamera()->Info().Eye();

	// if the node has been determined as visible with its LOD
	// it means that we do not need children geometry which are
	// blocks of lower detail which are not going to be used anyway.
	// First valid the LOD, second check if current node is visible
	// and contains entity(ies) for this LOD and if it is the case
	// and that it contains children as well, delete them.
	BBOOL isVisible = false;
	BBOOL isFar     = false;
	if(nodeLOD < lodCount)
	{
		if(nodeToDraw->m_bVisible && nodeToDraw->m_entity.Size() != 0)
		{
			for(USMALLINT currChild = 0; currChild < 8; currChild++)
			{
				// Grab the current child.
				Node* tempChild = *toRecurse->Child(currChild);
				if(tempChild && nodeToDraw->m_bChildVisible[currChild])
				{
					if(tempChild->Data())
					{
						if(tempChild->Data()->m_entity.Size() != 0)
						{
							releaseNodeGeometry(tempChild->Data());
						}
					}
				}
			}
		}

		// Result for the current node's main block entity.
		isVisible = boundsVisible(nodeBounds, eyePos, nodeLOD, &isFar);//frustumTest(nodeBounds);

		// Now check regarding to the LOD if TCells have to be set to visible as well.
		// The main block is the first entity in the entity's array, and 6 TCells will
		// follow if LOD > 0.
		if(nodeToDraw->m_iKeyID > -1)
		{
			if((nodeLOD > 0) && (isVisible) && (nodeToDraw->m_entity.Size() != 0))
			{
				if(nodeToDraw->m_entity.Size() > 1)
				{
					// Compute Bounds for neighboring TCells, so at each face
					// of the main block.
					for(USMALLINT currFace = 0; currFace < 6; currFace++)
					{
						// Compute block face bounds.
						Vector3 nodeBoundsScale    = nodeBounds.Scale();
						Vector3 currFaceCorrection = Vector3(nodeBoundsScale.getX() * tCellBoundsSign[currFace][0], nodeBoundsScale.getY() * tCellBoundsSign[currFace][1], nodeBoundsScale.getZ() * tCellBoundsSign[currFace][2]); 
						AABBox currFaceBounds(BaseDXData(), nodeBounds.Minimum() + currFaceCorrection, nodeBounds.Maximum() + currFaceCorrection, AABB_GENERATOR_MINMAX);
						if((currFaceBounds.Center() - eyePos).Vec3LengthSquared() > (nodeBounds.Center() - eyePos).Vec3LengthSquared())
							continue;

						BBOOL currFaceVisible = boundsVisible(currFaceBounds, eyePos, nodeLOD, NULL);//frustumTest(currFaceBounds);

						SMALLINT currTCellID  = nodeToDraw->m_iTransitionIndex[currFace];
						if(currTCellID > -1)
						{
							nodeToDraw->m_entity[currTCellID]->SetVisible(!currFaceVisible);
						}
					}
				}
			}

			// Set the main block bool to visible.
			nodeToDraw->m_bVisible = isVisible;

			// If no entity(ies) present but has to be rendered
			// compute geometry needed for node entity creation.
			if((nodeToDraw->m_entity.Size() == 0)/* && !(nodeToDraw->m_bToLoad)*/ && (nodeToDraw->m_bVisible))
			{
				LOD(nodeLOD)->refreshGeometry(nodeToDraw->m_iKeyID);
				//nodeToDraw->m_bToLoad = true;
			}

			// If lowest detailed LOD and not visible, do not need geometry anymore
			// so delete it.
			if((nodeToDraw->m_entity.Size() != 0) && !(nodeToDraw->m_bVisible) && (nodeLOD == lodCount - 1) && isFar)
			{
				releaseNodeGeometry(nodeToDraw);
			}
		}
		else
			// Else set the main block bool visible to not visible.
			nodeToDraw->m_bVisible = false;
	}

	// Here, the recursion take place.
	// If child visible, restart process for children.
	BBOOL childVisible = false;
	for(USMALLINT currChild = 0; currChild < 8; currChild++)
	{
		if(!nodeToDraw->m_bChildVisible[currChild] && isFar)
		{
			continue;
		}
		BBOOL currChildVisible = updateVisibility(*toRecurse->Child(currChild));
		nodeToDraw->m_bChildVisible[currChild] = currChildVisible;
		childVisible = childVisible || currChildVisible;
	}

	// for every time being applied in this case
	// If entity(ies) in the node and not visible
	// Delete it and save memory.
	if((nodeToDraw->m_entity.Size() != 0) && !(nodeToDraw->m_bVisible) && (nodeToDraw->m_iLod > 0) && !isFar)
	{
		// If children have to be visible, we cannot delete the
		// parent node else it is okay
		BBOOL canDelete = true;
		// For each child, grab it and see if has to be seen.
		for(USMALLINT currChild = 0; currChild < 8; currChild++)
		{
			Node* tempChild = *toRecurse->Child(currChild);
			if(tempChild)
			{
				if(tempChild->Data())
				{
					if((tempChild->Data()->m_entity.Size() == 0) && (tempChild->Data()->m_bVisible) && (tempChild->Data()->m_iKeyID > -1))
					{
						// If at least one of the children has to be drawn
						// and the parent can be deleted, so set to false
						// and break.
						canDelete = false;
						break;
					}
				}
			}
		}

		// If zero children have to be drawn
		// delete parent.
		if(canDelete)
		{
			releaseNodeGeometry(nodeToDraw);
		}
	}

	// Return either if entity exists or if children exist
	return (nodeToDraw->m_entity.Size() != 0)/* || nodeToDraw->m_bToLoad*/ || (childVisible);
}

BBOOL    OcelotVoxelMesh::updateVisibility2(Node* toRecurse)
{
	if(!toRecurse)
		return false;

	// Grab node's useful information.
	VoxelNodeToDraw* nodeToDraw = toRecurse->Data();
	USMALLINT lodCount          = LodCount();
	USMALLINT nodeLOD           = nodeToDraw->m_iLod;
	AABBox nodeBounds           = nodeToDraw->m_bounds;
	Vector3   eyePos            = m_info.m_data.GetCamera()->Info().Eye();

	// if the node has been determined as visible with its LOD
	// it means that we do not need children geometry which are
	// blocks of lower detail which are not going to be used anyway.
	// First valid the LOD, second check if current node is visible
	// and contains entity(ies) for this LOD and if it is the case
	// and that it contains children as well, delete them.
	BBOOL isVisible = false;
	BBOOL isFar     = false;
	if(nodeLOD < lodCount)
	{
		/*if(nodeToDraw->m_bVisible && nodeToDraw->m_entity.Size() != 0)
		{
			for(USMALLINT currChild = 0; currChild < 8; currChild++)
			{
				// Grab the current child.
				Node* tempChild = *toRecurse->Child(currChild);
				if(tempChild && nodeToDraw->m_bChildVisible[currChild])
				{
					if(tempChild->Data())
					{
						if(tempChild->Data()->m_entity.Size() != 0)
						{
							releaseNodeGeometry(tempChild->Data());
						}
					}
				}
			}
		}*/

		// Result for the current node's main block entity.
		isVisible = boundsVisible(nodeBounds, eyePos, nodeLOD, &isFar);//frustumTest(nodeBounds);

		// Now check regarding to the LOD if TCells have to be set to visible as well.
		// The main block is the first entity in the entity's array, and 6 TCells will
		// follow if LOD > 0.
		if(nodeToDraw->m_iKeyID > -1)
		{
			if((nodeLOD > 0) && (isVisible) && (nodeToDraw->m_entity.Size() != 0))
			{
				if(nodeToDraw->m_entity.Size() > 1)
				{
					// Compute Bounds for neighboring TCells, so at each face
					// of the main block.
					for(USMALLINT currFace = 0; currFace < 6; currFace++)
					{
						// Compute block face bounds.
						Vector3 nodeBoundsScale    = nodeBounds.Scale();
						Vector3 currFaceCorrection = Vector3(nodeBoundsScale.getX() * tCellBoundsSign[currFace][0], nodeBoundsScale.getY() * tCellBoundsSign[currFace][1], nodeBoundsScale.getZ() * tCellBoundsSign[currFace][2]); 
						AABBox currFaceBounds(BaseDXData(), nodeBounds.Minimum() + currFaceCorrection, nodeBounds.Maximum() + currFaceCorrection, AABB_GENERATOR_MINMAX);
						if((currFaceBounds.Center() - eyePos).Vec3LengthSquared() > (nodeBounds.Center() - eyePos).Vec3LengthSquared())
							continue;

						BBOOL currFaceVisible = boundsVisible(currFaceBounds, eyePos, nodeLOD, NULL);//frustumTest(currFaceBounds);

						SMALLINT currTCellID  = nodeToDraw->m_iTransitionIndex[currFace];
						if(currTCellID > -1)
						{
							nodeToDraw->m_entity[currTCellID]->SetVisible(currFaceVisible);

							if(nodeToDraw->m_entity[currTCellID]->IsVisible() && m_bTorchOn)
								nodeToDraw->m_entity[currTCellID]->TorchOn();
							else
								nodeToDraw->m_entity[currTCellID]->TorchOff();
						}
					}
				}
			}

			// Set the main block bool to visible.
			nodeToDraw->m_bVisible = isVisible;
			nodeToDraw->m_entity[0]->SetVisible(isVisible);
			if(nodeToDraw->m_entity[0]->IsVisible() && m_bTorchOn)
				nodeToDraw->m_entity[0]->TorchOn();
			else
				nodeToDraw->m_entity[0]->TorchOff();

			// If no entity(ies) present but has to be rendered
			// compute geometry needed for node entity creation.
			if((nodeToDraw->m_entity.Size() == 0)/* && !(nodeToDraw->m_bToLoad)*/ && (nodeToDraw->m_bVisible))
			{
				LOD(nodeLOD)->refreshGeometry(nodeToDraw->m_iKeyID);
				//nodeToDraw->m_bToLoad = true;
			}

			// If lowest detailed LOD and not visible, do not need geometry anymore
			// so delete it.
			/*if((nodeToDraw->m_entity.Size() != 0) && !(nodeToDraw->m_bVisible) && (nodeLOD == lodCount - 1) && isFar)
			{
				releaseNodeGeometry(nodeToDraw);
			}*/
		}
		else
		{
			// Else set the main block bool visible to not visible
			// because empty node.
			nodeToDraw->m_bVisible = false;
			//nodeToDraw->m_entity[0]->SetVisible(false);
		}
	}

	// Here, the recursion take place.
	// If child visible, restart process for children.
	BBOOL childVisible = false;
	for(USMALLINT currChild = 0; currChild < 8; currChild++)
	{
		if(!nodeToDraw->m_bChildVisible[currChild] && isFar)
		{
			continue;
		}
		BBOOL currChildVisible = updateVisibility2(*toRecurse->Child(currChild));
		nodeToDraw->m_bChildVisible[currChild] = currChildVisible;
		childVisible = childVisible || currChildVisible;
	}

	// for every time being applied in this case
	// If entity(ies) in the node and not visible
	// Delete it and save memory.
	/*if((nodeToDraw->m_entity.Size() != 0) && !(nodeToDraw->m_bVisible) && (nodeToDraw->m_iLod > 0) && !isFar)
	{
		// If children have to be visible, we cannot delete the
		// parent node else it is okay
		BBOOL canDelete = true;
		// For each child, grab it and see if has to be seen.
		for(USMALLINT currChild = 0; currChild < 8; currChild++)
		{
			Node* tempChild = *toRecurse->Child(currChild);
			if(tempChild)
			{
				if(tempChild->Data())
				{
					if((tempChild->Data()->m_entity.Size() == 0) && (tempChild->Data()->m_bVisible) && (tempChild->Data()->m_iKeyID > -1))
					{
						// If at least one of the children has to be drawn
						// and the parent can be deleted, so set to false
						// and break.
						canDelete = false;
						break;
					}
				}
			}
		}

		// If zero children have to be drawn
		// delete parent.
		if(canDelete)
		{
			releaseNodeGeometry(nodeToDraw);
		}
	}*/

	// Return either if entity exists or if children exist
	return (nodeToDraw->m_entity.Size() != 0)/* || nodeToDraw->m_bToLoad*/ || (childVisible);
}

VVOID    OcelotVoxelMesh::updateOctree2(BIGINT keyID, USMALLINT lod, BBOOL remove)
{
	// Function which will trigger modifications
	// on the octree content regarding to what
	// has been edited.
	// First, grab the voxel block.
	OcelotVoxelGeometry* block = Block(keyID, lod);

	// Check the validity.
	if(!block)
		return;

	// Then, two cases, either remove it if determined
	// as needed no more, or add a new one.
	if(remove)
	{
		//Node* res = static_cast<Node*>(m_root.search(SearchMethod, block->GetAABBox()));
		Node* result = FindNode(block->GetAABBox(), &m_root);
		if(!result)
		{
			// Already removed.
			return;
		}
		releaseNodeToDraw(result->Data());
	}
	else
	{
		// Add a new one.
		Node* root = &m_root;
		AABBox rootBounds = root->Data()->m_bounds;
		AABBox newBounds  = block->GetAABBox();
		newBounds.SetBaseData(m_info.m_data);

		// Create the new VoxelNode.
		VoxelNodeToDraw* newOne = new VoxelNodeToDraw(lod, keyID, newBounds);

		{   
			// Create the mesh when node has to be created,
			// NOTE: in this scenario, every node containing
			// a potential mesh (that is keyID != -1) will 
			// have their potential mesh loaded at octree's
			// node creation.
			USMALLINT entityCount = 1; // Main block by default. 
	
			// (NOTE: + 6 Transition block if higher LOD than 0).
			if(lod > 0)
				entityCount = 7;

			newOne->m_entity.unmap(); // we never know.
			newOne->m_entity.map(entityCount);

			// Create new entity(ies).
			VoxelVertex* vertices  = block->VBuffer(); // shared vertex buffer.
			UBIGINT      vertCount = block->VCount();
			AABBox blockBounds(block->GetAABBox());
			blockBounds.SetBaseData(m_info.m_data);

			for(USMALLINT currEnt = 0; currEnt < entityCount; currEnt++)
			{
				OcelotVoxelEntityInfo entInfo(m_info.m_sEffectPath, m_info.m_light, m_info.m_fog, m_info.m_material, m_info.m_matPalette, blockBounds.Minimum());
				newOne->m_entity[currEnt] = new OcelotVoxelEntity(m_info.m_data, entInfo);
				newOne->m_entity[currEnt]->SetBounds(new AABBox(blockBounds));
			}

			// Set TCell index to not existing by default.
			for(USMALLINT currEnt = 0; currEnt < entityCount - 1; currEnt++)
				newOne->m_iTransitionIndex[currEnt] = -1;

			USMALLINT entityCounter = 0;
			for(USMALLINT currEnt = 0; currEnt < entityCount; currEnt++)
			{
				// Check if no indices.
				UBIGINT  indCount = block->ICount(currEnt);
				if(indCount == 0)
				{
					// And is a TCell if any, set to invalid.
					if(currEnt > 0)
					{
						newOne->m_iTransitionIndex[currEnt - 1] = -1;
					}

					// create even if no indices or rendering issue will appear while iterating through octree
					// the node contains no indices but will be set to visible and thus to render, so render nothing
					// but do not bug.
					newOne->m_entity[currEnt]->createInstance(vertices, vertCount, NULL, indCount);

					// Else, pass to the next entity.
					continue;
				}

				// Grab indices for the curr entity (main block and TCells if any).
				// Each part of the block has its own indices buffer.
				UBIGINT* indices  = block->IBuffer(currEnt);

				// If one of the TCells
				if(currEnt > 0)
				{
					newOne->m_iTransitionIndex[currEnt - 1] = entityCounter;
				}
				entityCounter++;

				// Initialize entity(ies) normaly 
				newOne->m_entity[currEnt]->createInstance(vertices, vertCount, indices, indCount);
			}

			// Save memory, release block geometry now it is
			// in the node to draw.
			block->releaseGeometry();

			// If TCells (LOD > 0) and entity for TCells exist,
			// Set entity to not visible for now, because the 
			// node could not be seen.
			if(lod > 0 && newOne->m_entity.Size() > 1)
			{
				for(USMALLINT currTrans = 0; currTrans < 6; currTrans++)
				{
					SMALLINT transIndex = newOne->m_iTransitionIndex[currTrans];

					if(transIndex > -1)
					{
						newOne->m_entity[transIndex]->NotVisible();
					}
				}
			}
		}

		// Infinite loop waiting for break call if matching.
		for( ; ; )
		{
			// compute the child index for reaching its position in the tree.
			SMALLINT childIndex = determineChildInParentBox(rootBounds, newBounds.Center());

			if(childIndex == -1)
				return;

			// Then, reach the child. (NOTE: casting only because of inherited class and use of base class methods)
			Node** tempChild = root->Child(childIndex);

			// if node
			if(*tempChild)
			{
				// Else, grab the child box.
				AABBox childBounds = (**tempChild).Data()->m_bounds;

				// If the child bounds are equal to the new block's bounds to process,
				// replace the previous node by the new one and break.
				if(Equal<AABBox>(childBounds, newBounds))
				{
					// But check if previous node had geometry components and if it is the
					// case, it will mean that edits have been done, so that we have to re
					// compute geometry for the current LOD for the passed block keyID.
					if((**tempChild).Data()->m_entity.Size() != 0)
					{
						newOne->m_entity  = (**tempChild).Data()->m_entity;
						newOne->m_entity.SetCopy(false); // Become the original
						(**tempChild).Data()->m_entity.SetCopy(true); // Become the copy and will not be destroy when releasing node to draw.
						//newOne->m_bToLoad = true;
						LOD(lod)->refreshGeometry(newOne->m_iKeyID);
					}

					// To Do: Verify if memory leaks if previous
					// voxel node to draw here has to be deleted
					// before to insert the new one. Modified for mem leaks correction on 15th November 2012.
					delete (**tempChild).Data();
					(**tempChild).SetData(NULL);
					(**tempChild).SetData(newOne);
					break;
				}
				else
				{
					// Else, go one level down and check for children existing.
					// If children are present, set the current root and its bounds
					// with the child's pointer as current root and its bounds as
					// current root's bounds (going one level down in the process)
					// and restart to the start of the loop.
					if((determineChildInParentBox(childBounds, newBounds.Center()) != -1) && (childBounds.Scale().getX() > newBounds.Scale().getX()))
					{
						// set child visible.
						root->Data()->m_bChildVisible[childIndex] = true;

						// Reset current info for going one level down.
						root       = *tempChild;
						rootBounds = childBounds;

						// Restart the loop.
						continue;
					}
					else
					{
						// Else, no twice bigger children for a lower LOD needed, so create 
						// the octree part containing them between the current root and 
						// its current child, then restart the loop.
						Node* toInsertAfter = *tempChild;
						*tempChild          = new Node();

						// Compute the new children's bounds twice bigger.
						Vector3 oldChildBoundScale = childBounds.Scale();
						UBIGINT childBoundScaleX   = (UBIGINT)oldChildBoundScale.getX();
						UBIGINT childBoundScaleY   = (UBIGINT)oldChildBoundScale.getY();
						UBIGINT childBoundScaleZ   = (UBIGINT)oldChildBoundScale.getZ();

						Vector3 oldChildBoundMinimum = childBounds.Minimum();
						Vector3 childBoundMinimum(oldChildBoundMinimum - Vector3((FFLOAT)(((BIGINT)oldChildBoundMinimum.getX())%(childBoundScaleX * 2)), (FFLOAT)(((BIGINT)oldChildBoundMinimum.getY())%(childBoundScaleY * 2)), (FFLOAT)(((BIGINT)oldChildBoundMinimum.getZ())%(childBoundScaleZ * 2))));

						AABBox  newChildBounds(m_info.m_data, childBoundMinimum, childBoundMinimum + oldChildBoundScale * 2.0f, AABB_GENERATOR_MINMAX);

						VoxelNodeToDraw* newData = new VoxelNodeToDraw(0, -1, newChildBounds);
						(*tempChild)->SetData(newData);

						// Check if previous current child bounds are in the newly created parent Box, that is the new current
						// root's child bounds which will contain previous current children of the root as its own 
						// children is matching.
						SMALLINT newChildIndex = determineChildInParentBox(newChildBounds, childBounds.Center());
						if(newChildIndex == -1)
							return; // shouldn't happen.

						(*tempChild)->SetChild(newChildIndex, toInsertAfter);

						// Restart the loop.
						continue;
					}
				}
			}
			// Node empty, Insert Here
			else
			{
				*tempChild = new Node(newOne);
				//root->SetChild(childIndex, tempChild);
				break;
			}
		} // end for infinite
	} // end else of if(remove).
}

VVOID    OcelotVoxelMesh::releaseInstance()
{
	// Release Node to draw in the Octree.
	m_root.applyToTree(ReleaseNode);

	// Release Octree.
	m_root.destroy();
}

UBIGINT  OcelotVoxelMesh::GetVertexCount(Node* toRecurse)
{
	// Check node validity
	if(!toRecurse)
		return 0;

	// Grab the node to draw.
	VoxelNodeToDraw* toDraw = toRecurse->Data();

	// Check node validity
	if(!toDraw)
		return 0;

	// Get the entire vertices count in the octree's node to draw :-).
	// Just for drawing later.
	UBIGINT counter = 0;

	// Add entity(ies) vertex count for this node.
	if(toDraw->m_entity.Size() != 0)
	{
		for(USMALLINT currEnt = 0; currEnt < (USMALLINT)toDraw->m_entity.Size(); currEnt++)
		{
			counter += toDraw->m_entity[currEnt]->VertexNb();
		}
	}

	// Then, recurse through children.
	for(USMALLINT currChild = 0; currChild < 8; currChild++)
	{
		counter += GetVertexCount(*toRecurse->Child(currChild));
	}

	return counter;
}
