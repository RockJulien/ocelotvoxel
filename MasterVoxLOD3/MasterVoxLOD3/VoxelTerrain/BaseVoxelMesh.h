#ifndef DEF_BASEVOXELMESH_H
#define DEF_BASEVOXELMESH_H

#include "DiscreteVoxelMeshLOD.h"

namespace Ocelot
{
	class BaseVoxelMesh
	{
	protected:

		// Attributes
		vector<DiscreteVoxelMeshLOD*> m_LODs;

	public:

		// Constructor & Destructor
		BaseVoxelMesh();
		BaseVoxelMesh(DiscreteVoxelMeshLODInfo info, USMALLINT iLODs);
		~BaseVoxelMesh();

		// Methods
		VVOID addToEdit(OcelotEditVoxel* toEdit, BBOOL refreshGeo);

		// Accessors
		inline DiscreteVoxelMeshLOD* LOD(USMALLINT lod) { return m_LODs[lod];}
		inline USMALLINT             LodCount() const { return m_LODs.size();}
		inline OcelotVoxelGeometry*  Block(BIGINT keyID, USMALLINT lod) { return m_LODs[lod]->GetBlock(keyID);}
	};
}

#endif