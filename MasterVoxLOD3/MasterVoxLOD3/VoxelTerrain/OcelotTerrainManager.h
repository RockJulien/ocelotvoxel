#ifndef DEF_OCELOTTERRAINMANAGER_H
#define DEF_OCELOTTERRAINMANAGER_H

#include "OcelotVoxelMesh.h"
#include "..\Utilities\OcelotEditAABBox.h"
#include "..\Utilities\OcelotEditBSphere.h"

namespace Ocelot
{
	class OcelotTerrainManager
	{
	private:

			// Attributes
			std::vector<OcelotEditVoxel*> m_edits;  // Store user's edition.
			OcelotVoxelMesh*              m_terrainMesh;
			
			FFLOAT                        m_fLODDistance;
			UBIGINT                       m_iEditScale;
			BBOOL                         m_bTorchOn;

			// Private Copy and Assignment
			OcelotTerrainManager(const OcelotTerrainManager&);
			OcelotTerrainManager& operator= (const OcelotTerrainManager&);

			// Private Methods
			// For dynamic edits only and called in Handle Inputs
			VVOID   increaseScale();
			VVOID   decreaseScale();
			VVOID   addAABBox();
			VVOID   addSphere();
			VVOID   remAABBox();
			VVOID   remSphere();
			Vector3 sign(Vector3 camDir);
			VVOID   releaseEdits();

	public:

			// Constructor & Destructor
			OcelotTerrainManager(OcelotVoxelMeshInfo info, USMALLINT iLODs, FFLOAT lODDistance = LOD_BASE_DISTANCE);
			~OcelotTerrainManager();

			// Methods per frame.
			VVOID update(FFLOAT deltaTime);
			VVOID handleInput();
			VVOID render();

			// Methods to use once and allow procedural pre-computed terrain generation.
			VVOID addAABBox(AABBox area, BBOOL refresh = false);
			VVOID addSphere(BSphere area, BBOOL refresh = false);
			VVOID remAABBox(AABBox area, BBOOL refresh = false, BBOOL remove = false);
			VVOID remSphere(BSphere area, BBOOL refresh = false, BBOOL remove = false);
			VVOID releaseMesh();

			// Save & Load Edits.
			VVOID loadEdits(STRING path);
			VVOID saveEdits();

			// Accessors
			inline OcelotVoxelMesh* GetTerrainMesh() { return m_terrainMesh;}
			inline UBIGINT          GetVertexCount() const { return m_terrainMesh->GetVertexCount();}
			inline UBIGINT          GetEditScale() const { return m_iEditScale;}
			inline USMALLINT        GetLODCount() const { return m_terrainMesh->LodCount();}
			inline Vector3          GetCamPos() const { return m_terrainMesh->Info()->m_data.GetCamera()->Info().Eye();}
	};
}

#endif