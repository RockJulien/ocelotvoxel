#include "DiscreteVoxelMeshLOD.h"

using namespace Ocelot;
using namespace std;

DiscreteVoxelMeshLOD::DiscreteVoxelMeshLOD() :
m_info()
{

}

DiscreteVoxelMeshLOD::DiscreteVoxelMeshLOD(DiscreteVoxelMeshLODInfo info) :
m_info(info)
{

}

DiscreteVoxelMeshLOD::~DiscreteVoxelMeshLOD()
{
	releaseMap();
}

BIGINT  			 DiscreteVoxelMeshLOD::GetKeyID(BIGINT blockPosX, BIGINT blockPosY, BIGINT blockPosZ)
{
	// A Voxel has 256 possible position on an edge.
	return (blockPosX + 128) + 256 * (blockPosY + 128) + 256 * 256 * (blockPosZ + 128);
}

OcelotVoxelGeometry* DiscreteVoxelMeshLOD::GetBlock(BIGINT keyID)
{
	map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_meshes.find(keyID); 
	if(iter == m_meshes.end()) 
		return NULL; 
	
	return iter->second;
}

VVOID                DiscreteVoxelMeshLOD::refreshGeometry(BIGINT keyID)
{
	// Grab the block at the parameter ID.
	OcelotVoxelGeometry* toRefresh(GetBlock(keyID));

	if(!toRefresh)
		return;

	// Re-compute the geometry.
	toRefresh->computeGeometry();

	// If either all in empty space or in solid space
	// remove the block because not needed.
	if(toRefresh->NoNeedBlock())
		remBlock(keyID);
}

VVOID                DiscreteVoxelMeshLOD::remBlock(BIGINT keyID)
{
	map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_meshes.find(keyID); 
	if(iter == m_meshes.end()) 
		return;
	
	if(m_meshes[keyID])
	{
		DeletePointer(m_meshes[keyID]);
	}

	m_meshes.erase(iter);
}

VVOID                DiscreteVoxelMeshLOD::releaseMap()
{
	// map structure does not free pointer for us
	// so be sure to release them before the class
	// be released itslef.
	for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = m_meshes.begin(); iter != m_meshes.end(); iter++)
		DeletePointer(iter->second);

	m_meshes.clear();
}

VVOID  DiscreteVoxelMeshLOD::addToEdit(OcelotEditVoxel* toEdit, BBOOL refreshGeo)
{
	// Grab area components for looping through block part overlapped.
	BIGINT startPosX;
	BIGINT startPosY;
	BIGINT startPosZ;
	BIGINT endPosX;
	BIGINT endPosY;
	BIGINT endPosZ;

	blockPartOverlappedByBox(toEdit->scaleAABBox(m_info.m_fMeshScale), 
												 &startPosX, 
												 &startPosY, 
												 &startPosZ, 
												 &endPosX, 
												 &endPosY, 
												 &endPosZ);

	// Loop through overlapped area.
	for(BIGINT currPartX = startPosX; currPartX <= endPosX; currPartX++)
	{
		for(BIGINT currPartY = startPosY; currPartY <= endPosY; currPartY++)
		{
			for(BIGINT currPartZ = startPosZ; currPartZ <= endPosZ; currPartZ++)
			{
				// Grab the current part to edit.
				OcelotVoxelGeometry* block = Block(currPartX, currPartY, currPartZ);

				// If there is nothing at this position.
				// simply add as a new block
				if(!block)
				{
					// Create the block and add it to the overall mesh.
					BIGINT keyID = addBlock(currPartX, currPartY, currPartZ);

					// Grab the new block for re-edit it then.
					block = m_meshes[keyID];
				}

				// Add what to edit to the block.
				block->addPartToEdit(toEdit);

				// In the case, refresh geometry is needed
				// compute geometry.
				if(refreshGeo)
					refreshGeometry(GetKeyID(currPartX, currPartY, currPartZ));
			}
		}
	}
}

BIGINT DiscreteVoxelMeshLOD::addBlock(BIGINT blockPosX, BIGINT blockPosY, BIGINT blockPosZ)
{
	// Grab the key ID thanks to position.
	BIGINT mapKeyID = GetKeyID(blockPosX, blockPosY, blockPosZ);

	// Create the new block.
	OcelotVoxelGeometry* newBlock = new OcelotVoxelGeometry(blockPosX * m_info.m_iMeshSizeX, 
															blockPosY * m_info.m_iMeshSizeY, 
															blockPosZ * m_info.m_iMeshSizeZ, 
															m_info.m_iMeshSizeX, 
															m_info.m_iMeshSizeY, 
															m_info.m_iMeshSizeZ, 
															m_info.m_fMeshScale, 
															m_info.m_bTransitionNeeded);

	// If already something at the position, free the pointer
	// first.
	if(m_meshes[mapKeyID])
		DeletePointer(m_meshes[mapKeyID]);

	// Store the new block.
	m_meshes[mapKeyID] = newBlock;

	// return the key ID of the new block added.
	return mapKeyID;
}

VVOID  DiscreteVoxelMeshLOD::remBlock(BIGINT blockPosX, BIGINT blockPosY, BIGINT blockPosZ)
{
	remBlock(GetKeyID(blockPosX, blockPosY, blockPosZ));
}

VVOID  DiscreteVoxelMeshLOD::blockPartOverlappedByBox(const AABBox& box, BIGINT* firstX, BIGINT* firstY, BIGINT* firstZ, BIGINT* lastX, BIGINT* lastY, BIGINT* lastZ)
{
	// Determine the part overlapped in the block by a first box passed
	// as parameter and return the area overlapped as another box.
	// We deal with integer for avoiding accumulation of floating point
	// error caused by different computation because results will be used
	// for mapping arrays (so, errors is not allowed).
	Vector3 boxPos   = box.Minimum() - Vector3(1.0f); // Minus the negative sample layer
	BIGINT minPosX   = boxPos.getX();
	BIGINT minPosY   = boxPos.getY();
	BIGINT minPosZ   = boxPos.getZ();
	boxPos   = box.Scale() + Vector3(2.0f);  // Plus the two positive sample layers
	BIGINT extentX   = boxPos.getX();
	BIGINT extentY   = boxPos.getY();
	BIGINT extentZ   = boxPos.getZ();

	// Compute the starting position of the area
	// overlapped.
	*firstX = minPosX / m_info.m_iMeshSizeX;
	if(minPosX < 0) --(*firstX);
	*firstY = minPosY / m_info.m_iMeshSizeY;
	if(minPosY < 0) --(*firstY);
	*firstZ = minPosZ / m_info.m_iMeshSizeZ;
	if(minPosZ < 0) --(*firstZ);

	// Compute the ending position of the area
	// overlapped.
	*lastX = (minPosX + extentX) / m_info.m_iMeshSizeX;
	if(minPosX + extentX < 0) --(*lastX);
	*lastY = (minPosY + extentY) / m_info.m_iMeshSizeY;
	if(minPosY + extentY < 0) --(*lastY);
	*lastZ = (minPosZ + extentZ) / m_info.m_iMeshSizeZ;
	if(minPosZ + extentZ < 0) --(*lastZ);
}
