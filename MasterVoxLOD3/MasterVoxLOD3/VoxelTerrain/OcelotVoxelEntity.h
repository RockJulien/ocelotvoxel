#ifndef DEF_OCELOTVOXELENTITY_H
#define DEF_OCELOTVOXELENTITY_H

#include "IDXVoxelEntity.h"
#include "..\DirectXAPI\DXHelperFunctions.h"
#include "..\Geometry\OcelotFog.h"
#include "..\Geometry\OcelotLight.h"

namespace Ocelot
{
						class OcelotVoxelEntityInfo
						{
						private:

							// Attributes
							OcelotLight*             m_light[2];
							OcelotFog*               m_fog;
							TriPlanarBumpMaterial*   m_material;       // <- will be common to every voxel meshes.
							TriPlanarTexturePalette* m_matPalette;
							WSTRING                  m_sEffectPath;
							//WSTRING*                 m_sTexturePath; // <- that is why these strings have been removed.
							Vector3				     m_vPosition;

						public:

							// Constructor & Destructor
							OcelotVoxelEntityInfo();
							OcelotVoxelEntityInfo(/*WSTRING texturePath[3],*/ WSTRING effectPath, OcelotLight* light[2], OcelotFog* fog, TriPlanarBumpMaterial* mater, TriPlanarTexturePalette* palette, Vector3 position);
							~OcelotVoxelEntityInfo();

							// Methods


							// Accessors
							inline OcelotLight*			  Light(USMALLINT whichOne) const { return m_light[whichOne];}
							inline OcelotFog*			  Fog() const { return m_fog;}
							inline TriPlanarBumpMaterial* Material() { return m_material;}
							inline TriPlanarTexturePalette* Palette() { return m_matPalette;}
							//inline WSTRING*  			  TexturePath() { return m_sTexturePath;}
							inline WSTRING				  EffectPath() const { return m_sEffectPath;}
							inline Vector3				  Position() const { return m_vPosition;}
							inline VVOID				  SetLight(OcelotLight* light, USMALLINT whichOne) { m_light[whichOne] = light;}
							inline VVOID				  SetFog(OcelotFog* fog) { m_fog = fog;}
							inline VVOID                  SetMaterial(TriPlanarBumpMaterial* mater) { if(m_material){ m_material->releaseCOM(); DeletePointer(m_material);} m_material = mater;}
							inline VVOID                  SetPalette(TriPlanarTexturePalette* palette) { if(m_matPalette) { m_matPalette->releaseCOM(); DeletePointer(m_matPalette);} m_matPalette = palette;}
							//inline VVOID                  SetTexturePath(WSTRING textPath[3]) { m_sTexturePath = textPath;}
							inline VVOID				  SetEffectPath(WSTRING effPath) { m_sEffectPath = effPath;}
							inline VVOID				  SetPosition(Vector3 position) { m_vPosition = position;}
						};

	class OcelotVoxelEntity : public IDXVoxelEntity
	{
	private:

		// Attributes
		OcelotVoxelEntityInfo m_info;

	public:

		// Constructor & Destructor
		OcelotVoxelEntity();
		OcelotVoxelEntity(BaseDXData dxData, OcelotVoxelEntityInfo info);
		~OcelotVoxelEntity();

		// Methods per frame
		VVOID update(FFLOAT deltaTime);
		VVOID updateEffectVariables();
		VVOID renderMesh();

		// Methods to use once.
		VVOID createInstance(VoxelVertex* vert, UBIGINT vertCount, UBIGINT* ind, UBIGINT indCount);
		VVOID initializeLayout();
		VVOID initializeEffect();
		VVOID initializeVBuffers(VoxelVertex* vert, UBIGINT vertCount);
		VVOID initializeIBuffers(UBIGINT* ind, UBIGINT indCount);
		VVOID initializeEffectVariables();
		VVOID releaseInstance();

		// Accessors.
		inline OcelotVoxelEntityInfo* Info() { return &m_info;}
	};
}

#endif