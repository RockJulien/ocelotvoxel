#include "SimpleLODExample.h"

using namespace Ocelot;

SimpleLODExample::SimpleLODExample(SimleLODMeshInfo info, USMALLINT iLODs) :
BaseVoxelMesh(info.m_voxLodInfo, iLODs), m_info(info)
{

}

SimpleLODExample::~SimpleLODExample()
{
	for(UBIGINT currEdit = 0; currEdit < (UBIGINT)m_edits.size(); currEdit++)
		DeletePointer(m_edits[currEdit]);
	m_edits.clear();

	m_shapes.clear();
}

VVOID SimpleLODExample::update(FFLOAT deltaTime)
{
	if(m_shapes.Size() != 0)
	{
		List2Iterator<OcelotVoxelEntity*> iter = m_shapes.iterator();
		for(iter.begin(); iter.notEnd(); iter.next())
		{
			iter.data()->update(deltaTime);
		}
	}

	handleInput();
}

VVOID SimpleLODExample::renderMesh()
{
	if(m_shapes.Size() != 0)
	{
		List2Iterator<OcelotVoxelEntity*> iter = m_shapes.iterator();
		for(iter.begin(); iter.notEnd(); iter.next())
		{
			iter.data()->renderMesh();
		}
	}
}

VVOID SimpleLODExample::handleInput()
{
	if(InMng->isPressedAndLocked(OCELOT_N))
		addExample();

	if(InMng->isPressedAndLocked(OCELOT_B))
	{
		if(m_shapes.Size() != 0)
		{
			List2Iterator<OcelotVoxelEntity*> iter = m_shapes.iterator();
			for(iter.begin(); iter.notEnd(); iter.next())
			{
				iter.data()->Bound();
			}
		}
	}
}

VVOID SimpleLODExample::addExample()
{
	// Create a new one at the LOD 0 just in Front of the camera.
	FFLOAT camOffset = 1.0f;
	Vector3 camPos = m_info.m_data.GetCamera()->Info().Eye();
	Vector3 camDir = m_info.m_data.GetCamera()->Info().LookAt();
	camDir.Vec3Normalise();
	Vector3 minBound;
	FFLOAT halfScale = (FFLOAT)(16/2);
	minBound.x((camPos.getX()  - halfScale) + (camOffset * camDir.getX()));
	minBound.y((camPos.getY()  - halfScale) + (camOffset * camDir.getY()));
	minBound.z((camPos.getZ()  - halfScale) + (camOffset * camDir.getZ()));
	Vector3 maxBounds = minBound + Vector3((FFLOAT)(16));
	AABBox newOneBounds(m_info.m_data, minBound, maxBounds, AABB_GENERATOR_MINMAX);
	OcelotEditAABBox* newOne = new OcelotEditAABBox(newOneBounds, false);
	m_edits.push_back(newOne); // Keep a track of created edit shapes.

	// Then, add it to the terrain
	this->addToEdit(newOne, true);

	this->prepareToRender();
}

VVOID SimpleLODExample::prepareToRender()
{
	for(USMALLINT currLOD = 0; currLOD < LodCount(); currLOD++)
	{
		// LOD 0
		for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = LOD(currLOD)->Meshes()->begin(); iter != LOD(currLOD)->Meshes()->end(); iter++)
		{
			if(currLOD == 0)
			{
				VoxelVertex* vertices  = iter->second->VBuffer();
				UBIGINT      vertCount = iter->second->VCount();
				UBIGINT* indices       = iter->second->IBuffer(0);
				UBIGINT  indCount      = iter->second->ICount(0);
				AABBox blockBounds(iter->second->GetAABBox());
				blockBounds.SetBaseData(m_info.m_data);
				OcelotVoxelEntityInfo currInfo(m_info.m_sEffectPath, m_info.m_light, m_info.m_fog, m_info.m_material, m_info.m_matPalette, blockBounds.Minimum());
				OcelotVoxelEntity* newOne = new OcelotVoxelEntity(m_info.m_data, currInfo);
				newOne->SetBounds(new AABBox(blockBounds));
				newOne->createInstance(vertices, vertCount, indices, indCount);
				m_shapes.putBACK(newOne);
				iter->second->releaseGeometry();
			}
			else
			{
				VoxelVertex* vertices  = iter->second->VBuffer();
				UBIGINT      vertCount = iter->second->VCount();
				AABBox blockBounds(iter->second->GetAABBox());
				blockBounds.SetBaseData(m_info.m_data);
				OcelotVoxelEntityInfo currInfo(m_info.m_sEffectPath, m_info.m_light, m_info.m_fog, m_info.m_material, m_info.m_matPalette, blockBounds.Minimum() + Vector3(currLOD * 20.0f, 0.0f, 0.0f));

				for(USMALLINT currEnt = 0; currEnt < 7; currEnt++)
				{
					UBIGINT  indCount = iter->second->ICount(currEnt);

					if(indCount != 0)
					{
						UBIGINT* indices = iter->second->IBuffer(currEnt);
						OcelotVoxelEntity* newOne = new OcelotVoxelEntity(m_info.m_data, currInfo);
						newOne->SetBounds(new AABBox(blockBounds));
						newOne->createInstance(vertices, vertCount, indices, indCount);
						m_shapes.putBACK(newOne);
					}
				}

				iter->second->releaseGeometry();
			}
		}
	}

	// LOD 1
	/*for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = LOD(1)->Meshes()->begin(); iter != LOD(1)->Meshes()->end(); iter++)
	{
		VoxelVertex* vertices  = iter->second->VBuffer();
		UBIGINT      vertCount = iter->second->VCount();
		AABBox blockBounds(iter->second->GetAABBox());
		blockBounds.SetBaseData(m_info.m_data);
		OcelotVoxelEntityInfo currInfo(m_info.m_sEffectPath, m_info.m_light, m_info.m_fog, m_info.m_material, m_info.m_matPalette, blockBounds.Minimum() + Vector3(20.0f, 0.0f, 0.0f));

		for(USMALLINT currEnt = 0; currEnt < 7; currEnt++)
		{
			UBIGINT  indCount = iter->second->ICount(currEnt);

			if(indCount != 0)
			{
				UBIGINT* indices = iter->second->IBuffer(currEnt);
				OcelotVoxelEntity* newOne = new OcelotVoxelEntity(m_info.m_data, currInfo);
				newOne->SetBounds(new AABBox(blockBounds));
				newOne->createInstance(vertices, vertCount, indices, indCount);
				m_shapes.putBACK(newOne);
			}
		}

		iter->second->releaseGeometry();
	}

	// LOD 2
	for(map<BIGINT, OcelotVoxelGeometry*>::iterator iter = LOD(2)->Meshes()->begin(); iter != LOD(2)->Meshes()->end(); iter++)
	{
		VoxelVertex* vertices  = iter->second->VBuffer();
		UBIGINT      vertCount = iter->second->VCount();
		AABBox blockBounds(iter->second->GetAABBox());
		blockBounds.SetBaseData(m_info.m_data);
		OcelotVoxelEntityInfo currInfo(m_info.m_sEffectPath, m_info.m_light, m_info.m_fog, m_info.m_material, m_info.m_matPalette, blockBounds.Minimum() + Vector3(40.0f, 0.0f, 0.0f));

		for(USMALLINT currEnt = 0; currEnt < 7; currEnt++)
		{
			UBIGINT  indCount = iter->second->ICount(currEnt);

			if(indCount != 0)
			{
				UBIGINT* indices  = iter->second->IBuffer(currEnt);
				OcelotVoxelEntity* newOne = new OcelotVoxelEntity(m_info.m_data, currInfo);
				newOne->SetBounds(new AABBox(blockBounds));
				newOne->createInstance(vertices, vertCount, indices, indCount);
				m_shapes.putBACK(newOne);
			}
		}

		iter->second->releaseGeometry();
	}*/
}
