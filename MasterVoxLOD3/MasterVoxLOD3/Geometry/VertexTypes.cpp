#include "VertexTypes.h"

using namespace Ocelot;

DomeVertex::DomeVertex() :
m_vPosition(0.0f)
{

}

DomeVertex::DomeVertex(Vector3 position) :
m_vPosition(position)
{

}

DomeVertex::DomeVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ) :
m_vPosition(posX, posY, posZ)
{

}

DomeVertex::~DomeVertex()
{

}

BasicVertex::BasicVertex() :
DomeVertex(), m_color(0.0f, 0.0f, 0.0f)
{

}

BasicVertex::BasicVertex(Vector3 position, Color color) :
DomeVertex(position), m_color(color)
{

}

BasicVertex::BasicVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Color color) :
DomeVertex(posX, posY, posZ), m_color(color)
{

}

BasicVertex::BasicVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT red, FFLOAT green, FFLOAT blue, FFLOAT alpha) :
DomeVertex(posX, posY, posZ), m_color(red, green, blue, alpha)
{

}

BasicVertex::~BasicVertex()
{
	
}

SimpleVertex::SimpleVertex() :
DomeVertex(), m_vNormal(0.0f), m_vTexCoord(0.0f)
{

}

SimpleVertex::SimpleVertex(Vector3 position, Vector3 normal, Vector2 texCoord) :
DomeVertex(position), m_vNormal(normal), m_vTexCoord(texCoord)
{

}

SimpleVertex::SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 normal, Vector2 texCoord) :
DomeVertex(posX, posY, posZ), m_vNormal(normal), m_vTexCoord(texCoord)
{

}

SimpleVertex::SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, Vector2 texCoord) :
DomeVertex(posX, posY, posZ), m_vNormal(normX, normY, normZ), m_vTexCoord(texCoord)
{

}

SimpleVertex::SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, FFLOAT texU, FFLOAT texV) :
DomeVertex(posX, posY, posZ), m_vNormal(normX, normY, normZ), m_vTexCoord(texU, texV)
{

}

SimpleVertex::~SimpleVertex()
{
	
}

ParticuleVertex::ParticuleVertex() :
DomeVertex(), m_vVelocity(0.0f), m_vScale(0.0f), m_fAge(0.0f), m_iType(0)
{

}

ParticuleVertex::ParticuleVertex(Vector3 position, Vector3 velocity, Vector2 scale, FFLOAT age, UBIGINT type) :
DomeVertex(position), m_vVelocity(velocity), m_vScale(scale), m_fAge(age), m_iType(type)
{

}

ParticuleVertex::ParticuleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 velocity, Vector2 scale, FFLOAT age, UBIGINT type) :
DomeVertex(posX, posY, posZ), m_vVelocity(velocity), m_vScale(scale), m_fAge(age), m_iType(type)
{

}

ParticuleVertex::ParticuleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, Vector2 scale, FFLOAT age, UBIGINT type) :
DomeVertex(posX, posY, posZ), m_vVelocity(velX, velY, velZ), m_vScale(scale), m_fAge(age), m_iType(type)
{

}

ParticuleVertex::ParticuleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, FFLOAT scaleX, FFLOAT scaleY, FFLOAT age, UBIGINT type) :
DomeVertex(posX, posY, posZ), m_vVelocity(velX, velY, velZ), m_vScale(scaleX, scaleY), m_fAge(age), m_iType(type)
{

}

ParticuleVertex::~ParticuleVertex()
{
	
}
