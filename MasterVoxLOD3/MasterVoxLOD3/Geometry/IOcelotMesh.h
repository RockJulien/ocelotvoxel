#ifndef DEF_IOCELOTMESH_H
#define DEF_IOCELOTMESH_H

/*****************************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   IOcelotMesh.h
/   
/  Description: This object is an interface for every object which
/               could be implemented for drawing through an
/               Graphic API and its pipeline.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    30/09/2012
/*****************************************************************************/

#include "OcelotBaseMesh.h"
#include "VertexTypes.h"
#include "ClassAttributeType.h"

namespace Ocelot
{
	class IOcelotMesh : public OcelotBaseMesh
	{
	public:

			// Constructor & Destructor
			IOcelotMesh() : OcelotBaseMesh(){}
	virtual ~IOcelotMesh(){ OcelotBaseMesh::~OcelotBaseMesh(); }

			// Methods per frame.
	virtual VVOID  update(FFLOAT deltaTime)    = 0;
	virtual BIGINT updateEffectVariables()     = 0;
	virtual BIGINT renderMesh()                = 0;

			// Methods to use once.
	virtual BIGINT createInstance()            = 0;
	virtual BIGINT initializeLayout()          = 0;
	virtual BIGINT initializeEffect()          = 0;
	virtual BIGINT initializeVertices()        = 0;
	virtual BIGINT initializeIndices()         = 0;
	virtual VVOID  initializeEffectVariables() = 0;
	virtual VVOID  releaseInstance()           = 0;

			// Extra functions grabbing or setting whatever
	virtual VVOID* GetMeshData(ClassAttributeType attrType) const        = 0;
	virtual VVOID  SetMeshData(VVOID* what, ClassAttributeType attrType) = 0;
	};
}

#endif
