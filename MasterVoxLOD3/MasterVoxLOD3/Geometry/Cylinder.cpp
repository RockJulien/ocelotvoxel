#include "Cylinder.h"

using namespace Ocelot;

Cylinder::Cylinder() :
OcelotDXMesh(), m_info(), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL), 
m_fxWorldInvTrans(NULL), m_fxLight(NULL), m_fxFog(NULL), m_fxEyePosition(NULL), m_iTopBaseIndex(0), 
m_iTopCenterIndex(0), m_iBottomBaseIndex(0), m_iBottomCenterIndex(0)
{
	// load textures.
	BumpMaterial mat;
	loadBumpMaterial(m_info.Device(), m_info.TexturePath(), mat);
	m_info.SetMaterial(mat);
}

Cylinder::Cylinder(CylinderInfo info) :
OcelotDXMesh(), m_info(info), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL), 
m_fxWorldInvTrans(NULL), m_fxLight(NULL), m_fxFog(NULL), m_fxEyePosition(NULL), m_iTopBaseIndex(0), 
m_iTopCenterIndex(0), m_iBottomBaseIndex(0), m_iBottomCenterIndex(0)
{
	// load textures.
	BumpMaterial mat;
	loadBumpMaterial(m_info.Device(), m_info.TexturePath(), mat);
	m_info.SetMaterial(mat);
}

Cylinder::~Cylinder()
{
	releaseInstance();
	OcelotDXMesh::~OcelotDXMesh();
}

VVOID  Cylinder::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());
}

BIGINT Cylinder::updateEffectVariables()
{
	HRESULT hr = S_OK;

	// Compute the world inverse transpose matrix.
	Matrix4x4 matV, matP, matWorldInvTrans;
	matV = m_info.GetCamera()->View();
	matP = m_info.GetCamera()->Proj();
	m_matWorld.Matrix4x4Inverse(matWorldInvTrans);
	matWorldInvTrans.Matrix4x4Transpose(matWorldInvTrans);

	// set mesh's attributes to the shader's attributes.
	HR(m_fxLight->SetRawValue(m_info.Light(), 0, sizeof(OcelotLight)));
	HR(m_fxFog->SetRawValue(m_info.Fog(), 0, sizeof(OcelotFog)));
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&matV));
	HR(m_fxProj->SetMatrix((FFLOAT*)&matP));
	HR(m_fxWorldInvTrans->SetMatrix((FFLOAT*)&matWorldInvTrans));
	HR(m_fxEyePosition->SetRawValue(&m_info.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));
	HR(m_fxAmbient->SetResource(m_info.Material().Ambient()));
	HR(m_fxDiffuse->SetResource(m_info.Material().Diffuse()));
	HR(m_fxSpecular->SetResource(m_info.Material().Specular()));

	return (BIGINT)hr;
}

BIGINT Cylinder::renderMesh()
{
	// set the input layout and buffers
	m_info.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UINT stride = sizeof(EnhancedVertex);
	UINT offset = 0;
	m_info.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_info.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_info.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if(m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for(UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			m_info.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT Cylinder::initializeLayout()
{
	HRESULT hr = S_OK;

	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 36, D3D10_INPUT_PER_VERTEX_DATA, 0 },
	};
	UBIGINT nbElements = sizeof( layout ) / sizeof( layout[0] );

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_info.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

BIGINT Cylinder::initializeEffect()
{
	return initializeDXEffect(m_info.Device(), m_info.EffectPath(), &m_effect);
}

BIGINT Cylinder::initializeVertices()
{
	HRESULT hr = S_OK;

	Linked2List<EnhancedVertex> tempVert;

	FFLOAT ringHeight = (FFLOAT)m_info.Height() / (FFLOAT)m_info.Rings();

	UBIGINT numRings = m_info.Rings() + 1;

	// calculate vertices
	for(UBIGINT currRing = 0; currRing < numRings; currRing++)
	{
		// calculate the y coordinate which will be the same for every vertices in the same ring.
		FFLOAT YCoord = (-0.5f * (FFLOAT)m_info.Height()) + (FFLOAT)((FFLOAT)currRing * ringHeight) + m_info.Position().getY();

		// then we cut the current ring into slices
		for(UBIGINT currSlice = 0; currSlice <= m_info.Slices(); currSlice++)
		{
			// angle of every slice. ( (circle)360 / nbSlices )
			// or 2*PI / nbSlicesPerRing.
			FFLOAT sliceAngle = (FFLOAT)(2.0f * PI / (FFLOAT)m_info.Slices());

			FFLOAT tempCos = cosf((FFLOAT)currSlice * sliceAngle);
			FFLOAT tempSin = sinf((FFLOAT)currSlice * sliceAngle);

			// calculate the x coordinate of the vertex
			FFLOAT XCoord = ((FFLOAT)m_info.Radius() * tempCos) + m_info.Position().getX();

			// calculate the z coordinate of the vertex
			FFLOAT ZCoord = ((FFLOAT)m_info.Radius() * tempSin) + m_info.Position().getZ();

			/********************  Calculate texture coordinates of a vertex  **************************************/
			// calculate the u coordinate of the texture
			FFLOAT UCoord = ((FFLOAT)currSlice / (FFLOAT)m_info.Slices());

			// calculate the v coordinate of the texture
			FFLOAT VCoord = (FFLOAT)(1.0f - (FFLOAT)currRing / (FFLOAT)m_info.Rings());

			/********************  Calculate unit tangent at vertex  ************************************************/
			// compute tangent
			Vector3 tangent(-tempSin, 0.0f, tempCos);

			// compute normal
			Vector3 normal(tempCos, 0.0f, tempSin);

			// then store the vertex
			tempVert.putBACK(EnhancedVertex(Vector3(XCoord, YCoord, ZCoord), normal, tangent, Vector2(UCoord, VCoord)));
		}
	}

	/******************************************************** calculate Top hat vertices *****************************************************************/
	m_iTopBaseIndex = (UBIGINT)tempVert.Size();

	FFLOAT posTopY  = ((FFLOAT)m_info.Height() * 0.5f) + m_info.Position().getY(); // pos on Y equal height / 2

	FFLOAT angleTop = (FFLOAT)(2.0f * PI / (FFLOAT)m_info.Slices());  // compute the angle between vertices

	for(UBIGINT currSlice = 0; currSlice <= m_info.Slices(); currSlice++)
	{
		// X and Z position
		FFLOAT posTopX = (FFLOAT)m_info.Radius() * cosf((FFLOAT)currSlice * angleTop) + m_info.Position().getX();
		FFLOAT posTopZ = (FFLOAT)m_info.Radius() * sinf((FFLOAT)currSlice * angleTop) + m_info.Position().getZ();
		
		// texture coordinates
		FFLOAT posTopU = +(posTopX - m_info.Position().getX()) / (m_info.Height() / m_info.Radius())/* * 0.5f*/ / (FFLOAT)m_info.Radius() + 0.5f;
		FFLOAT posTopV = -(posTopZ - m_info.Position().getZ()) / (m_info.Height() / m_info.Radius())/* * 0.5f*/ / (FFLOAT)m_info.Radius() + 0.5f;

		// then store the vertex
		tempVert.putBACK(EnhancedVertex(posTopX, posTopY, posTopZ, 0.0f, 1.0f, 0.0f, Vector3(1.0f, 0.0f, 0.0f), Vector2(posTopU, posTopV)));
	}

	// center vertex for ending up.
	tempVert.putBACK(EnhancedVertex(m_info.Position().getX(), posTopY, m_info.Position().getZ(), 0.0f, 1.0f, 0.0f, Vector3(1.0f, 0.0f, 0.0f), Vector2(0.5f, 0.5f)));

	m_iTopCenterIndex = (UBIGINT)tempVert.Size() - 1;

	/********************************************************* calculate Bottom hat vertices ***************************************************************/
	m_iBottomBaseIndex = (UBIGINT)tempVert.Size();

	posTopY  = -((FFLOAT)m_info.Height() * 0.5f) + m_info.Position().getY(); // pos on Y equal -height / 2

	for(UBIGINT currSlice = 0; currSlice <= m_info.Slices(); currSlice++)
	{
		// X and Z position
		FFLOAT posTopX = (FFLOAT)m_info.Radius() * cosf((FFLOAT)currSlice * angleTop) + m_info.Position().getX();
		FFLOAT posTopZ = (FFLOAT)m_info.Radius() * sinf((FFLOAT)currSlice * angleTop) + m_info.Position().getZ();
		
		// texture coordinates
		FFLOAT posTopU = +(posTopX - m_info.Position().getX()) / (m_info.Height() / m_info.Radius())/* * 0.5f*/ / (FFLOAT)m_info.Radius() + 0.5f;
		FFLOAT posTopV = -(posTopZ - m_info.Position().getZ()) / (m_info.Height() / m_info.Radius())/* * 0.5f*/ / (FFLOAT)m_info.Radius() + 0.5f;

		// then store the vertex
		tempVert.putBACK(EnhancedVertex(posTopX, posTopY, posTopZ, 0.0f, -1.0f, 0.0f, Vector3(1.0f, 0.0f, 0.0f), Vector2(posTopU, posTopV)));
	}

	// center vertex for ending up.
	tempVert.putBACK(EnhancedVertex(m_info.Position().getX(), posTopY, m_info.Position().getZ(), 0.0f, -1.0f, 0.0f, Vector3(1.0f, 0.0f, 0.0f), Vector2(0.5f, 0.5f)));

	m_iBottomCenterIndex = (UBIGINT)tempVert.Size() - 1;

	// then save the number total of vertices
	m_iNbVertices = (UBIGINT)tempVert.Size();

	// transfert data in a proper structure which could be passed
	// to the device.
	UBIGINT t = 0;
	Array1D<EnhancedVertex> vertices(m_iNbVertices);
	List2Iterator<EnhancedVertex> iter = tempVert.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), t++)
		vertices[t] = iter.data();

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(EnhancedVertex) * m_iNbVertices;   
	buffDesc.BindFlags		= D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &vertices[0];

	// Create vertex buffer
	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));

	return (BIGINT)hr;
}

BIGINT Cylinder::initializeIndices()
{
	HRESULT hr = S_OK;

	Linked2List<UBIGINT> tempInd;

	UBIGINT nbRingVertices = m_info.Slices() + 1;

	for(UBIGINT currRing = 0; currRing < m_info.Rings(); currRing++)
	{
		for(UBIGINT currSlice = 0; currSlice < m_info.Slices(); currSlice++)
		{
			// indices of the first triangle of the quad which corresponds to a
			// ring slice and store them.
			tempInd.putBACK(currRing * nbRingVertices + currSlice);
			tempInd.putBACK((currRing + 1) * nbRingVertices + currSlice);
			tempInd.putBACK((currRing + 1) * nbRingVertices + (currSlice + 1));
			// same stuff with the second triangle of the quad
			tempInd.putBACK(currRing * nbRingVertices + currSlice);
			tempInd.putBACK((currRing + 1) * nbRingVertices + (currSlice + 1));
			tempInd.putBACK(currRing * nbRingVertices + (currSlice + 1));
		}
	}

	/******************************************************** calculate Top hat indices ******************************************************************/
	for(UBIGINT currSlice = 0; currSlice < m_info.Slices(); currSlice++)
	{
		// the triangle for each slice of this wonderful cake :-)
		// all the time the center, then two others
		tempInd.putBACK(m_iTopCenterIndex);
		tempInd.putBACK(m_iTopBaseIndex + currSlice + 1);
		tempInd.putBACK(m_iTopBaseIndex + currSlice);
	}

	/******************************************************* calculate Bottom hat indices ****************************************************************/
	for(UBIGINT currSlice = 0; currSlice < m_info.Slices(); currSlice++)
	{
		tempInd.putBACK(m_iBottomCenterIndex);
		tempInd.putBACK(m_iBottomBaseIndex + currSlice);
		tempInd.putBACK(m_iBottomBaseIndex + currSlice + 1);
	}

	// then save the number total of indices
	m_iNbIndices = (UBIGINT)tempInd.Size();

	// transfert data in a proper structure which could be passed
	// to the device.
	UBIGINT t = 0;
	Array1D<UBIGINT> indices(m_iNbIndices);
	List2Iterator<UBIGINT> iter = tempInd.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), t++)
		indices[t] = iter.data();

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(UBIGINT) * m_iNbIndices;   
	buffDesc.BindFlags		= D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &indices[0];

	// Create indice buffer
	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));

	return (BIGINT)hr;
}

VVOID  Cylinder::initializeEffectVariables()
{
	m_fxTechnique     = m_effect->GetTechniqueByName("BumpMapping");
	m_fxWorld         = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView          = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj          = m_effect->GetVariableByName("gProjection")->AsMatrix();
	m_fxWorldInvTrans = m_effect->GetVariableByName("gWorldInvTrans")->AsMatrix();
	m_fxLight         = m_effect->GetVariableByName("gLight");
	m_fxFog           = m_effect->GetVariableByName("gFog");
	m_fxEyePosition   = m_effect->GetVariableByName("gEyePosW");
	m_fxAmbient       = m_effect->GetVariableByName("gTextureNor")->AsShaderResource();
	m_fxDiffuse       = m_effect->GetVariableByName("gTexture")->AsShaderResource();
	m_fxSpecular      = m_effect->GetVariableByName("gTextureSpe")->AsShaderResource();
}

VVOID  Cylinder::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();
	m_info.Material().releaseCOM();
	m_fxAmbient       = NULL;
	m_fxDiffuse       = NULL;
	m_fxSpecular      = NULL;
	m_fxWorldInvTrans = NULL;
	m_fxLight         = NULL;
	m_fxFog           = NULL;
	m_fxEyePosition   = NULL;
}

VVOID* Cylinder::GetMeshData(ClassAttributeType attrType) const
{
	VVOID* toReturn;

	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									toReturn = m_info.Device();
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									toReturn = m_info.GetCamera();
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									toReturn = &m_info.EffectPath();
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									toReturn = &m_info.TexturePath();
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									toReturn = m_vertexBuffer;
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									toReturn = (UBIGINT*)m_info.Radius();
									break;
	case CLASS_ATTRIBUTETYPE_POSITION:
									toReturn = &m_info.Position();
									break;
	case CLASS_ATTRIBUTETYPE_HEIGHT:
									toReturn = (UBIGINT*)m_info.Height();
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_SLICES:
									toReturn = (UBIGINT*)m_info.Slices();
									break;
	case CLASS_ATTRIBUTETYPE_CYLINDER_RINGS:
									toReturn = (UBIGINT*)m_info.Rings();
									break;
	case CLASS_ATTRIBUTETYPE_LIGHT:
									toReturn = m_info.Light();
									break;
	case CLASS_ATTRIBUTETYPE_MATERIAL:
									toReturn = &m_info.Material();
									break;
	default:
			toReturn = NULL;
	}

	return toReturn;
}

VVOID  Cylinder::SetMeshData(VVOID* what, ClassAttributeType attrType)
{
	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									m_info.SetDevice(static_cast<DevPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									m_info.SetCamera(static_cast<CamPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									m_info.SetEffectPath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									m_info.SetTexturePath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									m_vertexBuffer = static_cast<BufPtr>(what);
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									m_info.SetRadius((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_POSITION:
		                            m_info.SetPosition(*(static_cast<Vector3*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_HEIGHT:
									m_info.SetHeight((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_SLICES:
									m_info.SetSlices((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_CYLINDER_RINGS:
									m_info.SetRings((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_LIGHT:
		                            m_info.SetLight(static_cast<OcelotLight*>(what));
									break;
	case CLASS_ATTRIBUTETYPE_MATERIAL:
		                            m_info.SetMaterial(*(static_cast<BumpMaterial*>(what)));
									break;
	}
}

/******************************** Cylinder Info Class ***********************************/
CylinderInfo::CylinderInfo() :
BaseDXData(), m_light(NULL), m_fog(NULL), m_sEffectPath(L".\\Effects\\BumpMapping.fx"), m_sTexturePath(L".\\Resources\\StealWoodBox.jpg"), m_vPosition(0.0f), 
m_iRadius(2), m_iHeight(10), m_iNbRings(10), m_iNbSlices(10)
{

}

CylinderInfo::CylinderInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, UBIGINT radius, UBIGINT height, UBIGINT rings, UBIGINT slices) :
BaseDXData(device, camera), m_light(light), m_fog(fog), m_sEffectPath(effectPath), m_sTexturePath(texturePath), m_vPosition(position), 
m_iRadius(radius), m_iHeight(height), m_iNbRings(rings), m_iNbSlices(slices)
{

}

CylinderInfo::~CylinderInfo()
{

}
