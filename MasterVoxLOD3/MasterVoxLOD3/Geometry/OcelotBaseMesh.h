#ifndef DEF_OCELOTBASEMESH_H
#define DEF_OCELOTBASEMESH_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   OcelotBaseMesh.h/cpp
/   
/  Description: This object is a base for every object.
/               Contains common attributes none API-dependent
/               for being accessed by children.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include "..\DataStructures\DataStructureUtil.h"
#include "..\Maths\Matrix4x4.h"

namespace Ocelot
{
	class OcelotBaseMesh
	{
	protected:

			// Attributes
			Matrix4x4 m_matWorld;
			UBIGINT   m_iNbVertices;
			UBIGINT   m_iNbIndices;
			FFLOAT    m_fDeltaTime;
			BBOOL     m_bRenderBounds;
			BBOOL     m_bVisible;

	public:

			// Constructor & Destructor
			OcelotBaseMesh();
	virtual ~OcelotBaseMesh();

			// methods

			// Accessors
	inline  Matrix4x4 World() const { return m_matWorld;}
	inline  UBIGINT   VertexNb() const { return m_iNbVertices;}
	inline  UBIGINT   IndiceNb() const { return m_iNbIndices;}
	inline  FFLOAT    DeltaTime() const { return m_fDeltaTime;}
	inline  BBOOL     IsBounded() const { return m_bRenderBounds;}
	inline  BBOOL     IsVisible() const { return m_bVisible;}
	inline  VVOID     SetWorld(Matrix4x4 newWorld) { m_matWorld = newWorld;}
	inline  VVOID     Bound() { m_bRenderBounds = true;}
	inline  VVOID     UnBound() { m_bRenderBounds = false;}
	inline  VVOID     Visible() { m_bVisible = true;}
	inline  VVOID     NotVisible() { m_bVisible = false;}
	inline  VVOID     SetVisible(BBOOL state) { m_bVisible = state;}
	};
}

#endif
