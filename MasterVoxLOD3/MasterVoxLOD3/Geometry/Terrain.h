#ifndef DEF_TERRAIN_H
#define DEF_TERRAIN_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Terrain.h/cpp
/   
/  Description: This object is to create a terrain or simply a
/               plateform or floor.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    19/02/2012
/*****************************************************************/

#include "IDXBaseMesh.h"
#include "..\Collisions\AABB.h"
#include <fstream>

namespace Ocelot
{
									struct TerrainInfo
									{
										BaseData data;
										UINT     nbRows;
										UINT     nbCols;
										float    terrainWidth;
										float    terrainDepth;
										UINT     nbTexture;   // multi-texturing
										std::vector<std::wstring> textureNames;
									};

	class Terrain : public IDXBaseMesh
	{
	private:

		// Attributes
		UINT						m_iNbRows;
		UINT						m_iNbCols;
		float                       m_fTerrainWidth;
		float                       m_fTerrainDepth;
		float						m_fCellWidth;
		float                       m_fCellDepth;
		float						m_fHeightRange;
		float                       m_fHighestHeight;
		float                       m_fLowestHeight;
		Vector3                     m_vLightDir;
		ID3D10EffectVectorVariable* m_fxLightDir;
		ID3D10EffectScalarVariable* m_fxHeightRange;
		ID3D10EffectScalarVariable* m_fxHighestHeight;
		ID3D10EffectScalarVariable* m_fxMultiTextured;
		char*                       m_cHeightMapName;
		std::vector<float>          m_fHeightMap;
		bool                        m_bHeightMapUse;
		bool                        m_bMultiTexturing;
		std::vector<SimpleVertex>   vertices;
		std::vector<std::wstring>   m_vTextureNames;

		// can load until 4 textures for the terrain
		ID3D10EffectShaderResourceVariable* m_fxTexture1;
		ID3D10EffectShaderResourceVariable* m_fxTexture2;
		ID3D10EffectShaderResourceVariable* m_fxTexture3;
		ID3D10EffectShaderResourceVariable* m_fxTexture4;

	public:

		// Constructor & Destructor
		Terrain();
		Terrain(TerrainInfo inf);
		~Terrain();

		// Methods
		void	update(float deltaTime);
		INT 	renderMesh();
		INT 	initializeVertices();
		INT 	initializeIndices();
		INT     initializeLayout();
		INT     createEffect();
		void    initializeEffectVariables();
		INT     transfertEffectVariables();
		void    loadHeightMap(char* fileName);            // just for an outdoor terrain
		void    smoothTerrain();                          // just for an outdoor terrain
		float   averageHeight(UINT row, UINT col);        // just for an outdoor terrain
		bool    inTerrain(UINT row, UINT col);            // just for an outdoor terrain
		float   heightAtPosition(const float posX, const float posZ) const ; // for walking accross the terrain
		Vector3 normalAtPosition(const float posX, const float posZ) const;
		virtual bool setWorldBounds();

		// Accessors
		inline UINT		getNbRows() const { return m_iNbRows;}
		inline UINT		getNbCols() const { return m_iNbCols;}
		inline float	getTerrainWidth() const { return m_fTerrainWidth;}
		inline float	getTerrainDepth() const { return m_fTerrainDepth;}
		inline float	getCellWidth() const { return m_fCellWidth;}
		inline float	getCellDepth() const { return m_fCellDepth;}
		inline float	getHeightRange() const { return m_fHeightRange;}
		inline float	getHighestHeight() const { return m_fHighestHeight;}
		inline float    getLowestHeight() const { return m_fLowestHeight;}
		inline Vector3  getLightDir() const { return m_vLightDir;}
		inline bool     isHeightMapUse() const { return m_bHeightMapUse;}
		inline bool     isMultitextured() const { return m_bMultiTexturing;}
		inline void     setNbRows(const UINT& nbRows) { m_iNbRows = nbRows;}
		inline void     setNbCols(const UINT& nbCols) { m_iNbCols = nbCols;}
		inline void     setTerrainWidth(const float& terrainWidth) { m_fTerrainWidth = terrainWidth;}
		inline void     setTerrainDepth(const float& terrainDepth) { m_fTerrainDepth = terrainDepth;}
		inline void     setHighestHeight(const float& highestH) { m_fHighestHeight = highestH;}
		inline void     setLowestHeight(const float& lowestH) { m_fLowestHeight = lowestH;}
		inline void     setLightDir(const Vector3& lightDir) { m_vLightDir = lightDir;}
		inline void     setHeightMapUse(bool use) { m_bHeightMapUse = use;}
		inline void     setMultiTextured(bool multiTex) { m_bMultiTexturing = multiTex;}
		inline void     setHeightMapName(char* name) { m_cHeightMapName = name;}
	};
}

#endif