#ifndef DEF_OCELOTDXMESH_H
#define DEF_OCELOTDXMESH_H

/*****************************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   OcelotDXMesh.h/cpp
/   
/  Description: This object is a base class for every object which
/               could be implemented for drawing through the
/               DirectX API and its pipeline.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    30/09/2012
/*****************************************************************************/

#include "IOcelotMesh.h"
#include "BaseDXData.h"
#include "..\DirectXAPI\DXHelperFunctions.h"

namespace Ocelot
{
	typedef ID3D10Effect* EffPtr;
	typedef ID3D10Buffer* BufPtr;
	typedef ID3D10InputLayout* LayPtr;
	typedef ID3D10EffectTechnique* TecPtr;
	typedef ID3D10EffectVariable* VarPtr;
	typedef ID3D10EffectMatrixVariable* MatPtr;
	typedef ID3D10EffectScalarVariable* ScaPtr;
	typedef ID3D10ShaderResourceView* ResViewPtr;
	typedef ID3D10EffectShaderResourceVariable* TexPtr;

	class OcelotDXMesh : public IOcelotMesh
	{
	protected:

			// Attributes
			EffPtr  m_effect;
			TecPtr  m_fxTechnique;
			LayPtr  m_inputLayout;
			BufPtr  m_vertexBuffer;
			BufPtr  m_indiceBuffer;
			MatPtr  m_fxWorld;  // will let the GPU compute
			MatPtr  m_fxView;   // the world/view/projection.
			MatPtr  m_fxProj;

	public:

			// Constructor & Destructor
			OcelotDXMesh();
	virtual ~OcelotDXMesh();

			// Methods per frame
	virtual VVOID  update(FFLOAT deltaTime);
	virtual BIGINT updateEffectVariables();
	virtual BIGINT renderMesh();

			// Methods to use once.
	virtual BIGINT createInstance();
	virtual BIGINT initializeLayout();
	virtual BIGINT initializeEffect();
	virtual BIGINT initializeVertices();
	virtual BIGINT initializeIndices();
	virtual VVOID  initializeEffectVariables();
	virtual VVOID  releaseInstance();

			// Extra functions grabbing or setting whatever
	virtual VVOID* GetMeshData(ClassAttributeType attrType) const;
	virtual VVOID  SetMeshData(VVOID* what, ClassAttributeType attrType);

			// Accessors
	inline  BufPtr VBuffer() const { return m_vertexBuffer;}
	inline  VVOID  SetVBuffer(BufPtr newBuf) { m_vertexBuffer = newBuf;}
	};
}

#endif
