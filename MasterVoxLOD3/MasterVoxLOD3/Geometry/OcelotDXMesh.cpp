#include "OcelotDXMesh.h"

using namespace Ocelot;

OcelotDXMesh::OcelotDXMesh() :
IOcelotMesh(), m_effect(NULL), m_fxTechnique(NULL), m_inputLayout(NULL),
m_vertexBuffer(NULL), m_indiceBuffer(NULL), m_fxWorld(NULL), m_fxView(NULL), m_fxProj(NULL)
{

}

OcelotDXMesh::~OcelotDXMesh()
{
	releaseInstance();
	IOcelotMesh::~IOcelotMesh();
}

VVOID  OcelotDXMesh::update(FFLOAT deltaTime)
{

}

BIGINT OcelotDXMesh::updateEffectVariables()
{
	// do not know for now
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::renderMesh()
{
	HRESULT hr = S_OK;

	if(m_bRenderBounds)
	{
		// render Bounds
	}

	return (BIGINT)hr;
}

BIGINT OcelotDXMesh::createInstance()
{
	HRESULT hr = S_OK;

	/**************************************Initialize D3D*****************************************/
	// create effect object
	HR(initializeEffect());

	// initialize effect Variables
	initializeEffectVariables();

	/**************************************Initialize World***************************************/
	// create the input layout
	HR(initializeLayout());

	// compute vertices
	HR(initializeVertices());

	// compute indices
	HR(initializeIndices());

	return (BIGINT)hr;
}

BIGINT OcelotDXMesh::initializeLayout()
{
	// do not know for now
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::initializeEffect()
{
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::initializeVertices()
{
	// do not know for now
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::initializeIndices()
{
	// do not know for now
	return (BIGINT)S_OK;
}

VVOID  OcelotDXMesh::initializeEffectVariables()
{
	// do not know for now
}

VVOID  OcelotDXMesh::releaseInstance()
{
	// release every COM of DirectX
	ReleaseCOM(m_effect);
	ReleaseCOM(m_inputLayout);
	ReleaseCOM(m_vertexBuffer);
	ReleaseCOM(m_indiceBuffer);
	m_fxTechnique = NULL;
	m_fxWorld     = NULL;
	m_fxView      = NULL;
	m_fxProj      = NULL;
}

VVOID* OcelotDXMesh::GetMeshData(ClassAttributeType attrType) const
{
	return NULL;
}

VVOID  OcelotDXMesh::SetMeshData(VVOID* what, ClassAttributeType attrType)
{

}
