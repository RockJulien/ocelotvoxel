#ifndef DEF_LINE_H
#define DEF_LINE_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Line.h/cpp
/   
/  Description: This file provides a class for creating simple
/               line between two endpoints.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "OcelotDXMesh.h"

namespace Ocelot
{
						class LineInfo : public BaseDXData
						{
						private:

							// Attributes
							WSTRING m_sEffectPath;
							WSTRING m_sTexturePath;
							Color   m_lineColor;
							Vector3 m_vFrom;
							Vector3 m_vTo;


						public:

							// Constructor & Destructor
							LineInfo();
							LineInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, Vector3 from, Vector3 to, Color lineCol = OcelotColor::RED);
							~LineInfo();

							// Methods


							// Accessors
							inline WSTRING TexturePath() const { return m_sTexturePath;}
							inline WSTRING EffectPath() const { return m_sEffectPath;}
							inline Color   LineColor() const { return m_lineColor;}
							inline Vector3 From() const { return m_vFrom;}
							inline Vector3 To() const { return m_vTo;}
							inline VVOID   SetTexturePath(WSTRING texPath) { m_sTexturePath = texPath;}
							inline VVOID   SetEffectPath(WSTRING effPath) { m_sEffectPath = effPath;}
							inline VVOID   SetColor(Color lineColor) { m_lineColor = lineColor;}
							inline VVOID   SetFrom(Vector3 from) { m_vFrom = from;}
							inline VVOID   SetTo(Vector3 to) { m_vTo = to;}
						};

	class Line : public OcelotDXMesh
	{
	private:

		// Attributes
		LineInfo m_info;

	public:

		// Constructor & Destructor
		Line();
		Line(LineInfo info);
		~Line();

		// Methods
		VVOID  update(FFLOAT deltaTime);
		BIGINT updateEffectVariables();
		BIGINT renderMesh();

		// Methods to use once.
		BIGINT initializeLayout();
		BIGINT initializeEffect();
		BIGINT initializeVertices();
		BIGINT initializeIndices();
		VVOID  initializeEffectVariables();
		VVOID  releaseInstance();

		// Extra functions grabbing or setting whatever
		VVOID* GetMeshData(ClassAttributeType attrType) const;
		VVOID  SetMeshData(VVOID* what, ClassAttributeType attrType);

		// Accessors
		inline LineInfo Info() const { return m_info;}
		inline VVOID    SetInfo(LineInfo info) { m_info = info;}
	};
}

#endif