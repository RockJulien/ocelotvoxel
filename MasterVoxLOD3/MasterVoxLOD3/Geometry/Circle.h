#ifndef DEF_CIRCLE_H
#define DEF_CIRCLE_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Circle.h/cpp
/   
/  Description: This file provides a class for creating simple
/               circle by giving the axis regarding to which the
/               circle will turn around, the radius and the count
/               of slices per circle giving  an according smoothy
/               circle.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "OcelotDXMesh.h"
#include "..\Utilities\OcelotUtility.h"

namespace Ocelot
{
						class CircleInfo : public BaseDXData
						{
						private:

							// Attributes
							WSTRING m_sEffectPath;
							WSTRING m_sTexturePath;
							Color   m_cCircleColor;
							Vector3 m_vCenter;
							UBIGINT m_iRadius;
							UBIGINT m_iSlices;
							Axes    m_axis;

						public:

							// Constructor & Destructor
							CircleInfo();
							CircleInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, Vector3 center, UBIGINT radius, UBIGINT slices = 10, Axes axis = ZAxis, Color circleColor = OcelotColor::RED);
							~CircleInfo();

							// Methods


							// Accessors
							inline WSTRING TexturePath() const { return m_sTexturePath;}
							inline WSTRING EffectPath() const { return m_sEffectPath;}
							inline Color   CircleColor() const { return m_cCircleColor;}
							inline Vector3 Center() const { return m_vCenter;}
							inline UBIGINT Radius() const { return m_iRadius;}
							inline UBIGINT Slices() const { return m_iSlices;}
							inline Axes    Axis() const { return m_axis;}
							inline VVOID   SetTexturePath(WSTRING texPath) { m_sTexturePath = texPath;}
							inline VVOID   SetEffectPath(WSTRING effPath) { m_sEffectPath = effPath;}
							inline VVOID   SetCircleColor(Color color) { m_cCircleColor = color;}
							inline VVOID   SetCenter(Vector3 center) { m_vCenter = center;}
							inline VVOID   SetRadius(UBIGINT radius) { m_iRadius = radius;}
							inline VVOID   SetSlices(UBIGINT slices) { m_iSlices = slices;}
							inline VVOID   SetAxis(Axes axis) { m_axis = axis;}
						};

	class Circle : public OcelotDXMesh
	{
	private:

		// Attributes
		CircleInfo m_info;

	public:

		// Constructor & Destructor
		Circle();
		Circle(CircleInfo info);
		~Circle();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT updateEffectVariables();
		BIGINT renderMesh();

		// Methods to use once.
		BIGINT initializeLayout();
		BIGINT initializeEffect();
		BIGINT initializeVertices();
		BIGINT initializeIndices();
		VVOID  initializeEffectVariables();
		VVOID  releaseInstance();

		// Extra functions grabbing or setting whatever
		VVOID* GetMeshData(ClassAttributeType attrType) const;
		VVOID  SetMeshData(VVOID* what, ClassAttributeType attrType);

		// Accessors
		inline CircleInfo Info() const { return m_info;}
		inline VVOID      SetInfo(CircleInfo info) { m_info = info;}
	};
}

#endif