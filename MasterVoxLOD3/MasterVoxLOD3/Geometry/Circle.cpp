#include "Circle.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

Circle::Circle() :
OcelotDXMesh(), m_info()
{
	m_iNbVertices = 10;
}

Circle::Circle(CircleInfo info) :
OcelotDXMesh(), m_info(info)
{
	m_iNbVertices = info.Slices();
}

Circle::~Circle()
{
	releaseInstance();
	OcelotDXMesh::~OcelotDXMesh();
}

VVOID  Circle::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());
}

BIGINT Circle::updateEffectVariables()
{
	HRESULT hr = S_OK;

	Matrix4x4 view = m_info.GetCamera()->View();
	Matrix4x4 proj = m_info.GetCamera()->Proj();

	// set mesh's attributes to the shader's attributes.
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&view));
	HR(m_fxProj->SetMatrix((FFLOAT*)&proj));

	return (BIGINT)hr;
}

BIGINT Circle::renderMesh()
{
	// set the input layout and buffers
	m_info.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UINT stride = sizeof(BasicVertex);
	UINT offset = sizeof(LINT);
	m_info.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_info.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_info.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINELIST);

	if(m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for(UBIGINT p = 0; p < techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			m_info.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT Circle::initializeLayout()
{
	HRESULT hr = S_OK;

	// create the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] = 
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,12, D3D10_INPUT_PER_VERTEX_DATA, 0},
	};
	UBIGINT nbElements = sizeof(layout)/sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_info.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));
	
	return (BIGINT)hr;
}

BIGINT Circle::initializeEffect()
{
	return initializeDXEffect(m_info.Device(), m_info.EffectPath(), &m_effect);
}

BIGINT Circle::initializeVertices()
{
	HRESULT hr = S_OK;

	FFLOAT anglePerSlice = (2.0f * PI) / (FFLOAT)m_iNbVertices; 

	// Create the array of vertices.
	Array1D<BasicVertex> aVertices;
	aVertices.resize(m_iNbVertices);
	
	if(m_info.Axis() == Axes::XAxis)
	{
		for(UBIGINT currSlice = 0; currSlice < m_iNbVertices; currSlice++)
		{
			FFLOAT tempCos = cosf((FFLOAT)currSlice * anglePerSlice);
			FFLOAT tempSin = sinf((FFLOAT)currSlice * anglePerSlice);

			FFLOAT posX = 0.0f + m_info.Center().getX();
			FFLOAT posY = (FFLOAT)m_info.Radius() * tempCos + m_info.Center().getY();
			FFLOAT posZ = (FFLOAT)m_info.Radius() * tempSin + m_info.Center().getZ();

			// store the vertex
			aVertices[currSlice].SetPosition(Vector3(posX, posY, posZ));
			aVertices[currSlice].SetVColor(m_info.CircleColor());
		}
	}
	else if(m_info.Axis() == Axes::YAxis)
	{
		for(UBIGINT currSlice = 0; currSlice < m_iNbVertices; currSlice++)
		{
			FFLOAT tempCos = cosf((FFLOAT)currSlice * anglePerSlice);
			FFLOAT tempSin = sinf((FFLOAT)currSlice * anglePerSlice);

			FFLOAT posX = (FFLOAT)m_info.Radius() * tempCos + m_info.Center().getX();
			FFLOAT posY = 0.0f + m_info.Center().getY();
			FFLOAT posZ = (FFLOAT)m_info.Radius() * tempSin + m_info.Center().getZ();

			// store the vertex
			aVertices[currSlice].SetPosition(Vector3(posX, posY, posZ));
			aVertices[currSlice].SetVColor(m_info.CircleColor());
		}
	}
	else if(m_info.Axis() == Axes::ZAxis)
	{
		for(UBIGINT currSlice = 0; currSlice < m_iNbVertices; currSlice++)
		{
			FFLOAT tempCos = cosf((FFLOAT)currSlice * anglePerSlice);
			FFLOAT tempSin = sinf((FFLOAT)currSlice * anglePerSlice);

			FFLOAT posX = (FFLOAT)m_info.Radius() * tempCos + m_info.Center().getX();
			FFLOAT posY = (FFLOAT)m_info.Radius() * tempSin + m_info.Center().getY();
			FFLOAT posZ = 0.0f + m_info.Center().getZ();

			// store the vertex
			aVertices[currSlice].SetPosition(Vector3(posX, posY, posZ));
			aVertices[currSlice].SetVColor(m_info.CircleColor());
		}
	}

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(BasicVertex) * m_iNbVertices;
	buffDesc.BindFlags		= D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &aVertices[0];

	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));

	return (BIGINT)hr;
}

BIGINT Circle::initializeIndices()
{
	HRESULT hr = S_OK;

	m_iNbIndices  = m_iNbVertices * 2;

	// Create index buffer
	Array1D<UBIGINT> indices;
	indices.resize(m_iNbIndices);

	UBIGINT k = 0;
	for(UBIGINT currSlice = 0; currSlice < m_iNbVertices; currSlice++)
	{
		indices[k]     = currSlice;
		indices[k + 1] = currSlice + 1;
		if(currSlice == (m_iNbVertices - 1))
			indices[k + 1] = 0;
		k+=2;
	}

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(UBIGINT) * m_iNbIndices;
	buffDesc.BindFlags		= D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &indices[0];

	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));

	return (BIGINT)hr;
}

VVOID  Circle::initializeEffectVariables()
{
	m_fxTechnique = m_effect->GetTechniqueByName("RenderLine");
	m_fxWorld     = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView      = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj      = m_effect->GetVariableByName("gProj")->AsMatrix();
}

VVOID  Circle::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();
}

VVOID* Circle::GetMeshData(ClassAttributeType attrType) const
{
	VVOID* toReturn = NULL;

	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									toReturn = m_info.Device();
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									toReturn = m_info.GetCamera();
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									toReturn = &m_info.EffectPath();
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									toReturn = &m_info.TexturePath();
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									toReturn = m_vertexBuffer;
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									toReturn = (UBIGINT*)m_info.Radius();
									break;
	case CLASS_ATTRIBUTETYPE_POSITION:
									toReturn = &m_info.Center();
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_SLICES:
									toReturn = (UBIGINT*)m_info.Slices();
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_AXIS:
									toReturn = (UBIGINT*)m_info.Axis();
									break;
	case CLASS_ATTRIBUTETYPE_LINE_COLOR:
									toReturn = &m_info.CircleColor();
									break;

	default:
			toReturn = NULL;
	}

	return toReturn;
}

VVOID  Circle::SetMeshData(VVOID* what, ClassAttributeType attrType)
{
	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									m_info.SetDevice(static_cast<DevPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									m_info.SetCamera(static_cast<CamPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									m_info.SetEffectPath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									m_info.SetTexturePath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									m_vertexBuffer = static_cast<BufPtr>(what);
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									m_info.SetRadius((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_POSITION:
									m_info.SetCenter(*(static_cast<Vector3*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_SLICES:
									m_info.SetSlices((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_AXIS:
									//m_info.SetAxis(static_cast<Axis>(what));
									break;
	case CLASS_ATTRIBUTETYPE_LINE_COLOR:
									m_info.SetCircleColor(*(static_cast<Color*>(what)));
									break;
	}
}

/******************************** Circle Info Class ***********************************/
CircleInfo::CircleInfo() :
BaseDXData(), m_sTexturePath(L""), m_sEffectPath(L".\\Effects\\Line.fx"), m_cCircleColor(RED), m_vCenter(0.0f), m_iRadius(10), 
m_iSlices(10), m_axis(Axes::ZAxis)
{

}

CircleInfo::CircleInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, Vector3 center, UBIGINT radius, UBIGINT slices, Axes axis, Color circleColor) :
BaseDXData(device, camera), m_sTexturePath(texturePath), m_sEffectPath(effectPath), m_cCircleColor(circleColor), m_vCenter(center), m_iRadius(radius), 
m_iSlices(slices), m_axis(axis)
{

}

CircleInfo::~CircleInfo()
{

}
