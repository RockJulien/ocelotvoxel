#ifndef DEF_PYRACYLINDER_H
#define DEF_PYRACYLINDER_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   PyraCylinder.h/cpp
/   
/  Description: This file provides a class for creating pyramidal
/               cylinder with a variant top and bottom radius.
/               The mesh will do bump mapping
/               on the surface, so make sure that the texture
/               path is the same for the three needed texture in
/               "resources" but adding _NRM and _SPEC to proper
/               textures (Normal map and Spec map) before the
/               extension.
/               (e.g. test.jpg      => diffuse.
/                     test_NRM.jpg  => ambient.
/                     test_SPEC.jpg => specular.)
/               The loadBumpMaterial() in DXHelper will do the rest.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "Cylinder.h"

namespace Ocelot
{
								class PyraCylinderInfo : public CylinderInfo
								{
								private:

									// Attributes
									UBIGINT m_iBottomRadius;

								public:

									// Constructor & Destructor
									PyraCylinderInfo();
									PyraCylinderInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, UBIGINT radius = 2, UBIGINT bottomRadius = 6, UBIGINT height = 10, UBIGINT rings = 10, UBIGINT slices = 10);
									~PyraCylinderInfo();

									// Methods


									// Accessors
									inline UBIGINT BottomRadius() const { return m_iBottomRadius;}
									inline VVOID   SetBottomRadius(UBIGINT bottomRadius) { m_iBottomRadius = bottomRadius;}
								};

	class PyraCylinder :public OcelotDXMesh
	{
	private:

		// Attributes
		PyraCylinderInfo m_info;
		TexPtr			 m_fxAmbient;
		TexPtr			 m_fxDiffuse;
		TexPtr			 m_fxSpecular;
		MatPtr			 m_fxWorldInvTrans;
		VarPtr			 m_fxLight;
		VarPtr           m_fxFog;
		VarPtr			 m_fxEyePosition;

		// only useful for building indices.
		// for building top/bottom hats.
		UBIGINT			 m_iTopBaseIndex;
		UBIGINT			 m_iTopCenterIndex;
		UBIGINT			 m_iBottomBaseIndex;
		UBIGINT			 m_iBottomCenterIndex;

	public:

		// Constructor & Destructor
		PyraCylinder();
		PyraCylinder(PyraCylinderInfo info);
		~PyraCylinder();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT updateEffectVariables();
		BIGINT renderMesh();

		// Methods to use once.
		BIGINT initializeLayout();
		BIGINT initializeEffect();
		BIGINT initializeVertices();
		BIGINT initializeIndices();
		VVOID  initializeEffectVariables();
		VVOID  releaseInstance();

		// Extra functions grabbing or setting whatever
		VVOID* GetMeshData(ClassAttributeType attrType) const;
		VVOID  SetMeshData(VVOID* what, ClassAttributeType attrType);

		// Accessors
		inline PyraCylinderInfo Info() const { return m_info;}
		inline VVOID            SetInfo(PyraCylinderInfo info) { m_info = info;}
	};
};

#endif