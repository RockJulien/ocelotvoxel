#include "Sphere.h"

using namespace Ocelot;

Sphere::Sphere() :
OcelotDXMesh(), m_info(), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL), 
m_fxWorldInvTrans(NULL), m_fxLight(NULL), m_fxFog(NULL), m_fxEyePosition(NULL), m_iNorthPoleIndex(0), 
m_iSouthPoleIndex(0)
{
	// load textures.
	BumpMaterial mat;
	loadBumpMaterial(m_info.Device(), m_info.TexturePath(), mat);
	m_info.SetMaterial(mat);
}

Sphere::Sphere(SphereInfo info) :
OcelotDXMesh(), m_info(info), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL), 
m_fxWorldInvTrans(NULL), m_fxLight(NULL), m_fxFog(NULL), m_fxEyePosition(NULL), m_iNorthPoleIndex(0), 
m_iSouthPoleIndex(0)
{
	// load textures.
	BumpMaterial mat;
	loadBumpMaterial(m_info.Device(), m_info.TexturePath(), mat);
	m_info.SetMaterial(mat);
}

Sphere::~Sphere()
{
	releaseInstance();
	OcelotDXMesh::~OcelotDXMesh();
}

VVOID  Sphere::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());
}

BIGINT Sphere::updateEffectVariables()
{
	HRESULT hr = S_OK;

	// Compute the world inverse transpose matrix.
	Matrix4x4 matV, matP, matWorldInvTrans;
	matV = m_info.GetCamera()->View();
	matP = m_info.GetCamera()->Proj();
	m_matWorld.Matrix4x4Inverse(matWorldInvTrans);
	matWorldInvTrans.Matrix4x4Transpose(matWorldInvTrans);

	// set mesh's attributes to the shader's attributes.
	HR(m_fxLight->SetRawValue(m_info.Light(), 0, sizeof(OcelotLight)));
	HR(m_fxFog->SetRawValue(m_info.Fog(), 0, sizeof(OcelotFog)));
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&matV));
	HR(m_fxProj->SetMatrix((FFLOAT*)&matP));
	HR(m_fxWorldInvTrans->SetMatrix((FFLOAT*)&matWorldInvTrans));
	HR(m_fxEyePosition->SetRawValue(&m_info.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));
	HR(m_fxAmbient->SetResource(m_info.Material().Ambient()));
	HR(m_fxDiffuse->SetResource(m_info.Material().Diffuse()));
	HR(m_fxSpecular->SetResource(m_info.Material().Specular()));

	return (BIGINT)hr;
}

BIGINT Sphere::renderMesh()
{
	// set the input layout and buffers
	m_info.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UINT stride = sizeof(EnhancedVertex);
	UINT offset = 0;
	m_info.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_info.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_info.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if(m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for(UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			m_info.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT Sphere::initializeLayout()
{
	HRESULT hr = S_OK;

	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 36, D3D10_INPUT_PER_VERTEX_DATA, 0 },
	};
	UBIGINT nbElements = sizeof( layout ) / sizeof( layout[0] );

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_info.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

BIGINT Sphere::initializeEffect()
{
	return initializeDXEffect(m_info.Device(), m_info.EffectPath(), &m_effect);
}

BIGINT Sphere::initializeVertices()
{
	HRESULT hr = S_OK;

	Linked2List<EnhancedVertex> tempVert;

	/******************************************* Calculate and store the vertices *****************************************************************/
	// for each ring (sphere poles are excluded from these ring calculation) !!!

	// calculate the ring step
	FFLOAT deltaPhi = PI / (FFLOAT)m_info.Rings();

	// calculate the slice step
	FFLOAT deltaTheta = (FFLOAT)(2.0f * PI / (FFLOAT)m_info.Slices());

	// start from 1 avoiding to count the first pole until numRingWithoutPole.
	for(UBIGINT currRing = 1; currRing < m_info.Rings(); currRing++)
	{
		// calculate the ring angle
		FFLOAT ringAngle =  ((FFLOAT)currRing * deltaPhi);

		// cut the current ring into slices and calculate vertices of slices
		for(UBIGINT currSlice = 0; currSlice <= m_info.Slices(); currSlice++)
		{
			// calculate the slice angle
			FFLOAT sliceAngle = ((FFLOAT)currSlice * deltaTheta);

			// calculate the X position of the vertex
			FFLOAT X = (FFLOAT)m_info.Radius() * sinf(ringAngle) * cosf(sliceAngle) + m_info.Position().getX();

			// calculate the Y position of the vertex
			FFLOAT Y = (FFLOAT)m_info.Radius() * cosf(ringAngle) + m_info.Position().getY();

			// calculate the Z position of the vertex
			FFLOAT Z = (FFLOAT)m_info.Radius() * sinf(ringAngle) * sinf(sliceAngle) + m_info.Position().getZ();
			Vector3 position(X, Y, Z);

			/********************** calculate the texture coordinates of the vertex *****************************/
			// calculate the U coordinate of the vertex
			FFLOAT U = sliceAngle / (2.0f * PI); //(2.0f * PI);

			// calculate the v coordinate of the vertex
			FFLOAT V = ringAngle / PI;
			Vector2 texture(U, V);

			/********************** calculate the unit tangent at the vertex ************************************/
			// calculate components of the vertex tangent
			Vector3 tangent(-((FFLOAT)m_info.Radius() * sinf(ringAngle) * sinf(sliceAngle)), 0.0f, ((FFLOAT)m_info.Radius() * sinf(ringAngle) * cosf(sliceAngle)));

			/********************** calculate the unit normal at the vertex *************************************/
			// calculate components of the vertex normal
			Vector3 normal = position;
			normal.Vec3Normalise();

			// store the vertex in an array
			tempVert.putBACK(EnhancedVertex(position, normal, tangent, texture));
		}
	}

	/**************************** set and store the vertices at the poles of the sphere *************************/
	// South pole stored as last vertex
	// calculate the position of the South pole vertex
	Vector3 positionSouthPole(m_info.Position().getX(), -(FFLOAT)m_info.Radius() + m_info.Position().getY(), m_info.Position().getZ());

	// calculate the tangent 
	Vector3 tangentSouthPole(1.0f, 0.0f, 0.0f);

	// calculate the normal
	Vector3 normalSouthPole(0.0f, -1.0f, 0.0f);

	// calculate the texture coordinates
	Vector2 textureSouthPole(0.0f, 1.0f);

	// store the vertex
	tempVert.putBACK(EnhancedVertex(positionSouthPole, normalSouthPole, tangentSouthPole, textureSouthPole));
	
    // North pole stored as penultimate vertex
	// calculate the position of the North pole vertex
	Vector3 positionNorthPole(m_info.Position().getX(), (FFLOAT)m_info.Radius() + m_info.Position().getY(), m_info.Position().getZ());

	// calculate the tangent
	Vector3 tangentNorthPole(1.0f, 0.0f, 0.0f);

	// calculate the normal
	Vector3 normalNorthPole(0.0f, 1.0f, 0.0f);

	// calculate the texture coordinates
	Vector2 textureNorthPole(0.0f, 0.0f);

	// store the vertex
	tempVert.putBACK(EnhancedVertex(positionNorthPole, normalNorthPole, tangentNorthPole, textureNorthPole));

	m_iNbVertices = (UBIGINT)tempVert.Size();

	// prepare  north and south indices for indices generation
	m_iNorthPoleIndex = m_iNbVertices - 1;
	m_iSouthPoleIndex = m_iNbVertices - 2;

	// transfert data in a proper structure which could be passed
	// to the device.
	UBIGINT t = 0;
	Array1D<EnhancedVertex> vertices(m_iNbVertices);
	List2Iterator<EnhancedVertex> iter = tempVert.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), t++)
		vertices[t] = iter.data();

	// then pass the vertices array
	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(EnhancedVertex) * m_iNbVertices;  
	buffDesc.BindFlags		= D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &vertices[0];

	// Create vertex buffer
	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));
	
	return (BIGINT)hr;
}

BIGINT Sphere::initializeIndices()
{
	HRESULT hr = S_OK;

	Linked2List<UBIGINT> tempInd;

	/*********************************** calculate the vertex indices for each mesh triangle **********************************************/
	// first, process rings not connected to poles
	UBIGINT nbRingVertices = m_info.Slices() + 1;

	for(UBIGINT currRing = 0; currRing < (m_info.Rings() - 2); currRing++)
	{
		for(UBIGINT currSlice = 0; currSlice < m_info.Slices(); currSlice++)
		{
			tempInd.putBACK(currRing * nbRingVertices + currSlice);
			tempInd.putBACK(currRing * nbRingVertices + (currSlice + 1));
			tempInd.putBACK((currRing + 1) * nbRingVertices + currSlice);

			tempInd.putBACK((currRing + 1) * nbRingVertices + currSlice);
			tempInd.putBACK(currRing * nbRingVertices + (currSlice + 1));
			tempInd.putBACK((currRing + 1) * nbRingVertices + (currSlice + 1));
		}
	}

	// we calculate the index of the first vertex on the last ring
	UBIGINT lastRingVIndex = nbRingVertices * (m_info.Rings() - 2);

	for(UBIGINT currSlice = 0; currSlice < m_info.Slices(); currSlice++)
	{
		// indices for the north pole which corresponds to pole slice
		// store triangle indices
		tempInd.putBACK(m_iNorthPoleIndex);
		tempInd.putBACK(currSlice + 1);
		tempInd.putBACK(currSlice);

		// indices for the south pole which corresponds to pole slice
		// store triangle indices
		tempInd.putBACK(m_iSouthPoleIndex);
		tempInd.putBACK(lastRingVIndex + currSlice);
		tempInd.putBACK(lastRingVIndex + (currSlice + 1));
	}

	m_iNbIndices = (UBIGINT)tempInd.Size();

	// transfert data in a proper structure which could be passed
	// to the device.
	UBIGINT t = 0;
	Array1D<UBIGINT> indices(m_iNbIndices);
	List2Iterator<UBIGINT> iter = tempInd.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), t++)
		indices[t] = iter.data();

	// then pass the indices array
	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(UBIGINT) * m_iNbIndices;
	buffDesc.BindFlags		= D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &indices[0];

	// Create indice buffer
	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));
	
	return (BIGINT)hr;
}

VVOID  Sphere::initializeEffectVariables()
{
	m_fxTechnique     = m_effect->GetTechniqueByName("BumpMapping");
	m_fxWorld         = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView          = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj          = m_effect->GetVariableByName("gProjection")->AsMatrix();
	m_fxWorldInvTrans = m_effect->GetVariableByName("gWorldInvTrans")->AsMatrix();
	m_fxLight         = m_effect->GetVariableByName("gLight");
	m_fxFog           = m_effect->GetVariableByName("gFog");
	m_fxEyePosition   = m_effect->GetVariableByName("gEyePosW");
	m_fxAmbient       = m_effect->GetVariableByName("gTextureNor")->AsShaderResource();
	m_fxDiffuse       = m_effect->GetVariableByName("gTexture")->AsShaderResource();
	m_fxSpecular      = m_effect->GetVariableByName("gTextureSpe")->AsShaderResource();
}

VVOID  Sphere::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();
	m_info.Material().releaseCOM();
	m_fxAmbient       = NULL;
	m_fxDiffuse       = NULL;
	m_fxSpecular      = NULL;
	m_fxWorldInvTrans = NULL;
	m_fxLight         = NULL;
	m_fxFog           = NULL;
	m_fxEyePosition   = NULL;
}

VVOID* Sphere::GetMeshData(ClassAttributeType attrType) const
{
	VVOID* toReturn;

	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									toReturn = m_info.Device();
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									toReturn = m_info.GetCamera();
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									toReturn = &m_info.EffectPath();
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									toReturn = &m_info.TexturePath();
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									toReturn = m_vertexBuffer;
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									toReturn = (UBIGINT*)m_info.Radius();
									break;
	case CLASS_ATTRIBUTETYPE_POSITION:
									toReturn = &m_info.Position();
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_SLICES:
									toReturn = (UBIGINT*)m_info.Slices();
									break;
	case CLASS_ATTRIBUTETYPE_CYLINDER_RINGS:
									toReturn = (UBIGINT*)m_info.Rings();
									break;
	case CLASS_ATTRIBUTETYPE_LIGHT:
									toReturn = m_info.Light();
									break;
	case CLASS_ATTRIBUTETYPE_MATERIAL:
									toReturn = &m_info.Material();
									break;
	default:
			toReturn = NULL;
	}

	return toReturn;
}

VVOID  Sphere::SetMeshData(VVOID* what, ClassAttributeType attrType)
{
	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									m_info.SetDevice(static_cast<DevPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									m_info.SetCamera(static_cast<CamPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									m_info.SetEffectPath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									m_info.SetTexturePath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									m_vertexBuffer = static_cast<BufPtr>(what);
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									m_info.SetRadius((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_POSITION:
		                            m_info.SetPosition(*(static_cast<Vector3*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_CIRCLE_SLICES:
									m_info.SetSlices((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_CYLINDER_RINGS:
									m_info.SetRings((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_LIGHT:
		                            m_info.SetLight(static_cast<OcelotLight*>(what));
									break;
	case CLASS_ATTRIBUTETYPE_MATERIAL:
		                            m_info.SetMaterial(*(static_cast<BumpMaterial*>(what)));
									break;
	}
}

/******************************** Sphere Info Class ***********************************/
SphereInfo::SphereInfo() :
BaseDXData(), m_light(NULL), m_fog(NULL), m_sEffectPath(L".\\Effects\\BumpMapping.fx"), m_sTexturePath(L".\\Resources\\StealWoodBox.jpg"), m_vPosition(0.0f), 
m_iRadius(2), m_iNbRings(10), m_iNbSlices(10)
{

}

SphereInfo::SphereInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, UBIGINT radius, UBIGINT rings, UBIGINT slices) :
BaseDXData(device, camera), m_light(light), m_fog(fog), m_sEffectPath(effectPath), m_sTexturePath(texturePath), m_vPosition(position), 
m_iRadius(radius), m_iNbRings(rings), m_iNbSlices(slices)
{

}

SphereInfo::~SphereInfo()
{

}
