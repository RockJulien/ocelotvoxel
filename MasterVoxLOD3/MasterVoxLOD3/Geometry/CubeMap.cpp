#include "CubeMap.h"

using namespace Ocelot;

CubeMap::CubeMap() :
OcelotDXMesh(), m_info(), m_fxTexture(NULL), m_aVertices(12), m_fRotatAngle(0.0f)
{
	// load the texture.
	m_rvTexture = loadCubeMap(m_info.Device(), m_info.TexturePath());
}

CubeMap::CubeMap(CubeMapInfo info) :
OcelotDXMesh(), m_info(info), m_fxTexture(NULL), m_aVertices(12), m_fRotatAngle(0.0f)
{
	// load the texture.
	m_rvTexture = loadCubeMap(m_info.Device(), m_info.TexturePath());
}

CubeMap::~CubeMap()
{
	releaseInstance();
	OcelotDXMesh::~OcelotDXMesh();
}

VVOID  CubeMap::update(FFLOAT deltaTime)
{
	m_fDeltaTime = deltaTime;

	// refresh variables in the shader file
	HR(updateEffectVariables());
}

BIGINT CubeMap::updateEffectVariables()
{
	HRESULT hr = S_OK;

	// insure that the background DOME stay center
	// regarding to the eye of the viewer.
	Vector3 viewerEye = m_info.GetCamera()->Info().Eye();

	m_matWorld.Matrix4x4Identity();
	m_matWorld.Matrix4x4Translation(m_matWorld, viewerEye.getX(), viewerEye.getY(), viewerEye.getZ());

	// extra: add a rotation to the CubeMap for making turning the sky and moving clouds.
	m_fRotatAngle += ((FFLOAT)m_info.Speed() / 1000.0f) * m_fDeltaTime;

	Matrix4x4 rotat;
	rotat.Matrix4x4Identity();
	rotat.Matrix4x4RotationY(m_matWorld, m_fRotatAngle);

	Matrix4x4 view = m_info.GetCamera()->View();
	Matrix4x4 proj = m_info.GetCamera()->Proj();

	// set mesh's attributes to the shader's attributes.
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&view));
	HR(m_fxProj->SetMatrix((FFLOAT*)&proj));
	HR(m_fxTexture->SetResource(m_rvTexture));

	return (BIGINT)hr;
}

BIGINT CubeMap::renderMesh()
{
	// set the input layout and buffers
	m_info.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UINT stride = sizeof(DomeVertex);
	UINT offset = sizeof(LINT); // structure used are DomeVertex which are classes, if in classes there are virtual function, sizeof(thisClass) counts these V-functions adding an extra 4 bytes. So offset 4 bytes.
	m_info.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_info.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_info.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if(m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for(UBIGINT p = 0; p < techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			m_info.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT CubeMap::initializeLayout()
{
	HRESULT hr = S_OK;

	D3D10_INPUT_ELEMENT_DESC layout[] = 
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0}
	};
	UBIGINT nbElements = sizeof(layout)/sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_info.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

BIGINT CubeMap::initializeEffect()
{
	return initializeDXEffect(m_info.Device(), m_info.EffectPath(), &m_effect);
}

BIGINT CubeMap::initializeVertices()
{
	HRESULT hr = S_OK;

	UBIGINT tempTess = Smaller<UBIGINT>(m_info.Tesselation(), 5);
	m_info.SetTesselation(tempTess);

	const UBIGINT nbVertices = 12;

	m_iNbVertices = nbVertices;

	m_aVertices.resize((BIGINT)m_iNbVertices);

	const FFLOAT coordX = 0.525731f;
	const FFLOAT coordZ = 0.850651f;

	m_aVertices[0]  = DomeVertex(-coordX, 0.0f,  coordZ);
	m_aVertices[1]  = DomeVertex(coordX, 0.0f,   coordZ);
	m_aVertices[2]  = DomeVertex(-coordX, 0.0f, -coordZ);
	m_aVertices[3]  = DomeVertex(coordX, 0.0f,  -coordZ);
	m_aVertices[4]  = DomeVertex(0.0f,  coordZ,  coordX);
	m_aVertices[5]  = DomeVertex(0.0f,  coordZ, -coordX);
	m_aVertices[6]  = DomeVertex(0.0f, -coordZ,  coordX);
	m_aVertices[7]  = DomeVertex(0.0f, -coordZ, -coordX);
	m_aVertices[8]  = DomeVertex(coordZ,  coordX,  0.0f);
	m_aVertices[9]  = DomeVertex(-coordZ,  coordX, 0.0f);
	m_aVertices[10] = DomeVertex(coordZ, -coordX,  0.0f);
	m_aVertices[11] = DomeVertex(-coordZ, -coordX, 0.0f);

	return (BIGINT)hr;
}

BIGINT CubeMap::initializeIndices()
{
	HRESULT hr = S_OK;

	const UBIGINT nbIndices  = 60;

	m_iNbIndices = nbIndices;

	Array1D<UBIGINT> indices;
	indices.resize((BIGINT)m_iNbIndices);

	UBIGINT index[nbIndices] = 
	{
		1,4,0, 4,9,0, 4,5,9, 8,5,4, 1,8,4,     // 0-4 triangles
		1,10,8, 10,3,8, 8,3,5, 3,2,5, 3,7,2,   // 5-9
		3,10,7, 10,6,7, 6,11,7, 6,0,11, 6,1,0, // 10-14
		10,1,6, 11,0,9, 2,11,9, 5,2,9, 11,2,7  // 15-19
	};

	for(UINT currInds = 0; currInds < nbIndices; currInds++)
	{
		indices[currInds] = index[currInds];
	}

	// tesselate
	for(UBIGINT curr = 0; curr < m_info.Tesselation(); curr++)
	{
		tesselate(m_aVertices, indices);
	}

	// refresh after tesselation
	m_iNbVertices = (UBIGINT)m_aVertices.Size();
	m_iNbIndices  = (UBIGINT)indices.Size();

	// rescale and turn into an ellipsoid for a flatter CM
	for(UINT currVert = 0; currVert < m_iNbVertices; currVert++)
	{
		Vector3 tempVert = m_aVertices[currVert].Position();
		tempVert.Vec3Normalise();     // bring back to unit sphere
		tempVert *= (FFLOAT)m_info.Radius();  // re-scale
		tempVert *= 0.5f;             // divide by 2.
		m_aVertices[currVert].SetPosition(tempVert);
	}

	// then pass to the device
	D3D10_BUFFER_DESC vBuffDesc;
	vBuffDesc.Usage			 = D3D10_USAGE_IMMUTABLE;
	vBuffDesc.ByteWidth		 = sizeof(DomeVertex) * m_iNbVertices;  
	vBuffDesc.BindFlags		 = D3D10_BIND_VERTEX_BUFFER;
	vBuffDesc.CPUAccessFlags = 0;
	vBuffDesc.MiscFlags		 = 0;

	D3D10_SUBRESOURCE_DATA vInitData;
	vInitData.pSysMem		 = &m_aVertices[0];

	// Create vertex buffer
	HR(m_info.Device()->CreateBuffer(&vBuffDesc, &vInitData, &m_vertexBuffer));

	// then pass the indices array
	D3D10_BUFFER_DESC iBuffDesc;
	iBuffDesc.Usage			 = D3D10_USAGE_IMMUTABLE;
	iBuffDesc.ByteWidth		 = sizeof(UBIGINT) * m_iNbIndices;
	iBuffDesc.BindFlags		 = D3D10_BIND_INDEX_BUFFER;
	iBuffDesc.CPUAccessFlags = 0;
	iBuffDesc.MiscFlags		 = 0;

	D3D10_SUBRESOURCE_DATA iInitData;
	iInitData.pSysMem		 = &indices[0];

	// Create indice buffer
	HR(m_info.Device()->CreateBuffer(&iBuffDesc, &iInitData, &m_indiceBuffer));

	return (BIGINT)hr;
}

VVOID  CubeMap::initializeEffectVariables()
{
	m_fxTechnique = m_effect->GetTechniqueByName("Background");
	m_fxWorld     = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView      = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj      = m_effect->GetVariableByName("gProj")->AsMatrix();
	m_fxTexture   = m_effect->GetVariableByName("gCubeMap")->AsShaderResource();
}

VVOID  CubeMap::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();
	ReleaseCOM(m_rvTexture);
	m_fxTexture = NULL;
}

VVOID* CubeMap::GetMeshData(ClassAttributeType attrType) const
{
	VVOID* toReturn = NULL;

	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									toReturn = m_info.Device();
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									toReturn = m_info.GetCamera();
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									toReturn = (UBIGINT*)m_info.Radius();
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									toReturn = &m_info.EffectPath();
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									toReturn = &m_info.TexturePath();
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									toReturn = m_vertexBuffer;
									break;
	case CLASS_ATTRIBUTETYPE_TESSELATION:
									toReturn = (UBIGINT*)m_info.Tesselation();
									break;
	case CLASS_ATTRIBUTETYPE_SPEED: 
									toReturn = (UBIGINT*)m_info.Speed();
									break;
	default:
			toReturn = NULL;
	}

	return toReturn;
}

VVOID  CubeMap::SetMeshData(VVOID* what, ClassAttributeType attrType)
{
	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									m_info.SetDevice(static_cast<DevPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									m_info.SetCamera(static_cast<CamPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_SCALE:
									m_info.SetRadius((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									m_info.SetEffectPath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									m_info.SetTexturePath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									m_vertexBuffer = static_cast<BufPtr>(what);
									break;
	case CLASS_ATTRIBUTETYPE_TESSELATION:
									m_info.SetTesselation((UBIGINT)what);
									break;
	case CLASS_ATTRIBUTETYPE_SPEED: 
									m_info.SetSpeed((UBIGINT)what);
									break;
	}
}

// private helper function for tesselating a mesh
VVOID  CubeMap::tesselate(Array1D<DomeVertex>& vertices, Array1D<UBIGINT>& indices)
{
	Array1D<DomeVertex> vertToTess = vertices;
	Array1D<UBIGINT>    indToTess  = indices;
	Linked2List<DomeVertex> tempVB;
	Linked2List<UBIGINT>    tempIB;

	// compute the number of triangle of the structure
	UBIGINT nbTri = (UBIGINT)indices.Size() / 3;
	
	// for each triangle
	for(UBIGINT currTri = 0; currTri < nbTri; currTri++)
	{
		Vector3 V0 = vertToTess[indToTess[currTri * 3 + 0]].Position(); // vertices position of the triangle
		Vector3 V1 = vertToTess[indToTess[currTri * 3 + 1]].Position();
		Vector3 V2 = vertToTess[indToTess[currTri * 3 + 2]].Position();

		Vector3 M0 = (V0 + V1) * 0.5f;  // mid point of the two vertices (lenght / 2).
		Vector3 M1 = (V1 + V2) * 0.5f;
		Vector3 M2 = (V0 + V2) * 0.5f;

		// new vertices
		tempVB.putBACK(DomeVertex(V0)); // new vertex 0
		tempVB.putBACK(DomeVertex(V1)); // new vertex 1
		tempVB.putBACK(DomeVertex(V2)); // new vertex 2
		tempVB.putBACK(DomeVertex(M0)); // new vertex 3
		tempVB.putBACK(DomeVertex(M1)); // new vertex 4
		tempVB.putBACK(DomeVertex(M2)); // new vertex 5

		// new indices
		// indices of the first new triangle
		tempIB.putBACK(currTri * 6 + 0); // shift of 6 vertices plus the index of the first vertices of the triangle.
		tempIB.putBACK(currTri * 6 + 3);
		tempIB.putBACK(currTri * 6 + 5);

		// indices of the second new triangle
		tempIB.putBACK(currTri * 6 + 3); // shift of 6 vertices plus the index of the first vertices of the triangle.
		tempIB.putBACK(currTri * 6 + 4);
		tempIB.putBACK(currTri * 6 + 5);

		// indices of the third new triangle
		tempIB.putBACK(currTri * 6 + 5); // shift of 6 vertices plus the index of the first vertices of the triangle.
		tempIB.putBACK(currTri * 6 + 4);
		tempIB.putBACK(currTri * 6 + 2);

		// indices of the fourth new triangle
		tempIB.putBACK(currTri * 6 + 3); // shift of 6 vertices plus the index of the first vertices of the triangle.
		tempIB.putBACK(currTri * 6 + 1);
		tempIB.putBACK(currTri * 6 + 4);
	}

	UBIGINT newVertCount = (UBIGINT)tempVB.Size();
	UBIGINT newIndCount  = (UBIGINT)tempIB.Size();
	List2Iterator<DomeVertex> vertIt = tempVB.iterator();
	List2Iterator<UBIGINT>    indIt  = tempIB.iterator();
	vertIt.begin();
	indIt.begin();

	vertices.resize(newVertCount);
	indices.resize(newIndCount);

	for(UBIGINT currVert = 0; vertIt.notEnd(); currVert++, vertIt.next())
	{
		vertices[currVert] = vertIt.data();
	}

	for(UBIGINT currInd = 0; indIt.notEnd(); currInd++, indIt.next())
	{
		indices[currInd] = indIt.data();
	}
}

/******************************** Cube Map Info Class ***********************************/
CubeMapInfo::CubeMapInfo() :
BaseDXData(), m_iRadius(5000), m_iTesselation(2), m_iSpeed(10), m_sEffectPath(L".\\Effects\\CubeMap.fx"), m_sTexturePath(L".\\Resources\\SnowMountain.dds")
{

}

CubeMapInfo::CubeMapInfo(DevPtr device, CamPtr camera, UBIGINT radius, WSTRING texturePath, WSTRING effectPath, UBIGINT speed, UBIGINT nbTess) :
BaseDXData(device, camera), m_iRadius(radius), m_iTesselation(nbTess), m_iSpeed(speed), m_sTexturePath(texturePath), m_sEffectPath(effectPath)
{

}

CubeMapInfo::~CubeMapInfo()
{
	
}
