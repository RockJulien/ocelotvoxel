#include "Line.h"

using namespace Ocelot;

Line::Line() :
OcelotDXMesh(), m_info()
{

}

Line::Line(LineInfo info) :
OcelotDXMesh(), m_info(info)
{

}

Line::~Line()
{
	releaseInstance();
	OcelotDXMesh::~OcelotDXMesh();
}

VVOID  Line::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());
}

BIGINT Line::updateEffectVariables()
{
	HRESULT hr = S_OK;

	Matrix4x4 view = m_info.GetCamera()->View();
	Matrix4x4 proj = m_info.GetCamera()->Proj();

	// set mesh's attributes to the shader's attributes.
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&view));
	HR(m_fxProj->SetMatrix((FFLOAT*)&proj));

	return (BIGINT)hr;
}

BIGINT Line::renderMesh()
{
	// set the input layout and buffers
	m_info.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UINT stride = sizeof(BasicVertex);
	UINT offset = sizeof(LINT);
	m_info.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_info.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_info.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINELIST);

	if(m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for(UBIGINT p = 0; p < techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			m_info.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT Line::initializeLayout()
{
	HRESULT hr = S_OK;

	// create the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] = 
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,12, D3D10_INPUT_PER_VERTEX_DATA, 0},
	};
	UBIGINT nbElements = sizeof(layout)/sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_info.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));
	
	return (BIGINT)hr;
}

BIGINT Line::initializeEffect()
{
	return initializeDXEffect(m_info.Device(), m_info.EffectPath(), &m_effect);
}

BIGINT Line::initializeVertices()
{
	HRESULT hr = S_OK;

	m_iNbVertices = 2;

	BasicVertex vertices[2] = 
	{
		BasicVertex(m_info.From(), m_info.LineColor()),
		BasicVertex(m_info.To(), m_info.LineColor())
	};

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(BasicVertex) * m_iNbVertices;
	buffDesc.BindFlags		= D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &vertices;

	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));
	
	return (BIGINT)hr;
}

BIGINT Line::initializeIndices()
{
	HRESULT hr = S_OK;

	m_iNbIndices  = 2;

	// Create index buffer
	UBIGINT indices[2] =
	{
		0,1,
	};

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(UBIGINT) * m_iNbIndices;
	buffDesc.BindFlags		= D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &indices;

	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));
	
	return (BIGINT)hr;
}

VVOID  Line::initializeEffectVariables()
{
	m_fxTechnique = m_effect->GetTechniqueByName("RenderLine");
	m_fxWorld     = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView      = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj      = m_effect->GetVariableByName("gProj")->AsMatrix();
}

VVOID  Line::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();
}

VVOID* Line::GetMeshData(ClassAttributeType attrType) const
{
	VVOID* toReturn = NULL;

	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									toReturn = m_info.Device();
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									toReturn = m_info.GetCamera();
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									toReturn = &m_info.EffectPath();
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									toReturn = &m_info.TexturePath();
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									toReturn = m_vertexBuffer;
									break;
	case CLASS_ATTRIBUTETYPE_LINE_FROM:
									toReturn = &m_info.From();
									break;
	case CLASS_ATTRIBUTETYPE_LINE_TO:
									toReturn = &m_info.To();
									break;
	case CLASS_ATTRIBUTETYPE_LINE_COLOR:
									toReturn = &m_info.LineColor();
									break;
	default:
			toReturn = NULL;
	}

	return toReturn;
}

VVOID  Line::SetMeshData(VVOID* what, ClassAttributeType attrType)
{
	switch(attrType)
	{
	case CLASS_ATTRIBUTETYPE_DEVICE:
									m_info.SetDevice(static_cast<DevPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_CAMERA:
									m_info.SetCamera(static_cast<CamPtr>(what));
									break;
	case CLASS_ATTRIBUTETYPE_EFFECTPATH:
									m_info.SetEffectPath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_TEXTUREPATH:
									m_info.SetTexturePath(*(static_cast<WSTRING*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_VERTEXBUFFER:
									m_vertexBuffer = static_cast<BufPtr>(what);
									break;
	case CLASS_ATTRIBUTETYPE_LINE_FROM:
									m_info.SetFrom(*(static_cast<Vector3*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_LINE_TO:
									m_info.SetTo(*(static_cast<Vector3*>(what)));
									break;
	case CLASS_ATTRIBUTETYPE_LINE_COLOR:
									m_info.SetColor(*(static_cast<Color*>(what)));
									break;
	}
}

/******************************** Line Info Class ***********************************/
LineInfo::LineInfo() :
BaseDXData(), m_sEffectPath(L".\\Effects\\Line.fx"), m_sTexturePath(L""), m_lineColor(OcelotColor::RED), 
m_vFrom(0.0f), m_vTo(1.0f)
{

}

LineInfo::LineInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, Vector3 from, Vector3 to, Color lineCol) :
BaseDXData(device, camera), m_sEffectPath(effectPath), m_sTexturePath(texturePath), m_lineColor(lineCol), 
m_vFrom(from), m_vTo(to)
{

}

LineInfo::~LineInfo()
{

}
