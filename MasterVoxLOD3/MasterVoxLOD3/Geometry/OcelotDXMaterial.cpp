#include "OcelotDXMaterial.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

PhongMaterial::PhongMaterial() :
m_cAmbient(DARKGREY), m_cDiffuse(DARKGREY), m_cSpecular(DARKGREY), m_rvTexture(NULL)
{

}

PhongMaterial::PhongMaterial(Color ambient, Color diffuse, Color specular, ID3D10ShaderResourceView* texture) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_rvTexture(texture)
{
	
}

PhongMaterial::~PhongMaterial()
{
	//ReleaseCOM(m_rvTexture);
}

VVOID PhongMaterial::releaseCOM()
{
	ReleaseCOM(m_rvTexture);
}

BumpMaterial::BumpMaterial() :
m_rvTexture(NULL), m_rvNormalMap(NULL), m_rvSpecularMap(NULL)
{

}

BumpMaterial::BumpMaterial(ID3D10ShaderResourceView* texture, ID3D10ShaderResourceView* normalMap, ID3D10ShaderResourceView* specularMap) :
m_rvTexture(texture), m_rvNormalMap(normalMap), m_rvSpecularMap(specularMap)
{

}

BumpMaterial::~BumpMaterial()
{
	/*ReleaseCOM(m_rvTexture);
	ReleaseCOM(m_rvNormalMap);
	ReleaseCOM(m_rvSpecularMap);*/
}

VVOID BumpMaterial::releaseCOM()
{
	ReleaseCOM(m_rvTexture);
	ReleaseCOM(m_rvNormalMap);
	ReleaseCOM(m_rvSpecularMap);
}

TriPlanarBumpMaterial::TriPlanarBumpMaterial()
{
	for(USMALLINT i = 0; i < 9; i++)
		m_rvTriPlanarTextures[i] = NULL;
}

TriPlanarBumpMaterial::~TriPlanarBumpMaterial()
{

}

VVOID TriPlanarBumpMaterial::releaseCOM()
{
	for(USMALLINT i = 0; i < 9; i++)
		ReleaseCOM(m_rvTriPlanarTextures[i]);
}

TriPlanarTexturePalette::TriPlanarTexturePalette() :
m_rvPalette4096x4096(NULL)
{

}

TriPlanarTexturePalette::~TriPlanarTexturePalette()
{

}

VVOID TriPlanarTexturePalette::releaseCOM()
{
	ReleaseCOM(m_rvPalette4096x4096);
}
