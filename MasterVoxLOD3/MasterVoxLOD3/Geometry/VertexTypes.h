#ifndef DEF_VERTEXTYPES_H
#define DEF_VERTEXTYPES_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   VertexTypes.h/cpp
/   
/  Description: This file provides multiple class for vertices 
/               of different kind of objects and effect intended.
/               Going from simple shape colored with simple Colors
/               to more complex ones with normal and tangent for 
/               Phong shading processing for example.
/               Particular structure for particles is provided
/               as well.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include "..\Maths\Color.h"
#include "..\Maths\Vector3.h"
#include "..\Maths\Vector2.h"
#include "..\DataStructures\DataStructureUtil.h"

namespace Ocelot
{
	// Special vertex class for CubeMap
	// which could serve as Base vertex
	// class.
	class DomeVertex
	{
	protected:

			// Attributes
			Vector3 m_vPosition;

	public:

			// Constructor & Destructor
			DomeVertex();
			DomeVertex(Vector3 position);
			DomeVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ);
	virtual ~DomeVertex();

			// Methods


			// Accessors
	inline  Vector3 Position() const { return m_vPosition;}
	inline  VVOID   SetPosition(Vector3 position) { m_vPosition = position;}
	};


	// Basic vertex class with only 
	// position and color components.
	class BasicVertex : public DomeVertex
	{
	private:

		// Attributes
		Color m_color;

	public:

		// Constructor & Destructor
		BasicVertex();
		BasicVertex(Vector3 position, Color color);
		BasicVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Color color);
		BasicVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT red, FFLOAT green, FFLOAT blue, FFLOAT alpha);
		~BasicVertex();

		// Methods


		// Accessors
		inline Color VColor() const { return m_color;}
		inline VVOID SetVColor(Color color) { m_color = color;}
	};

	// Simple enhanced vertex class with position, 
	// normal and texture coordinates allowing 
	// basic lighting and texturization.
	class SimpleVertex : public DomeVertex
	{
	protected:

			// Attributes
			Vector3 m_vNormal;
			Vector2 m_vTexCoord;

	public:

			// Constructor & Destructor
			SimpleVertex();
			SimpleVertex(Vector3 position, Vector3 normal, Vector2 texCoord);
			SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 normal, Vector2 texCoord);
			SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, Vector2 texCoord);
			SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, FFLOAT texU, FFLOAT texV);
	virtual ~SimpleVertex();

			// Methods


			// Accessors
	inline  Vector3 Normal() const { return m_vNormal;}
	inline  Vector2 TexCoord() const { return m_vTexCoord;}
	inline  VVOID   SetNormal(Vector3 normal) { m_vNormal = normal;}
	inline  VVOID   SetTexCoord(Vector2 texCoord) { m_vTexCoord = texCoord;}
	};

	// Particular vertex class for Particules.
	// Can be used for Fire, Rain, Snow, etc...
	// Extracted from the example of Franck LUNA.
	class ParticuleVertex : public DomeVertex
	{
	private:

		// Attributes
		Vector3 m_vVelocity; // moving entity involving a velocity.
		Vector2 m_vScale;
		FFLOAT  m_fAge;      // the age of the particule at which it will die.
		UBIGINT m_iType;     // the particule type.

	public:

		// Constructor & Destructor
		ParticuleVertex();
		ParticuleVertex(Vector3 position, Vector3 velocity, Vector2 scale, FFLOAT age, UBIGINT type);
		ParticuleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 velocity, Vector2 scale, FFLOAT age, UBIGINT type);
		ParticuleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, Vector2 scale, FFLOAT age, UBIGINT type);
		ParticuleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, FFLOAT scaleX, FFLOAT scaleY, FFLOAT age, UBIGINT type);
		~ParticuleVertex();

		// Methods


		// Accessors
		inline Vector3 Velocity() const { return m_vVelocity;}
		inline Vector2 Scale() const { return m_vScale;}
		inline FFLOAT  Age() const { return m_fAge;}
		inline UBIGINT Type() const { return m_iType;}
		inline VVOID   SetVelocity(Vector3 velocity) { m_vVelocity = velocity;}
		inline VVOID   SetScale(Vector2 scale) { m_vScale = scale;}
		inline VVOID   SetAge(FFLOAT age) { m_fAge = age;}
		inline VVOID   SetType(UBIGINT type) { m_iType = type;}
	};

	// Enhanced vertex structure with position, normal,
	// tangent and texture coordinates allowing
	// bump mapping enhancing lighting and texturing.
	struct EnhancedVertex
	{
		// constructors
		EnhancedVertex(){}
		EnhancedVertex(Vector3 pos, Vector3 norm, Vector3 tang, Vector2 tex) :
		Position(pos), Normal(norm), Tangent(tang), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, Vector3 norm, Vector3 tan, Vector2 tex) :
		Position(posX, posY, posZ), Normal(norm), Tangent(tan), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, Vector3 tan, Vector2 tex) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tan), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, Vector2 tex) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float texU, float texV) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ), TexCoord(texU, texV){}

		// attributes
		Vector3 Position;
		Vector3 Normal;
		Vector3 Tangent;
		Vector2 TexCoord;
	};

	// Particle vertex structure.
	struct ParticleVertex
	{
		// Constructors.
		ParticleVertex(){}
		ParticleVertex(Vector3 position, Vector2 tex, Vector2 lifeNRot, UBIGINT color) :
		Position(position), TexCoord(tex), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector2 tex, Vector2 lifeNRot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(tex), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT texU, FFLOAT texV, Vector2 lifeNRot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(texU, texV), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT texU, FFLOAT texV, FFLOAT life, FFLOAT rot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(texU, texV), LifeNRot(life, rot), Color(color) {}

		// Attributes.
		Vector3 Position;
		Vector2 TexCoord;
		Vector2 LifeNRot;
		UBIGINT Color;
	};

	struct VoxelVertex
	{
		VoxelVertex() :
		m_vPosition(0.0f), m_vNormal(1.0f) {}
		VoxelVertex(Vector3 position, Vector3 normal) :
		m_vPosition(position), m_vNormal(normal) {}
		VoxelVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 normal) :
		m_vPosition(posX, posY, posZ), m_vNormal(normal) {}
		VoxelVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ) :
		m_vPosition(posX, posY, posZ), m_vNormal(normX, normY, normZ) {}

		Vector3 m_vPosition;
		Vector3 m_vNormal;
	};
}

#endif