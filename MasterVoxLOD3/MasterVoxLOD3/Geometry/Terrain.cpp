#include "Terrain.h"
#include <Windows.h>

using namespace Ocelot;
using namespace std;

// default constructor (no device)
Terrain::Terrain() : 
IDXBaseMesh(), m_iNbRows(10), m_iNbCols(10), m_fTerrainWidth(100.0f), 
m_fTerrainDepth(100.0f), m_fHeightRange(1.0f), m_fHighestHeight(0.0f), m_fLowestHeight(0.0f), 
m_vLightDir(-0.707f, 0.707f, 0.0f), m_bHeightMapUse(false), m_bMultiTexturing(false),
m_fxTexture1(NULL), m_fxTexture2(NULL), m_fxTexture3(NULL), m_fxTexture4(NULL)
{
	m_fScale = Higher<float>(m_fTerrainWidth, m_fTerrainDepth);
	// tempo
	m_sEffectFileName  = L".\\Effects\\terrain.fx";
	m_sTextureFileName = L"brickFloor.jpg";
	m_particle->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
}

// Constructor overload
Terrain::Terrain(TerrainInfo inf) :
IDXBaseMesh(inf.data), m_iNbRows(inf.nbRows), m_iNbCols(inf.nbCols), m_fTerrainWidth(inf.terrainWidth), 
m_fTerrainDepth(inf.terrainDepth), m_fHeightRange(1.0f), m_fHighestHeight(0.0f), m_fLowestHeight(0.0f), 
m_vLightDir(-0.707f, 0.707f, 0.0f), m_bHeightMapUse(false), m_bMultiTexturing(false),
m_fxTexture1(NULL), m_fxTexture2(NULL), m_fxTexture3(NULL), m_fxTexture4(NULL)
{
	m_fScale = Higher<float>(m_fTerrainWidth, m_fTerrainDepth);
	m_sEffectFileName  = L".\\Effects\\terrain.fx";
	m_particle->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
	m_particle->SetInverseMass(0.0f);

	m_iNbResources  = inf.nbTexture;
	m_vTextureNames = inf.textureNames;
}

// Destructor
Terrain::~Terrain()
{
	releaseMesh();
}

// update matrices and set dynamic variables through
// the pipeline
void Terrain::update(float deltaTime)
{
	// update world matrix first
	IDXBaseMesh::update(deltaTime);
}

// render the terrain
INT  Terrain::renderMesh()
{
	HRESULT hr = S_OK;

	// refresh variables in the shader file
	HR(transfertEffectVariables());

	// set the input layout and buffers
	m_device->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UINT stride = sizeof(::SimpleVertex);
	UINT offset = 0;
	m_device->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_device->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// render the terrain
	HR(IDXBaseMesh::renderMesh());

	return (INT)hr;
}

// initialize terrain's vertices
INT  Terrain::initializeVertices()
{
	HRESULT hr = S_OK;

	float ref  = 1.0f;
	float ref2 = 1000.0f;

	Vector3 P1, P2;

	// compute the number of vertices
	m_iNbVertices    = m_iNbRows * m_iNbCols;

	// compute the size of a cell with th esize of the terrain
	m_fCellWidth     = m_fTerrainWidth / ((float)(m_iNbCols - 1));
	m_fCellDepth     = m_fTerrainDepth / ((float)(m_iNbRows - 1));

	vertices.resize(m_iNbVertices);

	// check if heightMap asked
	if(m_bHeightMapUse)
	{
		// load the heightMap
		loadHeightMap(m_cHeightMapName);

		// smooth the terrain
		smoothTerrain();
	}

	if(m_bMultiTexturing)
	{
		// first, empty the array of texture pre loaded by default.
		if(m_rVTextures.getSize() != 0)
		{
			/*Object<ID3D10ShaderResourceView*>* temp = m_rVTextures.getFirst();

			while(temp)
			{
				// destroy every RV in the list
				ID3D10ShaderResourceView* rv = temp->getObject();
				temp = temp->getNext();
				ReleaseCOM(rv);
			}*/

			// then destroy the list structure
			m_rVTextures.destroyList();
		}

		wstring path = L".\\\\Resources\\\\";

		// then load in good textures.
		for(UINT currTex = 0; currTex < m_vTextureNames.size(); currTex++)
		{
			wstring completeName = path + m_vTextureNames[currTex];
			m_rVTextures.putBack(ResFactory->researchByName(completeName));
		}
	}

	float dU = 1.0f / (float)m_iNbCols - 1.0f;
	float dV = 1.0f / (float)m_iNbRows - 1.0f;

	for(UINT currRow = 0; currRow < m_iNbRows; currRow++)
	{
		for(UINT currCol = 0; currCol < m_iNbCols; currCol++)
		{
			// compute index once for storing vertices
			UINT index = currRow * m_iNbCols + currCol;

			// position on X
			float posX = (float)((float)currCol * m_fCellWidth + (-m_fTerrainWidth * 0.5f));
			
			// position on Y
			float posY = 0.0f;

			// load the height map values as Y components if heightMap mode.
			if(m_bHeightMapUse)
			{
				posY = m_fHeightMap[index];
			}

			// find the highest height of the terrain for bounding box generation
			// and multi-texturing as well.
			if(posY > ref)
			{
				ref = posY;
				m_fHighestHeight = posY;
			}

			// find the lowest height of the terrain for computing the HeightRange
			// later on for the multi-texturing process.
			if(posY < ref2)
			{
				ref2 = posY;
				m_fLowestHeight = posY;
			}
			
			// position on Z
			float posZ = (float)(-((float)currRow * m_fCellDepth) + (m_fTerrainDepth * 0.5f));

			// compute normal if the terrain stay flat
			if(!m_bHeightMapUse)
			{
				vertices[index].Normal = Vector3(0.0f, 1.0f, 0.0f);
			}

			// texture coordinate on U
			float TexU = (float)currCol * dU;

			// texture coordinate on V
			float TexV = (float)currRow * dV;

			// store them in the array
			vertices[index].Position = Vector3(posX, posY, posZ);
			vertices[index].TexCoord = Vector2(TexU, TexV);
		}
	}

	// compute the heightRange
	m_fHeightRange = m_fHighestHeight - m_fLowestHeight;

	// compute normals if deformations
	if(m_bHeightMapUse)
	{
		float TwoCellWidthInv = 1.0f / (2.0f * m_fCellWidth);
		float TwoCellDepthInv = 1.0f / (2.0f * m_fCellDepth);
		// create a temporary array for storing generated normal for then using it
		// for setting normals in the right order which are here shifted by 2.
		vector<SimpleVertex> tempVertData(m_iNbVertices);
		for(UINT currCellZ = 2; currCellZ < (m_iNbRows - 1); currCellZ++)
		{
			for(UINT currCellX = 2; currCellX < (m_iNbCols - 1); currCellX++)
			{
				float t = m_fHeightMap[(currCellZ - 1) * m_iNbCols + currCellX];
				float b = m_fHeightMap[(currCellZ + 1) * m_iNbCols + currCellX];
				float l = m_fHeightMap[currCellZ * m_iNbCols + currCellX - 1];
				float r = m_fHeightMap[currCellZ * m_iNbCols + currCellX + 1];

				// compute tangent to the surface on X and Z
				Vector3 tanZ(0.0f, (t - b) * TwoCellDepthInv, 1.0f);
				Vector3 tanX(1.0f, (r - l) * TwoCellWidthInv, 0.0f);

				Vector3 normal;
				normal = tanZ.Vec3CrossProduct(tanX);
				normal.Vec3Normalise();

				UINT index = currCellZ * m_iNbCols + currCellX;
				tempVertData[index].Normal = normal;
				//vertices[currCellZ * m_iNbCols + currCellX].Normal = normal;
			}
		}

		// pass normals in the right array in the right order starting from 0.
		for(UINT index = 0; index < m_iNbVertices; index++)
		{
			Vector3 temp = tempVertData[index].Normal;
			if(temp == Vector3(0.0f))
				vertices[index].Normal = Vector3(0.0f, 1.0f, 0.0f);
			else
				vertices[index].Normal = temp;
		}
	}

	// store extremities for BB.
	float sizeTempX = m_fTerrainWidth * 0.5f;
	float sizeTempZ = m_fTerrainDepth * 0.5f;
	P1 = Vector3(-sizeTempX, m_fHighestHeight, -sizeTempZ);
	P2 = Vector3(sizeTempX, m_fLowestHeight, sizeTempZ);

	m_bounds = new AABBMinMax3D(P1, P2);
	m_bounds->CreateBounds(m_device, m_camera);

	// now transfert vertices in the vertex buffer
	D3D10_BUFFER_DESC VBDesc;
	VBDesc.Usage		  = D3D10_USAGE_IMMUTABLE;  // change with dynamic if it could change on the fly
	VBDesc.ByteWidth	  = sizeof(SimpleVertex) * m_iNbVertices;
	VBDesc.BindFlags	  = D3D10_BIND_VERTEX_BUFFER;
	VBDesc.CPUAccessFlags = 0;
	VBDesc.MiscFlags      = 0;

	// subresource creation
	D3D10_SUBRESOURCE_DATA  InitData;
	InitData.pSysMem      = &vertices[0];

	// create vertex buffer
	HR(m_device->CreateBuffer(&VBDesc, &InitData, &m_vertexBuffer));
	
	return (INT)hr;
}

// initialize terrain's indices
INT  Terrain::initializeIndices()
{
	HRESULT hr = S_OK;

	std::vector<DWORD> indices;

	// compute the number of indices
	UINT nbCells     = (m_iNbRows - 1) * (m_iNbCols - 1);
	UINT nbTriangles = nbCells * 2;
	m_iNbIndices     = nbTriangles * 3;

	indices.resize(m_iNbIndices, 0);

	int index = 0;
	for(UINT currRow = 0; currRow < (m_iNbRows - 1); currRow++)
	{
		for(UINT currCol = 0; currCol < (m_iNbCols - 1); currCol++)
		{
			// indices for a cell

			// first triangle
			indices[index]     = currRow       * m_iNbCols + currCol;
			indices[index + 1] = currRow       * m_iNbCols + (currCol + 1);
			indices[index + 2] = (currRow + 1) * m_iNbCols + currCol;

			// second triangle
			indices[index + 3] = (currRow + 1) * m_iNbCols + currCol;
			indices[index + 4] = currRow       * m_iNbCols + (currCol + 1);
			indices[index + 5] = (currRow + 1) * m_iNbCols + (currCol + 1);
			
			index += 6;  // next cell
		}
	}

	// now transfert indices in the index buffer
	D3D10_BUFFER_DESC IBDesc;
	IBDesc.Usage		  = D3D10_USAGE_IMMUTABLE;  // change with dynamic if it could change on the fly
	IBDesc.ByteWidth      = sizeof(DWORD) * m_iNbIndices;
	IBDesc.BindFlags      = D3D10_BIND_INDEX_BUFFER;
	IBDesc.CPUAccessFlags = 0;
	IBDesc.MiscFlags      = 0;

	// subresource creation
	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem      = &indices[0];

	// create the index buffer
	HR(m_device->CreateBuffer(&IBDesc, &InitData, &m_indiceBuffer));
	
	return (INT)hr;
}

INT  Terrain::initializeLayout()
{
	HRESULT hr = S_OK;

	D3D10_INPUT_ELEMENT_DESC layout[] = 
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0,12, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0,24, D3D10_INPUT_PER_VERTEX_DATA, 0},
	};
	UINT nbElements = sizeof(layout)/sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_device->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));
	
	return (INT)hr;
}

// set the effect
INT  Terrain::createEffect()
{
	return IDXBaseMesh::createEffect();
}

// initialize shader variables
void Terrain::initializeEffectVariables()
{
	m_fxTechnique     = m_effect->GetTechniqueByName("WearTerrain");
	m_fxWorldViewProj = m_effect->GetVariableByName("gWorldViewProj")->AsMatrix();
	m_fxWorld         = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxLightDir      = m_effect->GetVariableByName("gLightDir")->AsVector();
	m_fxHeightRange   = m_effect->GetVariableByName("gHeightRange")->AsScalar();
	m_fxHighestHeight = m_effect->GetVariableByName("gHighestHeight")->AsScalar();
	m_fxNbResources   = m_effect->GetVariableByName("gNbTexture")->AsScalar();
	m_fxMultiTextured = m_effect->GetVariableByName("gMultiTexturing")->AsScalar();
	m_fxTexture1      = m_effect->GetVariableByName("gTexture1")->AsShaderResource();

	if(m_bMultiTexturing)
	{
		m_fxTexture2      = m_effect->GetVariableByName("gTexture2")->AsShaderResource();
		m_fxTexture3      = m_effect->GetVariableByName("gTexture3")->AsShaderResource();
		m_fxTexture4      = m_effect->GetVariableByName("gTexture4")->AsShaderResource();
	}
}

// transfert into the pipeline
INT  Terrain::transfertEffectVariables()
{

	HRESULT hr = S_OK;

	Matrix4x4 matV, matP, worldViewProj;
	matV = m_camera->getViewMat();
	matP = m_camera->getProjMat();

	// creation of the world view projection matrix
	m_matWorld.Matrix4x4Mutliply(worldViewProj, matV, matP);

	HR(m_fxWorld->SetMatrix((float*)&m_matWorld));
	HR(m_fxWorldViewProj->SetMatrix((float*)&worldViewProj));
	HR(m_fxLightDir->SetFloatVector((float*)&m_vLightDir));
	HR(m_fxTexture1->SetResource(m_rVTextures.researchByIndex(0)));
	HR(m_fxHeightRange->SetFloat(m_fHeightRange));
	HR(m_fxHighestHeight->SetFloat(m_fHighestHeight));
	HR(m_fxNbResources->SetInt(m_iNbResources));
	HR(m_fxMultiTextured->SetBool(m_bMultiTexturing));

	if(m_bMultiTexturing)
	{
		if(m_iNbResources > 1)
		{
			if(m_iNbResources == 2)
			{
				HR(m_fxTexture2->SetResource(m_rVTextures.researchByIndex(1)));
			}
			else if(m_iNbResources == 3)
			{
				HR(m_fxTexture2->SetResource(m_rVTextures.researchByIndex(1)));
				HR(m_fxTexture3->SetResource(m_rVTextures.researchByIndex(2)));
			}
			else if(m_iNbResources == 4)
			{
				HR(m_fxTexture2->SetResource(m_rVTextures.researchByIndex(1)));
				HR(m_fxTexture3->SetResource(m_rVTextures.researchByIndex(2)));
				HR(m_fxTexture4->SetResource(m_rVTextures.researchByIndex(3)));
			}
			// more than four not allowed by ME !!!
		}
	}

	return (INT)hr;
}

// load a heightMap
void    Terrain::loadHeightMap(char* fileName)
{
	std::vector<unsigned char> input(m_iNbRows * m_iNbCols);

	std::ifstream inputStream;
	inputStream.open(fileName, std::ios_base::binary);

	if(inputStream)
	{
		inputStream.read((char*)&input[0], (std::streamsize)input.size());
		inputStream.close();
	}

	m_fHeightMap.resize((m_iNbRows * m_iNbCols), 0.0f);
	for(UINT currHei = 0; currHei < (m_iNbRows * m_iNbCols); currHei++)
	{
		m_fHeightMap[currHei] = 0.35f * (float)input[currHei] - 20.0f;
	}
}

// for smoothing the terrain
void    Terrain::smoothTerrain()
{
	vector<float> smoothy(m_fHeightMap.size(), 0.0f);

	for(UINT currRow = 0; currRow < m_iNbRows; currRow++)
	{
		for(UINT currCol = 0; currCol < m_iNbCols; currCol++)
		{
			float newHeight = averageHeight(currRow, currCol);
			smoothy[(currRow * m_iNbCols) + currCol] = newHeight;
		}
	}

	// if everything is okay, insert new heights
	m_fHeightMap = smoothy;
}

// for computing an average of terrain's heights
float   Terrain::averageHeight(UINT currRow, UINT currCol)
{
	float heightAverage = 0.0f;
	UINT  nbNeighBors   = 0;

	for(UINT m = currRow-1; m <= currRow+1; m++)
	{
		for(UINT currentCol = currCol-1; currentCol <= currCol+1; currentCol++)
		{
			if(inTerrain(m, currentCol))  // check if the point is in the heightMap, so the terrain.
			{
				heightAverage += m_fHeightMap[(m * m_iNbCols) + currentCol];
				nbNeighBors++;
			}
		}
	}

	// make the average between all summed value and the number of value
	return heightAverage / (float)nbNeighBors;
}

// for checking if pixel are in terrain's bounds
bool    Terrain::inTerrain(UINT row, UINT col)
{
	return row >= 0 && row < m_iNbRows && col >= 0 && col < m_iNbCols;
}

// for taking back terrain's height allowing to walk
// accross it properly
float   Terrain::heightAtPosition(float posX, float posZ) const
{
	float heightAtPos;

	if(m_bHeightMapUse)
	{
		// determine the cell space coordinates
		float c = (posX + (0.5f * m_fTerrainWidth)) / m_fCellWidth;
		float d = (posZ - (0.5f * m_fTerrainDepth)) / -m_fCellDepth;

		// get the row and the column where the player is.
		int row = (int)floorf(d);
		int col = (int)floorf(c);

		// take back the height where the player is
		float TopLeftCorner		= m_fHeightMap[row * m_iNbCols + col];
		float TopRightCorner	= m_fHeightMap[row * m_iNbCols + col + 1];
		float BottomLeftCorner	= m_fHeightMap[(row + 1) * m_iNbCols + col];
		float BottomRightCorner	= m_fHeightMap[(row + 1) * m_iNbCols + col + 1];

		// now we know the cell we are, let's find in which triangle
		// of the cell we are as well
		float s = c - (float)col;
		float t = d - (float)row;

		// if upper triangle
		if(s + t <= 1.0f)
		{
			float Uy = TopRightCorner - TopLeftCorner;
			float Vy = BottomLeftCorner - TopLeftCorner;

			return TopLeftCorner + s * Uy + t * Vy;
		}
		else // lower triangle
		{
			float Uy = BottomLeftCorner - BottomRightCorner;
			float Vy = TopRightCorner - BottomRightCorner;

			return BottomRightCorner + (1.0f - s) * Uy + (1.0f - t) * Vy;
		}

	}
	else
	{
		// if flat terrain always return same value
		// so we return the world Y coordinate of the terrain.
		heightAtPos = m_matWorld.get_42();
	}

	return heightAtPos;
}

bool Terrain::setWorldBounds()
{
	float sizeTempX = m_fTerrainWidth * 0.5f;
	float sizeTempZ = m_fTerrainDepth * 0.5f;

	Vector3 minL(-sizeTempX * 0.5f, m_fHighestHeight * 0.5f, -sizeTempZ * 0.5f);
	Vector3 maxL(sizeTempX * 0.5f, m_fLowestHeight * 0.5f, sizeTempZ * 0.5f);

	Matrix4x4 cWorld = m_bounds->GetWorld();

	Vector4 maxH = cWorld.Transform(Vector4(maxL.getX(), maxL.getY(), maxL.getZ(), 1.0f));
	Vector4 minH = cWorld.Transform(Vector4(minL.getX(), minL.getY(), minL.getZ(), 1.0f));

	maxL = Vector3(maxH.getX(), maxH.getY(), maxH.getZ());
	minL = Vector3(minH.getX(), minH.getY(), minH.getZ());

	SetMaxW(maxL);
	SetMinW(minL);

	return true;
}

Vector3 Terrain::normalAtPosition(const float posX, const float posZ) const
{
	float col = floor((posX + (0.5f * m_fTerrainWidth)) / m_fCellWidth);
	float row = floor((posZ - (0.5f * m_fTerrainDepth)) / -m_fCellDepth);

	return vertices[(int)col * m_iNbCols + (int)row].Normal;
}