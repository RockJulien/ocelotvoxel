#ifndef DEF_CUBEMAP_H
#define DEF_CUBEMAP_H

#include "OcelotDXMesh.h"

namespace Ocelot
{
						class  CubeMapInfo : public BaseDXData
						{
						private:

							// Attributes
							UBIGINT m_iRadius;
							UBIGINT m_iTesselation;
							UBIGINT m_iSpeed;
							WSTRING m_sEffectPath;
							WSTRING m_sTexturePath;

						public:

							// Constructor & Destructor
							CubeMapInfo();
							CubeMapInfo(DevPtr device, CamPtr camera, UBIGINT radius, WSTRING texturePath, WSTRING effectPath, UBIGINT speed = 10, UBIGINT nbTess = 2);
							~CubeMapInfo();

							// Methods

							// Accessors
							inline UBIGINT Radius() const { return m_iRadius;}
							inline WSTRING TexturePath() const { return m_sTexturePath;}
							inline WSTRING EffectPath() const { return m_sEffectPath;}
							inline UBIGINT Tesselation() const { return m_iTesselation;}
							inline UBIGINT Speed() const { return m_iSpeed;}
							inline VVOID   SetRadius(UBIGINT radius) { m_iRadius = radius;}
							inline VVOID   SetTexturePath(WSTRING texPath) { m_sTexturePath = texPath;}
							inline VVOID   SetEffectPath(WSTRING effPath) { m_sEffectPath = effPath;}
							inline VVOID   SetTesselation(UBIGINT nbTess) { m_iTesselation = nbTess;}
							inline VVOID   SetSpeed(UBIGINT speed) { m_iSpeed = speed;}
						};

	class CubeMap : public OcelotDXMesh
	{
	private:

		// Attributes
		TexPtr				m_fxTexture;
		ResViewPtr          m_rvTexture;
		Array1D<DomeVertex> m_aVertices;
		FFLOAT              m_fRotatAngle;
		CubeMapInfo			m_info;

		// Private methods (just helper functions usually)
		VVOID  tesselate(Array1D<DomeVertex>& vertices, Array1D<UBIGINT>& indices);

	public:

		// Constructor & Destructor
		CubeMap();
		CubeMap(CubeMapInfo info);
		~CubeMap();

		// Methods
		VVOID  update(FFLOAT deltaTime);
		BIGINT updateEffectVariables();
		BIGINT renderMesh();

		// Methods to use once.
		BIGINT initializeLayout();
		BIGINT initializeEffect();
		BIGINT initializeVertices();
		BIGINT initializeIndices();
		VVOID  initializeEffectVariables();
		VVOID  releaseInstance();

		// Extra functions grabbing or setting whatever
		VVOID* GetMeshData(ClassAttributeType attrType) const;
		VVOID  SetMeshData(VVOID* what, ClassAttributeType attrType);

		// Accessors
		inline CubeMapInfo Info() const { return m_info;}
		inline VVOID       SetInfo(CubeMapInfo info) { m_info = info;}
	};
}

#endif