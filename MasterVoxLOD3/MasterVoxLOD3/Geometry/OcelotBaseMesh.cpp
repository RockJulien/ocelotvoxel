#include "OcelotBaseMesh.h"

using namespace Ocelot;

OcelotBaseMesh::OcelotBaseMesh() :
m_iNbVertices(0), m_iNbIndices(0), m_bRenderBounds(false), m_bVisible(true)
{
	m_matWorld.Matrix4x4Identity();
}

OcelotBaseMesh::~OcelotBaseMesh()
{

}
