#include "OcelotFog.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

OcelotFog::OcelotFog() : 
m_cColor(GREY), m_fStart(2000.0f), m_fRange(1000.0f)
{

}

OcelotFog::OcelotFog(FFLOAT from, FFLOAT range, Color col) : 
m_cColor(col), m_fStart(from), m_fRange(range)
{

}

OcelotFog::~OcelotFog()
{

}
