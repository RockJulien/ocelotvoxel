#ifndef DEF_IOCELOTAUDIODEVICE_H
#define DEF_IOCELOTAUDIODEVICE_H

#include <windows.h>
#include "RevEnum.h"
#include "..\Maths\Vector3.h"
#include "..\Maths\Vector4.h"
#include <stdio.h>

namespace Audio
{
	class IOcelotAudioDevice
	{
	protected:

			// Attributes
			HWND      m_hMainWindow;
			HINSTANCE m_hDllMod;
			bool      m_bRunning;
			bool      m_bLogFileOpen;
			FILE*     m_pLog;

	public:

			// Constructor & Destructor
			IOcelotAudioDevice(){}
	virtual ~IOcelotAudioDevice(){}

			// Methods
	virtual HRESULT initialize(bool)                    = 0;  // for initializing the device
	virtual HRESULT startSound()                        = 0;  // for stopping all sounds
	virtual HRESULT loadSound(const char*, bool)        = 0;  // load a sound
	virtual HRESULT playSound(UINT, float delta)        = 0;  // play a loaded sound
	virtual void    stopSound()                         = 0;  // stop a sound's playing
	virtual HRESULT setReverb(ReverbEffect)             = 0;  // set the reverb effect
	virtual HRESULT trigger()                           = 0;  // trig a non looped sound
	virtual void    setListener(Vector3 pos, Vector3 dir, Vector3 up, Vector3 v) = 0;  // set listener parameter in 3D world
	virtual void    setSoundPosition(Vector3, UINT)     = 0;            // set parameters for sound source.
	virtual void    setSoundDirection(Vector3, Vector3 v, UINT)    = 0;
	virtual void    setSoundMaxDistance(const float& dist, UINT s) = 0;
	virtual void    release()                           = 0;  // for releasing the device

			// Accessors
	inline bool isRunning() const { return m_bRunning;}
	};

	extern "C"
	{
		HRESULT createAudioDevice(HINSTANCE hDllMod, IOcelotAudioDevice** pInterface);
		typedef HRESULT (*CREATEAUDIODEVICE)(HINSTANCE hDllMod, IOcelotAudioDevice** pInterface);

		HRESULT releaseAudioDevice(IOcelotAudioDevice** pInterface);
		typedef HRESULT (*RELEASEAUDIODEVICE)(IOcelotAudioDevice** pInterface);
	}
}

#endif
