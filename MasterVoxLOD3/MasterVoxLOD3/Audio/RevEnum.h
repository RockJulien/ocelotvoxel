#ifndef DEF_REVENUM_H
#define DEF_REVENUM_H

namespace Audio
{
	enum ReverbEffect
	{
		Forest,
		Default,
		Generic,
		PaddedCell,
		Room,
		Bathroom,
		LivingRoom,
		StoneRoom,
		Auditorium,
		ConcertHall,
		Cave,
		Arena,
		Hangar,
		CarpetedHallway,
		Hallway,
		StoneCorridor,
		Alley,
		City,
		Mountains,
		Quarry,
		Plain,
		ParkingLot,
		SewerPipe,
		Underwater,
		SmallRoom,
		MediumRoom,
		LargeRoom,
		MediumHall,
		LargeHall,
		Plate
	};
}

#endif