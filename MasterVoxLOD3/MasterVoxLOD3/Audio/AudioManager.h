#ifndef DEF_AUDIOMANAGER_H
#define DEF_AUDIOMANAGER_H

#include "OcelotAudio.h"
#include "..\StringHelper\STRINGHelpers.h"
#include <vector>

// include the audio lib file
#ifdef _DEBUG
#pragma comment(lib, "OcelotAudio.lib")
#else
#pragma comment(lib, "OcelotAudioR.lib")
#endif // _DEBUG


#define AudioMng AudioManager::instance()

namespace Ocelot
{
	class AudioManager
	{
	public:

	typedef std::vector<Audio::OcelotAudio*>        Channels;
	typedef std::vector<Audio::IOcelotAudioDevice*> Musics;
	typedef std::vector<std::wstring>        Names;

	private:

			// Attributes
			// Audio Device.
			HINSTANCE m_hInst;
			Channels  m_pAudios;
			Musics    m_pDevices;
			Names     m_sNames;

			// Constructor
			AudioManager(){}

			AudioManager(const AudioManager&);
			AudioManager& operator = (const AudioManager&);

	public:

			// Destructor
			~AudioManager();

			// Singleton
	static  AudioManager* instance();

			// Methods
			void    initialize(HINSTANCE);
			HRESULT createSound(const std::wstring&, bool, bool);
			HRESULT update(float);
			HRESULT startASound(std::wstring shortName);
			HRESULT restartASound(std::wstring shortName);
			void    stopASound(std::wstring shortName);
			HRESULT setReverbForAll(Audio::ReverbEffect);
			HRESULT setReverbForOne(Audio::ReverbEffect, std::wstring shortName);
			char*   wstringToCharPointer(const wchar_t *lpwstr);
			void    release();

			// Accessors
	inline  Musics Device() const { return m_pDevices;}
	inline  Audio::IOcelotAudioDevice* GetAudioByName(std::wstring shortName) 
	{ 
		Names::iterator lIter = m_sNames.begin();
		for(int lCounter = 0; lIter != m_sNames.end(); lIter++, lCounter++)
		{
			if((*lIter) == shortName)
			{
				return m_pDevices[lCounter];
			}
		}

		return NULL;
	}
	};
}

#endif