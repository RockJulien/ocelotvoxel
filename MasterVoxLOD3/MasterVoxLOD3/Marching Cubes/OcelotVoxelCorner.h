#ifndef DEF_OCELOTVOXELCORNER_H
#define DEF_OCELOTVOXELCORNER_H

/************************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   OcelotVoxelCorner.h/cpp
/
/ Description: This class is the corner's sample value which will
/              be contained by each corner. This signed 8-bits scalar
/              will serve to determine if a corner is outside in empty 
/              space or inside in solid space if the value is either 
/              positive or negative respectively.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        25/09/2012
/************************************************************************************/

#include "..\DataStructures\DataStructureUtil.h"

namespace Ocelot
{
	class OcelotVoxelCorner
	{
	private:

		// Attributes
		// 0x???? ???? corresponding to the corner's state
		// Clamped from -128/127 to -127/127 for symmetry.
		// Either negative (solid space) or positive (empty space)
		// Eight of these corners sample will be concatenated
		// for building the index for mapping the first look-up T.
		//CCHAR m_cSample;
		FFLOAT m_fSample;

	public:

		// Constructor & Destructor
		OcelotVoxelCorner();
		//OcelotVoxelCorner(CCHAR sample);
		OcelotVoxelCorner(FFLOAT sample);
		~OcelotVoxelCorner();

		// Methods
		BBOOL isNegative() const;
		BBOOL isPositive() const;

		// Accessors.
		//inline CCHAR Sample() const { return m_cSample;}
		//inline VVOID SetSample(CCHAR sample) { m_cSample = sample;}
		inline FFLOAT Sample() const { return m_fSample;}
		inline VVOID  SetSample(FFLOAT sample) { m_fSample = sample;}
	};
}

#endif