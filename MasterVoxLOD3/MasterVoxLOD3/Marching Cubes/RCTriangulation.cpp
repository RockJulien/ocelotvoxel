#include "RCTriangulation.h"

using namespace Ocelot;

RCTriangulation::RCTriangulation() :
ILookUpTriangulation(), m_ucGeometryCounts(0x00)
{
	for(USMALLINT i = 0; i < 15; i++)
		m_ucIndiceBuffer[i] = DEFINDICE; // default value 255
}

RCTriangulation::RCTriangulation(UCCHAR geoCounts, UCCHAR ind1, UCCHAR ind2, UCCHAR ind3, UCCHAR ind4, UCCHAR ind5, UCCHAR ind6, UCCHAR ind7, UCCHAR ind8,
								 UCCHAR ind9, UCCHAR ind10, UCCHAR ind11, UCCHAR ind12, UCCHAR ind13, UCCHAR ind14, UCCHAR ind15) :
ILookUpTriangulation(), m_ucGeometryCounts(geoCounts)
{
	m_ucIndiceBuffer[0]  = ind1;  m_ucIndiceBuffer[1]   = ind2;  m_ucIndiceBuffer[2]   = ind3;     // up to 5 triangles.
	m_ucIndiceBuffer[3]  = ind4;  m_ucIndiceBuffer[4]   = ind5;  m_ucIndiceBuffer[5]   = ind6; 
	m_ucIndiceBuffer[6]  = ind7;  m_ucIndiceBuffer[7]   = ind8;  m_ucIndiceBuffer[8]   = ind9; 
	m_ucIndiceBuffer[9]  = ind10; m_ucIndiceBuffer[10]  = ind11; m_ucIndiceBuffer[11]  = ind12; 
	m_ucIndiceBuffer[12] = ind13; m_ucIndiceBuffer[13]  = ind14; m_ucIndiceBuffer[14]  = ind15;
}

RCTriangulation::~RCTriangulation()
{
	
}

USMALLINT RCTriangulation::GetVertexCount() const
{
	// shift of 4 bits on the right for
	// overwriting the lower 4-bits by the
	// higher 4-bits (new higher 4-bits will
	// be 0000.
    return (m_ucGeometryCounts >> 4);
}

USMALLINT RCTriangulation::GetTriangleCount() const
{
	// mask applied on the higher 4-bits
	// for overwriting them and having 
	// no more than the original lower 
	// 4-bits.
    return (m_ucGeometryCounts & 0x0F);
}
