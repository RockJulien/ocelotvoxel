#ifndef DEF_OCELOTVOXELGEOMETRY_H
#define DEF_OCELOTVOXELGEOMETRY_H

#include <map>
#include "MCTables.cpp"
#include "..\Geometry\VertexTypes.h"
#include "OcelotVoxelBlock.h"
#include "OcelotVoxelCell.h"
#include "..\Collision\AABBox.h"
#include "..\Utilities\OcelotEditVoxel.h"
#include "..\DataStructures\Linked2List.h"
#include "..\Transition Voxels\TVTables.cpp"
#include "..\Transition Voxels\OcelotVoxelTCell.h"
#include "..\Transition Voxels\OcelotTransitionBlock.h"

namespace Ocelot
{
#define MAX_BLOCK_INDICE_BUFFER 7
#define TCELL_BLOCKFACE_SAMPLE_COUNT 9

									enum OcelotTCellPlan
									{
										OCELOT_TCELL_PLAN_XY,
										OCELOT_TCELL_PLAN_YZ,
										OCELOT_TCELL_PLAN_XZ
									};

									enum VoxelBlockFace
									{
										VOXELBLOCK_FACE_POS_Z,
										VOXELBLOCK_FACE_NEG_Z,
										VOXELBLOCK_FACE_POS_Y,
										VOXELBLOCK_FACE_NEG_Y,
										VOXELBLOCK_FACE_POS_X,
										VOXELBLOCK_FACE_NEG_X
									};

									enum VoxelCornerNumerotation
									{
										// Numerotation which insure the corner
										// ordering described by Lengyel for 
										// a regular cell.
										VOXELCORNER_NEAR_LOWERLEFT,
										VOXELCORNER_NEAR_LOWERRIGHT,
										VOXELCORNER_FAR_LOWERLEFT,
										VOXELCORNER_FAR_LOWERRIGHT,
										VOXELCORNER_NEAR_UPPERLEFT,
										VOXELCORNER_NEAR_UPPERRIGHT,
										VOXELCORNER_FAR_UPPERLEFT,
										VOXELCORNER_FAR_UPPERRIGHT
									};

									enum TCellCornerNumerotation
									{
										// Numerotation which insure the corner
										// ordering described by Lengyel for 
										// a transition cell.
										TRANSCELLCORNER_LOWERLEFT,
										TRANSCELLCORNER_LOWERMID,
										TRANSCELLCORNER_LOWERRIGHT,
										TRANSCELLCORNER_MIDLEFT,
										TRANSCELLCORNER_MIDMID,
										TRANSCELLCORNER_MIDRIGHT,
										TRANSCELLCORNER_UPPERLEFT,
										TRANSCELLCORNER_UPPERMID,
										TRANSCELLCORNER_UPPERRIGHT
									};

									enum BlockIndicesBuffer
									{
										INDICESBUFFER_MAINBLOCK,
										INDICESBUFFER_TCELLBLOCK_FACE_POS_Z,
										INDICESBUFFER_TCELLBLOCK_FACE_NEG_Z,
										INDICESBUFFER_TCELLBLOCK_FACE_POS_Y,
										INDICESBUFFER_TCELLBLOCK_FACE_NEG_Y,
										INDICESBUFFER_TCELLBLOCK_FACE_POS_X,
										INDICESBUFFER_TCELLBLOCK_FACE_NEG_X,
										INDICESBUFFER_NONE // Last one for allowing that
										// this enum be an array index helper as well.
									};

									struct BlockGeometryInfo
									{
										BlockGeometryInfo() :
										m_iStartPosX(1), m_iStartPosY(1), m_iStartPosZ(1), m_iSizeX(16), m_iSizeY(16), m_iSizeZ(16), m_fVoxelScale(1.0f), m_bTransitionNeeded(false){}
										BlockGeometryInfo(BIGINT startPosX, BIGINT startPosY, BIGINT startPosZ, UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ, FFLOAT voxScale,BBOOL transitionNeeded) :
										m_iStartPosX(startPosX), m_iStartPosY(startPosY), m_iStartPosZ(startPosZ), m_iSizeX(sizeX), m_iSizeY(sizeY), m_iSizeZ(sizeZ), m_fVoxelScale(voxScale), m_bTransitionNeeded(transitionNeeded){}

										BIGINT  m_iStartPosX;
										BIGINT  m_iStartPosY;
										BIGINT  m_iStartPosZ;
										UBIGINT m_iSizeX;
										UBIGINT m_iSizeY;
										UBIGINT m_iSizeZ;
										// The scale of a voxel, that is between corner's
										// samples, so cube's egdes.
										FFLOAT  m_fVoxelScale;
										// Boolean informing if the block is a block
										// between two different LODs.
										BBOOL   m_bTransitionNeeded;
									};

	class OcelotVoxelGeometry// : public OcelotVoxelBlock
	{
	private:

		// NOTE: Changed data structures with std::vector for stability.

		// Attributes
		// The two following stuctures will be the final vertices and indices buffers.
		UBIGINT                        m_iVerticesCount;
		VoxelVertex*		           m_vVerticesBuffer;
		// Will contain the block's indices plus the 6 block's 
		// face transition blocks indices if any (7).
		UBIGINT                        m_iIndicesCount[MAX_BLOCK_INDICE_BUFFER];
		UBIGINT*                       m_iIndicesBuffer[MAX_BLOCK_INDICE_BUFFER];

		// Tempo structure containing indices and vertices computed
		// helping the overall block geometry creation.
		std::vector<UBIGINT>           m_iIndicesComputed[MAX_BLOCK_INDICE_BUFFER];
		std::map<UBIGINT, VoxelVertex> m_tableVerticesComputed;

		OcelotVoxelBlock               m_mainBlock;
		// Array of transition block sample maps if the block needs
		// transition cells at its borders (6 faces).
		OcelotTransitionBlock*         m_facesTransitionBlock;

		// Base information for the block geometry creation.
		BlockGeometryInfo              m_geoInfo;

		// Structure holding the parts to edit.
		std::vector<OcelotEditVoxel*>  m_partsToEdit;

		// Private Methods
		// For regular cells.
		Vector3 GetCornerPos(USMALLINT corner, Vector3 minimalCornerPos);
		Vector3 computeVertexPosOnEdge(USMALLINT endpoint1, USMALLINT endpoint2, UBIGINT minPosX, UBIGINT minPosY, UBIGINT minPosZ, BaseBlockMap* samples);

		// For transition cells.
		Vector3 GetTCellCornerPos(const USMALLINT corner, Vector3 minimalCornerPos, const BIGINT tCellCornerPos[9][3]);
		Vector3 computeTCellVertexPosOnEdge(USMALLINT endpoint1, USMALLINT endpoint2, UBIGINT minPosX, UBIGINT minPosY, UBIGINT minPosZ, const BIGINT tCellCornerPos[9][3], BaseBlockMap* samples);
		VVOID   initializeTCellBlock(UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ);

		// Generic.
		FFLOAT  GetSample(UBIGINT posX, UBIGINT posY, UBIGINT posZ, BaseBlockMap* samples);
		UBIGINT GetVertexID(UBIGINT onX, UBIGINT onY, UBIGINT onZ);
		UBIGINT GetVertexIDToReuse(UBIGINT onX, UBIGINT onY, UBIGINT onZ, UCCHAR higherByte);
		FFLOAT  computeFloatingPointInterpolParam(FFLOAT sampleEndpoint1, FFLOAT sampleEndpoint2);
		Vector3 interpolateVertexOnEdge(const FFLOAT floatedPointParam, Vector3 posEndpoint1, Vector3 posEndpoint2);

	public:

		// Constructor & Destructor
		OcelotVoxelGeometry(BIGINT startPosX, BIGINT startPosY, BIGINT startPosZ, UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ, FFLOAT scale, BBOOL transNeeded);
		~OcelotVoxelGeometry();

		// Methods
		VVOID			 computeGeometry();
		//VVOID			 runMarchingCubes(BIGINT fromX, BIGINT fromY, BIGINT fromZ, BIGINT toX, BIGINT toY, BIGINT toZ, std::map<UBIGINT, VoxelVertex>* vertBuffer, BlockIndicesBuffer buffer = INDICESBUFFER_MAINBLOCK);
		VVOID			 runMarchingCubes(BIGINT fromX, BIGINT fromY, BIGINT fromZ, BIGINT toX, BIGINT toY, BIGINT toZ, std::map<UBIGINT, VoxelVertex>* vertBuffer, std::vector<UBIGINT>* indBuffer, BaseBlockMap* samples);
		//VVOID			 runTransitionVox(BIGINT fromX, BIGINT fromY, BIGINT fromZ, BIGINT toX, BIGINT toY, BIGINT toZ, VoxelBlockFace face, BlockIndicesBuffer buffer);
		VVOID			 runTransitionVox(BIGINT fromX, BIGINT fromY, BIGINT fromZ, BIGINT toX, BIGINT toY, BIGINT toZ, std::map<UBIGINT, VoxelVertex>* vertBuffer, std::vector<UBIGINT>* indBuffer, BaseBlockMap* mainSamples, BaseBlockMap* tCellSamples, BIGINT plane, BBOOL invert);
		VVOID			 addPartToEdit(OcelotEditVoxel* toEdit);
		VVOID			 correctNormal(const BIGINT startPosX, const BIGINT startPosY, const BIGINT startPosZ, const BIGINT endPosX, const BIGINT endPosY, const BIGINT endPosZ);
		VVOID			 buildFinalBuffer();
		VVOID			 releaseGeometry();

		// Accessors
		inline VoxelVertex*			  VBuffer() { return m_vVerticesBuffer;}
		inline UBIGINT				  VCount() const { return m_iVerticesCount;}
		inline UBIGINT*         	  IBuffer(USMALLINT whichOne) { return m_iIndicesBuffer[whichOne];}
		inline UBIGINT                ICount(USMALLINT whichOne) const { return m_iIndicesCount[whichOne];}
		inline OcelotVoxelBlock*      MainBlock() { return &m_mainBlock;}
		inline OcelotTransitionBlock* TBlockBuffer() { return m_facesTransitionBlock;}
		inline BlockGeometryInfo      GeoInfo() const { return m_geoInfo;}
		inline AABBox                 GetAABBox() { return AABBox(BaseDXData(), m_geoInfo.m_iStartPosX * m_geoInfo.m_fVoxelScale, 
																			    m_geoInfo.m_iStartPosY * m_geoInfo.m_fVoxelScale, 
																				m_geoInfo.m_iStartPosZ * m_geoInfo.m_fVoxelScale, 
																				m_geoInfo.m_iStartPosX * m_geoInfo.m_fVoxelScale + m_geoInfo.m_iSizeX * m_geoInfo.m_fVoxelScale, 
																				m_geoInfo.m_iStartPosY * m_geoInfo.m_fVoxelScale + m_geoInfo.m_iSizeY * m_geoInfo.m_fVoxelScale, 
																				m_geoInfo.m_iStartPosZ * m_geoInfo.m_fVoxelScale + m_geoInfo.m_iSizeZ * m_geoInfo.m_fVoxelScale, AABB_GENERATOR_MINMAX);}
		inline BBOOL                  NoNeedBlock() { return MainBlock()->NoNeedBlock();}
		inline VVOID                  SetGeoInfo(BlockGeometryInfo geoInfo) { m_geoInfo = geoInfo;}
	};
}

#endif