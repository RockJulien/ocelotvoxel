#ifndef DEF_OCELOTVOXELBLOCK_H
#define DEF_OCELOTVOXELBLOCK_H

#include "..\Utilities\BaseBlockMap.h"

namespace Ocelot
{
	class OcelotVoxelBlock : public BaseBlockMap
	{
	protected:

			// Attributes

	public:

			// Constructor & Destructor
			OcelotVoxelBlock();
			OcelotVoxelBlock(UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ, BIGINT offsetX, BIGINT offsetY, BIGINT offsetZ);
	virtual ~OcelotVoxelBlock();

			// Methods


			// Accessors

	};
}

#endif