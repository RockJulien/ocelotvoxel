#include "OcelotVoxelGeometry.h"

using namespace std;
using namespace Ocelot;

#pragma warning (disable : 4244)

// just for comprehension of value
// and size of array dimension.
#define NBPLAN 3
#define NBAXIS 3

// static for avoiding to re-create the array each time the function which uses it
// is called (welcome to SaveEveryMicroSecond.com). joke.
static const BIGINT fullResSampleOffset[NBPLAN][TCELL_BLOCKFACE_SAMPLE_COUNT][NBAXIS] = 
{
	{	// sample offset of TCell block face on the plan YZ (NOTE: X always equal to 0).
		{0, 0, 0}, {0, 1, 0}, {0, 2, 0}, 
		{0, 2, 1}, {0, 2, 2}, {0, 1, 2}, 
		{0, 0, 2}, {0, 0, 1}, {0, 1, 1}
	},
    {	// sample offset of TCell block face on the plan XY (NOTE: Z always equal to 0).
		{0, 0, 0}, {1, 0, 0}, {2, 0, 0}, 
		{2, 1, 0}, {2, 2, 0}, {1, 2, 0}, 
		{0, 2, 0}, {0, 1, 0}, {1, 1, 0}
	},
    {	// sample offset of TCell block face on the plan XZ (NOTE: Y always equal to 0).
		{0, 0, 0}, {1, 0, 0}, {2, 0, 0}, 
		{2, 0, 1}, {2, 0, 2}, {1, 0, 2}, 
		{0, 0, 2}, {0, 0, 1}, {1, 0, 1}
	}
};

OcelotVoxelGeometry::OcelotVoxelGeometry(BIGINT startPosX, BIGINT startPosY, BIGINT startPosZ, UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ, FFLOAT scale, BBOOL transNeeded) : 
m_mainBlock(sizeX + 3, sizeY + 3, sizeZ + 3, 1, 1, 1), 
m_vVerticesBuffer(NULL), 
m_geoInfo(startPosX,startPosY, startPosZ, sizeX, sizeY, sizeZ, scale, transNeeded), 
m_facesTransitionBlock(NULL)
{
	for(USMALLINT i = 0; i < 7; i++)
		m_iIndicesBuffer[i] = 0;

	initializeTCellBlock(sizeX, sizeY, sizeZ);
}

OcelotVoxelGeometry::~OcelotVoxelGeometry()
{
	// Release pointers on block face and delete them.
	if(m_facesTransitionBlock)
		DeleteArray1D(m_facesTransitionBlock);

	if(m_vVerticesBuffer)
		DeleteArray1D(m_vVerticesBuffer);

	if(m_iIndicesBuffer)
		DeleteStatic(m_iIndicesBuffer, MAX_BLOCK_INDICE_BUFFER);
}

FFLOAT  OcelotVoxelGeometry::GetSample(UBIGINT posX, UBIGINT posY, UBIGINT posZ, BaseBlockMap* samples)
{
	return samples->GetCornerSample(((BIGINT)posX) - 1, ((BIGINT)posY) - 1, ((BIGINT)posZ) - 1);
}

Vector3 OcelotVoxelGeometry::GetCornerPos(USMALLINT corner, Vector3 minimalCornerPos)
{
	// Get the voxel corner desired, by passing the
	// minimal corner of the voxel and simply
	// adding 1 unit on the expected axes for
	// grabbing it.
	Vector3 toReturn = Vector3(0.0f);

	switch(corner)
	{
	case VOXELCORNER_NEAR_LOWERLEFT:
		toReturn = minimalCornerPos;
		break;
	case VOXELCORNER_NEAR_LOWERRIGHT:
		{
			Vector3 toNearLowerRight(1.0f, 0.0f, 0.0f);
			toReturn = minimalCornerPos + toNearLowerRight;
		}
		break;
	case VOXELCORNER_FAR_LOWERLEFT:
		{
			Vector3 toFarLowerLeft(0.0f, 0.0f, 1.0f);
			toReturn = minimalCornerPos + toFarLowerLeft;
		}
		break;
	case VOXELCORNER_FAR_LOWERRIGHT:
		{
			Vector3 toFarLowerRight(1.0f, 0.0f, 1.0f);
			toReturn = minimalCornerPos + toFarLowerRight;
		}
		break;
	case VOXELCORNER_NEAR_UPPERLEFT:
		{
			Vector3 toNearUpperLeft(0.0f, 1.0f, 0.0f);
			toReturn = minimalCornerPos + toNearUpperLeft;
		}
		break;
	case VOXELCORNER_NEAR_UPPERRIGHT:
		{
			Vector3 toNearUpperRight(1.0f, 1.0f, 0.0f);
			toReturn = minimalCornerPos + toNearUpperRight;
		}
		break;
	case VOXELCORNER_FAR_UPPERLEFT:
		{
			Vector3 toFarUpperLeft(0.0f, 1.0f, 1.0f);
			toReturn = minimalCornerPos + toFarUpperLeft;
		}
		break;
	case VOXELCORNER_FAR_UPPERRIGHT:
		{
			Vector3 toFarUpperRight(1.0f, 1.0f, 1.0f);
			toReturn = minimalCornerPos + toFarUpperRight;
		}
		break;
	}

	return toReturn;
}

Vector3 OcelotVoxelGeometry::computeVertexPosOnEdge(USMALLINT endpoint1, USMALLINT endpoint2, UBIGINT minPosX, UBIGINT minPosY, UBIGINT minPosZ, BaseBlockMap* samples)
{
	Vector3 toReturn = Vector3(0.0f);

	// For a accurate mapping, minimal pos are passed as unsigned for preventing
	// from signed problem and floating point error because float of a vector struct
	// can be negative and 1.0f can be 0.999999f which make that the map index
	// which has to be 1 becomes 0 when passing to Integer for mapping the block
	// array.
	Vector3 minimalCornerPos((FFLOAT)minPosX, (FFLOAT)minPosY, (FFLOAT)minPosZ);

	// First, get back the two endpoint positions for this voxel.
	Vector3 tempEndpoint1 = GetCornerPos(endpoint1, minimalCornerPos);
	Vector3 tempEndpoint2 = GetCornerPos(endpoint2, minimalCornerPos);

	// Then, grab the sample for these corners above.
	// Note: take into account the negative layer of sample.
	FFLOAT sample1 = GetSample(tempEndpoint1.getX(), tempEndpoint1.getY(), tempEndpoint1.getZ(), samples);
	FFLOAT sample2 = GetSample(tempEndpoint2.getX(), tempEndpoint2.getY(), tempEndpoint2.getZ(), samples);

	// Compute the fixed-point interpolation parameter needed for
	// the vertex interpolation of its position along the edge.
	FFLOAT floatedPointParam = computeFloatingPointInterpolParam(sample1, sample2);

	// Then, iterpolate the position on the edge.
	toReturn = interpolateVertexOnEdge(floatedPointParam, tempEndpoint1, tempEndpoint2);

	return toReturn;
}

Vector3 OcelotVoxelGeometry::GetTCellCornerPos(const USMALLINT corner, Vector3 minimalCornerPos, const BIGINT tCellCornerPos[9][3])
{
	Vector3 toReturn = Vector3(0.0f);

	switch(corner)
	{
	case TRANSCELLCORNER_LOWERLEFT:
		toReturn = minimalCornerPos;
		break;
	case TRANSCELLCORNER_LOWERMID:
		{
			Vector3 toLowerMid = Vector3((FFLOAT)tCellCornerPos[1][0], (FFLOAT)tCellCornerPos[1][1], (FFLOAT)tCellCornerPos[1][2]);
			toReturn = minimalCornerPos + toLowerMid * 0.5f;
		}
		break;
	case TRANSCELLCORNER_LOWERRIGHT:
		{
			Vector3 toLowerRight = Vector3((FFLOAT)tCellCornerPos[2][0], (FFLOAT)tCellCornerPos[2][1], (FFLOAT)tCellCornerPos[2][2]);
			toReturn = minimalCornerPos + toLowerRight * 0.5f;
		}
		break;
	case TRANSCELLCORNER_MIDLEFT:
		{
			Vector3 toMidLeft = Vector3((FFLOAT)tCellCornerPos[7][0], (FFLOAT)tCellCornerPos[7][1], (FFLOAT)tCellCornerPos[7][2]);
			toReturn = minimalCornerPos + toMidLeft * 0.5f;
		}
		break;
	case TRANSCELLCORNER_MIDMID:
		{
			Vector3 toMidMid = Vector3((FFLOAT)tCellCornerPos[8][0], (FFLOAT)tCellCornerPos[8][1], (FFLOAT)tCellCornerPos[8][2]);
			toReturn = minimalCornerPos + toMidMid * 0.5f;
		}
		break;
	case TRANSCELLCORNER_MIDRIGHT:
		{
			Vector3 toMidRight = Vector3((FFLOAT)tCellCornerPos[3][0], (FFLOAT)tCellCornerPos[3][1], (FFLOAT)tCellCornerPos[3][2]);
			toReturn = minimalCornerPos + toMidRight * 0.5f;
		}
		break;
	case TRANSCELLCORNER_UPPERLEFT:
		{
			Vector3 toUpperLeft = Vector3((FFLOAT)tCellCornerPos[6][0], (FFLOAT)tCellCornerPos[6][1], (FFLOAT)tCellCornerPos[6][2]);
			toReturn = minimalCornerPos + toUpperLeft * 0.5f;
		}
		break;
	case TRANSCELLCORNER_UPPERMID:
		{
			Vector3 toUpperMid = Vector3((FFLOAT)tCellCornerPos[5][0], (FFLOAT)tCellCornerPos[5][1], (FFLOAT)tCellCornerPos[5][2]);
			toReturn = minimalCornerPos + toUpperMid * 0.5f;
		}
		break;
	case TRANSCELLCORNER_UPPERRIGHT:
		{
			Vector3 toUpperRight = Vector3((FFLOAT)tCellCornerPos[4][0], (FFLOAT)tCellCornerPos[4][1], (FFLOAT)tCellCornerPos[4][2]);
			toReturn = minimalCornerPos + toUpperRight * 0.5f;
		}
		break;
	}

	return toReturn;
}

Vector3 OcelotVoxelGeometry::computeTCellVertexPosOnEdge(USMALLINT endpoint1, USMALLINT endpoint2, UBIGINT minPosX, UBIGINT minPosY, UBIGINT minPosZ, const BIGINT tCellCornerPos[9][3], BaseBlockMap* samples)
{
	Vector3 toReturn = Vector3(0.0f);

	Vector3 minimalCornerPos((FFLOAT)minPosX, (FFLOAT)minPosY, (FFLOAT)minPosZ);

	// First, get back the two endpoint positions for this voxel.
	Vector3 tempEndpoint1 = GetTCellCornerPos(endpoint1, minimalCornerPos, tCellCornerPos);
	Vector3 tempEndpoint2 = GetTCellCornerPos(endpoint2, minimalCornerPos, tCellCornerPos);

	// Then, grab the sample for these corners above.
	// Note: take into account the negative layer of sample
	// and the size of a half-resolution face is twice bigger
	// so, position of samples on the full-resolution are 
	// multiplied as well.
	FFLOAT sample1 = GetSample(tempEndpoint1.getX() * 2.0f - 1, tempEndpoint1.getY() * 2.0f - 1, tempEndpoint1.getZ() * 2.0f - 1, samples);
	FFLOAT sample2 = GetSample(tempEndpoint2.getX() * 2.0f - 1, tempEndpoint2.getY() * 2.0f - 1, tempEndpoint2.getZ() * 2.0f - 1, samples);

	// Compute the fixed-point interpolation parameter needed for
	// the vertex interpolation of its position along the edge.
	FFLOAT floatedPointParam = computeFloatingPointInterpolParam(sample1, sample2);

	// Then, iterpolate the position on the edge.
	toReturn = interpolateVertexOnEdge(floatedPointParam, tempEndpoint1, tempEndpoint2);

	return toReturn;
}

UBIGINT OcelotVoxelGeometry::GetVertexID(UBIGINT onX, UBIGINT onY, UBIGINT onZ)
{
	// Get the vertex ID thanks to position parameters which
	// once combined with the block sample map will represent 
	// the ID.
	UBIGINT toReturn = 0;

	toReturn = 20 * (onZ * (m_geoInfo.m_iSizeY + 3) * (m_geoInfo.m_iSizeX + 3) + onY * (m_geoInfo.m_iSizeX + 3) + onX);

	return toReturn;
}

UBIGINT OcelotVoxelGeometry::GetVertexIDToReuse(UBIGINT onX, UBIGINT onY, UBIGINT onZ, UCCHAR higherByte)
{
	UBIGINT toReturn = 0;

	// Get the vertex ID to re-use thanks to the normal GetVertexID function
	// combined with Lengyel's look-up table 16-bits returned value (higher byte).
	USMALLINT lowQuartetVertexIDToReuse = higherByte & 0x0F;
	USMALLINT highQuartetDirToFollow    = (higherByte & 0xF0) >> 4;

	// too big but is Okay.
	//USMALLINT moveOnX = highQuartetDirToFollow & 0x01;        // keep the LSB safe. (means going one unit back on X if equal to 1).
	//USMALLINT moveOnY = (highQuartetDirToFollow & 0x02) >> 1; // keep the 2pow(1) bit safe. (means going one unit back on Y if equal to 1).
	//USMALLINT moveOnZ = (highQuartetDirToFollow & 0x04) >> 2; // so on. (means going one unit back on Z if equal to 1).

	USMALLINT moveOnX = highQuartetDirToFollow%2;        // compute the move to apply for reaching the vertex to reuse on X
	USMALLINT moveOnY = (highQuartetDirToFollow >> 2)%2; // compute the move to apply for reaching the vertex to reuse on Y
	USMALLINT moveOnZ = (highQuartetDirToFollow >> 1)%2; // So on.

	// The 2pow(3) bit will determine if force to create a new vertex way before
	// in the process.
	toReturn = GetVertexID(onX - moveOnX, onY - moveOnY, onZ - moveOnZ) + (lowQuartetVertexIDToReuse - 1);

	return toReturn;
}

FFLOAT  OcelotVoxelGeometry::computeFloatingPointInterpolParam(FFLOAT sampleEndpoint1, FFLOAT sampleEndpoint2)
{
	// Be sure that endpoint1 be the lower-numbered corner or seams will appear.
	FFLOAT floatedPoint = 0;

	// Prevent to divide by zero.
	//if(!(sampleEndpoint2 == sampleEndpoint1))
	// Follow a consistent ordering of the endpoints, so endpoint2 must be the higher-numbered corner.
	//fixedPoint = (BIGINT)((sampleEndpoint2 << 8) / (sampleEndpoint2 - sampleEndpoint1));
	floatedPoint = (0.0f - sampleEndpoint1) / (sampleEndpoint2 - sampleEndpoint1);

	return floatedPoint;
}

Vector3 OcelotVoxelGeometry::interpolateVertexOnEdge(FFLOAT floatedPointParam, Vector3 posEndpoint1, Vector3 posEndpoint2)
{
	// Interpolate thanks to the fixed point parameter and the two endpoint positions.
	Vector3 vertexOnEdge = Vector3(0.0f);

	// Still follow the consistent ordering rule, so posEndpoint1 must be the lower-numbered corner's position.
	//BIGINT u = 0x0100 - fixedPointParam;
	//vertexOnEdge = fixedPointParam * posEndpoint1 + u * posEndpoint2;
	vertexOnEdge = posEndpoint1 + floatedPointParam * (posEndpoint2 - posEndpoint1);

	return vertexOnEdge;
}

VVOID   OcelotVoxelGeometry::initializeTCellBlock(UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ)
{
	if(m_geoInfo.m_bTransitionNeeded)
	{
		// Allocate memory for each block of each face.
		m_facesTransitionBlock = new OcelotTransitionBlock[6];

		// No offset on Z because of the size in sample which is only of 1.
		// -halfBlockSize as offset for neg faces instead of zero because of the opposite starting pos in the sample map.
		// initialize Z face's TCellBlock.
		m_facesTransitionBlock[0] = OcelotTransitionBlock(sizeX * 2 + 1 + 3, sizeY * 2 + 1 + 3, 1, 1, 1, 0);
		m_facesTransitionBlock[1] = OcelotTransitionBlock(sizeX * 2 + 1 + 3, sizeY * 2 + 1 + 3, 1, 1, 1, -sizeZ * 2);
		// No offset on Y because of the size in sample which is only of 1.
		// initialize Y face's TCellBlock.
		m_facesTransitionBlock[2] = OcelotTransitionBlock(sizeX * 2 + 1 + 3, 1, sizeZ * 2 + 1 + 3, 1, 0, 1);
		m_facesTransitionBlock[3] = OcelotTransitionBlock(sizeX * 2 + 1 + 3, 1, sizeZ * 2 + 1 + 3, 1, -sizeY * 2, 1);
		// No offset on X because of the size in sample which is only of 1.
		// initialize X face's TCellBlock.
		m_facesTransitionBlock[4] = OcelotTransitionBlock(1, sizeY * 2 + 1 + 3, sizeZ * 2 + 1 + 3, 0, 1, 1);
		m_facesTransitionBlock[5] = OcelotTransitionBlock(1, sizeY * 2 + 1 + 3, sizeZ * 2 + 1 + 3, -sizeX * 2, 1, 1);

	}
}

VVOID			 OcelotVoxelGeometry::computeGeometry()
{
	// Each time we will re-compute the block
	// we must release previously computed 
	// vertices and indices first.
	releaseGeometry();

	// First, map main block of samples
	m_mainBlock.map();

	// If transitions are needed, map
	// with 6 transition blocks along 
	// main block faces.
	if(m_geoInfo.m_bTransitionNeeded)
	{
		for(USMALLINT currFace = 0; currFace < 6; currFace++)
			m_facesTransitionBlock[currFace].map();
	}

	// Main geometry process.
	// Build voxel's samples for part to edit. By creating samples
	// instead of loading and using them from memory, It saves lot
	// of memory, but increases the run-time computation. Fortunately,
	// only parts which will need to be edited will be recomputed.
	vector<OcelotEditVoxel*>::const_iterator iter = m_partsToEdit.begin();
	for(; iter != m_partsToEdit.end(); iter++)
	{
		// Edit the main block sample map
		(*iter)->buildVoxel(m_geoInfo.m_fVoxelScale, 
							m_geoInfo.m_iStartPosX - 1, 
							m_geoInfo.m_iStartPosY - 1, 
							m_geoInfo.m_iStartPosZ - 1, 
							m_geoInfo.m_iStartPosX + m_geoInfo.m_iSizeX + 2, 
							m_geoInfo.m_iStartPosY + m_geoInfo.m_iSizeY + 2,
							m_geoInfo.m_iStartPosZ + m_geoInfo.m_iSizeZ + 2, 
							&m_mainBlock);

		// Then, if this part is a part between two different LODs
		// Edit transition block sample maps as well.
		if(m_geoInfo.m_bTransitionNeeded)
		{
			// Move position for a twice bigger block
			m_geoInfo.m_iStartPosX *= 2;
			m_geoInfo.m_iStartPosY *= 2;
			m_geoInfo.m_iStartPosZ *= 2;

			// Remember, TCells are one cell thick in the direction of the axis they are facing.
			(*iter)->buildVoxel((m_geoInfo.m_fVoxelScale / 2.0f), 
								m_geoInfo.m_iStartPosX - 1, 
								m_geoInfo.m_iStartPosY - 1, 
								m_geoInfo.m_iStartPosZ - 0, 
								m_geoInfo.m_iStartPosX + m_geoInfo.m_iSizeX * 2 + 2, 
								m_geoInfo.m_iStartPosY + m_geoInfo.m_iSizeY * 2 + 2,
								m_geoInfo.m_iStartPosZ + 1, 
								&m_facesTransitionBlock[0]);

			(*iter)->buildVoxel((m_geoInfo.m_fVoxelScale / 2.0f), 
								m_geoInfo.m_iStartPosX - 1, 
								m_geoInfo.m_iStartPosY - 1, 
								m_geoInfo.m_iStartPosZ + m_geoInfo.m_iSizeZ * 2 - 0, 
								m_geoInfo.m_iStartPosX + m_geoInfo.m_iSizeX * 2 + 2, 
								m_geoInfo.m_iStartPosY + m_geoInfo.m_iSizeY * 2 + 2,
								m_geoInfo.m_iStartPosZ + m_geoInfo.m_iSizeZ * 2 + 1, 
								&m_facesTransitionBlock[1]);

			(*iter)->buildVoxel((m_geoInfo.m_fVoxelScale / 2.0f), 
								m_geoInfo.m_iStartPosX - 1, 
								m_geoInfo.m_iStartPosY - 0, 
								m_geoInfo.m_iStartPosZ - 1, 
								m_geoInfo.m_iStartPosX + m_geoInfo.m_iSizeX * 2 + 2, 
								m_geoInfo.m_iStartPosY + 1,
								m_geoInfo.m_iStartPosZ + m_geoInfo.m_iSizeZ * 2 + 2, 
								&m_facesTransitionBlock[2]);

			(*iter)->buildVoxel((m_geoInfo.m_fVoxelScale / 2.0f), 
								m_geoInfo.m_iStartPosX - 1, 
								m_geoInfo.m_iStartPosY + m_geoInfo.m_iSizeY * 2 - 0, 
								m_geoInfo.m_iStartPosZ - 1, 
								m_geoInfo.m_iStartPosX + m_geoInfo.m_iSizeX * 2 + 2, 
								m_geoInfo.m_iStartPosY + m_geoInfo.m_iSizeY * 2 + 1,
								m_geoInfo.m_iStartPosZ + m_geoInfo.m_iSizeZ * 2 + 2, 
								&m_facesTransitionBlock[3]);

			(*iter)->buildVoxel((m_geoInfo.m_fVoxelScale / 2.0f), 
								m_geoInfo.m_iStartPosX - 0, 
								m_geoInfo.m_iStartPosY - 1, 
								m_geoInfo.m_iStartPosZ - 1, 
								m_geoInfo.m_iStartPosX + 1, 
								m_geoInfo.m_iStartPosY + m_geoInfo.m_iSizeY * 2 + 2,
								m_geoInfo.m_iStartPosZ + m_geoInfo.m_iSizeZ * 2 + 2, 
								&m_facesTransitionBlock[4]);

			(*iter)->buildVoxel((m_geoInfo.m_fVoxelScale / 2.0f), 
								m_geoInfo.m_iStartPosX + m_geoInfo.m_iSizeX * 2 - 0, 
								m_geoInfo.m_iStartPosY - 1, 
								m_geoInfo.m_iStartPosZ - 1, 
								m_geoInfo.m_iStartPosX + m_geoInfo.m_iSizeX * 2 + 1, 
								m_geoInfo.m_iStartPosY + m_geoInfo.m_iSizeY * 2 + 2,
								m_geoInfo.m_iStartPosZ + m_geoInfo.m_iSizeZ * 2 + 2, 
								&m_facesTransitionBlock[5]);

			// Reset start pos to main block ones.
			m_geoInfo.m_iStartPosX /= 2;
			m_geoInfo.m_iStartPosY /= 2;
			m_geoInfo.m_iStartPosZ /= 2;
		}
	}

	BIGINT startPosX = 0;
	BIGINT startPosY = 0;
	BIGINT startPosZ = 0;
	BIGINT endPosX   = m_geoInfo.m_iSizeX + 2;
	BIGINT endPosY   = m_geoInfo.m_iSizeY + 2;
	BIGINT endPosZ   = m_geoInfo.m_iSizeZ + 2;

	// Once edits are taken into account, run marching cubes algorithm
	// and extract iso-surfaces.
	runMarchingCubes(startPosX + 1, startPosY + 1, startPosZ + 1, 
					 endPosX - 1, endPosY - 1, endPosZ - 1, 
					 &m_tableVerticesComputed, 
					 &m_iIndicesComputed[0], 
					 &m_mainBlock);

	// Correct Normals.
	correctNormal(startPosX, startPosY, startPosZ, endPosX, endPosY, endPosZ);

	// Then, if transition blocks are needed because neighboring blocks
	// have different LODs, run transition voxels algorithm.
	if(m_geoInfo.m_bTransitionNeeded)
	{
		runTransitionVox(startPosX + 1, 
						 startPosY + 1, 
						 1, 
						 endPosX - 1, 
						 endPosY - 1, 
						 2, 
						 &m_tableVerticesComputed, 
						 &m_iIndicesComputed[1], 
						 &m_mainBlock, 
						 &m_facesTransitionBlock[0], 
						 1, 
						 false);
		runTransitionVox(startPosX + 1, 
						 startPosY + 1, 
						 m_geoInfo.m_iSizeZ + 1, 
						 endPosX - 1, 
						 endPosY - 1, 
						 m_geoInfo.m_iSizeZ + 2, 
						 &m_tableVerticesComputed, 
						 &m_iIndicesComputed[2], 
						 &m_mainBlock, 
						 &m_facesTransitionBlock[1], 
						 1, 
						 true);
		runTransitionVox(startPosX + 1, 
						 1, 
						 startPosZ + 1, 
						 endPosX - 1, 
						 2, 
						 endPosZ - 1, 
						 &m_tableVerticesComputed, 
						 &m_iIndicesComputed[3], 
						 &m_mainBlock, 
						 &m_facesTransitionBlock[2], 
						 2, 
						 true);
		runTransitionVox(startPosX + 1, 
						 m_geoInfo.m_iSizeY + 1, 
						 startPosZ + 1, 
						 endPosX - 1, 
						 m_geoInfo.m_iSizeY + 2, 
						 endPosZ - 1, 
						 &m_tableVerticesComputed, 
						 &m_iIndicesComputed[4], 
						 &m_mainBlock, 
						 &m_facesTransitionBlock[3], 
						 2, 
						 false);
		runTransitionVox(1, 
						 startPosY + 1, 
						 startPosZ + 1, 
						 2, 
						 endPosY - 1, 
						 endPosZ - 1, 
						 &m_tableVerticesComputed, 
						 &m_iIndicesComputed[5], 
						 &m_mainBlock, 
						 &m_facesTransitionBlock[4], 
						 0, 
						 false);
		runTransitionVox(m_geoInfo.m_iSizeX + 1, 
						 startPosY + 1, 
						 startPosZ + 1, 
						 m_geoInfo.m_iSizeX + 2, 
						 endPosY - 1, 
						 endPosZ - 1, 
						 &m_tableVerticesComputed, 
						 &m_iIndicesComputed[6], 
						 &m_mainBlock, 
						 &m_facesTransitionBlock[5], 
						 0, 
						 true);
	}

	// Once samples needed for the main 
	// block are used, released them.
	m_mainBlock.unmap();

	// If transvoxel has been computed, so
	// it means that tCellBlocks of samples
	// must be released as well.
	if(m_geoInfo.m_bTransitionNeeded)
	{
		for(USMALLINT currFace = 0; currFace < 6; currFace++)
			m_facesTransitionBlock[currFace].unmap();
	}

	// Once every vertices and indices are
	// pre-computed, build final buffers.
	buildFinalBuffer();
}

VVOID			 OcelotVoxelGeometry::runMarchingCubes(BIGINT fromX, BIGINT fromY, BIGINT fromZ, BIGINT toX, BIGINT toY, BIGINT toZ, std::map<UBIGINT, VoxelVertex>* vertBuffer, std::vector<UBIGINT>* indBuffer, BaseBlockMap* samples)
{
	for(UBIGINT currSampPosZ = fromZ; currSampPosZ < (unsigned)toZ; currSampPosZ++)
	{
		for(UBIGINT currSampPosY = fromY; currSampPosY < (unsigned)toY; currSampPosY++)
		{
			for(UBIGINT currSampPosX = fromX; currSampPosX < (unsigned)toX; currSampPosX++)
			{
				// Note: add 1 to parameters following the Lengyel's corners ordering (Figure 3.7 page 18)
				// and create virtually the cube.
				OcelotVoxelCell tempCell(GetSample(currSampPosX, currSampPosY, currSampPosZ, samples), 
										 GetSample(currSampPosX + 1, currSampPosY, currSampPosZ, samples), 
										 GetSample(currSampPosX, currSampPosY, currSampPosZ + 1, samples), 
										 GetSample(currSampPosX + 1, currSampPosY, currSampPosZ + 1, samples),
										 GetSample(currSampPosX, currSampPosY + 1, currSampPosZ, samples), 
										 GetSample(currSampPosX + 1, currSampPosY + 1, currSampPosZ, samples), 
										 GetSample(currSampPosX, currSampPosY + 1, currSampPosZ + 1, samples), 
										 GetSample(currSampPosX + 1, currSampPosY + 1, currSampPosZ + 1, samples));

				// Automatic cell's function for generating class index.
				UCCHAR classIndex        = tempCell.concatenateSamples();
				
				// Check if non-trivial triangulation, else case 0 or 255 means empty, thus do nothing.
				//if((classIndex ^ ((tempCell.VoxelSamples().Array()[7].Sample() >> 7) & 0xFF)) != 0)
				if(classIndex != 0 && classIndex != 255)
				{
					// Now the Lengyel's Look-up tables enter in action.
					// Grabs the triangulation case index.
					UCCHAR triangulationCase = GetRegularCellClass(classIndex);

					// Get back the concrete triangulation corresponding to the case.
					const RCTriangulation cellTriangulation = GetRegularCellData(triangulationCase);

					// Keep a track of vertex to reuse IDs (up to 12: see Lookup tables).
					UBIGINT vertToReuseID[12];

					// Compute vertices needed for triangulation.
					for(USMALLINT currVert = 0; currVert < cellTriangulation.GetVertexCount(); currVert++)
					{
						// Grabs vertex data from look up table.
						USMALLINT vertData = GetRegularVertexData(classIndex, currVert);

						// Grabs endpoints from low byte of the 16-bits vertex data value.
						USMALLINT endpoint1 = vertData & 0x0F;
						USMALLINT endpoint2 = (vertData & 0xF0) >> 4;

						// Grabs vertex ID to re-use by passing the higher byte.
						UBIGINT tempID   = GetVertexIDToReuse(currSampPosX, currSampPosY, currSampPosZ, vertData >> 8);

						// store the ID.
						vertToReuseID[currVert] = tempID;

						// Compare with the last pairs in the hash table.
						// If vertex does not exist, create it.
						if(vertBuffer->find(tempID) == vertBuffer->end())
						{
							// compute vertex position
							Vector3 vertexPosOnEdge = computeVertexPosOnEdge(endpoint1, endpoint2, currSampPosX, currSampPosY, currSampPosZ, samples);

							// scale it.
							vertexPosOnEdge *= m_geoInfo.m_fVoxelScale;

							// Then, store it.
							vertBuffer->insert(pair<UBIGINT, VoxelVertex>(tempID, VoxelVertex(vertexPosOnEdge, Vector3(0.0f))));
						}
					} // end for

					// Compute indices for vertex triangulation.
					for(USMALLINT currInd = 0; currInd < (cellTriangulation.GetTriangleCount() * 3); currInd += 3)
					{
						// Grab vertices indices
						// As we compute normal per triangle, grab the three next indices.
						UBIGINT indice1 = vertToReuseID[cellTriangulation.IndiceBuffer(currInd)];
						UBIGINT indice2 = vertToReuseID[cellTriangulation.IndiceBuffer(currInd + 1)];
						UBIGINT indice3 = vertToReuseID[cellTriangulation.IndiceBuffer(currInd + 2)];

						// Compute normals per triangle thanks to vertex indices.
						VoxelVertex* tempVert1(&(*vertBuffer)[indice1]);
						VoxelVertex* tempVert2(&(*vertBuffer)[indice2]);
						VoxelVertex* tempVert3(&(*vertBuffer)[indice3]);

						// check if triangle vertices are the same one.
						if((tempVert1->m_vPosition == tempVert2->m_vPosition) || (tempVert1->m_vPosition == tempVert3->m_vPosition) || (tempVert2->m_vPosition == tempVert3->m_vPosition))
							continue;

						Vector3 fromVert2toVert1 = tempVert2->m_vPosition - tempVert1->m_vPosition;
						Vector3 fromVert3toVert1 = tempVert3->m_vPosition - tempVert1->m_vPosition;
						Vector3 toAdd = fromVert2toVert1.Vec3CrossProduct(fromVert3toVert1);
						tempVert1->m_vNormal += toAdd;
						tempVert2->m_vNormal += toAdd;
						tempVert3->m_vNormal += toAdd;

						// store indices in a temp structure allowing not knowing the final size.
						// will save often resize call in another less flexible structure.
						if(indBuffer)
						{
							// Store indices 
							indBuffer->push_back(indice1);
							indBuffer->push_back(indice2);
							indBuffer->push_back(indice3);
						}
					} // end for
				} // end if look up table case != 0 & 255.
			} // end for
		} // end for
	} // end for
}

VVOID			 OcelotVoxelGeometry::runTransitionVox(BIGINT fromX, BIGINT fromY, BIGINT fromZ, BIGINT toX, BIGINT toY, BIGINT toZ, std::map<UBIGINT, VoxelVertex>* vertBuffer, std::vector<UBIGINT>* indBuffer, BaseBlockMap* mainSamples, BaseBlockMap* tCellSamples, BIGINT plane, BBOOL invert)
{
	// The difference with the MC algo is that TCell generation 
	// is a 2D process, that is why the TCell block is one sample
	// thick, and thus that it is 1 cell thick. Lengyel calls these 
	// blocks simply face blocks.
	// Obviously, the axis on which the face block has to be 
	// computed will influence the process and that is why an
	// array containing sample block's offset for grabing the
	// samples is done.
	// Note: the ordering of the fullResSampleOffset array has been done 
	// for convenience while iterating through loops.

	// Grab the plan thanks to the face on which we would like to make
	// the TCell block.
	/*OcelotTCellPlan tCellPlan;
	if(face == VOXELBLOCK_FACE_NEG_Z || face == VOXELBLOCK_FACE_POS_Z)
		tCellPlan = OCELOT_TCELL_PLAN_XY;
	else if(face == VOXELBLOCK_FACE_NEG_Y || face == VOXELBLOCK_FACE_POS_Y)
		tCellPlan = OCELOT_TCELL_PLAN_XZ;
	else if(face == VOXELBLOCK_FACE_NEG_X || face == VOXELBLOCK_FACE_POS_X)
		tCellPlan = OCELOT_TCELL_PLAN_YZ;*/
	for(UBIGINT currSampPosZ = fromZ; currSampPosZ < (unsigned)toZ; currSampPosZ++)
	{
		for(UBIGINT currSampPosY = fromY; currSampPosY < (unsigned)toY; currSampPosY++)
		{
			for(UBIGINT currSampPosX = fromX; currSampPosX < (unsigned)toX; currSampPosX++)
			{
				// First scale sample position to a half resolution block
				// and reduce to start from NEGATIVE_SAMPLE_LAYER in the 
				// block sample map.
				currSampPosX *= 2; currSampPosY *= 2; currSampPosZ *= 2;
				currSampPosX -= 1; currSampPosY -= 1; currSampPosZ -= 1;

				// Compute the TCell classIndex for grabbing
				// the class, then the triangulation case for
				// the current TCell to process.
				OcelotVoxelTCell tempCell(GetSample(currSampPosX + fullResSampleOffset[plane][0][0], currSampPosY + fullResSampleOffset[plane][0][1], currSampPosZ + fullResSampleOffset[plane][0][2], tCellSamples), 
										  GetSample(currSampPosX + fullResSampleOffset[plane][1][0], currSampPosY + fullResSampleOffset[plane][1][1], currSampPosZ + fullResSampleOffset[plane][1][2], tCellSamples), 
										  GetSample(currSampPosX + fullResSampleOffset[plane][2][0], currSampPosY + fullResSampleOffset[plane][2][1], currSampPosZ + fullResSampleOffset[plane][2][2], tCellSamples), 
										  GetSample(currSampPosX + fullResSampleOffset[plane][3][0], currSampPosY + fullResSampleOffset[plane][3][1], currSampPosZ + fullResSampleOffset[plane][3][2], tCellSamples), 
										  GetSample(currSampPosX + fullResSampleOffset[plane][4][0], currSampPosY + fullResSampleOffset[plane][4][1], currSampPosZ + fullResSampleOffset[plane][4][2], tCellSamples),
										  GetSample(currSampPosX + fullResSampleOffset[plane][5][0], currSampPosY + fullResSampleOffset[plane][5][1], currSampPosZ + fullResSampleOffset[plane][5][2], tCellSamples), 
										  GetSample(currSampPosX + fullResSampleOffset[plane][6][0], currSampPosY + fullResSampleOffset[plane][6][1], currSampPosZ + fullResSampleOffset[plane][6][2], tCellSamples), 
										  GetSample(currSampPosX + fullResSampleOffset[plane][7][0], currSampPosY + fullResSampleOffset[plane][7][1], currSampPosZ + fullResSampleOffset[plane][7][2], tCellSamples), 
										  GetSample(currSampPosX + fullResSampleOffset[plane][8][0], currSampPosY + fullResSampleOffset[plane][8][1], currSampPosZ + fullResSampleOffset[plane][8][2], tCellSamples));

				// Automatic cell's function for generating class index.
				USMALLINT classIndex = tempCell.concatenateSamples();

				currSampPosX += 1; currSampPosY += 1; currSampPosZ += 1;
				currSampPosX /= 2; currSampPosY /= 2; currSampPosZ /= 2;

				// Check if non-trivial cases else no need to go further.
				if(classIndex != 0 && classIndex != 511)
				{
					// Now the Lengyel's Look-up tables enter in action.
					// Grabs the triangulation case index.
					UCCHAR triangulationCase = GetTransitionCellClass(classIndex);

					// the MSB of this new value above (triCase)
					// signify that the winding order has to be reverse.
					// and only the 7 lower bits will serve to map the fifth
					// look-up table providing the concrete triangulation.
					//UCCHAR reverseOrder = (triangulationCase & 0x80) >> 7; // keep the sign bit safe.
					UCCHAR caseIndex    = triangulationCase & 0x7F;

					// Get back the concrete triangulation corresponding to the case.
					const TCTriangulation cellTriangulation = GetTransitionCellData(caseIndex);

					// Keep a track of vertex to reuse IDs (up to 12: see Lookup tables).
					UBIGINT vertToReuseID[12];
					Vector3 currVertNormal(0.0f);

					// Compute vertices needed for triangulation.
					for(USMALLINT currVert = 0; currVert < cellTriangulation.GetVertexCount(); currVert++)
					{
						// Grabs vertex data from look up table.
						USMALLINT vertData = GetTransitionVertexData(classIndex, currVert);

						// At this moment, two different cases: either create new vertices
						// or re-use vertices from preceding TCells and two different part:
						// the half resolution part and the full resolution part with their 
						// mapping 8-bits codes for each edge.
						USMALLINT higherByteVertexInfo      = vertData >> 8;
						USMALLINT lowQuartetVertexIDToReuse = higherByteVertexInfo & 0x0F;

						// In the case where the lowerQuartet is equal to 8 or 9
						// meaning that we are on the half-resolution
						// face of the TCell, find the Vertex Index regarding to
						// plan.
						if((lowQuartetVertexIDToReuse == 9) || (lowQuartetVertexIDToReuse == 8))
						{
							// For each plan, find the temp vertex ID which will be added
							// to the final vertex ID for grabbing it regarding to the
							// sample block size.
							if(plane == 1)
								lowQuartetVertexIDToReuse -= 6; // will be equal to 2 or 3 respectively.
							else if(plane == 2)
							{
								// see code assigned to each edge of half resolution cell
								// page 44, Figure 4.18.
								if(lowQuartetVertexIDToReuse == 9)
									lowQuartetVertexIDToReuse = 1;
								else
									lowQuartetVertexIDToReuse = 2;
							}
							else if(plane == 0)
							{
								if(lowQuartetVertexIDToReuse == 9)
									lowQuartetVertexIDToReuse = 1;
								else
									lowQuartetVertexIDToReuse = 3;
							}

							// Cannot use the GetVertexIDToResuse() function because
							// of different plans which will change the computation.
							// Thus, grab the direction to follow for grabbing the
							// re-usable vertex ID.
							USMALLINT highQuartetDirToFollow    = (higherByteVertexInfo & 0xF0) >> 4;

							// Compute move to apply for reaching the vertex
							// Default plan XY, so 1 on Z.
							//USMALLINT moveOnX = highQuartetDirToFollow & 0x01;
							//USMALLINT moveOnY = (highQuartetDirToFollow & 0x02) >> 1;
							USMALLINT moveOnX = highQuartetDirToFollow%2;
							USMALLINT moveOnY = (highQuartetDirToFollow >> 1)%2;
							USMALLINT moveOnZ = 1;

							// Then, for two other plans.
							if(plane == 0)
							{
								// Plan YZ, so 1 on X.
								moveOnX = 1;
								//USMALLINT moveOnY = highQuartetDirToFollow & 0x01;
								//USMALLINT moveOnZ = (highQuartetDirToFollow & 0x02) >> 1;
								moveOnY = highQuartetDirToFollow%2;
								moveOnZ = (highQuartetDirToFollow >> 1)%2;
							}
							else if(plane == 2)
							{
								// moveOnX will be the same as default plan XY.
								// and here plan XZ, so Y equal 1.
								moveOnY = 1;
								//moveOnZ = (highQuartetDirToFollow & 0x02) >> 1;
								moveOnZ = (highQuartetDirToFollow >> 1)%2;
							}

							// Finally, grab the vertex ID.
							UBIGINT vertexID = GetVertexID(currSampPosX - moveOnX, currSampPosY - moveOnY, currSampPosZ - moveOnZ) + (lowQuartetVertexIDToReuse - 1);

							// Store the vertex ID for Indices generation.
							vertToReuseID[currVert] = vertexID;

							// And give as normal for TCell vertex the normal of vertexToReuse
							// which is already computed.
							std::map<UBIGINT, VoxelVertex>::iterator iter = vertBuffer->find(vertexID);
							if(iter != m_tableVerticesComputed.end())
								currVertNormal = iter->second.m_vNormal;
						}
					}
					for(USMALLINT currVert = 0; currVert < cellTriangulation.GetVertexCount(); currVert++)
					{
						// Grabs vertex data from look up table.
						USMALLINT vertData = GetTransitionVertexData(classIndex, currVert);

						// At this moment, two different cases: either create new vertices
						// or re-use vertices from preceding TCells and two different part:
						// the half resolution part and the full resolution part with their 
						// mapping 8-bits codes for each edge.
						USMALLINT higherByteVertexInfo      = vertData >> 8;
						USMALLINT lowQuartetVertexIDToReuse = higherByteVertexInfo & 0x0F;

						// If on the full-resolution face.
						if(!(lowQuartetVertexIDToReuse == 9 || lowQuartetVertexIDToReuse == 8))
						{
							// Cases where we are in inner corner, so create vertices.
							// Thus grab endpoints.
							USMALLINT endpoint1 = vertData & 0x0F;
							USMALLINT endpoint2 = (vertData & 0xF0) >> 4;

							// Compute the vertex position on edge.
							Vector3 currVertPos = computeTCellVertexPosOnEdge(endpoint1, endpoint2, currSampPosX, currSampPosY, currSampPosZ, fullResSampleOffset[plane], tCellSamples);

							// Scale to the cell scale.
							currVertPos *= m_geoInfo.m_fVoxelScale;

							// Prepare axis moves for grabbing Vertex ID regarding to the sample block map size.
							USMALLINT highQuartetDirToFollow = (higherByteVertexInfo & 0xF0) >> 4;

							// Compute move to apply for reaching the vertex
							// Default plan XY, so 1 on Z.
							//USMALLINT moveOnX = highQuartetDirToFollow & 0x01;
							//USMALLINT moveOnY = (highQuartetDirToFollow & 0x02) >> 1;
							USMALLINT moveOnX = highQuartetDirToFollow%2;
							USMALLINT moveOnY = (highQuartetDirToFollow >> 1)%2;
							USMALLINT moveOnZ = 1;

							// Then, for two other plans.
							if(plane == 0)
							{
								// Plan YZ, so 1 on X.
								moveOnX = 1;
								//USMALLINT moveOnY = highQuartetDirToFollow & 0x01;
								//USMALLINT moveOnZ = (highQuartetDirToFollow & 0x02) >> 1;
								moveOnY = highQuartetDirToFollow%2;
								moveOnZ = (highQuartetDirToFollow >> 1)%2;
							}
							else if(plane == 2)
							{
								// moveOnX will be the same as default plan XY.
								// and here plan XZ, so Y equal 1.
								moveOnY = 1;
								//moveOnZ = (highQuartetDirToFollow & 0x02) >> 1;
								moveOnZ = (highQuartetDirToFollow >> 1)%2;
							}

							// and grab the ID.
							UBIGINT vertexID = GetVertexID(currSampPosX - moveOnX, currSampPosY - moveOnY, currSampPosZ - moveOnZ) + (lowQuartetVertexIDToReuse - 1) + 20 * (plane + 1) * (m_geoInfo.m_iSizeX + 3) * (m_geoInfo.m_iSizeY + 3) * (m_geoInfo.m_iSizeZ + 3);

							// If case inner vertices with code 4, so vertex to create as well.
							// do not move on axis when grab ID.
							if(highQuartetDirToFollow == 4)
								vertexID = GetVertexID(currSampPosX, currSampPosY, currSampPosZ) + 200 * (plane + 1) * (m_geoInfo.m_iSizeX + 3) * (m_geoInfo.m_iSizeY + 3) * (m_geoInfo.m_iSizeZ + 3) + currVert;

							// Store new vertices as well as there IDs.
							vertBuffer->insert(pair<UBIGINT, VoxelVertex>(vertexID, VoxelVertex(currVertPos, currVertNormal)));
							vertToReuseID[currVert] = vertexID;

						} // end if
					} // end for

					// Compute indices for vertex triangulation.
					for(USMALLINT currInd = 0; currInd < (cellTriangulation.GetVertexCount() * 3); currInd += 3)
					{
						// Grab vertices indices
						// As we compute normal per triangle, grab the three next indices.
						UBIGINT indice1 = vertToReuseID[cellTriangulation.IndiceBuffer(currInd)];
						UBIGINT indice2 = vertToReuseID[cellTriangulation.IndiceBuffer(currInd + 1)];
						UBIGINT indice3 = vertToReuseID[cellTriangulation.IndiceBuffer(currInd + 2)];

						// If normal equal zero, compute it.
						if(currVertNormal == Vector3(0.0f))
						{
							// Compute normals per triangle thanks to vertex indices.
							VoxelVertex* tempVert1(&(*vertBuffer)[indice1]);
							VoxelVertex* tempVert2(&(*vertBuffer)[indice2]);
							VoxelVertex* tempVert3(&(*vertBuffer)[indice3]);

							// check if triangle vertices are the same one.
							if((tempVert1->m_vPosition == tempVert2->m_vPosition) || (tempVert1->m_vPosition == tempVert3->m_vPosition) || (tempVert2->m_vPosition == tempVert3->m_vPosition))
								continue;
							
							Vector3 fromVert2toVert1 = tempVert2->m_vPosition - tempVert1->m_vPosition;
							Vector3 fromVert3toVert1 = tempVert3->m_vPosition - tempVert1->m_vPosition;
							Vector3 toAdd = fromVert2toVert1.Vec3CrossProduct(fromVert3toVert1);
							tempVert1->m_vNormal += toAdd;
							tempVert2->m_vNormal += toAdd;
							tempVert3->m_vNormal += toAdd;

						} // end if

						// End up by storing indices, but if need to invert
						// triangulation, invert indices order.
						// Note: addition of a little trick for inverting the 
						// inversion process regarding to the face processed.
						USMALLINT lengyelBitInvert = 1;

						// Inverse reversion process.
						if(invert)
							lengyelBitInvert = 0;

						if(((USMALLINT)caseIndex >> 7)%2 == lengyelBitInvert)
						{
							// Force to keep normal triangulation
							indBuffer->push_back(indice1);
							indBuffer->push_back(indice2);
							indBuffer->push_back(indice3);
						}
						// else reverse.
						else
						{
							indBuffer->push_back(indice2);
							indBuffer->push_back(indice1);
							indBuffer->push_back(indice3);
						}
					} // end for
				} // end if look up table case != 0 & 511. 
			} // end for
		} // end for
	} // end for
}

VVOID			 OcelotVoxelGeometry::addPartToEdit(OcelotEditVoxel* toEdit)
{
	if(!toEdit)
		return;

	m_partsToEdit.push_back(toEdit);
}

VVOID			 OcelotVoxelGeometry::correctNormal(const BIGINT startPosX, const BIGINT startPosY, const BIGINT startPosZ, const BIGINT endPosX, const BIGINT endPosY, const BIGINT endPosZ)
{

	const BIGINT normalCorrection[6][6] = 
	{
		{ startPosX,   startPosY,   startPosZ,   endPosX,		endPosY,	   startPosZ + 1},
		{ startPosX,   startPosY,   endPosZ - 1, endPosX,		endPosY,	   endPosZ      },  
		{ startPosX,   startPosY,   startPosZ,   endPosX,		startPosY + 1, endPosZ      }, 
		{ startPosX,   endPosY - 1, startPosZ,   endPosX,       endPosY,       endPosZ      }, 
		{ startPosX,   startPosY,   startPosZ,   startPosX + 1, endPosY,       endPosZ      }, 
		{ endPosX - 1, startPosY,   startPosZ,   endPosX,		endPosY,       endPosZ      }
	};

	std::map<UBIGINT, VoxelVertex> tempBuffer;
	for(USMALLINT currFace = 0; currFace < 6; currFace++)
	{
		runMarchingCubes((UBIGINT)normalCorrection[currFace][0], (UBIGINT)normalCorrection[currFace][1], (UBIGINT)normalCorrection[currFace][2],
						 (UBIGINT)normalCorrection[currFace][3], (UBIGINT)normalCorrection[currFace][4], (UBIGINT)normalCorrection[currFace][5], 
						 &tempBuffer, 
						 NULL, 
						 &m_mainBlock);
	}

	// Correct normals by adding new one computed at the ones already in
	// the array.
	for(std::map<UBIGINT, VoxelVertex>::iterator tempIter = tempBuffer.begin(); tempIter != tempBuffer.end(); tempIter++)
	{
		std::map<UBIGINT, VoxelVertex>::iterator alreadyInNormalPtr = m_tableVerticesComputed.find(tempIter->first);
		if(alreadyInNormalPtr != m_tableVerticesComputed.end())
			alreadyInNormalPtr->second.m_vNormal += tempIter->second.m_vNormal;
	}
}

VVOID			 OcelotVoxelGeometry::buildFinalBuffer()
{
	// After having computed Vertices and Indices
	// for the entire block, we store everything
	// in proper voxelVertex and indices buffer
	// because the two buffers used for computation were
	// only for helping triangulation creation and management.
	UBIGINT tempVerticesCount = m_tableVerticesComputed.size();

	// Reset counters.
	m_iVerticesCount = 0;
	for(USMALLINT currIndBuf = 0; currIndBuf < MAX_BLOCK_INDICE_BUFFER; currIndBuf++)
		m_iIndicesCount[currIndBuf] = 0;

	// If no vertices, empty block, thus no triangulation.
	if(tempVerticesCount == 0)
		return;

	// map the final vertices buffer to the proper size.
	m_vVerticesBuffer = new VoxelVertex[tempVerticesCount];

	// Scale vertices to the proper scale and prepare the table of indices for indices final creation
	// according to final vertices present.
	std::map<UBIGINT, UBIGINT> tempTableIndices;
	for(std::map<UBIGINT, VoxelVertex>::iterator iter = m_tableVerticesComputed.begin(); iter != m_tableVerticesComputed.end(); iter++)
	{
		// Normalize the normal direction and scale the vertex position.
		VoxelVertex newVertex;
		newVertex.m_vNormal  = iter->second.m_vNormal;
		newVertex.m_vNormal.Vec3Normalise();
		newVertex.m_vPosition = iter->second.m_vPosition - Vector3(1.0f) * m_geoInfo.m_fVoxelScale;

		// Store the vertex in the final structure and set vertice's keys in the vertices same order.
		m_vVerticesBuffer[m_iVerticesCount] = newVertex;
		tempTableIndices.insert(pair<UBIGINT, UBIGINT>(iter->first, m_iVerticesCount));

		// Then, incremente the counter.
		m_iVerticesCount++;
	}

	// Second, map final indices buffers to the proper size and fill them
	// thanks to the tempTableIndices previously filled in the right order.
	for(USMALLINT currIndBuf = 0; currIndBuf < MAX_BLOCK_INDICE_BUFFER; currIndBuf++)
	{
		// Main block, then, first face's LOD block and so on.
		UBIGINT tempIndBufSize = (UBIGINT)m_iIndicesComputed[currIndBuf].size();

		if(m_iIndicesBuffer[currIndBuf])
			DeleteArray1D(m_iIndicesBuffer[currIndBuf]);

		m_iIndicesBuffer[currIndBuf] = new UBIGINT[tempIndBufSize];

		vector<UBIGINT>::const_iterator iter = m_iIndicesComputed[currIndBuf].begin();
		for( ; iter != m_iIndicesComputed[currIndBuf].end(); iter++)
		{
			UBIGINT newIndice = tempTableIndices.find((*iter))->second;
			m_iIndicesBuffer[currIndBuf][m_iIndicesCount[currIndBuf]] = newIndice;

			// Incremente current indices counter.
			m_iIndicesCount[currIndBuf]++;
		}
	}
	tempTableIndices.clear();

	// Finally, release temp structures used for triangulation creation.
	for(USMALLINT currArrInd = 0; currArrInd < MAX_BLOCK_INDICE_BUFFER; currArrInd++)
	{
		// Free entirely.
		m_iIndicesComputed[currArrInd].clear();
	}
	// Free entirely
	m_tableVerticesComputed.clear();
}

VVOID			 OcelotVoxelGeometry::releaseGeometry()
{
	if(m_vVerticesBuffer)
		DeleteArray1D(m_vVerticesBuffer);
	// The first one is the main block indices.
	// just keep indices of TCellBlock if any
	// for neighboring blocks boundaries.
	if(m_iIndicesBuffer[0])
		DeleteArray1D(m_iIndicesBuffer[0]);
}
