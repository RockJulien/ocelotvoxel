#include "OcelotVoxelBlock.h"

using namespace Ocelot;

OcelotVoxelBlock::OcelotVoxelBlock() : 
BaseBlockMap()
{

}

OcelotVoxelBlock::OcelotVoxelBlock(UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ, BIGINT offsetX, BIGINT offsetY, BIGINT offsetZ) :
BaseBlockMap(BaseBlockMapInfo(sizeX, sizeY, sizeZ, offsetX, offsetY, offsetZ))
{
	
}

OcelotVoxelBlock::~OcelotVoxelBlock()
{
	unmap();
}
