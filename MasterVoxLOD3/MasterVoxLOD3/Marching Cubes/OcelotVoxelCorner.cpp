#include "OcelotVoxelCorner.h"

using namespace Ocelot;

OcelotVoxelCorner::OcelotVoxelCorner() :
//m_cSample(0x00)
m_fSample(-1.0f)
{

}

/*OcelotVoxelCorner::OcelotVoxelCorner(CCHAR sample) :
m_cSample(sample)
{

}*/

OcelotVoxelCorner::OcelotVoxelCorner(FFLOAT sample) :
m_fSample(sample)
{

}

OcelotVoxelCorner::~OcelotVoxelCorner()
{

}

BBOOL OcelotVoxelCorner::isNegative() const
{
	//if(m_cSample < 0)
	if(m_fSample <= 0.0f)
		return true;

	return false;
}

BBOOL OcelotVoxelCorner::isPositive() const
{
	//if(m_cSample >= 0)
	if(m_fSample > 0.0f)
		return true;

	return false;
}
