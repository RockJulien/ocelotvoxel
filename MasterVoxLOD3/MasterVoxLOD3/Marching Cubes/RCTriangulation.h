#ifndef DEF_RCTRIANGULATION_H
#define DEF_RCTRIANGULATION_H

/************************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   RCTriangulation.h/cpp
/
/ Description: This class provides a Helper structure containing triangulation for 
/              one Regular cell regarding to one class of the Lengyel's modified 
/              Marching Cubes algorithm and returned by the Look-up table #2.
/
/ Information: Portions of the code are:
/              Copyright (C) 2010 Eric Lengyel's Modified Marching Cubes
/              Open Source Look-up Tables: http://www.terathon.com/voxels/
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        25/09/2012
/************************************************************************************/

#include "..\Utilities\ILookUpTriangulation.h"

namespace Ocelot
{
	// Helper structure containing triangulation for one Regular cell regarding to one 
	// class of the Lengyel's modified Marching Cubes algorithm and returned by the 
	// Look-up table #2.
	class RCTriangulation : public ILookUpTriangulation
	{
	private:

		/********************** Attributes ******************************/
		// Header 8-bits hexadecimal value containing 
		// Higher 4-bits which is the vertex count and
		// the lower 4-bits which is the triangle count.
		UCCHAR	m_ucGeometryCounts;

		// Indices buffer containing the triangle's 
		// triangulation and can contain up to five
		// triangles whence the size of 3*5 = 15.
		UCCHAR	m_ucIndiceBuffer[15];

	public:

		// Constructor(s) & Destructor
		RCTriangulation();
		RCTriangulation(UCCHAR geoCounts, UCCHAR ind1 = DEFINDICE, UCCHAR ind2 = DEFINDICE, UCCHAR ind3 = DEFINDICE, UCCHAR ind4 = DEFINDICE, UCCHAR ind5 = DEFINDICE, UCCHAR ind6 = DEFINDICE, UCCHAR ind7 = DEFINDICE, UCCHAR ind8 = DEFINDICE,
						UCCHAR ind9 = DEFINDICE, UCCHAR ind10 = DEFINDICE, UCCHAR ind11 = DEFINDICE, UCCHAR ind12 = DEFINDICE, UCCHAR ind13 = DEFINDICE, UCCHAR ind14 = DEFINDICE, UCCHAR ind15 = DEFINDICE);
		~RCTriangulation();

		// Methods
		USMALLINT GetVertexCount() const;
		USMALLINT GetTriangleCount() const;

		// Accessors
		inline const UCCHAR* IndiceBuffer() const { return m_ucIndiceBuffer;}
		inline UCCHAR        IndiceBuffer(USMALLINT indiceIndex) const { return m_ucIndiceBuffer[indiceIndex];}
	};
}

#endif