#ifndef DEF_OCELOTVOXELCELL_H
#define DEF_OCELOTVOXELCELL_H

#include "OcelotVoxelCorner.h"
#include "..\Maths\Utility.h"
#include "..\DataStructures\Array1D.h"
#include <vector>

namespace Ocelot
{
	class OcelotVoxelCell
	{
	private:

		// Attributes
		// Will contains 2*2*2 = 8 (CUBE) corner's 
		// sample values for building the first 
		// Look-up table index for iso-surface
		// extraction.
		std::vector<OcelotVoxelCorner> m_samples;

		// Iso-value.
		FFLOAT                     m_fIsoValue;

	public:

		// Constructor & Destructor
		OcelotVoxelCell();
		/*OcelotVoxelCell(CCHAR sample1, CCHAR sample2, CCHAR sample3, CCHAR sample4,
					    CCHAR sample5, CCHAR sample6, CCHAR sample7, CCHAR sample8, FFLOAT isoValue = 0.0f);*/
		OcelotVoxelCell(FFLOAT sample1, FFLOAT sample2, FFLOAT sample3, FFLOAT sample4,
					    FFLOAT sample5, FFLOAT sample6, FFLOAT sample7, FFLOAT sample8, FFLOAT isoValue = 0.0f);
		~OcelotVoxelCell();

		// Methods
		UCCHAR concatenateSamples();
		UCCHAR concatenateSamplesIsoValueSensitive();

		// Accessors
		inline std::vector<OcelotVoxelCorner> VoxelSamples() const { return m_samples;}
		inline FFLOAT                         IsoValue() const { return m_fIsoValue;}
		/*inline VVOID                       SetVoxelSamples(CCHAR sample1, CCHAR sample2, CCHAR sample3, CCHAR sample4,
														   CCHAR sample5, CCHAR sample6, CCHAR sample7, CCHAR sample8) { m_samples[0] = sample1; m_samples[1] = sample2; m_samples[2] = sample3; m_samples[3] = sample4;
																														 m_samples[4] = sample5; m_samples[5] = sample6; m_samples[6] = sample7; m_samples[7] = sample8;}
		inline VVOID                       SetVoxelSamples(CCHAR toChange, USMALLINT index) { m_samples[index] = toChange;}*/
		inline VVOID                          SetVoxelSamples(FFLOAT sample1, FFLOAT sample2, FFLOAT sample3, FFLOAT sample4,
														      FFLOAT sample5, FFLOAT sample6, FFLOAT sample7, FFLOAT sample8) { m_samples[0].SetSample(sample1); m_samples[1].SetSample(sample2); m_samples[2].SetSample(sample3); m_samples[3].SetSample(sample4);
																														        m_samples[4].SetSample(sample5); m_samples[5].SetSample(sample6); m_samples[6].SetSample(sample7); m_samples[7].SetSample(sample8);}
		inline VVOID                          SetVoxelSamples(FFLOAT toChange, USMALLINT index) { m_samples[index].SetSample(toChange);}
		inline VVOID                          SetIsoValue(FFLOAT isoVal) { m_fIsoValue = isoVal;}
	};
}

#endif