#ifndef DEF_MCTABLES_H
#define DEF_MCTABLES_H

/************************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   MCTables.h
/
/ Description: This file provide Look-up Tables for indices triangulation
/              of triangles inside regular cells as well as vertices reuse 
/              information according to the Lengyel's modified Marching Cubes.
/
/ Information: Portions of the code are:
/              Copyright (C) 2010 Eric Lengyel's Modified Marching Cubes
/              Open Source Look-up Tables: http://www.terathon.com/voxels/
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        25/09/2012
/************************************************************************************/

#include "RCTriangulation.h"

namespace Ocelot
{
	// C++ safe Lookup getters (Transferred in .cpp file cause of inline linkers problems).
	//inline UCCHAR                GetRegularCellClass(UCCHAR classIndex);
	//inline const RCTriangulation GetRegularCellData(UCCHAR caseIndex);
	//inline USMALLINT             GetRegularVertexData(UCCHAR classIndex, UCCHAR vertexLocIndex);
}

#endif
