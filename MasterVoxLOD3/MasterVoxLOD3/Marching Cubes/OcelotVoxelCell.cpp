#include "OcelotVoxelCell.h"

using namespace Ocelot;

OcelotVoxelCell::OcelotVoxelCell() :
//m_samples(8),     // Eight corners, one sampe per corner.
m_fIsoValue(0.0f) // Default Iso-value for symetrie.
{
	
}

/*OcelotVoxelCell::OcelotVoxelCell(CCHAR sample1, CCHAR sample2, CCHAR sample3, CCHAR sample4,
								 CCHAR sample5, CCHAR sample6, CCHAR sample7, CCHAR sample8, FFLOAT isoValue) :
m_samples(8), // Eight corners, one sampe per corner.
m_fIsoValue(isoValue)
{
	// Eight corners, one sample per corner.
	m_samples[0] = sample1;
	m_samples[1] = sample2;
	m_samples[2] = sample3;
	m_samples[3] = sample4;
	m_samples[4] = sample5;
	m_samples[5] = sample6;
	m_samples[6] = sample7;
	m_samples[7] = sample8;
}*/

OcelotVoxelCell::OcelotVoxelCell(FFLOAT sample1, FFLOAT sample2, FFLOAT sample3, FFLOAT sample4,
								 FFLOAT sample5, FFLOAT sample6, FFLOAT sample7, FFLOAT sample8, FFLOAT isoValue) :
//m_samples(8), // Eight corners, one sampe per corner.
m_fIsoValue(isoValue)
{
	m_samples.reserve(8);

	// Eight corners, one sample per corner.
	m_samples.push_back(OcelotVoxelCorner(sample1));
	m_samples.push_back(OcelotVoxelCorner(sample2));
	m_samples.push_back(OcelotVoxelCorner(sample3));
	m_samples.push_back(OcelotVoxelCorner(sample4));
	m_samples.push_back(OcelotVoxelCorner(sample5));
	m_samples.push_back(OcelotVoxelCorner(sample6));
	m_samples.push_back(OcelotVoxelCorner(sample7));
	m_samples.push_back(OcelotVoxelCorner(sample8));
}

OcelotVoxelCell::~OcelotVoxelCell()
{
	m_samples.clear();
}

UCCHAR OcelotVoxelCell::concatenateSamples()
{
	UCCHAR lookUpIndex = 0;

	// Perform a signed right shift on each corner's sample
	// to fill the lookUp index with copies of each sign bit
	/*lookUpIndex = ((m_samples[0].Sample() >> 7) & 0x01) |
				  ((m_samples[1].Sample() >> 6) & 0x02) |
				  ((m_samples[2].Sample() >> 5) & 0x04) |
				  ((m_samples[3].Sample() >> 4) & 0x08) |
				  ((m_samples[4].Sample() >> 3) & 0x10) |
				  ((m_samples[5].Sample() >> 2) & 0x20) |
				  ((m_samples[6].Sample() >> 1) & 0x40) |
				  ( m_samples[7].Sample()       & 0x80);*/

	if(m_samples[0].Sample() < 0.0f)
		lookUpIndex |= 0x01;
	if(m_samples[1].Sample() < 0.0f)
		lookUpIndex |= 0x02;
	if(m_samples[2].Sample() < 0.0f)
		lookUpIndex |= 0x04;
	if(m_samples[3].Sample() < 0.0f)
		lookUpIndex |= 0x08;
	if(m_samples[4].Sample() < 0.0f)
		lookUpIndex |= 0x10;
	if(m_samples[5].Sample() < 0.0f)
		lookUpIndex |= 0x20;
	if(m_samples[6].Sample() < 0.0f)
		lookUpIndex |= 0x40;
	if(m_samples[7].Sample() < 0.0f)
		lookUpIndex |= 0x80;

	return lookUpIndex;
}

UCCHAR OcelotVoxelCell::concatenateSamplesIsoValueSensitive()
{
	UCCHAR lookUpIndex = 0;

	// Perform a signed right shift on each corner's sample
	// to fill the lookUp index with copies of each sign bit
	// and take into account the iso-value given as class parameter.
	// If sample is smaller than the iso-value, ORing it else
	// skip it.
	// Note: if the iso-value is 0.0f, the function will be
	// equivalent to the non-iso-value sensitive one.
	/*if(m_samples[0].Sample() < m_fIsoValue)
		lookUpIndex  = ((m_samples[0].Sample() >> 7) & 0x01);
	if(m_samples[1].Sample() < m_fIsoValue)
		lookUpIndex |= ((m_samples[1].Sample() >> 6) & 0x02);
	if(m_samples[2].Sample() < m_fIsoValue)
		lookUpIndex |= ((m_samples[2].Sample() >> 5) & 0x04);
	if(m_samples[3].Sample() < m_fIsoValue)
		lookUpIndex |= ((m_samples[3].Sample() >> 4) & 0x08);
	if(m_samples[4].Sample() < m_fIsoValue)
		lookUpIndex |= ((m_samples[4].Sample() >> 3) & 0x10);
	if(m_samples[5].Sample() < m_fIsoValue)
		lookUpIndex |= ((m_samples[5].Sample() >> 2) & 0x20);
	if(m_samples[6].Sample() < m_fIsoValue)
		lookUpIndex |= ((m_samples[6].Sample() >> 1) & 0x40);
	if(m_samples[7].Sample() < m_fIsoValue)
		lookUpIndex |= ( m_samples[7].Sample()       & 0x80);*/

	if(m_samples[0].Sample() < m_fIsoValue)
		lookUpIndex |= 0x01;
	if(m_samples[1].Sample() < m_fIsoValue)
		lookUpIndex |= 0x02;
	if(m_samples[2].Sample() < m_fIsoValue)
		lookUpIndex |= 0x04;
	if(m_samples[3].Sample() < m_fIsoValue)
		lookUpIndex |= 0x08;
	if(m_samples[4].Sample() < m_fIsoValue)
		lookUpIndex |= 0x10;
	if(m_samples[5].Sample() < m_fIsoValue)
		lookUpIndex |= 0x20;
	if(m_samples[6].Sample() < m_fIsoValue)
		lookUpIndex |= 0x40;
	if(m_samples[7].Sample() < m_fIsoValue)
		lookUpIndex |= 0x80;

	return lookUpIndex;
}
