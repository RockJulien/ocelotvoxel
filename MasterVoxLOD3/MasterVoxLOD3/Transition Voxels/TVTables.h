#ifndef DEF_TVTABLES_H
#define DEF_TVTABLES_H

/************************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   TVTables.h
/
/ Description: This file provide Look-up Tables for indices triangulation
/              of triangles inside transition cells as well as vertices reuse 
/              information according to the Lengyel's LOD Transvoxel algorithm.
/
/ Information: Portions of the code are:
/              Copyright (C) 2010 Eric Lengyel's Transvoxel Algorithm.
/              Open Source Look-up Tables: http://www.terathon.com/voxels/
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        25/09/2012
/************************************************************************************/

#include "TCTriangulation.h"

namespace Ocelot
{
	// C++ safe Lookup getters
	//inline UCCHAR                GetTransitionCellClass(USMALLINT classIndex);
	//inline const TCTriangulation GetTransitionCellData(UCCHAR caseIndex);
	//inline UCCHAR                GetTransitionCornerData(UCCHAR cornerIndex);
	//inline USMALLINT             GetTransitionVertexData(USMALLINT classIndex, UCCHAR vertexLocIndex);
}

#endif
