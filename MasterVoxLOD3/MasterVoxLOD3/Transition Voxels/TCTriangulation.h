#ifndef DEF_TCTRIANGULATION_H
#define DEF_TCTRIANGULATION_H

#include "..\Utilities\ILookUpTriangulation.h"

// Helper structure containing triangulation for one Transition cell regarding 
// to one class of the Lengyel's Transvoxel algorithm and returned by the 
// Look-up table #5.
class TCTriangulation : public ILookUpTriangulation
{
private:

	/********************** Attributes ******************************/
	// Header 8-bits hexadecimal value containing 
	// Higher 4-bits which is the vertex count and
	// the lower 4-bits which is the triangle count.
    UCCHAR m_ucGeometryCounts;

	// Indices buffer containing the triangle's 
	// triangulation and can contain up to twelve
	// triangles whence the size of 3*12 = 36.
    UCCHAR m_ucIndiceBuffer[36];

public:

	// Constructor(s) & Destructor
	TCTriangulation();
	TCTriangulation(UCCHAR geoCounts, UCCHAR ind1 = DEFINDICE, UCCHAR ind2 = DEFINDICE, UCCHAR ind3 = DEFINDICE, UCCHAR ind4 = DEFINDICE, UCCHAR ind5 = DEFINDICE, UCCHAR ind6 = DEFINDICE, UCCHAR ind7 = DEFINDICE,
			        UCCHAR ind8  = DEFINDICE, UCCHAR ind9  = DEFINDICE, UCCHAR ind10 = DEFINDICE, UCCHAR ind11 = DEFINDICE, UCCHAR ind12 = DEFINDICE, UCCHAR ind13 = DEFINDICE, UCCHAR ind14 = DEFINDICE, UCCHAR ind15 = DEFINDICE,
					UCCHAR ind16 = DEFINDICE, UCCHAR ind17 = DEFINDICE, UCCHAR ind18 = DEFINDICE, UCCHAR ind19 = DEFINDICE, UCCHAR ind20 = DEFINDICE, UCCHAR ind21 = DEFINDICE, UCCHAR ind22 = DEFINDICE, UCCHAR ind23 = DEFINDICE,
					UCCHAR ind24 = DEFINDICE, UCCHAR ind25 = DEFINDICE, UCCHAR ind26 = DEFINDICE, UCCHAR ind27 = DEFINDICE, UCCHAR ind28 = DEFINDICE, UCCHAR ind29 = DEFINDICE, UCCHAR ind30 = DEFINDICE, UCCHAR ind31 = DEFINDICE,
					UCCHAR ind32 = DEFINDICE, UCCHAR ind33 = DEFINDICE, UCCHAR ind34 = DEFINDICE, UCCHAR ind35 = DEFINDICE, UCCHAR ind36 = DEFINDICE);
	~TCTriangulation();

	// Methods
	USMALLINT GetVertexCount() const;
	USMALLINT GetTriangleCount() const;

	// Accessors
	inline const UCCHAR* IndiceBuffer() const { return m_ucIndiceBuffer;}
	inline UCCHAR        IndiceBuffer(USMALLINT indiceIndex) const { return m_ucIndiceBuffer[indiceIndex];}
};

#endif