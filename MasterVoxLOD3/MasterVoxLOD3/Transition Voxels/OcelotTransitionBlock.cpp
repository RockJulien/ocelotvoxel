#include "OcelotTransitionBlock.h"

using namespace Ocelot;

OcelotTransitionBlock::OcelotTransitionBlock() : 
BaseBlockMap()
{

}

OcelotTransitionBlock::OcelotTransitionBlock(UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ, BIGINT offsetX, BIGINT offsetY, BIGINT offsetZ) :
BaseBlockMap(BaseBlockMapInfo(sizeX, sizeY, sizeZ, offsetX, offsetY, offsetZ))
{

}

OcelotTransitionBlock::~OcelotTransitionBlock()
{
	unmap();
}
