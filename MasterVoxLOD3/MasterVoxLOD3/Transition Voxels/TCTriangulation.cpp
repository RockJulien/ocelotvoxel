#include "TCTriangulation.h"

TCTriangulation::TCTriangulation() :
m_ucGeometryCounts(0x00)
{
	for(USMALLINT i = 0; i < 36; i++)
		m_ucIndiceBuffer[i] = DEFINDICE;
}

TCTriangulation::TCTriangulation(UCCHAR geoCounts, UCCHAR ind1, UCCHAR ind2, UCCHAR ind3, UCCHAR ind4, UCCHAR ind5, UCCHAR ind6, UCCHAR ind7,
								 UCCHAR ind8, UCCHAR ind9, UCCHAR ind10, UCCHAR ind11, UCCHAR ind12, UCCHAR ind13, UCCHAR ind14, UCCHAR ind15,
								 UCCHAR ind16, UCCHAR ind17, UCCHAR ind18, UCCHAR ind19, UCCHAR ind20, UCCHAR ind21, UCCHAR ind22, UCCHAR ind23,
								 UCCHAR ind24, UCCHAR ind25, UCCHAR ind26, UCCHAR ind27, UCCHAR ind28, UCCHAR ind29, UCCHAR ind30, UCCHAR ind31,
								 UCCHAR ind32, UCCHAR ind33, UCCHAR ind34, UCCHAR ind35, UCCHAR ind36)
{
	m_ucIndiceBuffer[0]  = ind1;  m_ucIndiceBuffer[1]   = ind2;  m_ucIndiceBuffer[2]   = ind3;     // up to 12 triangles.
	m_ucIndiceBuffer[3]  = ind4;  m_ucIndiceBuffer[4]   = ind5;  m_ucIndiceBuffer[5]   = ind6; 
	m_ucIndiceBuffer[6]  = ind7;  m_ucIndiceBuffer[7]   = ind8;  m_ucIndiceBuffer[8]   = ind9; 
	m_ucIndiceBuffer[9]  = ind10; m_ucIndiceBuffer[10]  = ind11; m_ucIndiceBuffer[11]  = ind12; 
	m_ucIndiceBuffer[12] = ind13; m_ucIndiceBuffer[13]  = ind14; m_ucIndiceBuffer[14]  = ind15;
	m_ucIndiceBuffer[15] = ind16; m_ucIndiceBuffer[16]  = ind17; m_ucIndiceBuffer[17]  = ind18;
	m_ucIndiceBuffer[18] = ind19; m_ucIndiceBuffer[19]  = ind20; m_ucIndiceBuffer[20]  = ind21; 
	m_ucIndiceBuffer[21] = ind22; m_ucIndiceBuffer[22]  = ind23; m_ucIndiceBuffer[23]  = ind24; 
	m_ucIndiceBuffer[24] = ind25; m_ucIndiceBuffer[25]  = ind26; m_ucIndiceBuffer[26]  = ind27; 
	m_ucIndiceBuffer[27] = ind28; m_ucIndiceBuffer[28]  = ind29; m_ucIndiceBuffer[29]  = ind30;
	m_ucIndiceBuffer[30] = ind31; m_ucIndiceBuffer[31]  = ind32; m_ucIndiceBuffer[32]  = ind33;
	m_ucIndiceBuffer[33] = ind34; m_ucIndiceBuffer[34]  = ind35; m_ucIndiceBuffer[35]  = ind36;
}

TCTriangulation::~TCTriangulation()
{
	
}

USMALLINT TCTriangulation::GetVertexCount() const
{
	// shift of 4 bits on the right for
	// overwriting the lower 4-bits by the
	// higher 4-bits (new higher 4-bits will
	// be 0000.
    return (m_ucGeometryCounts >> 4);
}

USMALLINT TCTriangulation::GetTriangleCount() const
{
	// mask applied on the higher 4-bits
	// for overwriting them and having 
	// no more than the original lower 
	// 4-bits.
    return (m_ucGeometryCounts & 0x0F);
}
