#ifndef DEF_OCELOTTRANSITIONBLOCK_H
#define DEF_OCELOTTRANSITIONBLOCK_H

#include "..\Utilities\BaseBlockMap.h"

namespace Ocelot
{
	class OcelotTransitionBlock : public BaseBlockMap
	{
	protected:

			// Attributes

	public:

			// Constructor & Destructor
			OcelotTransitionBlock();
			OcelotTransitionBlock(UBIGINT sizeX, UBIGINT sizeY, UBIGINT sizeZ, BIGINT offsetX, BIGINT offsetY, BIGINT offsetZ);
	virtual ~OcelotTransitionBlock();

			// Methods


			// Accessors

	};
}

#endif