#include "OcelotVoxelTCell.h"

using namespace Ocelot;

OcelotVoxelTCell::OcelotVoxelTCell() :
//m_samples(9),     // Eight corners, one sampe per corner.
m_fIsoValue(0.0f) // Default Iso-value for symetrie.
{

}

/*OcelotVoxelTCell::OcelotVoxelTCell(CCHAR sample1, CCHAR sample2, CCHAR sample3, CCHAR sample4, CCHAR sample5, 
					CCHAR sample6, CCHAR sample7, CCHAR sample8, CCHAR sample9, FFLOAT isoValue) :
m_samples(9),         // Eight corners, one sampe per corner.
m_fIsoValue(isoValue) // Default Iso-value for symetrie.
{
	// Nine samples, one sample per corner.
	m_samples[0] = sample1;
	m_samples[1] = sample2;
	m_samples[2] = sample3;
	m_samples[3] = sample4;
	m_samples[4] = sample5;
	m_samples[5] = sample6;
	m_samples[6] = sample7;
	m_samples[7] = sample8;
	m_samples[8] = sample9;
}*/

OcelotVoxelTCell::OcelotVoxelTCell(FFLOAT sample1, FFLOAT sample2, FFLOAT sample3, FFLOAT sample4, FFLOAT sample5, 
						           FFLOAT sample6, FFLOAT sample7, FFLOAT sample8, FFLOAT sample9, FFLOAT isoValue) :
//m_samples(9),         // Eight corners, one sampe per corner.
m_fIsoValue(isoValue) // Default Iso-value for symetrie.
{
	m_samples.reserve(9);

	// Nine samples, one sample per corner.
	m_samples.push_back(OcelotVoxelCorner(sample1));
	m_samples.push_back(OcelotVoxelCorner(sample2));
	m_samples.push_back(OcelotVoxelCorner(sample3));
	m_samples.push_back(OcelotVoxelCorner(sample4));
	m_samples.push_back(OcelotVoxelCorner(sample5));
	m_samples.push_back(OcelotVoxelCorner(sample6));
	m_samples.push_back(OcelotVoxelCorner(sample7));
	m_samples.push_back(OcelotVoxelCorner(sample8));
	m_samples.push_back(OcelotVoxelCorner(sample9));
}

OcelotVoxelTCell::~OcelotVoxelTCell()
{
	m_samples.clear();
}

USMALLINT OcelotVoxelTCell::concatenateSamples()
{
	USMALLINT lookUpIndex = 0;

	// If sample'value is negative, add Lengyel weight
	// intended for this particular sample for classIndex        <-- <-- <--
	// creation.												 |  	   ^
	// Note that the growing order of hexadecimal weights        v         |
	// to add follow the same ordering than the array            --> -->   ^
	// of sample offset showing that there is a particular                 |
	// direction to follow each time we deal with TCell.         --> --> -->
	// Kind of spiral.
	// (Adding all weights together is equal to 511).
	/*const USMALLINT cornerWeight0 = 0x0001;
	const USMALLINT cornerWeight1 = 0x0002;
	const USMALLINT cornerWeight2 = 0x0004;
	const USMALLINT cornerWeight3 = 0x0008;
	const USMALLINT cornerWeight4 = 0x0010;
	const USMALLINT cornerWeight5 = 0x0020;
	const USMALLINT cornerWeight6 = 0x0040;
	const USMALLINT cornerWeight7 = 0x0080;
	const USMALLINT cornerWeight8 = 0x0100;

	// Note that the array will contain
	// samples in the spiral order described
	// above.
	if(m_samples[0].isNegative())
		lookUpIndex += cornerWeight0;
	if(m_samples[1].isNegative())
		lookUpIndex += cornerWeight1;
	if(m_samples[2].isNegative())
		lookUpIndex += cornerWeight2;
	if(m_samples[3].isNegative())
		lookUpIndex += cornerWeight3;
	if(m_samples[4].isNegative())
		lookUpIndex += cornerWeight4;
	if(m_samples[5].isNegative())
		lookUpIndex += cornerWeight5;
	if(m_samples[6].isNegative())
		lookUpIndex += cornerWeight6;
	if(m_samples[7].isNegative())
		lookUpIndex += cornerWeight7;
	if(m_samples[8].isNegative())
		lookUpIndex += cornerWeight8;*/

	USMALLINT weightToAdd = 1;
	for(USMALLINT currSample = 0; currSample < 9; currSample++)
	{
		if(m_samples[currSample].Sample() < 0.0f)
			lookUpIndex |= weightToAdd;
		// Increase the weight for the next sample.
		weightToAdd *= 2;
	}

	return lookUpIndex;
}
