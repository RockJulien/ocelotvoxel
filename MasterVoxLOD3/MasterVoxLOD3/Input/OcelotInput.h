#ifndef DEF_OCELOTINPUT_H
#define DEF_OCELOTINPUT_H

#include "OcelotInputDevice.h"

namespace Ocelot
{
	class OcelotInput
	{
	private:

			// Attributes
			OcelotInputDevice* m_pDevice;
			HINSTANCE          m_hInst;
			HMODULE            m_hDllMod;

	public:

			// Constructor & Destructor
			OcelotInput(HINSTANCE hInst);
			~OcelotInput();

			// Methods
			HRESULT createDevice();
			void    release();

			// Accessors
	inline  OcelotInputDevice* getDevice() const { return m_pDevice;}
	inline  HINSTANCE          getModule() const { return m_hDllMod;}
	};
}

#endif