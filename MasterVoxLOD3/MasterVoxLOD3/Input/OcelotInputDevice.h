#ifndef DEF_OCELOTINPUTDEVICE_H
#define DEF_OCELOTINPUTDEVICE_H

#include "OcelotInputUtil.h"

namespace Ocelot
{
	class OcelotInputDevice
	{
	protected:

			// Attributes
			HWND      m_hWndMain;
			HINSTANCE m_hDLL;
			bool      m_bRunning;
			FILE*     m_pLog;

	public:

			// Constructor & Destructor
			OcelotInputDevice(){}
	virtual ~OcelotInputDevice(){}

			// Methods
	virtual INT  initialize(HWND, const RECT*, bool) = 0;
	virtual INT  update()                            = 0;
	virtual INT  positionOnScreen(InputType, POINT*) = 0;   // works with mouse and joystick only
	virtual void release()                           = 0;
	virtual bool isRunning()                         = 0;
	virtual bool hasJoystick(char*)                  = 0;
	virtual bool isPressed(InputType, UINT)          = 0;   // for keyboard, mouse and joystick
	virtual bool isReleased(InputType, UINT)         = 0;   // for keyboard, mouse and joystick
	virtual bool isPressedAndLocked(UINT)            = 0; // only for keyboard.

			// Accessors

	};

// definition of exported functions for DLL.
extern "C"
{
	HRESULT createInputDevice(HINSTANCE hDll, OcelotInputDevice** pInterface);
	typedef HRESULT (*CREATEINPUTDEVICE)(HINSTANCE hDll, OcelotInputDevice** pInterface);

	HRESULT releaseInputDevice(OcelotInputDevice** pInterface);
	typedef HRESULT (*RELEASEINPUTDEVICE)(OcelotInputDevice** pInterface);
}

// definition of the ocelot own key names style.
// EXA extracted from dinput.h
#define OCELOT_ESC          0x01
#define OCELOT_TAB          0x0F
#define OCELOT_SPACE        0x39
#define OCELOT_RETURN       0x1C
#define OCELOT_BACK         0x0E
#define OCELOT_CAPITAL      0x3A

#define OCELOT_MINUS        0x0C
#define OCELOT_EQUAL        0x0D
#define OCELOT_LBRACKET     0x1A
#define OCELOT_RBRACKET     0x1B
#define OCELOT_SEMICOLON    0x27
#define OCELOT_APOSTROPHE   0x28
#define OCELOT_GRAVE        0x29
#define OCELOT_BACKSLASH    0x2B
#define OCELOT_COMMA        0x33
#define OCELOT_PERIOD       0x34
#define OCELOT_SLASH        0x35

// CHARACTER KEYS
#define OCELOT_A            0x1E
#define OCELOT_B            0x30
#define OCELOT_C            0x2E
#define OCELOT_D            0x20
#define OCELOT_E            0x12
#define OCELOT_F            0x21
#define OCELOT_G            0x22
#define OCELOT_H            0x23
#define OCELOT_I            0x17
#define OCELOT_J            0x24
#define OCELOT_K            0x25
#define OCELOT_L            0x26
#define OCELOT_M            0x32
#define OCELOT_N            0x31
#define OCELOT_O            0x18
#define OCELOT_P            0x19
#define OCELOT_Q            0x10
#define OCELOT_R            0x13
#define OCELOT_S            0x1F
#define OCELOT_T            0x14
#define OCELOT_U            0x16
#define OCELOT_V            0x2F
#define OCELOT_W            0x11
#define OCELOT_X            0x2D
#define OCELOT_Y            0x15
#define OCELOT_Z            0x2C

// MAIN NUM KEYS
#define OCELOT_0            0x0B
#define OCELOT_1            0x02
#define OCELOT_2            0x03
#define OCELOT_3            0x04
#define OCELOT_4            0x05
#define OCELOT_5            0x06
#define OCELOT_6            0x07
#define OCELOT_7            0x08
#define OCELOT_8            0x09
#define OCELOT_9            0x0A

// FUNCTION KEYS
#define OCELOT_F1           0x3B
#define OCELOT_F2           0x3C
#define OCELOT_F3           0x3D
#define OCELOT_F4           0x3E
#define OCELOT_F5           0x3F
#define OCELOT_F6           0x40
#define OCELOT_F7           0x41
#define OCELOT_F8           0x42
#define OCELOT_F9           0x43
#define OCELOT_F10          0x44
#define OCELOT_F11          0x57
#define OCELOT_F12          0x58

// ON NUMPAD
#define OCELOT_NPPLUS       0x4E
#define OCELOT_NPMINUS      0x4A
#define OCELOT_NPDIV        0xB5
#define OCELOT_NPMUL        0x37
#define OCELOT_NPDECIMAL    0x53
#define OCELOT_NPCOMMA      0xB3
#define OCELOT_NPENTER      0x9C
#define OCELOT_NUMLOCK      0x45
#define OCELOT_NP0          0x52
#define OCELOT_NP1          0x4F
#define OCELOT_NP2          0x50
#define OCELOT_NP3          0x51
#define OCELOT_NP4          0x4B
#define OCELOT_NP5          0x4C
#define OCELOT_NP6          0x4D
#define OCELOT_NP7          0x47
#define OCELOT_NP8          0x48
#define OCELOT_NP9          0x49

// CTRL, ALT, SHFT and WINDWS
#define OCELOT_RSHIFT       0x36
#define OCELOT_LSHIFT       0x2A
#define OCELOT_RCTRL        0x9D
#define OCELOT_LCTRL        0x1D
#define OCELOT_RALT         0xB8
#define OCELOT_LALT         0x38
#define OCELOT_LWIN         0xDB
#define OCELOT_RWIN         0xDC

// ON ARROW KEYPAD
#define OCELOT_UP           0xC8
#define OCELOT_DOWN         0xD0
#define OCELOT_LEFT         0xCB
#define OCELOT_RIGHT        0xCD
#define OCELOT_INSERT       0xD2
#define OCELOT_DELETE       0xD3
#define OCELOT_HOME         0xC7
#define OCELOT_END          0xCF
#define OCELOT_PGDOWN       0xD1
#define OCELOT_PGUP         0xC9

#define OCELOT_PAUSE        0xC5    /* Pause */
#define OCELOT_SCROLL       0x46    /* Scroll Lock */
}

#endif