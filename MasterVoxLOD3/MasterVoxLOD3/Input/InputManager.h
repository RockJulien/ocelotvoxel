#ifndef DEF_INPUTMANAGER_H
#define DEF_INPUTMANAGER_H

#include "OcelotInput.h"
#include "..\DirectXAPI\DXUtility.h"

// include the input lib file
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "OcelotInput.lib")
#else
#pragma comment(lib, "OcelotInputR.lib")
#endif // _DEBUG

#define InMng InputManager::instance()

namespace Ocelot
{
						struct InfoInput
						{
							HWND      hWnd;
							HINSTANCE hInst;
							UINT      widthWindow;
							UINT      heightWindow;
						};

	class InputManager
	{
	private:

			// Attributes
			// input device
			OcelotInput*       m_pInput;
			OcelotInputDevice* m_pDevice;

			// Constructor
			InputManager(){}

			// cpy cst & assgmt
			InputManager(const InputManager&);
			InputManager& operator = (const InputManager&);

	public:

			// Destructor
			~InputManager();

			// Singleton
	static  InputManager* instance();

			// Methods
			void initialize(InfoInput inf);
			void update();
			bool isPressed(InputType type, UINT keyID);
			bool isReleased(InputType type, UINT keyID);
			bool isPressedAndLocked(UINT keyID);
			void release();

			// Accessors
	inline  OcelotInputDevice* Device() const { return m_pDevice;}
	};
}

#endif