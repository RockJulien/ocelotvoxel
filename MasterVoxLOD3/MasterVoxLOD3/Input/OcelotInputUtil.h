#ifndef DEF_OCELOTINPUTUTIL_H
#define DEF_OCELOTINPUTUTIL_H

#include <Windows.h>
#include <iostream>

// definition of some useful macro
#define DeletePointer(X) {if(X){delete X; X = NULL;}}
#define ReleaseID(X) {if(X){ X->Unacquire(); X->Release(); X = NULL;}}

namespace Ocelot
{
	enum InputType
	{
		Mouse,
		Keyboard,
		Joystick
	};
}

#endif