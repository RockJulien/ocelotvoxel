#include "DXUtility.h"

void FormatOutput(LPCWSTR formatstring, ...)
{
	int nSize = 0;
	wchar_t buff[BUFFCOUNT];
	memset(buff, 0, sizeof(buff));
	va_list args;
	va_start(args, formatstring);
	nSize = _vsnwprintf_s(buff, BUFFCOUNT - 1, BUFFCOUNT, formatstring, args);
	wprintf(L"%ls\n", buff);
}