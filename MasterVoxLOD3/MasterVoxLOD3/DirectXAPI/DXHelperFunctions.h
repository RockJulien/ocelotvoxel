#ifndef DEF_DXHELPERFUNCTIONS_H
#define DEF_DXHELPERFUNCTIONS_H

#include "..\Geometry\OcelotDXMaterial.h"
#include "..\DataStructures\DataStructureUtil.h"
#include "..\StringHelper\STRINGHelpers.cpp"

namespace Ocelot
{
	// load a 2D texture as a DirectX resource view texture.
	inline ID3D10ShaderResourceView* loadTexture(ID3D10Device* device, WSTRING texturePath)
	{
		ID3D10ShaderResourceView* newOne = NULL;

		if(texturePath.size() != 0)
		{
			STRING charPath;
			toAString(texturePath, charPath);

			// create it and store it.
			ID3D10Texture2D* texture2D = ResMng::getTexture2DFromFile(device, charPath.c_str());

			if(texture2D != NULL)
			{
				D3D10_TEXTURE2D_DESC desc;
				texture2D->GetDesc(&desc);

				D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
				ZeroMemory(&SRVDesc, sizeof(SRVDesc));

				SRVDesc.Format				= desc.Format;
				SRVDesc.ViewDimension       = D3D10_SRV_DIMENSION_TEXTURE2D;
				SRVDesc.Texture2D.MipLevels = desc.MipLevels;

				HR(device->CreateShaderResourceView(texture2D, &SRVDesc, &newOne));

				// release 2DTexture
				texture2D->Release();
			}
		}

		return newOne;
	}

	inline ID3D10ShaderResourceView* loadRV(ID3D10Device* device, WSTRING texturePath)
	{
		ID3D10ShaderResourceView* newOne = NULL;

		D3DX10CreateShaderResourceViewFromFile(device, texturePath.c_str(), 0, 0, &newOne, NULL);

		return newOne;
	}

	// load a 2D texture as a DirectX cube map texture.
	inline ID3D10ShaderResourceView* loadCubeMap(ID3D10Device* device, WSTRING texturePath)
	{
		ID3D10ShaderResourceView* newOne = NULL;

		if(texturePath.size() != 0)
		{
			// create it and store it.
			// Particular way to load the texture for a cube map!!!
			// just set the flag to D3D10_RESOURCE_MISC_TEXTURECUBE.
			D3DX10_IMAGE_LOAD_INFO Info;
			Info.MiscFlags = D3D10_RESOURCE_MISC_TEXTURECUBE;

			ID3D10Texture2D* C_M = 0;
			HR(D3DX10CreateTextureFromFile(device, 
										   texturePath.c_str(), 
										   &Info, 
										   NULL, 
										   (ID3D10Resource**)&C_M, 
										   NULL));

			if(C_M != NULL)
			{
				D3D10_TEXTURE2D_DESC desc;
				C_M->GetDesc(&desc);

				D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
				ZeroMemory(&SRVDesc, sizeof(SRVDesc));

				SRVDesc.Format				= desc.Format;
				SRVDesc.ViewDimension       = D3D10_SRV_DIMENSION_TEXTURECUBE;
				SRVDesc.Texture2D.MipLevels = desc.MipLevels;

				HR(device->CreateShaderResourceView(C_M, &SRVDesc, &newOne));

				// release 2DTexture
				C_M->Release();
			}
		}

		return newOne;
	}

	// load the Normal map, Specular map and the diffuse texture by providing the path from the basic diffuse texture.
	inline VVOID                     loadBumpMaterial(ID3D10Device* device, WSTRING texturePath, BumpMaterial& material)
	{
		WSTRING checker = L"";
		if(!(texturePath == checker))
		{
			LWSTRING res;

			// split the extension.
			splitWithDelimiter(texturePath, res, '.');

			WSTRING ambientPath  = L"." + res.First()->Next()->Data() + L"_NRM." + res.Last()->Data();  // normal map
			WSTRING specularPath = L"." + res.First()->Next()->Data() + L"_SPEC." + res.Last()->Data(); // specular map

			material.SetAmbient(loadRV(device, ambientPath));
			material.SetDiffuse(loadRV(device, texturePath));
			material.SetSpecular(loadRV(device, specularPath));
		}
	}

	inline VVOID                     loadTriPlanarMaterial(ID3D10Device* device, WSTRING texturePath[3], TriPlanarBumpMaterial& material)
	{
		WSTRING checker = L"";
		USMALLINT textCounter = 0;
		for(USMALLINT currTextPath = 0; currTextPath < 3; currTextPath++)
		{
			if(!(texturePath[currTextPath] == checker))
			{
				LWSTRING res;

				// split the extension.
				splitWithDelimiter(texturePath[currTextPath], res, '.');

				WSTRING ambientPath  = L"." + res.First()->Next()->Data() + L"_NRM." + res.Last()->Data();  // normal map
				WSTRING specularPath = L"." + res.First()->Next()->Data() + L"_SPEC." + res.Last()->Data(); // specular map

				material.SetTexture(loadRV(device, texturePath[currTextPath]), textCounter); // Diffuse
				material.SetTexture(loadRV(device, ambientPath), textCounter + 1);           // Ambient
				material.SetTexture(loadRV(device, specularPath), textCounter + 2);          // Specular
				textCounter += 3;
			}
		}
	}

	inline BIGINT                    initializeDXEffect(ID3D10Device* device, WSTRING effectPath, ID3D10Effect** effect)
	{
		HRESULT hr = S_OK;

		// Create the effect
		DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;

		#if defined( DEBUG ) || defined( _DEBUG )
		// Set the D3D10_SHADER_DEBUG flag to embed debug information in the shaders.
		dwShaderFlags |= D3D10_SHADER_DEBUG;
		#endif

		// Gather effect compilation errors
		ID3D10Blob *	pFXcompileErrors = NULL;

		hr = D3DX10CreateEffectFromFile(effectPath.c_str(), NULL, NULL, "fx_4_0", dwShaderFlags, 0, device, NULL, NULL, effect, &pFXcompileErrors, NULL);
		if(FAILED(hr))
		{
			if(hr == D3D10_ERROR_FILE_NOT_FOUND)
					MessageBoxA(NULL, "The FX file cannot be located. Please run this executable from the parent of the directory (named 'Effects') which contains the FX file.", "ERROR", MB_OK);
			else
			{
				MessageBoxA(NULL, "Failed to compile the FX file.  Please debug the FX code.", "ERROR", MB_OK );

				#if defined(DEBUG) || defined(_DEBUG)
				// Display effect compile errors (if any) on debugging console
				if ( pFXcompileErrors != 0 && pFXcompileErrors->GetBufferPointer() != 0)
				{
					OutputDebugStringA("\n--------------------------------------------------------------------------\n");
					OutputDebugStringA("                           Effect Compilation Errors\n");
					OutputDebugStringA("--------------------------------------------------------------------------\n\n");
					OutputDebugStringA((CCHAR *)pFXcompileErrors->GetBufferPointer());
					OutputDebugStringA("\n--------------------------------------------------------------------------\n\n");
				}
				#endif // defined( DEBUG ) || defined( _DEBUG )
			}
			return (BIGINT)hr;
		} // end if failed

		return (BIGINT)hr;
	}
}

#endif