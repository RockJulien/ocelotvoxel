#ifndef DEF_MATRIX2X2_H
#include "..\..\MasterVoxLOD3\Maths\Matrix2x2.h"
#include "..\..\MasterVoxLOD3\Maths\Matrix2x2.cpp"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;

BOOST_AUTO_TEST_CASE(identityMat2x2_test)
{
	Matrix2x2 mat;
	mat.Matrix2x2Identity();

	Matrix2x2 expectedResult(1.0f, 0.0f, 0.0f, 1.0f);

	BOOST_CHECK(mat == expectedResult);
}

BOOST_AUTO_TEST_CASE(isIdentityMat2x2_test)
{
	Matrix2x2 mat;
	mat.Matrix2x2Identity();

	Matrix2x2 temp(1.0f, 0.0f, 0.0f, 1.0f);
	Matrix2x2 temp2(0.0f, 0.0f, 0.0f, 0.0f);

	bool expectedResult = true;
	bool expectedResult2 = false;

	BOOST_CHECK((mat == temp) == expectedResult);
	BOOST_CHECK((mat == temp2) == expectedResult2);
	BOOST_CHECK(mat.Matrix2x2IsIdentity() == expectedResult);
	BOOST_CHECK(temp2.Matrix2x2IsIdentity() == expectedResult2);
}

BOOST_AUTO_TEST_CASE(transposeMat2x2_test)
{
	Matrix2x2 mat(2.0f, 4.0f, 6.0f, 8.0f);
	Matrix2x2 staySameAfterTranspose = mat;
	Matrix2x2 result;
	mat.Matrix2x2Transpose(result);

	Matrix2x2 expectedResult(2.0f, 6.0f, 4.0f, 8.0f);

	BOOST_CHECK((staySameAfterTranspose == mat) == true);
	BOOST_CHECK(result == expectedResult);
}

BOOST_AUTO_TEST_CASE(inverseMat2x2_test)
{
	Matrix2x2 mat(2.0f, 4.0f, 6.0f, 8.0f);
	Matrix2x2 staySameAfterInverse = mat;
	Matrix2x2 result;
	mat.Matrix2x2Inverse(result);

	Matrix2x2 expectedResult(-1.0f, 0.5f, 0.75f, -0.25f);

	BOOST_CHECK((staySameAfterInverse == mat) == true);
	BOOST_CHECK(result == expectedResult);
}

BOOST_AUTO_TEST_CASE(determinantMat2x2_test)
{
	Matrix2x2 mat(2.0f, 4.0f, 6.0f, 8.0f);

	float val = mat.Matrix2x2Determinant();

	float expectedResult = -8.0f;

	BOOST_CHECK(fabsf(val - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(multiplyMat2x2_test)
{
	Matrix2x2 mat1(2.0f, 4.0f, 6.0f, 8.0f);
	Matrix2x2 mat2(1.0f, 3.0f, 7.0f, 2.0f);
	Matrix2x2 stillTheSame = mat1;
	Matrix2x2 result;

	mat1.Matrix2x2Mutliply(result, mat2);

	Matrix2x2 expectedResult(30.0f, 22.0f, 62.0f, 34.0f);

	BOOST_CHECK(stillTheSame == mat1);
	BOOST_CHECK(result == expectedResult);
	BOOST_CHECK((mat1 * mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(doubleMultiplyMat2x2_test)
{
	Matrix2x2 mat1(2.0f, 4.0f, 6.0f, 8.0f);
	Matrix2x2 mat2(1.0f, 3.0f, 7.0f, 2.0f);
	Matrix2x2 mat3(10.0f, 5.0f, 1.0f, 3.0f);
	Matrix2x2 stillTheSame = mat1;
	Matrix2x2 stillTheSame2 = mat2;
	Matrix2x2 result;

	mat1.Matrix2x2Mutliply(result, mat2, mat3);

	Matrix2x2 expectedResult(322.0f, 96.0f, 654.0f, 412.0f);

	BOOST_CHECK(stillTheSame == mat1);
	BOOST_CHECK(stillTheSame2 == mat2);
	BOOST_CHECK(result == expectedResult);
	BOOST_CHECK((mat1 * mat2 * mat3) == expectedResult);

}

BOOST_AUTO_TEST_CASE(addMat2x2_test)
{
	Matrix2x2 mat1(1.0f, 2.0f, 3.0f, 4.0f);
	Matrix2x2 mat2(5.0f, 6.0f, 7.0f, 8.0f);

	Matrix2x2 expectedResult(6.0f, 8.0f, 10.0f, 12.0f);

	BOOST_CHECK((mat1 + mat2) == expectedResult);
	BOOST_CHECK((mat1 += mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(subMat2x2_test)
{
	Matrix2x2 mat1(1.0f, 2.0f, 3.0f, 4.0f);
	Matrix2x2 mat2(5.0f, 6.0f, 7.0f, 8.0f);

	Matrix2x2 expectedResult(-4.0f, -4.0f, -4.0f, -4.0f);

	BOOST_CHECK((mat1 - mat2) == expectedResult);
	BOOST_CHECK((mat1 -= mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(divMat2x2_test)
{
	Matrix2x2 mat1(1.0f, 2.0f, 3.0f, 4.0f);
	float val = 2.0f;

	Matrix2x2 expectedResult(0.5f, 1.0f, 1.5f, 2.0f);

	BOOST_CHECK((mat1 / val) == expectedResult);
	BOOST_CHECK((mat1 /= val) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulMat2x2_test)
{
	Matrix2x2 mat1(2.0f, 4.0f, 6.0f, 8.0f);
	Matrix2x2 mat2(1.0f, 3.0f, 7.0f, 2.0f);

	Matrix2x2 expectedResult(30.0f, 22.0f, 62.0f, 34.0f);

	BOOST_CHECK((mat1 * mat2) == expectedResult);
	BOOST_CHECK((mat1 *= mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulConstMat2x2_test)
{
	Matrix2x2 mat1(1.0f, 2.0f, 3.0f, 4.0f);
	float val = 2.0f;

	Matrix2x2 expectedResult(2.0f, 4.0f, 6.0f, 8.0f);

	BOOST_CHECK((mat1 * val) == expectedResult);
	BOOST_CHECK((mat1 *= val) == expectedResult);
}
