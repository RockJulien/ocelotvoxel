#ifndef DEF_MATRIX4X4_H
#include "..\..\MasterVoxLOD3\Maths\Matrix4x4.h"
#include "..\..\MasterVoxLOD3\Maths\Matrix4x4.cpp"
#endif

#include "boost\test\unit_test.hpp"

using namespace std;

BOOST_AUTO_TEST_CASE(identityMat4x4_test)
{
	Matrix4x4 mat;
	mat.Matrix4x4Identity();

	Matrix4x4 expectedResult(1.0f, 0.0f, 0.0f, 0.0f,
							 0.0f, 1.0f, 0.0f, 0.0f,
							 0.0f, 0.0f, 1.0f, 0.0f,
							 0.0f, 0.0f, 0.0f, 1.0f);

	BOOST_CHECK(mat == expectedResult);
}

BOOST_AUTO_TEST_CASE(isIdentityMat4x4_test)
{
	Matrix4x4 mat;
	mat.Matrix4x4Identity();

	Matrix4x4 temp(1.0f, 0.0f, 0.0f, 0.0f,
				   0.0f, 1.0f, 0.0f, 0.0f,
				   0.0f, 0.0f, 1.0f, 0.0f,
				   0.0f, 0.0f, 0.0f, 1.0f);
	Matrix4x4 temp2(1.0f, 0.0f, 0.0f,10.0f,
					0.0f, 3.0f, 6.0f, 0.0f,
					0.0f, 0.0f, 2.0f, 0.0f,
					5.0f, 0.0f, 1.0f, 0.0f);

	bool expectedResult = true;
	bool expectedResult2 = false;

	BOOST_CHECK((mat == temp) == expectedResult);
	BOOST_CHECK((mat == temp2) == expectedResult2);
	BOOST_CHECK(mat.Matrix4x4IsIdentity() == expectedResult);
	BOOST_CHECK(temp2.Matrix4x4IsIdentity() == expectedResult2);
}

BOOST_AUTO_TEST_CASE(transposeMat4x4_test)
{
	Matrix4x4 mat( 2.0f, 4.0f, 6.0f, 8.0f,
				  10.0f,12.0f,14.0f,16.0f,
				  18.0f,20.0f,22.0f,24.0f,
				  26.0f,28.0f,30.0f,32.0f);
	Matrix4x4 staySameAfterTranspose = mat;
	Matrix4x4 result;
	mat.Matrix4x4Transpose(result);

	Matrix4x4 expectedResult( 2.0f,10.0f,18.0f,26.0f,
							  4.0f,12.0f,20.0f,28.0f,
							  6.0f,14.0f,22.0f,30.0f,
							  8.0f,16.0f,24.0f,32.0f);

	BOOST_CHECK((staySameAfterTranspose == mat) == true);
	BOOST_CHECK(result == expectedResult);
}

BOOST_AUTO_TEST_CASE(inverseMat4x4_test)
{
	/*Matrix4x4 mat( 2.0f,  4.0f,  6.0f,
				   8.0f,  5.0f, 12.0f,
				   7.0f, 16.0f, 13.0f);
	Matrix4x4 staySameAfterInverse = mat;
	Matrix4x4 result;
	mat.Matrix4x4Inverse(result);

	Matrix4x4 expectedResult(-0.56696f, 0.19642f, 0.080357f,
							 -0.089285f,-0.071428f, 0.10714f,
							  0.41517f,-0.017857f,-0.098214f);

	BOOST_CHECK((staySameAfterInverse == mat) == true);
	BOOST_CHECK((result.get_11() - expectedResult.get_11()) < 0.00001f);
	BOOST_CHECK((result.get_12() - expectedResult.get_12()) < 0.00001f);
	BOOST_CHECK((result.get_13() - expectedResult.get_13()) < 0.00001f);
	BOOST_CHECK((result.get_21() - expectedResult.get_21()) < 0.00001f);
	BOOST_CHECK((result.get_22() - expectedResult.get_22()) < 0.00001f);
	BOOST_CHECK((result.get_23() - expectedResult.get_23()) < 0.00001f);
	BOOST_CHECK((result.get_31() - expectedResult.get_31()) < 0.00001f);
	BOOST_CHECK((result.get_32() - expectedResult.get_32()) < 0.00001f);
	BOOST_CHECK((result.get_33() - expectedResult.get_33()) < 0.00001f);*/
}

BOOST_AUTO_TEST_CASE(determinantMat4x4_test)
{
	Matrix4x4 mat( 2.0f,  4.0f,  6.0f, 5.0f,
				   8.0f,  5.0f, 12.0f, 7.0f,
				   7.0f, 16.0f, 13.0f, 1.0f,
				   2.0f,  3.0f,  9.0f, 1.0f);

	float val = mat.Matrix4x4Determinant();

	float expectedResult = -2329.0f;

	BOOST_CHECK(fabsf(val - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(multiplyMat4x4_test)
{
	Matrix4x4 mat1( 2.0f,  4.0f,  6.0f,  5.0f,
				    8.0f,  5.0f, 12.0f,  7.0f,
				    7.0f, 16.0f, 13.0f,  1.0f,
				    2.0f,  3.0f,  9.0f,  1.0f);
	Matrix4x4 mat2(10.0f,  3.0f, 18.0f,  5.0f,
				    1.0f,  7.0f, 17.0f,  4.0f,
				    9.0f,  9.0f, 21.0f, 11.0f,
				   10.0f, 11.0f, 12.0f, 13.0f);
	Matrix4x4 stillTheSame = mat1;
	Matrix4x4 result;

	mat1.Matrix4x4Mutliply(result, mat2);

	Matrix4x4 expectedResult;
	expectedResult.set_11((mat1.get_11() * mat2.get_11()) + (mat1.get_12() * mat2.get_21()) + (mat1.get_13() * mat2.get_31()) + (mat1.get_14() * mat2.get_41()));
	expectedResult.set_12((mat1.get_11() * mat2.get_12()) + (mat1.get_12() * mat2.get_22()) + (mat1.get_13() * mat2.get_32()) + (mat1.get_14() * mat2.get_42()));
	expectedResult.set_13((mat1.get_11() * mat2.get_13()) + (mat1.get_12() * mat2.get_23()) + (mat1.get_13() * mat2.get_33()) + (mat1.get_14() * mat2.get_43()));
	expectedResult.set_14((mat1.get_11() * mat2.get_14()) + (mat1.get_12() * mat2.get_24()) + (mat1.get_13() * mat2.get_34()) + (mat1.get_14() * mat2.get_44()));
	/*****************Second row***************************/
	expectedResult.set_21((mat1.get_21() * mat2.get_11()) + (mat1.get_22() * mat2.get_21()) + (mat1.get_23() * mat2.get_31()) + (mat1.get_24() * mat2.get_41()));
	expectedResult.set_22((mat1.get_21() * mat2.get_12()) + (mat1.get_22() * mat2.get_22()) + (mat1.get_23() * mat2.get_32()) + (mat1.get_24() * mat2.get_42()));
	expectedResult.set_23((mat1.get_21() * mat2.get_13()) + (mat1.get_22() * mat2.get_23()) + (mat1.get_23() * mat2.get_33()) + (mat1.get_24() * mat2.get_43()));
	expectedResult.set_24((mat1.get_21() * mat2.get_14()) + (mat1.get_22() * mat2.get_24()) + (mat1.get_23() * mat2.get_34()) + (mat1.get_24() * mat2.get_44()));
	/*****************Third row****************************/
	expectedResult.set_31((mat1.get_31() * mat2.get_11()) + (mat1.get_32() * mat2.get_21()) + (mat1.get_33() * mat2.get_31()) + (mat1.get_34() * mat2.get_41()));
	expectedResult.set_32((mat1.get_31() * mat2.get_12()) + (mat1.get_32() * mat2.get_22()) + (mat1.get_33() * mat2.get_32()) + (mat1.get_34() * mat2.get_42()));
	expectedResult.set_33((mat1.get_31() * mat2.get_13()) + (mat1.get_32() * mat2.get_23()) + (mat1.get_33() * mat2.get_33()) + (mat1.get_34() * mat2.get_43()));
	expectedResult.set_34((mat1.get_31() * mat2.get_14()) + (mat1.get_32() * mat2.get_24()) + (mat1.get_33() * mat2.get_34()) + (mat1.get_34() * mat2.get_44()));
	/*****************Fourth row***************************/
	expectedResult.set_41((mat1.get_41() * mat2.get_11()) + (mat1.get_42() * mat2.get_21()) + (mat1.get_43() * mat2.get_31()) + (mat1.get_44() * mat2.get_41()));
	expectedResult.set_42((mat1.get_41() * mat2.get_12()) + (mat1.get_42() * mat2.get_22()) + (mat1.get_43() * mat2.get_32()) + (mat1.get_44() * mat2.get_42()));
	expectedResult.set_43((mat1.get_41() * mat2.get_13()) + (mat1.get_42() * mat2.get_23()) + (mat1.get_43() * mat2.get_33()) + (mat1.get_44() * mat2.get_43()));
	expectedResult.set_44((mat1.get_41() * mat2.get_14()) + (mat1.get_42() * mat2.get_24()) + (mat1.get_43() * mat2.get_34()) + (mat1.get_44() * mat2.get_44()));

	BOOST_CHECK(stillTheSame == mat1);
	BOOST_CHECK(result == expectedResult);
	BOOST_CHECK((mat1 * mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(translateMat4x4_test)
{
	float Tx = 2.0f;
	float Ty = 3.0f;
	float Tz = 4.0f;

	Matrix4x4 matTrans;
	matTrans.Matrix4x4Identity();
	matTrans.Matrix4x4Translation(matTrans, Tx, Ty, Tz); 

	Matrix4x4 expectedResult;
	expectedResult.Matrix4x4Identity();
	expectedResult.set_41(Tx);  // for 3D Games
	expectedResult.set_42(Ty);
	expectedResult.set_43(Tz);

	BOOST_CHECK(Equal<Matrix4x4>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(rotateMat4x4_test)
{
	float valRx = 2.0f;
	float Cos = cosf(valRx);
	float Sin = sinf(valRx);

	Matrix4x4 matTrans;
	matTrans.Matrix4x4Identity();
	matTrans.Matrix4x4RotationX(matTrans, valRx); 

	Matrix4x4 expectedResult;
	expectedResult.Matrix4x4Identity();
	expectedResult.set_22(Cos);  // for 3D Games
	expectedResult.set_23(Sin);
	expectedResult.set_32(-Sin);
	expectedResult.set_33(Cos);

	BOOST_CHECK(Equal<Matrix4x4>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(scaleMat4x4_test)
{
	float Sx = 2.0f;
	float Sy = 3.0f;
	float Sz = 4.0f;

	Matrix4x4 matTrans;
	matTrans.Matrix4x4Identity();
	matTrans.Matrix4x4Scaling(matTrans, Sx, Sy, Sz); 

	Matrix4x4 expectedResult;
	expectedResult.Matrix4x4Identity();
	expectedResult.set_11(Sx);  // for 3D Games
	expectedResult.set_22(Sy);
	expectedResult.set_33(Sz);

	BOOST_CHECK(Equal<Matrix4x4>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(rotationGenMat4x4_test)
{
	float Rx = 2.0f;
	float Ry = 3.0f;
	float Rz = 4.0f;

	Matrix4x4 matTrans;
	matTrans.Matrix4x4Identity();
	matTrans.Matrix4x4RotationGeneralized(matTrans, Rx, Ry, Rz); 

	Matrix4x4 expectedResult;
	expectedResult.Matrix4x4Identity();

	float CosAlpha = cos(Rz);  // Yaw (Z)
	float SinAlpha = sin(Rz);  // Yaw (Z)
	float CosBeta  = cos(Ry);  // Pitch (Y)
	float SinBeta  = sin(Ry);  // Pitch (Y)
	float CosPhi   = cos(Rx);  // Roll (X)
	float SinPhi   = sin(Rx);  // Roll (X)

	expectedResult.set_11((CosAlpha * CosBeta)); expectedResult.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); expectedResult.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	expectedResult.set_21((SinAlpha * CosBeta)); expectedResult.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); expectedResult.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	expectedResult.set_31((-SinBeta));           expectedResult.set_32((CosBeta * SinPhi));                                    expectedResult.set_33((CosBeta * CosPhi));

	BOOST_CHECK(Equal<Matrix4x4>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(doubleMultiplyMat4x4_test)
{
	Matrix4x4 mat1( 2.0f,  4.0f,  6.0f,  5.0f,
				    8.0f,  5.0f, 12.0f,  7.0f,
				    7.0f, 16.0f, 13.0f,  1.0f,
				    2.0f,  3.0f,  9.0f,  1.0f);
	Matrix4x4 mat2(10.0f,  3.0f, 18.0f,  5.0f,
				    1.0f,  7.0f, 17.0f,  4.0f,
				    9.0f,  9.0f, 21.0f, 11.0f,
				   10.0f, 11.0f, 12.0f, 13.0f);
	Matrix4x4 mat3(20.0f,  4.0f,  6.0f,  5.0f,
				    8.0f,  5.0f, 11.0f,  7.0f,
				    2.0f, 19.0f, 13.0f, 11.0f,
				    3.0f,  5.0f,  9.0f,  1.0f);
	Matrix4x4 stillTheSame = mat1;
	Matrix4x4 stillTheSame2 = mat2;
	Matrix4x4 result;

	mat1.Matrix4x4Mutliply(result, mat2, mat3);

	Matrix4x4 expectedResult;
	mat1.Matrix4x4Mutliply(expectedResult, mat2);
	expectedResult.Matrix4x4Mutliply(expectedResult, mat3);

	BOOST_CHECK(stillTheSame == mat1);
	BOOST_CHECK(stillTheSame2 == mat2);
	BOOST_CHECK(result == expectedResult);
	BOOST_CHECK((mat1 * mat2 * mat3) == expectedResult);

}

BOOST_AUTO_TEST_CASE(addMat4x4_test)
{
	Matrix4x4 mat1( 2.0f,  4.0f,  6.0f,  5.0f,
				    8.0f,  5.0f, 12.0f,  7.0f,
				    7.0f, 16.0f, 13.0f,  1.0f,
				    2.0f,  3.0f,  9.0f,  1.0f);
	Matrix4x4 mat2(10.0f,  3.0f, 18.0f,  5.0f,
				    1.0f,  7.0f, 17.0f,  4.0f,
				    9.0f,  9.0f, 21.0f, 11.0f,
				   10.0f, 11.0f, 12.0f, 13.0f);

	Matrix4x4 expectedResult(12.0f,  7.0f, 24.0f, 10.0f,
							  9.0f, 12.0f, 29.0f, 11.0f,
							 16.0f, 25.0f, 34.0f, 12.0f,
							 12.0f, 14.0f, 21.0f, 14.0f);

	BOOST_CHECK((mat1 + mat2) == expectedResult);
	BOOST_CHECK((mat1 += mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(subMat4x4_test)
{
	Matrix4x4 mat1( 2.0f,  4.0f,  6.0f,  5.0f,
				    8.0f,  5.0f, 12.0f,  7.0f,
				    7.0f, 16.0f, 13.0f,  1.0f,
				    2.0f,  3.0f,  9.0f,  1.0f);
	Matrix4x4 mat2(10.0f,  3.0f, 18.0f,  5.0f,
				    1.0f,  7.0f, 17.0f,  4.0f,
				    9.0f,  9.0f, 21.0f, 11.0f,
				   10.0f, 11.0f, 12.0f, 13.0f);

	Matrix4x4 expectedResult(-8.0f,  1.0f,-12.0f,  0.0f,
							  7.0f, -2.0f, -5.0f,  3.0f,
							 -2.0f,  7.0f, -8.0f,-10.0f,
							 -8.0f, -8.0f, -3.0f,-12.0f);

	BOOST_CHECK((mat1 - mat2) == expectedResult);
	BOOST_CHECK((mat1 -= mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(divMat4x4_test)
{
	Matrix4x4 mat1(10.0f,  3.0f, 18.0f,  5.0f,
				    1.0f,  7.0f, 17.0f,  4.0f,
				    9.0f,  9.0f, 21.0f, 11.0f,
				   10.0f, 11.0f, 12.0f, 13.0f);
	float val = 2.0f;

	Matrix4x4 expectedResult( 5.0f,  1.5f,  9.0f,  2.5f,
							  0.5f,  3.5f,  8.5f,  2.0f,
							  4.5f,  4.5f, 10.5f,  5.5f,
							  5.0f,  5.5f,  6.0f,  6.5f);

	BOOST_CHECK((mat1 / val) == expectedResult);
	BOOST_CHECK((mat1 /= val) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulMat4x4_test)
{
	Matrix4x4 mat1( 2.0f,  4.0f,  6.0f,  5.0f,
				    8.0f,  5.0f, 12.0f,  7.0f,
				    7.0f, 16.0f, 13.0f,  1.0f,
				    2.0f,  3.0f,  9.0f,  1.0f);
	Matrix4x4 mat2(10.0f,  3.0f, 18.0f,  5.0f,
				    1.0f,  7.0f, 17.0f,  4.0f,
				    9.0f,  9.0f, 21.0f, 11.0f,
				   10.0f, 11.0f, 12.0f, 13.0f);

	Matrix4x4 expectedResult;
	mat1.Matrix4x4Mutliply(expectedResult, mat2);

	BOOST_CHECK((mat1 * mat2) == expectedResult);
	BOOST_CHECK((mat1 *= mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulConstMat4x4_test)
{
	Matrix4x4 mat1( 2.0f,  4.0f,  6.0f,  5.0f,
					8.0f,  5.0f, 12.0f,  7.0f,
					7.0f, 16.0f, 13.0f,  1.0f,
					2.0f,  3.0f,  9.0f,  1.0f);
	float val = 2.0f;

	Matrix4x4 expectedResult( 4.0f,  8.0f, 12.0f, 10.0f,
							 16.0f, 10.0f, 24.0f, 14.0f,
							 14.0f, 32.0f, 26.0f,  2.0f,
							  4.0f,  6.0f, 18.0f,  2.0f);

	BOOST_CHECK((mat1 * val) == expectedResult);
	BOOST_CHECK((mat1 *= val) == expectedResult);
}
