#ifndef DEF_ATREENODE_H
#include "..\..\MasterVoxLOD3\DataStructures\ATreeNode.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;
using namespace Ocelot;

#define NBCHILD 8

VVOID drawValue2(ATreeNode<UBIGINT, UBIGINT>* t)
{
	cout << endl << t->Data() << endl ;
}

ATreeNode<UBIGINT, UBIGINT>* searchMethod2(ATreeNode<UBIGINT, UBIGINT>* toTest, UBIGINT toSearch)
{
	if(toTest->Data() == toSearch)
		return toTest;

	return NULL;
}

BOOST_AUTO_TEST_CASE(ATreeNodeCreation_test)
{
	ATreeNode<UBIGINT, UBIGINT>* root    = new ATreeNode<UBIGINT, UBIGINT>(0, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd   = new ATreeNode<UBIGINT, UBIGINT>(1, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd1  = new ATreeNode<UBIGINT, UBIGINT>(2, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd2  = new ATreeNode<UBIGINT, UBIGINT>(3, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd3  = new ATreeNode<UBIGINT, UBIGINT>(4, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd4  = new ATreeNode<UBIGINT, UBIGINT>(5, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd5  = new ATreeNode<UBIGINT, UBIGINT>(6, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd6  = new ATreeNode<UBIGINT, UBIGINT>(7, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd7  = new ATreeNode<UBIGINT, UBIGINT>(8, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd8  = new ATreeNode<UBIGINT, UBIGINT>(9, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd9  = new ATreeNode<UBIGINT, UBIGINT>(10, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd10 = new ATreeNode<UBIGINT, UBIGINT>(11, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd11 = new ATreeNode<UBIGINT, UBIGINT>(12, NBCHILD);

	toAdd->SetChild(0, toAdd1);
	toAdd->SetChild(1, toAdd2);
	root->SetChild(0, toAdd);

	toAdd3->SetChild(0, toAdd4);
	toAdd3->SetChild(1, toAdd5);
	toAdd3->SetChild(2, toAdd6);
	root->SetChild(1, toAdd3);

	toAdd7->SetChild(0, toAdd8);
	toAdd7->SetChild(1, toAdd9);
	root->SetChild(2, toAdd7);

	toAdd10->SetChild(0, toAdd11);
	root->SetChild(3, toAdd10);

	//root->applyToTree(drawValue2);

	cout << endl << endl;

	DeletePointer(root); // No memory leaks during recursion (do not delete parent but only children or it will kill the recursivity).
}

BOOST_AUTO_TEST_CASE(ATreeNodeSearch_test)
{
	ATreeNode<UBIGINT, UBIGINT>* root    = new ATreeNode<UBIGINT, UBIGINT>(0, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd   = new ATreeNode<UBIGINT, UBIGINT>(1, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd1  = new ATreeNode<UBIGINT, UBIGINT>(2, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd2  = new ATreeNode<UBIGINT, UBIGINT>(3, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd3  = new ATreeNode<UBIGINT, UBIGINT>(4, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd4  = new ATreeNode<UBIGINT, UBIGINT>(5, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd5  = new ATreeNode<UBIGINT, UBIGINT>(6, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd6  = new ATreeNode<UBIGINT, UBIGINT>(7, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd7  = new ATreeNode<UBIGINT, UBIGINT>(8, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd8  = new ATreeNode<UBIGINT, UBIGINT>(9, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd9  = new ATreeNode<UBIGINT, UBIGINT>(10, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd10 = new ATreeNode<UBIGINT, UBIGINT>(11, NBCHILD);
	ATreeNode<UBIGINT, UBIGINT>* toAdd11 = new ATreeNode<UBIGINT, UBIGINT>(12, NBCHILD);

	toAdd->SetChild(0, toAdd1);
	toAdd->SetChild(1, toAdd2);
	root->SetChild(0, toAdd);

	toAdd3->SetChild(0, toAdd4);
	toAdd3->SetChild(1, toAdd5);
	toAdd3->SetChild(2, toAdd6);
	root->SetChild(1, toAdd3);

	toAdd7->SetChild(0, toAdd8);
	toAdd7->SetChild(1, toAdd9);
	root->SetChild(2, toAdd7);

	toAdd10->SetChild(0, toAdd11);
	root->SetChild(3, toAdd10);

	//root->applyToTree(drawValue2);

	cout << endl << endl;

	ATreeNode<UBIGINT, UBIGINT>::Node* returned = root->search(searchMethod2, 7);

	UBIGINT expectedResult = 7;

	BOOST_CHECK(returned->Data() == expectedResult);

	DeletePointer(root); // No memory leaks during recursion (do not delete parent but only children or it will kill the recursivity).
}
