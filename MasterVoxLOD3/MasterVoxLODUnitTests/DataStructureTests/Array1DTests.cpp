#ifndef DEF_ARRAY1D_H
#include "..\..\MasterVoxLOD3\DataStructures\Array1D.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;

BOOST_AUTO_TEST_CASE(Array1DCreation_test)
{
	Array1D<UBIGINT> test(12);

	for(UBIGINT i = 0; i < 12; i++)
		test[i] = i + 1;

	for(UBIGINT i = 0; i < 12; i++)
		BOOST_CHECK(test[i] == i + 1);
}

BOOST_AUTO_TEST_CASE(Array1DCopy_test)
{
	Array1D<UBIGINT> test(12);
	Array1D<UBIGINT> test3(12);

	for(UBIGINT i = 0; i < 12; i++)
		test[i] = i + 1;

	Array1D<UBIGINT> test2(test);
	// assignment operator
	test3 = test2;

	for(UBIGINT i = 0; i < 12; i++)
		BOOST_CHECK(test[i] == i + 1);
	for(UBIGINT i = 0; i < 12; i++)
		BOOST_CHECK(test2[i] == i + 1);
	for(UBIGINT i = 0; i < 12; i++)
		BOOST_CHECK(test3[i] == i + 1);
}

BOOST_AUTO_TEST_CASE(Array1DResize_test)
{
	Array1D<UBIGINT> test(12);

	for(UBIGINT i = 0; i < 12; i++)
		test[i] = i + 1;

	test.resize(20);

	test[12] = 13;
	test[13] = 14;
	test[14] = 15;
	test[15] = 16;
	test[16] = 17;
	test[17] = 18;
	test[18] = 19;
	test[19] = 20;

	BOOST_CHECK(test.Size() == 20);

	for(UBIGINT i = 0; i < 20; i++)
		BOOST_CHECK(test[i] == i + 1);
}

BOOST_AUTO_TEST_CASE(Array1DInsert_test)
{
	Array1D<UBIGINT> test(12);

	for(UBIGINT i = 0; i < 12; i++)
		test.insert(i + 1, i);

	for(UBIGINT i = 0; i < 12; i++)
		BOOST_CHECK(test[i] == i + 1);
}

BOOST_AUTO_TEST_CASE(Array1DEquality_test)
{
	Array1D<UBIGINT> test(12);

	for(UBIGINT i = 0; i < 12; i++)
		test.insert(i + 1, i);

	Array1D<UBIGINT> test2(test);

	BOOST_CHECK(test == test2);
}

BOOST_AUTO_TEST_CASE(Array1DDifference_test)
{
	Array1D<UBIGINT> test(12);

	for(UBIGINT i = 0; i < 12; i++)
		test.insert(i + 1, i);

	Array1D<UBIGINT> test2(test);
	Array1D<UBIGINT> test3(12);

	for(UBIGINT i = 0; i < 12; i++)
		test2.insert(2, i);

	for(UBIGINT i = 0; i < 12; i++)
		test3.insert(5, i);

	// equality despite changes because copy of the original array.
	BOOST_CHECK(test == test2);
	BOOST_CHECK(test != test3);
}

BOOST_AUTO_TEST_CASE(Array1DRemove_test)
{
	Array1D<UBIGINT> test(12);

	for(UBIGINT i = 0; i < 12; i++)
		test.insert(i + 1, i);

	test.remove(0);

	Array1D<UBIGINT> expectedResult(12);

	expectedResult[0]  = 2; // will shift the array values at the index pos
	expectedResult[1]  = 3;
	expectedResult[2]  = 4;
	expectedResult[3]  = 5;
	expectedResult[4]  = 6;
	expectedResult[5]  = 7;
	expectedResult[6]  = 8;
	expectedResult[7]  = 9;
	expectedResult[8]  = 10;
	expectedResult[9]  = 11;
	expectedResult[10] = 12; // so , the two last will be identical.
	expectedResult[11] = 12; // unless if this is the last one aimed.

	BOOST_CHECK(test == expectedResult);
}
