#ifndef DEF_OCELOTAOCTREE_H
#include "..\..\MasterVoxLOD3\DataStructures\OcelotAOctree.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;
using namespace Ocelot;

VVOID drawOctValue2(ATreeNode<UBIGINT, UBIGINT>* t)
{
	cout << endl << t->Data() << endl ;
}

BOOST_AUTO_TEST_CASE(OcelotAOctreeCreation_test)
{
	ATreeNode<UBIGINT, UBIGINT>* root    = new OcelotAOctree<UBIGINT, UBIGINT>(0);
	ATreeNode<UBIGINT, UBIGINT>* toAdd   = new OcelotAOctree<UBIGINT, UBIGINT>(1);
	ATreeNode<UBIGINT, UBIGINT>* toAdd1  = new OcelotAOctree<UBIGINT, UBIGINT>(2);
	ATreeNode<UBIGINT, UBIGINT>* toAdd2  = new OcelotAOctree<UBIGINT, UBIGINT>(3);
	ATreeNode<UBIGINT, UBIGINT>* toAdd3  = new OcelotAOctree<UBIGINT, UBIGINT>(4);
	ATreeNode<UBIGINT, UBIGINT>* toAdd4  = new OcelotAOctree<UBIGINT, UBIGINT>(5);
	ATreeNode<UBIGINT, UBIGINT>* toAdd5  = new OcelotAOctree<UBIGINT, UBIGINT>(6);
	ATreeNode<UBIGINT, UBIGINT>* toAdd6  = new OcelotAOctree<UBIGINT, UBIGINT>(7);
	ATreeNode<UBIGINT, UBIGINT>* toAdd7  = new OcelotAOctree<UBIGINT, UBIGINT>(8);
	ATreeNode<UBIGINT, UBIGINT>* toAdd8  = new OcelotAOctree<UBIGINT, UBIGINT>(9);
	ATreeNode<UBIGINT, UBIGINT>* toAdd9  = new OcelotAOctree<UBIGINT, UBIGINT>(10);
	ATreeNode<UBIGINT, UBIGINT>* toAdd10 = new OcelotAOctree<UBIGINT, UBIGINT>(11);
	ATreeNode<UBIGINT, UBIGINT>* toAdd11 = new OcelotAOctree<UBIGINT, UBIGINT>(12);

	toAdd->SetChild(0, toAdd1);
	toAdd->SetChild(1, toAdd2);
	root->SetChild(0, toAdd);

	toAdd3->SetChild(0, toAdd4);
	toAdd3->SetChild(1, toAdd5);
	toAdd3->SetChild(2, toAdd6);
	root->SetChild(1, toAdd3);

	toAdd7->SetChild(0, toAdd8);
	toAdd7->SetChild(1, toAdd9);
	root->SetChild(2, toAdd7);

	toAdd10->SetChild(0, toAdd11);
	root->SetChild(3, toAdd10);

	//root->applyToTree(drawOctValue2);

	cout << endl << endl;

	DeletePointer(root);
}
