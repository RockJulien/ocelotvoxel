#ifndef DEF_LINKEDHASHTABLE_H
#include "..\..\MasterVoxLOD3\DataStructures\LinkedHashTable.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;

BOOST_AUTO_TEST_CASE(LinkedHashTableCreation_test)
{
	LinkedHashTable<UBIGINT, string> test(10, HashInteger);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");
	test.insert(6, "Bob");
	test.insert(7, "Len");
	test.insert(8, "Claude");
	test.insert(9, "Damien");
	test.insert(10, "Guillaume");

	string name = test.search(7)->Data();
	UBIGINT key = test.search(7)->Key();

	string expectedName = "Len";
	UBIGINT expectedKey = 7;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);
}

BOOST_AUTO_TEST_CASE(LinkedHashTableDefaultCreation_test)
{
	// default cst. HashInteger as default Hash function
	// and everything set to zero even array size and list size.
	LinkedHashTable<UBIGINT, string> test;

	// need to reserve memory.
	test.map(10);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");
	test.insert(6, "Bob");
	test.insert(7, "Len");
	test.insert(8, "Claude");
	test.insert(9, "Damien");
	test.insert(10, "Guillaume");

	string name = test.search(7)->Data();
	UBIGINT key = test.search(7)->Key();

	string expectedName = "Len";
	UBIGINT expectedKey = 7;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);
}

BOOST_AUTO_TEST_CASE(LinkedHashTableMapUnmapAfterNonDefaultCreation_test)
{
	LinkedHashTable<UBIGINT, string> test(10, HashInteger);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");
	test.insert(6, "Bob");
	test.insert(7, "Len");
	test.insert(8, "Claude");
	test.insert(9, "Damien");
	test.insert(10, "Guillaume");

	UBIGINT size = test.Pairs();

	BOOST_CHECK(size == 10);

	string name = test.search(7)->Data();
	UBIGINT key = test.search(7)->Key();

	string expectedName = "Len";
	UBIGINT expectedKey = 7;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	test.unmap();

	// re-map for 5.
	test.map(5);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");

	size = test.Pairs();

	BOOST_CHECK(size == 5);

	name = test.search(1)->Data();
	key = test.search(1)->Key();

	expectedName = "Julien";
	expectedKey  = 1;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(5)->Data();
	key  = test.search(5)->Key();

	expectedName = "Cathy";
	expectedKey  = 5;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);
}

BOOST_AUTO_TEST_CASE(LinkedHashTableMultipleMapUnmap_test)
{
	// Check stability.
	LinkedHashTable<UBIGINT, string> test(10, HashInteger);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");
	test.insert(6, "Bob");
	test.insert(7, "Len");
	test.insert(8, "Claude");
	test.insert(9, "Damien");
	test.insert(10, "Guillaume");

	UBIGINT size = test.Pairs();

	BOOST_CHECK(size == 10);

	string name = test.search(7)->Data();
	UBIGINT key = test.search(7)->Key();

	string expectedName = "Len";
	UBIGINT expectedKey = 7;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	test.unmap();

	// re-map for 5.
	test.map(5);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");

	size = test.Pairs();

	BOOST_CHECK(size == 5);

	name = test.search(1)->Data();
	key = test.search(1)->Key();

	expectedName = "Julien";
	expectedKey  = 1;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(5)->Data();
	key  = test.search(5)->Key();

	expectedName = "Cathy";
	expectedKey  = 5;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	test.unmap();
	test.map(100);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");

	size = test.Pairs();

	BOOST_CHECK(size == 5);

	test.unmap();
	test.map(1000);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");

	size = test.Pairs();

	BOOST_CHECK(size == 5);
}

BOOST_AUTO_TEST_CASE(LinkedHashTableRemove_test)
{
	LinkedHashTable<UBIGINT, string> test(10, HashInteger);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");
	test.insert(6, "Bob");
	test.insert(7, "Len");
	test.insert(8, "Claude");
	test.insert(9, "Damien");
	test.insert(10, "Guillaume");

	string name = test.search(7)->Data();
	UBIGINT key = test.search(7)->Key();

	string expectedName = "Len";
	UBIGINT expectedKey = 7;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	// beginning size.
	UBIGINT oldSize = test.Pairs(); // return the number of pairs which exist = 10 (!= from size which is the reserved allocation).
	BOOST_CHECK(oldSize == 10);

	// Remove tests.
	BBOOL removeChecker = false;
	removeChecker = test.remove(1); // will remove Julien, Noooooooooooooooooo !!!! ;-) I'll be back.

	// Check if found and removed.
	BOOST_CHECK(removeChecker == true);

	removeChecker = test.remove(1); // should return false now because nothing will be found.

	BOOST_CHECK(removeChecker == false);

	// second test for watching if number of cells has been decreased.
	UBIGINT newSize = test.Pairs(); // should be equal to 9 now.
	BOOST_CHECK(newSize == 9);

	// third test, check if search of the removed one returned NULL.
	HashCell<UBIGINT, string>* equalNull = test.search(1);
	BOOST_CHECK(equalNull == NULL);

	// Some re-checks
	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(3)->Data();
	key  = test.search(3)->Key();

	expectedName = "Steve";
	expectedKey  = 3;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(9)->Data();
	key  = test.search(9)->Key();

	expectedName = "Damien";
	expectedKey  = 9;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);
}

BOOST_AUTO_TEST_CASE(LinkedHashTableGetFirst_test)
{
	LinkedHashTable<UBIGINT, string> test(10, HashInteger);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");
	test.insert(6, "Bob");
	test.insert(7, "Len");
	test.insert(8, "Claude");
	test.insert(9, "Damien");
	test.insert(10, "Guillaume");

	string name = test.search(7)->Data();
	UBIGINT key = test.search(7)->Key();

	string expectedName = "Len";
	UBIGINT expectedKey = 7;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	// Grab the first hash cell
	// and check content for being sure that
	// this is the first.
	name = test.begin()->Data();
	key  = test.begin()->Key();

	// Guillaume has been put as the first by the hashing
	// function, so check if it is the case.
	expectedName = "Guillaume";
	expectedKey  = 10;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);
}

BOOST_AUTO_TEST_CASE(LinkedHashTableGetLast_test)
{
	LinkedHashTable<UBIGINT, string> test(10, HashInteger);

	test.insert(1, "Julien");
	test.insert(2, "Carl");
	test.insert(3, "Steve");
	test.insert(4, "Trevor");
	test.insert(5, "Cathy");
	test.insert(6, "Bob");
	test.insert(7, "Len");
	test.insert(8, "Claude");
	test.insert(9, "Damien");
	test.insert(10, "Guillaume");

	string name = test.search(7)->Data();
	UBIGINT key = test.search(7)->Key();

	string expectedName = "Len";
	UBIGINT expectedKey = 7;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(4)->Data();
	key  = test.search(4)->Key();

	expectedName = "Trevor";
	expectedKey  = 4;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	name = test.search(8)->Data();
	key  = test.search(8)->Key();

	expectedName = "Claude";
	expectedKey  = 8;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);

	// Grab the last hash cell
	// and check content for being sure that
	// this is the last.
	name = test.last()->Data();
	key  = test.last()->Key();

	// Damien has been put as the last by the hashing
	// function, so check if it is the case.
	expectedName = "Damien";
	expectedKey  = 9;

	BOOST_CHECK(name == expectedName);
	BOOST_CHECK(key == expectedKey);
}
