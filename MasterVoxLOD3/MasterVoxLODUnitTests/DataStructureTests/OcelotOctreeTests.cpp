#ifndef DEF_OCELOTOCTREE_H
#include "..\..\MasterVoxLOD3\DataStructures\OcelotOctree.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;

VVOID drawOctValue(TreeNode<UBIGINT, UBIGINT>* t)
{
	cout << endl << t->Data() << endl ;
}

BOOST_AUTO_TEST_CASE(OcelotOctreeCreation_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new OcelotOctree<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new OcelotOctree<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new OcelotOctree<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new OcelotOctree<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new OcelotOctree<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new OcelotOctree<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new OcelotOctree<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new OcelotOctree<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new OcelotOctree<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new OcelotOctree<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new OcelotOctree<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new OcelotOctree<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new OcelotOctree<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 13); // count the root.

	//root->applyToTree(drawOctValue);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(OcelotOctreeAddUpTo8_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new OcelotOctree<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new OcelotOctree<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new OcelotOctree<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new OcelotOctree<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new OcelotOctree<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new OcelotOctree<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new OcelotOctree<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new OcelotOctree<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new OcelotOctree<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new OcelotOctree<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new OcelotOctree<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new OcelotOctree<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new OcelotOctree<UBIGINT, UBIGINT>(12);

	TreeNode<UBIGINT, UBIGINT>* upTo81   = new OcelotOctree<UBIGINT, UBIGINT>(881);
	TreeNode<UBIGINT, UBIGINT>* upTo82   = new OcelotOctree<UBIGINT, UBIGINT>(882);
	TreeNode<UBIGINT, UBIGINT>* upTo83   = new OcelotOctree<UBIGINT, UBIGINT>(883);
	TreeNode<UBIGINT, UBIGINT>* upTo84   = new OcelotOctree<UBIGINT, UBIGINT>(884);
	TreeNode<UBIGINT, UBIGINT>* upTo85   = new OcelotOctree<UBIGINT, UBIGINT>(885);
	TreeNode<UBIGINT, UBIGINT>* upTo86   = new OcelotOctree<UBIGINT, UBIGINT>(886);
	TreeNode<UBIGINT, UBIGINT>* upTo87   = new OcelotOctree<UBIGINT, UBIGINT>(887);
	TreeNode<UBIGINT, UBIGINT>* upTo88   = new OcelotOctree<UBIGINT, UBIGINT>(888);

	toAdd->addChild(toAdd1); // 1 child
	toAdd->addChild(toAdd2); // 2 child
	toAdd->addChild(upTo81); // 3 child
	toAdd->addChild(upTo82); // 4 child
	toAdd->addChild(upTo83); // 5 child
	toAdd->addChild(upTo84); // 6 child
	toAdd->addChild(upTo85); // 7 child
	toAdd->addChild(upTo86); // 8 child
	toAdd->addChild(upTo87); // will not be inserted, so deleted
	toAdd->addChild(upTo88); // will not be inserted, so deleted.
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 19); // count the root.

	//root->applyToTree(drawOctValue);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(OcelotOctreeAddReplace_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new OcelotOctree<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new OcelotOctree<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new OcelotOctree<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new OcelotOctree<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new OcelotOctree<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new OcelotOctree<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new OcelotOctree<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new OcelotOctree<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new OcelotOctree<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new OcelotOctree<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new OcelotOctree<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new OcelotOctree<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new OcelotOctree<UBIGINT, UBIGINT>(12);

	TreeNode<UBIGINT, UBIGINT>* upTo81   = new OcelotOctree<UBIGINT, UBIGINT>(881);
	TreeNode<UBIGINT, UBIGINT>* upTo82   = new OcelotOctree<UBIGINT, UBIGINT>(882);
	TreeNode<UBIGINT, UBIGINT>* upTo83   = new OcelotOctree<UBIGINT, UBIGINT>(883);
	TreeNode<UBIGINT, UBIGINT>* upTo84   = new OcelotOctree<UBIGINT, UBIGINT>(884);
	TreeNode<UBIGINT, UBIGINT>* upTo85   = new OcelotOctree<UBIGINT, UBIGINT>(885);
	TreeNode<UBIGINT, UBIGINT>* upTo86   = new OcelotOctree<UBIGINT, UBIGINT>(886);
	TreeNode<UBIGINT, UBIGINT>* upTo87   = new OcelotOctree<UBIGINT, UBIGINT>(887);
	TreeNode<UBIGINT, UBIGINT>* upTo88   = new OcelotOctree<UBIGINT, UBIGINT>(888);

	upTo87->addChild(upTo88);

	toAdd->addChild(toAdd1); // 1 child
	toAdd->addChild(toAdd2); // 2 child
	toAdd->addChild(upTo81); // 3 child
	toAdd->addChild(upTo82); // 4 child
	toAdd->addChild(upTo83); // 5 child
	toAdd->addChild(upTo84); // 6 child
	toAdd->addChild(upTo85); // 7 child
	toAdd->addChild(upTo86); // 8 child
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	root->addChild(upTo87, 0); // will replace toAdd with upTo87 and will concatenate their children, so toAdd1, toAdd2, upTo81, upTo82, upTo83, upTo84, upTo85, upTo86 and upTo88

	// BUT, because there is already 8 children in the first list to concatenate, the ones in the remplacing node will deleted avoiding to go over 8 children.

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 19); // count the root.

	//root->applyToTree(drawOctValue);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(OcelotOctreeAddReplace2_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new OcelotOctree<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new OcelotOctree<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new OcelotOctree<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new OcelotOctree<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new OcelotOctree<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new OcelotOctree<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new OcelotOctree<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new OcelotOctree<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new OcelotOctree<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new OcelotOctree<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new OcelotOctree<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new OcelotOctree<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new OcelotOctree<UBIGINT, UBIGINT>(12);

	TreeNode<UBIGINT, UBIGINT>* upTo81   = new OcelotOctree<UBIGINT, UBIGINT>(881);
	TreeNode<UBIGINT, UBIGINT>* upTo82   = new OcelotOctree<UBIGINT, UBIGINT>(882);
	TreeNode<UBIGINT, UBIGINT>* upTo83   = new OcelotOctree<UBIGINT, UBIGINT>(883);
	TreeNode<UBIGINT, UBIGINT>* upTo84   = new OcelotOctree<UBIGINT, UBIGINT>(884);
	TreeNode<UBIGINT, UBIGINT>* upTo85   = new OcelotOctree<UBIGINT, UBIGINT>(885);
	TreeNode<UBIGINT, UBIGINT>* upTo86   = new OcelotOctree<UBIGINT, UBIGINT>(886);
	TreeNode<UBIGINT, UBIGINT>* upTo87   = new OcelotOctree<UBIGINT, UBIGINT>(887);
	TreeNode<UBIGINT, UBIGINT>* upTo88   = new OcelotOctree<UBIGINT, UBIGINT>(888);

	upTo87->addChild(upTo88);

	toAdd->addChild(toAdd1); // 1 child
	toAdd->addChild(toAdd2); // 2 child
	toAdd->addChild(upTo81); // 3 child
	toAdd->addChild(upTo82); // 4 child
	toAdd->addChild(upTo83); // 5 child
	toAdd->addChild(upTo84); // 6 child
	toAdd->addChild(upTo85); // 7 child
	toAdd->addChild(upTo86); // 8 child
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	root->addChild(upTo87, 1); // will replace toAdd3 with upTo87 and will concatenate their children, so toAdd4, toAdd5, upTo86, and upTo88

	// This time, enough spaces are present when concatenating, so count + 1 (upTo88) which was previously deleted.

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 20); // count the root.

	//root->applyToTree(drawOctValue);

	cout << endl << endl;

	DeletePointer(root);
}
