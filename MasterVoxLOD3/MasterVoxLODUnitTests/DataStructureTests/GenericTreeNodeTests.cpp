#ifndef DEF_LINKED2LIST_H
#include "..\..\MasterVoxLOD3\DataStructures\TreeNode.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;

VVOID drawValue(TreeNode<UBIGINT, UBIGINT>* t)
{
	cout << endl << t->Data() << endl ;
}

TreeNode<UBIGINT, UBIGINT>* searchMethod(TreeNode<UBIGINT, UBIGINT>* toTest, UBIGINT toSearch)
{
	if(toTest->Data() == toSearch)
		return toTest;

	return NULL;
}

BOOST_AUTO_TEST_CASE(TreeNodeCreation_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 13); // count the root.

	//root->applyToTree(drawValue);

	cout << endl << endl;

	DeletePointer(root); // No memory leaks during recursion (do not delete parent but only children or it will kill the recursivity).
}

BOOST_AUTO_TEST_CASE(TreeNodeAddChildIndex_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	TreeNode<UBIGINT, UBIGINT>* newToAddByIndex = new TreeNode<UBIGINT, UBIGINT>(92);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	root->addChild(newToAddByIndex, 2); // will replace toAdd7 containing 8 and keep toAdd7's children safe. (the new one does not have children so only toAdd7's children will be present,
																										   // but in the case where the new one has children, both list will be concatenated
																										   // by placing new children at the back keeping the insertion order).
	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 13); // still the same count because replace the one to the index.

	//root->applyToTree(drawValue);

	cout << endl << endl;

	DeletePointer(root); // No memory leaks during recursion (do not delete parent but only children or it will kill the recursivity).
}

BOOST_AUTO_TEST_CASE(TreeNodeAddChildIndexWithChildren_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	TreeNode<UBIGINT, UBIGINT>* newToAddByIndex  = new TreeNode<UBIGINT, UBIGINT>(92);
	TreeNode<UBIGINT, UBIGINT>* newToAddByIndex1 = new TreeNode<UBIGINT, UBIGINT>(93);
	TreeNode<UBIGINT, UBIGINT>* newToAddByIndex2 = new TreeNode<UBIGINT, UBIGINT>(94);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	newToAddByIndex->addChild(newToAddByIndex1);
	newToAddByIndex->addChild(newToAddByIndex2);
	root->addChild(newToAddByIndex, 2); // will replace toAdd7 containing 8 and keep toAdd7's children safe. (the new one has children so both list will be concatenated
																										   // by placing new children at the back keeping the insertion order).
	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 15); // two more from children of the new one added by index.

	//root->applyToTree(drawValue);

	cout << endl << endl;

	DeletePointer(root); // No memory leaks during recursion (do not delete parent but only children or it will kill the recursivity).
}

BOOST_AUTO_TEST_CASE(TreeNodeRemoveBack_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	root->removeLastChild();

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 11); // will remove toAdd10, so its unik child as well.

	//root->applyToTree(drawValue);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(TreeNodeRemoveFront_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);   // 2
	toAdd->addChild(toAdd2);   // 3
	root->addChild(toAdd);     // 1

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	root->removeFirstChild();

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 10); // will remove toAdd 1 , so its two children 2 & 3 as well.

	// function which show in console mode the resting values of the resulting tree.
	//root->applyToTree(drawValue);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(TreeNodeRemoveByIndex_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);   // 2
	toAdd->addChild(toAdd2);   // 3
	root->addChild(toAdd);     // 1

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	root->remove(1); // aims second child of the list of the root which contains 4 children (0: toAdd, 1: toAdd3, 2: toAdd7, 3: toAdd10)

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 9); // will remove toAdd3 4 , so its three children 5, 6 & 7 as well.

	// function which show in console mode the resting values of the resulting tree.
	//root->applyToTree(drawValue);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(TreeNodeSearch_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	UBIGINT count = root->subCount();

	BOOST_CHECK(count == 13); // count the root.

	//root->applyToTree(drawValue);

	cout << endl << endl;

	TreeNode<UBIGINT, UBIGINT>::Node* returned = root->search(searchMethod, 7, true);

	UBIGINT expectedResult = 7;

	BOOST_CHECK(returned->Data() == expectedResult);

	DeletePointer(root); // No memory leaks during recursion (do not delete parent but only children or it will kill the recursivity).
}
