#ifndef DEF_TREEITERATOR_H
#include "..\..\MasterVoxLOD3\DataStructures\TreeIterator.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;

VVOID drawValueIter(TreeNode<UBIGINT, UBIGINT>* t)
{
	cout << endl << t->Data() << endl ;
}

BOOST_AUTO_TEST_CASE(TreeIteratorCreation_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	// Tree iterator initialization.
	// Node that every node can be passed
	// instead of the root, so in these cases,
	// call the function backToRoot and the iterator
	// will return automatically to the root.
	TreeIterator<UBIGINT, UBIGINT> iter(root);

	UBIGINT rootValue = iter.currData();

	BOOST_CHECK(rootValue == 0);

	// order Leaf to Root.
	//iter.postOrder(root, drawValueIter);

	cout << endl << endl;

	// order Root to Leaf
	//iter.preOrder(root, drawValueIter);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(TreeIteratorBackToRoot_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	TreeIterator<UBIGINT, UBIGINT> iter(toAdd4); // this is a leaf which is passed as current node of the iter.

	UBIGINT rootValue = iter.currData();

	BOOST_CHECK(rootValue == 5); // check the curr value, should be 5.

	iter.backToRoot(); // Go back to root.

	rootValue = iter.currData(); // refresh the current value.

	BOOST_CHECK(rootValue == 0); // check if root value 0.

	cout << endl << endl;

	// check if the list is still the same.
	//iter.preOrder(iter.CurrNode(), drawValueIter);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(TreeIteratorChildrenIter_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	TreeIterator<UBIGINT, UBIGINT> iter(root);

	UBIGINT rootValue = iter.currData();

	BOOST_CHECK(rootValue == 0); 

	cout << endl << endl;

	// iterate through the children of the current node if any.
	UBIGINT counter = 0;

	iter.resetIterator();
	for(iter.firstChild(); iter.childValid(); iter.nextChild(), counter++)
	{
		//cout << endl << iter.childData() << endl;

		// check the four root's children values 
		if(counter == 0)
			BOOST_CHECK(iter.childData() == 1);
		if(counter == 1)
			BOOST_CHECK(iter.childData() == 4);
		if(counter == 2)
			BOOST_CHECK(iter.childData() == 8);
		if(counter == 3)
			BOOST_CHECK(iter.childData() == 11);
	}

	// check if the list is still the same.
	//iter.preOrder(iter.CurrNode(), drawValueIter);

	cout << endl << endl;

	DeletePointer(root);
}

BOOST_AUTO_TEST_CASE(TreeIteratorMoves_test)
{
	TreeNode<UBIGINT, UBIGINT>* root    = new TreeNode<UBIGINT, UBIGINT>(0);
	TreeNode<UBIGINT, UBIGINT>* toAdd   = new TreeNode<UBIGINT, UBIGINT>(1);
	TreeNode<UBIGINT, UBIGINT>* toAdd1  = new TreeNode<UBIGINT, UBIGINT>(2);
	TreeNode<UBIGINT, UBIGINT>* toAdd2  = new TreeNode<UBIGINT, UBIGINT>(3);
	TreeNode<UBIGINT, UBIGINT>* toAdd3  = new TreeNode<UBIGINT, UBIGINT>(4);
	TreeNode<UBIGINT, UBIGINT>* toAdd4  = new TreeNode<UBIGINT, UBIGINT>(5);
	TreeNode<UBIGINT, UBIGINT>* toAdd5  = new TreeNode<UBIGINT, UBIGINT>(6);
	TreeNode<UBIGINT, UBIGINT>* toAdd6  = new TreeNode<UBIGINT, UBIGINT>(7);
	TreeNode<UBIGINT, UBIGINT>* toAdd7  = new TreeNode<UBIGINT, UBIGINT>(8);
	TreeNode<UBIGINT, UBIGINT>* toAdd8  = new TreeNode<UBIGINT, UBIGINT>(9);
	TreeNode<UBIGINT, UBIGINT>* toAdd9  = new TreeNode<UBIGINT, UBIGINT>(10);
	TreeNode<UBIGINT, UBIGINT>* toAdd10 = new TreeNode<UBIGINT, UBIGINT>(11);
	TreeNode<UBIGINT, UBIGINT>* toAdd11 = new TreeNode<UBIGINT, UBIGINT>(12);

	toAdd->addChild(toAdd1);
	toAdd->addChild(toAdd2);
	root->addChild(toAdd);

	toAdd3->addChild(toAdd4);
	toAdd3->addChild(toAdd5);
	toAdd3->addChild(toAdd6);
	root->addChild(toAdd3);

	toAdd7->addChild(toAdd8);
	toAdd7->addChild(toAdd9);
	root->addChild(toAdd7);

	toAdd10->addChild(toAdd11);
	root->addChild(toAdd10);

	TreeIterator<UBIGINT, UBIGINT> iter(toAdd4); // this is a leaf which is passed as current node of the iter.

	UBIGINT rootValue = iter.currData();

	BOOST_CHECK(rootValue == 5); // check the curr value, should be 5.

	iter.backToRoot(); // Go back to root.

	rootValue = iter.currData(); // refresh the current value.

	BOOST_CHECK(rootValue == 0); // check if root value 0.

	// move on level down.
	iter.levelDOWN(); 
	
	// so because the first child of the current node is aimed automatically, one level down will reach this first child.
	// thus, the current node will pass from 0 (root) to 1 (toAdd).
	rootValue = iter.currData();

	BOOST_CHECK(rootValue == 1); // check if first root's child.

	// therefore, we can change of children pointed before to go one level down by going one sibling left or right.
	iter.backToRoot();
	iter.nextChild();
	iter.levelDOWN();

	rootValue = iter.currData();

	BOOST_CHECK(rootValue == 4); // check if second root's child.

	iter.backToRoot();
	iter.lastChild(); // will aim toAdd10, fourth root's child (last one).
	iter.levelDOWN();

	rootValue = iter.currData();

	BOOST_CHECK(rootValue == 11); // check if fourth root's child.

	iter.backToRoot();
	iter.lastChild();
	iter.prevChild(); // will reach the third root's child.
	iter.levelDOWN();

	rootValue = iter.currData();

	BOOST_CHECK(rootValue == 8); // check if third root's child.

	iter.levelUP(); // will go back to root.

	rootValue = iter.currData();

	BOOST_CHECK(rootValue == 0); // check if fourth root's child.

	cout << endl << endl;

	// check if the list is still the same.
	//iter.preOrder(iter.CurrNode(), drawValueIter);

	cout << endl << endl;

	DeletePointer(root);
}
