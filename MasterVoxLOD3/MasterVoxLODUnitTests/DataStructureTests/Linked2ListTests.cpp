#ifndef DEF_LINKED2LIST_H
#include "..\..\MasterVoxLOD3\DataStructures\Linked2List.h"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;

BOOST_AUTO_TEST_CASE(Linked2ListCreation_test)
{
	Linked2List<UBIGINT> test;

	for(UBIGINT i = 0; i < 12; i++)
		test.putBACK(i);

	UBIGINT counter = 0;
	List2Iterator<UBIGINT> iter = test.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		BOOST_CHECK(iter.data() == counter);
	}
}

BOOST_AUTO_TEST_CASE(NoMemLeakPtrType_test)
{
	Linked2List<UBIGINT*> test;

	for(UBIGINT i = 0; i < 12; i++)
	{
		UBIGINT* temp = new UBIGINT(i);
		test.putBACK(temp);
	}

	UBIGINT counter = 0;
	List2Iterator<UBIGINT*> iter = test.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		UBIGINT* toCheckNDestroy = iter.data();
		BOOST_CHECK(*toCheckNDestroy == counter);

		// Delete internal object pointers.
		DeletePointer(toCheckNDestroy);
	}
}

BOOST_AUTO_TEST_CASE(Linked2ListPutFront_test)
{
	Linked2List<UBIGINT> test;

	for(UBIGINT i = 0; i < 12; i++)
		test.putFRONT(i);

	// Inversion of content.
	UBIGINT counter = 11;
	List2Iterator<UBIGINT> iter = test.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter--)
	{
		BOOST_CHECK(iter.data() == counter);
	}
}

BOOST_AUTO_TEST_CASE(Linked2ListInsertToIterPos_test)
{
	Linked2List<UBIGINT> test;

	for(UBIGINT i = 0; i < 12; i++)
		test.putBACK(i);

	// Check if properly set.
	UBIGINT counter = 0;
	List2Iterator<UBIGINT> iter = test.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		BOOST_CHECK(iter.data() == counter);
	}

	// insert in the list.
	counter = 0;
	UBIGINT indexInsert = 6;
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		if(counter == indexInsert)
			test.insert(iter, 92); // new value which will be inserted between the previously neighbor nodes.
	}

	// Check again.
	counter = 0;
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		if(counter == indexInsert + 1)
			BOOST_CHECK(iter.data() == 92);
		else if(counter > indexInsert + 1)
			BOOST_CHECK(iter.data() == counter - 1);
		else
			BOOST_CHECK(iter.data() == counter);
	}
}

BOOST_AUTO_TEST_CASE(Linked2ListRemoveToIterPos_test)
{
	Linked2List<UBIGINT> test;

	for(UBIGINT i = 0; i < 12; i++)
		test.putBACK(i);

	UBIGINT counter = 0;
	List2Iterator<UBIGINT> iter = test.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		BOOST_CHECK(iter.data() == counter);
	}

	// remove in the list.
	counter = 0;
	UBIGINT indexInsert = 3;
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		if(counter == indexInsert)
			test.remove(iter); // remove node entirely. (pass from 12 cells to 11 cells, so a jump from 2 to 4 in UBIGINT values).
	}

	// Check again.
	counter = 0;
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		if(counter >= indexInsert)
			BOOST_CHECK(iter.data() == counter + 1);
		else
			BOOST_CHECK(iter.data() == counter);
	}
}

BOOST_AUTO_TEST_CASE(Linked2ListRemoveBack_test)
{
	Linked2List<UBIGINT> test;

	for(UBIGINT i = 0; i < 12; i++)
		test.putBACK(i);

	UBIGINT counter = 0;
	List2Iterator<UBIGINT> iter = test.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		BOOST_CHECK(iter.data() == counter);
	}

	// remove in the list.
	test.removeBACK();

	// Check again.
	counter = 0;
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		BOOST_CHECK(iter.data() == counter);
	}
}

BOOST_AUTO_TEST_CASE(Linked2ListRemoveFront_test)
{
	Linked2List<UBIGINT> test;

	for(UBIGINT i = 0; i < 12; i++)
		test.putBACK(i);

	UBIGINT counter = 0;
	List2Iterator<UBIGINT> iter = test.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		BOOST_CHECK(iter.data() == counter);
	}

	// remove in the list.
	test.removeFRONT();

	// Check again.
	counter = 1;
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		BOOST_CHECK(iter.data() == counter);
	}
}
