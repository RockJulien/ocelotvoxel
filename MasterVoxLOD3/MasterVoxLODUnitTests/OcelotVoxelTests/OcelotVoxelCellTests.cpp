#ifndef DEF_OCELOTVOXELCELL_H
#include "..\..\MasterVoxLOD3\Marching Cubes\OcelotVoxelCell.cpp"
#include "..\..\MasterVoxLOD3\Marching Cubes\OcelotVoxelCorner.cpp"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;
using namespace Ocelot;

BOOST_AUTO_TEST_CASE(OcelotVoxelCellCreation_test)
{
	OcelotVoxelCell test(-0xF, 0xF, 0x2, -0x5, 0xA, 0xE, -0x1, -0x8);

	FFLOAT isoVal = test.IsoValue();

	BOOST_CHECK(isoVal == 0.0f);

	for(UBIGINT currSamp = 0; currSamp < (UBIGINT)test.VoxelSamples().size(); currSamp++)
	{
		if(currSamp == 0)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isNegative() == true);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == -0xF);
		}
		else if(currSamp == 1)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isNegative() == false);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == 0xF);
		}
		else if(currSamp == 2)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isPositive() == true);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == 0x2);
		}
		else if(currSamp == 3)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isPositive() == false);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == -0x5);
		}
		else if(currSamp == 4)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isNegative() == false);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == 0xA);
		}
		else if(currSamp == 5)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isPositive() == true);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == 0xE);
		}
		else if(currSamp == 6)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isNegative() == true);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == -0x1);
		}
		else if(currSamp == 7)
		{
			BOOST_CHECK(test.VoxelSamples()[currSamp].isNegative() == true);
			BOOST_CHECK(test.VoxelSamples()[currSamp].Sample() == -0x8);
		}
	}
}

BOOST_AUTO_TEST_CASE(OcelotVoxelCellConcatenation_test)
{
	OcelotVoxelCell test(-127, -127, 112, -112, 90, 50, -10, -90);

	// simply takes sign bit of every samples for then concatenating them
	// in a unsigned 8 bits value representing the class index of the 
	// MC first look-up table.
	UCCHAR index = test.concatenateSamples();

	UCCHAR expectedResult = 203; // equal to 0b11001011

	BOOST_CHECK(index == expectedResult);
}

BOOST_AUTO_TEST_CASE(OcelotVoxelCellConcatenationWithIsoValue_test)
{
	OcelotVoxelCell test(-127, -127, 112, -112, 90, 50, -10, -90);

	// simply takes sign bit of every samples for then concatenating them
	// in a unsigned 8 bits value representing the class index of the 
	// MC first look-up table.
	UCCHAR index = test.concatenateSamplesIsoValueSensitive();

	// For now should be equivalent to the non-iso-value sensitive
	// function.
	UCCHAR expectedResult = 203; // equal to 0b11001011

	BOOST_CHECK(index == expectedResult);

	// Because only negative values will influence the computation,
	// the iso-value must be negative as well for influencing the 
	// computation.
	test.SetIsoValue(-20.0f); // will skip the -10 sign bit.

	index = test.concatenateSamplesIsoValueSensitive();

	expectedResult = 139;

	BOOST_CHECK(index == expectedResult);
}
