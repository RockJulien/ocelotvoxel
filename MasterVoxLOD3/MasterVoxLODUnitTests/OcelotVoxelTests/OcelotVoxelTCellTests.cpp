#ifndef DEF_OCELOTVOXELCELL_H
#include "..\..\MasterVoxLOD3\Transition Voxels\OcelotVoxelTCell.cpp"
#endif

#include <boost/test/unit_test.hpp>

using namespace std;
using namespace Ocelot;

BOOST_AUTO_TEST_CASE(OcelotVoxelTCellCreation_test)
{
	OcelotVoxelTCell test(-127, 56, 127, -56, 39, 20, -80, -100, 10);

	FFLOAT isoVal = test.IsoValue();

	BOOST_CHECK(isoVal == 0.0f);

	for(UBIGINT currSamp = 0; currSamp < (UBIGINT)test.TCellSamples().size(); currSamp++)
	{
		if(currSamp == 0)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isNegative() == true);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == -127);
		}
		else if(currSamp == 1)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isNegative() == false);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == 56);
		}
		else if(currSamp == 2)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isPositive() == true);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == 127);
		}
		else if(currSamp == 3)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isPositive() == false);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == -56);
		}
		else if(currSamp == 4)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isNegative() == false);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == 39);
		}
		else if(currSamp == 5)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isPositive() == true);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == 20);
		}
		else if(currSamp == 6)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isNegative() == true);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == -80);
		}
		else if(currSamp == 7)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isNegative() == true);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == -100);
		}
		else if(currSamp == 8)
		{
			BOOST_CHECK(test.TCellSamples()[currSamp].isPositive() == true);
			BOOST_CHECK(test.TCellSamples()[currSamp].Sample() == 10);
		}
	}
}

BOOST_AUTO_TEST_CASE(OcelotVoxelTCellConcatenation_test)
{
	OcelotVoxelTCell test(-127, 56, 127, -56, 39, 20, -80, -100, 10);

	// simply check if samples are negative and add to the futur
	// class index the appropriate weight for mapping from 0 to 511
	// the fourth Look-up table in Transition Voxels.
	USMALLINT index = test.concatenateSamples();
	                                                                  //MSB     LSB
	// sign bit binary temp val ordered in spiral fashion and equal to 0b011001001 (9-bits).
	USMALLINT expectedResult = 201; // equal to 0x0080 + 0x0040 + 0x0008 + 0x0001.

	BOOST_CHECK(index == expectedResult);
}
