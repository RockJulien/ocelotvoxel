#ifndef DEF_STRINGHELPERS_H
//#include "..\..\MasterVoxLOD\StringHelper\STRINGHelpers.h"
#include "..\..\MasterVoxLOD3\StringHelper\STRINGHelpers.cpp"
/*#include "..\..\MasterVoxLOD\Maths\Vector2.h"
#include "..\..\MasterVoxLOD\Maths\Vector2.cpp"
#include "..\..\MasterVoxLOD\Maths\Vector3.h"
#include "..\..\MasterVoxLOD\Maths\Vector3.cpp"
#include "..\..\MasterVoxLOD\Maths\Vector4.h"
#include "..\..\MasterVoxLOD\Maths\Vector4.cpp"
#include "..\..\MasterVoxLOD\Maths\Matrix2x2.h"
#include "..\..\MasterVoxLOD\Maths\Matrix2x2.cpp"
#include "..\..\MasterVoxLOD\Maths\Matrix3x3.h"
#include "..\..\MasterVoxLOD\Maths\Matrix3x3.cpp"
#include "..\..\MasterVoxLOD\Maths\Matrix4x4.h"
#include "..\..\MasterVoxLOD\Maths\Matrix4x4.cpp"
#include "..\..\MasterVoxLOD\Maths\Quaternion.h"
#include "..\..\MasterVoxLOD\Maths\Quaternion.cpp"*/
#endif

#include "boost/test/unit_test.hpp"

using namespace std;

BOOST_AUTO_TEST_CASE(splitWithDelimiter_test)
{
	STRING  testStr = "Il etait une fois un pinguin qui se lavait partout";
	LSSTRING result;

	// test function
	splitWithDelimiter(testStr, result, ' ');

	BOOST_CHECK((result.Size() != 0) && (result.Size() == 10));

	List2Iterator<STRING> iter = result.iterator();
	iter.begin();

	STRING expectedResult1 = "Il";
	STRING expectedResult2 = "etait";
	STRING expectedResult3 = "une";
	STRING expectedResult4 = "fois";
	STRING expectedResult5 = "un";
	STRING expectedResult6 = "pinguin";
	STRING expectedResult7 = "qui";
	STRING expectedResult8 = "se";
	STRING expectedResult9 = "lavait";
	STRING expectedResult10 = "partout";
	BOOST_CHECK(iter.data() == expectedResult1);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult2);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult3);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult4);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult5);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult6);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult7);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult8);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult9);
	iter.next();
	BOOST_CHECK(iter.data() == expectedResult10);
}

BOOST_AUTO_TEST_CASE(eraseBlank_test)
{
	STRING  testStr = "Il etait une fois un pinguin qui se lavait partout";

	// test function
	eraseBlanks(testStr);

	STRING expectedResult = "Iletaitunefoisunpinguinquiselavaitpartout";
	BOOST_CHECK(testStr == expectedResult);
}

BOOST_AUTO_TEST_CASE(toUString_test)
{
	CCHAR* toConv     = "Julien";
	STRING toConv2    = "Johnny";
	WSTRING returned;
	WSTRING returned2;
	WSTRING expectedResult  = L"Julien";
	WSTRING expectedResult2 = L"Johnny";

	toUString(toConv, returned);
	toUString(toConv2, returned2);

	BOOST_CHECK(returned == expectedResult);
	BOOST_CHECK(returned2 == expectedResult2);
}

BOOST_AUTO_TEST_CASE(toAString_test)
{
	WCHAR* toConv      = L"Julien";
	WSTRING toConv2    = L"Johnny";
	STRING returned;
	STRING returned2;
	STRING expectedResult  = "Julien";
	STRING expectedResult2 = "Johnny";

	toAString(toConv, returned);
	toAString(toConv2, returned2);

	BOOST_CHECK(returned == expectedResult);
	BOOST_CHECK(returned2 == expectedResult2);
}

BOOST_AUTO_TEST_CASE(toBool_test)
{
	CCHAR* charBool  = "true";
	WCHAR* wcharBool = L"false";
	STRING strBool   = "FALSE";
	WSTRING wstrBool = L"TRUE";

	BBOOL expectedResultChar  = true;
	BBOOL expectedResultWChar = false;
	BBOOL expectedResultStr   = false;
	BBOOL expectedResultWStr  = true;

	BBOOL resChar;
	BBOOL resWChar;
	BBOOL resStr;
	BBOOL resWStr;

	resChar  = toBool(charBool);
	resWChar = toBool(wcharBool);
	resStr   = toBool(strBool);
	resWStr  = toBool(wstrBool);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toFloat_test)
{
	CCHAR* charFloat  = "1.456";
	WCHAR* wcharFloat = L"105.4596";
	STRING strFloat   = "10.2";
	WSTRING wstrFloat = L"13.586398";

	FFLOAT expectedResultChar  = 1.456f;
	FFLOAT expectedResultWChar = 105.4596f;
	FFLOAT expectedResultStr   = 10.2f;
	FFLOAT expectedResultWStr  = 13.586398f;

	FFLOAT resChar;
	FFLOAT resWChar;
	FFLOAT resStr;
	FFLOAT resWStr;

	resChar  = toFloat(charFloat);
	resWChar = toFloat(wcharFloat);
	resStr   = toFloat(strFloat);
	resWStr  = toFloat(wstrFloat);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toSInt_test)
{
	CCHAR* charSInt  = "16";
	WCHAR* wcharSInt = L"1002";
	STRING strSInt   = "89";
	WSTRING wstrSInt = L"150";

	BIGINT expectedResultChar  = 16;
	BIGINT expectedResultWChar = 1002;
	BIGINT expectedResultStr   = 89;
	BIGINT expectedResultWStr  = 150;

	BIGINT resChar;
	BIGINT resWChar;
	BIGINT resStr;
	BIGINT resWStr;

	resChar  = toSInt(charSInt);
	resWChar = toSInt(wcharSInt);
	resStr   = toSInt(strSInt);
	resWStr  = toSInt(wstrSInt);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toUInt_test)
{
	CCHAR* charSInt  = "28";
	WCHAR* wcharSInt = L"2569";
	STRING strSInt   = "19";
	WSTRING wstrSInt = L"640";

	UBIGINT expectedResultChar  = 28;
	UBIGINT expectedResultWChar = 2569;
	UBIGINT expectedResultStr   = 19;
	UBIGINT expectedResultWStr  = 640;

	UBIGINT resChar;
	UBIGINT resWChar;
	UBIGINT resStr;
	UBIGINT resWStr;

	resChar  = toUInt(charSInt);
	resWChar = toUInt(wcharSInt);
	resStr   = toUInt(strSInt);
	resWStr  = toUInt(wstrSInt);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(Hex2UInt_test)
{
	CCHAR* charSInt  = "0x2A";
	WCHAR* wcharSInt = L"0xFF";
	STRING strSInt   = "0x506";
	WSTRING wstrSInt = L"0x7CB";

	UBIGINT expectedResultChar  = 42;
	UBIGINT expectedResultWChar = 255;
	UBIGINT expectedResultStr   = 1286;
	UBIGINT expectedResultWStr  = 1995;

	UBIGINT resChar;
	UBIGINT resWChar;
	UBIGINT resStr;
	UBIGINT resWStr;

	resChar  = Hex2UInt(charSInt, 2);
	resWChar = Hex2UInt(wcharSInt, 2);
	resStr   = Hex2UInt(strSInt, 3);
	resWStr  = Hex2UInt(wstrSInt, 3);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toVector2D_test)
{
	CCHAR* charSInt  = "2.0 4.0";
	WCHAR* wcharSInt = L"5.25 6.89";
	STRING strSInt   = "4.62 2.59";
	WSTRING wstrSInt = L"10.75 22.33";

	Vector2 expectedResultChar(2.0f, 4.0f);
	Vector2 expectedResultWChar(5.25f, 6.89f);
	Vector2 expectedResultStr(4.62f, 2.59f);
	Vector2 expectedResultWStr(10.75f, 22.33f);

	Vector2 resChar;
	Vector2 resWChar;
	Vector2 resStr;
	Vector2 resWStr;

	toVector2D(charSInt, resChar);
	toVector2D(wcharSInt, resWChar);
	toVector2D(strSInt, resStr);
	toVector2D(wstrSInt, resWStr);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toVector3D_test)
{
	CCHAR* charSInt  = "2.0 4.0 10.0";
	WCHAR* wcharSInt = L"5.25 6.89 15.24";
	STRING strSInt   = "4.62 2.59 25.6";
	WSTRING wstrSInt = L"10.75 22.33 32.5";

	Vector3 expectedResultChar(2.0f, 4.0f, 10.0f);
	Vector3 expectedResultWChar(5.25f, 6.89f, 15.24f);
	Vector3 expectedResultStr(4.62f, 2.59f, 25.6f);
	Vector3 expectedResultWStr(10.75f, 22.33f, 32.5f);

	Vector3 resChar;
	Vector3 resWChar;
	Vector3 resStr;
	Vector3 resWStr;

	toVector3D(charSInt, resChar);
	toVector3D(wcharSInt, resWChar);
	toVector3D(strSInt, resStr);
	toVector3D(wstrSInt, resWStr);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toVector4D_test)
{
	CCHAR* charSInt  = "2.0 4.0 10.0 12.5";
	WCHAR* wcharSInt = L"5.25 6.89 15.24 30.0";
	STRING strSInt   = "4.62 2.59 25.6 27.2";
	WSTRING wstrSInt = L"10.75 22.33 32.5 52.8";

	Vector4 expectedResultChar(2.0f, 4.0f, 10.0f, 12.5f);
	Vector4 expectedResultWChar(5.25f, 6.89f, 15.24f, 30.0f);
	Vector4 expectedResultStr(4.62f, 2.59f, 25.6f, 27.2f);
	Vector4 expectedResultWStr(10.75f, 22.33f, 32.5f, 52.8f);

	Vector4 resChar;
	Vector4 resWChar;
	Vector4 resStr;
	Vector4 resWStr;

	toVector4D(charSInt, resChar);
	toVector4D(wcharSInt, resWChar);
	toVector4D(strSInt, resStr);
	toVector4D(wstrSInt, resWStr);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toMatrix4x4CM_test)
{
	CCHAR* charSInt  = "2.0 5.0 13.0 17.0 4.0 10.0 14.0 18.0 10.0 11.0 15.0 19.0 12.5 12.0 16.0 20.0";
	WCHAR* wcharSInt = L"5.25 21.0 25.0 29.0 6.89 22.0 26.0 30.0 15.24 23.0 27.0 31.0 30.0 24.0 28.0 32.0";
	STRING strSInt   = "4.62 33.0 37.0 41.0 2.59 34.0 38.0 42.0 25.6 35.0 39.0 43.0 27.2 36.0 40.0 44.0";
	WSTRING wstrSInt = L"10.75 45.0 49.0 53.0 22.33 46.0 50.0 54.0 32.5 47.0 51.0 55.0 52.8 48.0 52.0 56.0";

	Matrix4x4 expectedResultChar(2.0f, 4.0f, 10.0f, 12.5f,
								 5.0f, 10.0f, 11.0f, 12.0f,
								 13.0f, 14.0f, 15.0f, 16.0f,
								 17.0f, 18.0f, 19.0f, 20.0f);
	Matrix4x4 expectedResultWChar(5.25f, 6.89f, 15.24f, 30.0f,
								  21.0f, 22.0f, 23.0f, 24.0f,
								  25.0f, 26.0f, 27.0f, 28.0f,
								  29.0f, 30.0f, 31.0f, 32.0f);
	Matrix4x4 expectedResultStr(4.62f, 2.59f, 25.6f, 27.2f,
							    33.0f, 34.0f, 35.0f, 36.0f,
								37.0f, 38.0f, 39.0f, 40.0f,
								41.0f, 42.0f, 43.0f, 44.0f);
	Matrix4x4 expectedResultWStr(10.75f, 22.33f, 32.5f, 52.8f,
								 45.0f, 46.0f, 47.0f, 48.0f,
								 49.0f, 50.0f, 51.0f, 52.0f,
								 53.0f, 54.0f, 55.0f, 56.0f);

	Matrix4x4 resChar;
	Matrix4x4 resWChar;
	Matrix4x4 resStr;
	Matrix4x4 resWStr;

	toMatrix4x4CM(charSInt, resChar);
	toMatrix4x4CM(wcharSInt, resWChar);
	toMatrix4x4CM(strSInt, resStr);
	toMatrix4x4CM(wstrSInt, resWStr);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toMatrix4x4RM_test)
{
	CCHAR* charSInt  = "2.0 4.0 10.0 12.5 5.0 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0";
	WCHAR* wcharSInt = L"5.25 6.89 15.24 30.0 21.0 22.0 23.0 24.0 25.0 26.0 27.0 28.0 29.0 30.0 31.0 32.0";
	STRING strSInt   = "4.62 2.59 25.6 27.2 33.0 34.0 35.0 36.0 37.0 38.0 39.0 40.0 41.0 42.0 43.0 44.0";
	WSTRING wstrSInt = L"10.75 22.33 32.5 52.8 45.0 46.0 47.0 48.0 49.0 50.0 51.0 52.0 53.0 54.0 55.0 56.0";

	Matrix4x4 expectedResultChar(2.0f, 4.0f, 10.0f, 12.5f,
								 5.0f, 10.0f, 11.0f, 12.0f,
								 13.0f, 14.0f, 15.0f, 16.0f,
								 17.0f, 18.0f, 19.0f, 20.0f);
	Matrix4x4 expectedResultWChar(5.25f, 6.89f, 15.24f, 30.0f,
								  21.0f, 22.0f, 23.0f, 24.0f,
								  25.0f, 26.0f, 27.0f, 28.0f,
								  29.0f, 30.0f, 31.0f, 32.0f);
	Matrix4x4 expectedResultStr(4.62f, 2.59f, 25.6f, 27.2f,
							    33.0f, 34.0f, 35.0f, 36.0f,
								37.0f, 38.0f, 39.0f, 40.0f,
								41.0f, 42.0f, 43.0f, 44.0f);
	Matrix4x4 expectedResultWStr(10.75f, 22.33f, 32.5f, 52.8f,
								 45.0f, 46.0f, 47.0f, 48.0f,
								 49.0f, 50.0f, 51.0f, 52.0f,
								 53.0f, 54.0f, 55.0f, 56.0f);

	Matrix4x4 resChar;
	Matrix4x4 resWChar;
	Matrix4x4 resStr;
	Matrix4x4 resWStr;

	toMatrix4x4RM(charSInt, resChar);
	toMatrix4x4RM(wcharSInt, resWChar);
	toMatrix4x4RM(strSInt, resStr);
	toMatrix4x4RM(wstrSInt, resWStr);

	BOOST_CHECK(resChar == expectedResultChar);
	BOOST_CHECK(resWChar == expectedResultWChar);
	BOOST_CHECK(resStr == expectedResultStr);
	BOOST_CHECK(resWStr == expectedResultWStr);
}

BOOST_AUTO_TEST_CASE(toBoolList_test)
{
	CCHAR*  charBool  = "TRUE FALSE FALSE TRUE FALSE TRUE TRUE"; //7
	WCHAR*  wcharBool = L"false false true false true false false true true"; //9
	STRING  strBool   = "0 1 0 0 0 1 false true 1 0 true TRUE"; //12
	WSTRING wstrBool  = L"TRUE false FALSE true"; //4

	Array1D<BBOOL> expectedResultCharBoolList(7);
	expectedResultCharBoolList.insert(true, 0);
	expectedResultCharBoolList.insert(false, 1);
	expectedResultCharBoolList.insert(false, 2);
	expectedResultCharBoolList.insert(true, 3);
	expectedResultCharBoolList.insert(false, 4);
	expectedResultCharBoolList.insert(true, 5);
	expectedResultCharBoolList.insert(true, 6);
	Array1D<BBOOL> expectedResultWCharBoolList(9);
	expectedResultWCharBoolList.insert(false, 0);
	expectedResultWCharBoolList.insert(false, 1);
	expectedResultWCharBoolList.insert(true, 2);
	expectedResultWCharBoolList.insert(false, 3);
	expectedResultWCharBoolList.insert(true, 4);
	expectedResultWCharBoolList.insert(false, 5);
	expectedResultWCharBoolList.insert(false, 6);
	expectedResultWCharBoolList.insert(true, 7);
	expectedResultWCharBoolList.insert(true, 8);
	Array1D<BBOOL> expectedResultStringBoolList(12);
	expectedResultStringBoolList.insert(false, 0);
	expectedResultStringBoolList.insert(true, 1);
	expectedResultStringBoolList.insert(false, 2);
	expectedResultStringBoolList.insert(false, 3);
	expectedResultStringBoolList.insert(false, 4);
	expectedResultStringBoolList.insert(true, 5);
	expectedResultStringBoolList.insert(false, 6);
	expectedResultStringBoolList.insert(true, 7);
	expectedResultStringBoolList.insert(true, 8);
	expectedResultStringBoolList.insert(false, 9);
	expectedResultStringBoolList.insert(true, 10);
	expectedResultStringBoolList.insert(true, 11);
	Array1D<BBOOL> expectedResultWStringBoolList(4);
	expectedResultWStringBoolList.insert(true, 0);
	expectedResultWStringBoolList.insert(false, 1);
	expectedResultWStringBoolList.insert(false, 2);
	expectedResultWStringBoolList.insert(true, 3);

	Array1D<BBOOL> resCharBoolList(7);
	Array1D<BBOOL> resWCharBoolList(9);
	Array1D<BBOOL> resStringBoolList(12);
	Array1D<BBOOL> resWStringBoolList(4);

	toBoolList(charBool, resCharBoolList);
	toBoolList(wcharBool, resWCharBoolList);
	toBoolList(strBool, resStringBoolList);
	toBoolList(wstrBool, resWStringBoolList);

	BOOST_CHECK(resCharBoolList.Size() == 7);
	BOOST_CHECK(resWCharBoolList.Size() == 9);
	BOOST_CHECK(resStringBoolList.Size() == 12);
	BOOST_CHECK(resWStringBoolList.Size() == 4);
	BOOST_CHECK(resCharBoolList == expectedResultCharBoolList);
	BOOST_CHECK(resWCharBoolList == expectedResultWCharBoolList);
	BOOST_CHECK(resStringBoolList == expectedResultStringBoolList);
	BOOST_CHECK(resWStringBoolList == expectedResultWStringBoolList);
}

BOOST_AUTO_TEST_CASE(toFloatList_test)
{
	CCHAR*  charFloat  = "2.0 3.0 4.0 5.0 6.0 7.0 8.0"; //7
	WCHAR*  wcharFloat = L"10.2 12.0 13.0 14.0 15.0 16.0 17.0 18.9 19.8"; //9
	STRING  strFloat   = "0.6 1.23 5.21 7.23 3.28 150.0 160.7 15.5 16.5 17.5 18.5 19.5"; //12
	WSTRING wstrFloat  = L"6.325 7.856 9.785 12.569"; //4

	Array1D<FFLOAT> expectedResultCharFloatList(7);
	expectedResultCharFloatList.insert(2.0f, 0);
	expectedResultCharFloatList.insert(3.0f, 1);
	expectedResultCharFloatList.insert(4.0f, 2);
	expectedResultCharFloatList.insert(5.0f, 3);
	expectedResultCharFloatList.insert(6.0f, 4);
	expectedResultCharFloatList.insert(7.0f, 5);
	expectedResultCharFloatList.insert(8.0f, 6);
	Array1D<FFLOAT> expectedResultWCharFloatList(9);
	expectedResultWCharFloatList.insert(10.2f, 0);
	expectedResultWCharFloatList.insert(12.0f, 1);
	expectedResultWCharFloatList.insert(13.0f, 2);
	expectedResultWCharFloatList.insert(14.0f, 3);
	expectedResultWCharFloatList.insert(15.0f, 4);
	expectedResultWCharFloatList.insert(16.0f, 5);
	expectedResultWCharFloatList.insert(17.0f, 6);
	expectedResultWCharFloatList.insert(18.9f, 7);
	expectedResultWCharFloatList.insert(19.8f, 8);
	Array1D<FFLOAT> expectedResultStringFloatList(12);
	expectedResultStringFloatList.insert(0.6f, 0);
	expectedResultStringFloatList.insert(1.23f, 1);
	expectedResultStringFloatList.insert(5.21f, 2);
	expectedResultStringFloatList.insert(7.23f, 3);
	expectedResultStringFloatList.insert(3.28f, 4);
	expectedResultStringFloatList.insert(150.0f, 5);
	expectedResultStringFloatList.insert(160.7f, 6);
	expectedResultStringFloatList.insert(15.5f, 7);
	expectedResultStringFloatList.insert(16.5f, 8);
	expectedResultStringFloatList.insert(17.5f, 9);
	expectedResultStringFloatList.insert(18.5f, 10);
	expectedResultStringFloatList.insert(19.5f, 11);
	Array1D<FFLOAT> expectedResultWStringFloatList(4);
	expectedResultWStringFloatList.insert(6.325f, 0);
	expectedResultWStringFloatList.insert(7.856f, 1);
	expectedResultWStringFloatList.insert(9.785f, 2);
	expectedResultWStringFloatList.insert(12.569f, 3);

	Array1D<FFLOAT> resCharFloatList(7);
	Array1D<FFLOAT> resWCharFloatList(9);
	Array1D<FFLOAT> resStringFloatList(12);
	Array1D<FFLOAT> resWStringFloatList(4);

	toFloatList(charFloat, resCharFloatList);
	toFloatList(wcharFloat, resWCharFloatList);
	toFloatList(strFloat, resStringFloatList);
	toFloatList(wstrFloat, resWStringFloatList);

	BOOST_CHECK(resCharFloatList.Size() == 7);
	BOOST_CHECK(resWCharFloatList.Size() == 9);
	BOOST_CHECK(resStringFloatList.Size() == 12);
	BOOST_CHECK(resWStringFloatList.Size() == 4);
	BOOST_CHECK(resCharFloatList == expectedResultCharFloatList);
	BOOST_CHECK(resWCharFloatList == expectedResultWCharFloatList);
	BOOST_CHECK(resStringFloatList == expectedResultStringFloatList);
	BOOST_CHECK(resWStringFloatList == expectedResultWStringFloatList);
}

BOOST_AUTO_TEST_CASE(toSIntList_test)
{
	CCHAR*  charSInt  = "2 3 4 5 6 7 8"; //7
	WCHAR*  wcharSInt = L"10 12 13 14 15 16 17 18 19"; //9
	STRING  strSInt   = "0 1 5 7 3 150 160 15 16 17 18 19"; //12
	WSTRING wstrSInt  = L"6325 7856 9785 12569"; //4

	Array1D<BIGINT> expectedResultCharSIntList(7);
	expectedResultCharSIntList.insert(2, 0);
	expectedResultCharSIntList.insert(3, 1);
	expectedResultCharSIntList.insert(4, 2);
	expectedResultCharSIntList.insert(5, 3);
	expectedResultCharSIntList.insert(6, 4);
	expectedResultCharSIntList.insert(7, 5);
	expectedResultCharSIntList.insert(8, 6);
	Array1D<BIGINT> expectedResultWCharSIntList(9);
	expectedResultWCharSIntList.insert(10, 0);
	expectedResultWCharSIntList.insert(12, 1);
	expectedResultWCharSIntList.insert(13, 2);
	expectedResultWCharSIntList.insert(14, 3);
	expectedResultWCharSIntList.insert(15, 4);
	expectedResultWCharSIntList.insert(16, 5);
	expectedResultWCharSIntList.insert(17, 6);
	expectedResultWCharSIntList.insert(18, 7);
	expectedResultWCharSIntList.insert(19, 8);
	Array1D<BIGINT> expectedResultStringSIntList(12);
	expectedResultStringSIntList.insert(0, 0);
	expectedResultStringSIntList.insert(1, 1);
	expectedResultStringSIntList.insert(5, 2);
	expectedResultStringSIntList.insert(7, 3);
	expectedResultStringSIntList.insert(3, 4);
	expectedResultStringSIntList.insert(150, 5);
	expectedResultStringSIntList.insert(160, 6);
	expectedResultStringSIntList.insert(15, 7);
	expectedResultStringSIntList.insert(16, 8);
	expectedResultStringSIntList.insert(17, 9);
	expectedResultStringSIntList.insert(18, 10);
	expectedResultStringSIntList.insert(19, 11);
	Array1D<BIGINT> expectedResultWStringSIntList(4);
	expectedResultWStringSIntList.insert(6325, 0);
	expectedResultWStringSIntList.insert(7856, 1);
	expectedResultWStringSIntList.insert(9785, 2);
	expectedResultWStringSIntList.insert(12569, 3);

	Array1D<BIGINT> resCharSIntList(7);
	Array1D<BIGINT> resWCharSIntList(9);
	Array1D<BIGINT> resStringSIntList(12);
	Array1D<BIGINT> resWStringSIntList(4);

	toSIntList(charSInt, resCharSIntList);
	toSIntList(wcharSInt, resWCharSIntList);
	toSIntList(strSInt, resStringSIntList);
	toSIntList(wstrSInt, resWStringSIntList);

	BOOST_CHECK(resCharSIntList.Size() == 7);
	BOOST_CHECK(resWCharSIntList.Size() == 9);
	BOOST_CHECK(resStringSIntList.Size() == 12);
	BOOST_CHECK(resWStringSIntList.Size() == 4);
	BOOST_CHECK(resCharSIntList == expectedResultCharSIntList);
	BOOST_CHECK(resWCharSIntList == expectedResultWCharSIntList);
	BOOST_CHECK(resStringSIntList == expectedResultStringSIntList);
	BOOST_CHECK(resWStringSIntList == expectedResultWStringSIntList);
}

BOOST_AUTO_TEST_CASE(toUIntList_test)
{
	CCHAR*  charUInt  = "2 3 4 5 6 7 8"; //7
	WCHAR*  wcharUInt = L"10 12 13 14 15 16 17 18 19"; //9
	STRING  strUInt   = "0 1 5 7 3 150 160 15 16 17 18 19"; //12
	WSTRING wstrUInt  = L"6325 7856 9785 12569"; //4

	Array1D<UBIGINT> expectedResultCharUIntList(7);
	expectedResultCharUIntList.insert(2, 0);
	expectedResultCharUIntList.insert(3, 1);
	expectedResultCharUIntList.insert(4, 2);
	expectedResultCharUIntList.insert(5, 3);
	expectedResultCharUIntList.insert(6, 4);
	expectedResultCharUIntList.insert(7, 5);
	expectedResultCharUIntList.insert(8, 6);
	Array1D<UBIGINT> expectedResultWCharUIntList(9);
	expectedResultWCharUIntList.insert(10, 0);
	expectedResultWCharUIntList.insert(12, 1);
	expectedResultWCharUIntList.insert(13, 2);
	expectedResultWCharUIntList.insert(14, 3);
	expectedResultWCharUIntList.insert(15, 4);
	expectedResultWCharUIntList.insert(16, 5);
	expectedResultWCharUIntList.insert(17, 6);
	expectedResultWCharUIntList.insert(18, 7);
	expectedResultWCharUIntList.insert(19, 8);
	Array1D<UBIGINT> expectedResultStringUIntList(12);
	expectedResultStringUIntList.insert(0, 0);
	expectedResultStringUIntList.insert(1, 1);
	expectedResultStringUIntList.insert(5, 2);
	expectedResultStringUIntList.insert(7, 3);
	expectedResultStringUIntList.insert(3, 4);
	expectedResultStringUIntList.insert(150, 5);
	expectedResultStringUIntList.insert(160, 6);
	expectedResultStringUIntList.insert(15, 7);
	expectedResultStringUIntList.insert(16, 8);
	expectedResultStringUIntList.insert(17, 9);
	expectedResultStringUIntList.insert(18, 10);
	expectedResultStringUIntList.insert(19, 11);
	Array1D<UBIGINT> expectedResultWStringUIntList(4);
	expectedResultWStringUIntList.insert(6325, 0);
	expectedResultWStringUIntList.insert(7856, 1);
	expectedResultWStringUIntList.insert(9785, 2);
	expectedResultWStringUIntList.insert(12569, 3);

	Array1D<UBIGINT> resCharUIntList(7);
	Array1D<UBIGINT> resWCharUIntList(9);
	Array1D<UBIGINT> resStringUIntList(12);
	Array1D<UBIGINT> resWStringUIntList(4);

	toUIntList(charUInt, resCharUIntList);
	toUIntList(wcharUInt, resWCharUIntList);
	toUIntList(strUInt, resStringUIntList);
	toUIntList(wstrUInt, resWStringUIntList);

	BOOST_CHECK(resCharUIntList.Size() == 7);
	BOOST_CHECK(resWCharUIntList.Size() == 9);
	BOOST_CHECK(resStringUIntList.Size() == 12);
	BOOST_CHECK(resWStringUIntList.Size() == 4);
	BOOST_CHECK(resCharUIntList == expectedResultCharUIntList);
	BOOST_CHECK(resWCharUIntList == expectedResultWCharUIntList);
	BOOST_CHECK(resStringUIntList == expectedResultStringUIntList);
	BOOST_CHECK(resWStringUIntList == expectedResultWStringUIntList);
}

BOOST_AUTO_TEST_CASE(toVector2DList_test)
{
	CCHAR*  charVector2D  = "2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0"; //4
	WCHAR*  wcharVector2D = L"10.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0"; //5
	STRING  strVector2D   = "0.6 1.2 5.4 7.9 3.1 150.0 160.0 15.0 16.0 17.0 18.0 19.0"; //6
	WSTRING wstrVector2D  = L"6325.0 7856.0 97.85 125.69"; //2

	Array1D<Vector2> expectedResultCharVector2DList(4);
	Vector2 nb1(2.0f, 3.0f);
	Vector2 nb2(4.0f, 5.0f);
	Vector2 nb3(6.0f, 7.0f);
	Vector2 nb4(8.0f, 9.0f);
	expectedResultCharVector2DList.insert(nb1, 0);
	expectedResultCharVector2DList.insert(nb2, 1);
	expectedResultCharVector2DList.insert(nb3, 2);
	expectedResultCharVector2DList.insert(nb4, 3);
	Array1D<Vector2> expectedResultWCharVector2DList(5);
	Vector2 nb5(10.0f, 12.0f);
	Vector2 nb6(13.0f, 14.0f);
	Vector2 nb7(15.0f, 16.0f);
	Vector2 nb8(17.0f, 18.0f);
	Vector2 nb9(19.0f, 20.0f);
	expectedResultWCharVector2DList.insert(nb5, 0);
	expectedResultWCharVector2DList.insert(nb6, 1);
	expectedResultWCharVector2DList.insert(nb7, 2);
	expectedResultWCharVector2DList.insert(nb8, 3);
	expectedResultWCharVector2DList.insert(nb9, 4);
	Array1D<Vector2> expectedResultStringVector2DList(6);
	Vector2 nb10(0.6f, 1.2f);
	Vector2 nb12(5.4f, 7.9f);
	Vector2 nb13(3.1f, 150.0f);
	Vector2 nb14(160.0f, 15.0f);
	Vector2 nb15(16.0f, 17.0f);
	Vector2 nb16(18.0f, 19.0f);
	expectedResultStringVector2DList.insert(nb10, 0);
	expectedResultStringVector2DList.insert(nb12, 1);
	expectedResultStringVector2DList.insert(nb13, 2);
	expectedResultStringVector2DList.insert(nb14, 3);
	expectedResultStringVector2DList.insert(nb15, 4);
	expectedResultStringVector2DList.insert(nb16, 5);
	Array1D<Vector2> expectedResultWStringVector2DList(2);
	Vector2 nb17(6325.0f, 7856.0f);
	Vector2 nb18(97.85f, 125.69f);
	expectedResultWStringVector2DList.insert(nb17, 0);
	expectedResultWStringVector2DList.insert(nb18, 1);

	Array1D<Vector2> resCharVector2DList(4);
	Array1D<Vector2> resWCharVector2DList(5);
	Array1D<Vector2> resStringVector2DList(6);
	Array1D<Vector2> resWStringVector2DList(2);

	toVector2DList(charVector2D, resCharVector2DList);
	toVector2DList(wcharVector2D, resWCharVector2DList);
	toVector2DList(strVector2D, resStringVector2DList);
	toVector2DList(wstrVector2D, resWStringVector2DList);

	BOOST_CHECK(resCharVector2DList.Size() == 4);
	BOOST_CHECK(resWCharVector2DList.Size() == 5);
	BOOST_CHECK(resStringVector2DList.Size() == 6);
	BOOST_CHECK(resWStringVector2DList.Size() == 2);
	BOOST_CHECK(resCharVector2DList == expectedResultCharVector2DList);
	BOOST_CHECK(resWCharVector2DList == expectedResultWCharVector2DList);
	BOOST_CHECK(resStringVector2DList == expectedResultStringVector2DList);
	BOOST_CHECK(resWStringVector2DList == expectedResultWStringVector2DList);
}

BOOST_AUTO_TEST_CASE(toVector3DList_test)
{
	CCHAR*  charVector3D  = "2.0 3.0 1.0 4.0 5.0 1.0 6.0 7.0 1.0 8.0 9.0 1.0"; //4
	WCHAR*  wcharVector3D = L"10.0 12.0 1.0 13.0 14.0 1.0 15.0 16.0 1.0 17.0 18.0 1.0 19.0 20.0 1.0"; //5
	STRING  strVector3D   = "0.6 1.2 1.0 5.4 7.9 1.0 3.1 150.0 1.0 160.0 15.0 1.0 16.0 17.0 1.0 18.0 19.0 1.0"; //6
	WSTRING wstrVector3D  = L"6325.0 7856.0 1.0 97.85 125.69 1.0"; //2

	Array1D<Vector3> expectedResultCharVector3DList(4);
	Vector3 nb1(2.0f, 3.0f, 1.0f);
	Vector3 nb2(4.0f, 5.0f, 1.0f);
	Vector3 nb3(6.0f, 7.0f, 1.0f);
	Vector3 nb4(8.0f, 9.0f, 1.0f);
	expectedResultCharVector3DList.insert(nb1, 0);
	expectedResultCharVector3DList.insert(nb2, 1);
	expectedResultCharVector3DList.insert(nb3, 2);
	expectedResultCharVector3DList.insert(nb4, 3);
	Array1D<Vector3> expectedResultWCharVector3DList(5);
	Vector3 nb5(10.0f, 12.0f, 1.0f);
	Vector3 nb6(13.0f, 14.0f, 1.0f);
	Vector3 nb7(15.0f, 16.0f, 1.0f);
	Vector3 nb8(17.0f, 18.0f, 1.0f);
	Vector3 nb9(19.0f, 20.0f, 1.0f);
	expectedResultWCharVector3DList.insert(nb5, 0);
	expectedResultWCharVector3DList.insert(nb6, 1);
	expectedResultWCharVector3DList.insert(nb7, 2);
	expectedResultWCharVector3DList.insert(nb8, 3);
	expectedResultWCharVector3DList.insert(nb9, 4);
	Array1D<Vector3> expectedResultStringVector3DList(6);
	Vector3 nb10(0.6f, 1.2f, 1.0f);
	Vector3 nb12(5.4f, 7.9f, 1.0f);
	Vector3 nb13(3.1f, 150.0f, 1.0f);
	Vector3 nb14(160.0f, 15.0f, 1.0f);
	Vector3 nb15(16.0f, 17.0f, 1.0f);
	Vector3 nb16(18.0f, 19.0f, 1.0f);
	expectedResultStringVector3DList.insert(nb10, 0);
	expectedResultStringVector3DList.insert(nb12, 1);
	expectedResultStringVector3DList.insert(nb13, 2);
	expectedResultStringVector3DList.insert(nb14, 3);
	expectedResultStringVector3DList.insert(nb15, 4);
	expectedResultStringVector3DList.insert(nb16, 5);
	Array1D<Vector3> expectedResultWStringVector3DList(2);
	Vector3 nb17(6325.0f, 7856.0f, 1.0f);
	Vector3 nb18(97.85f, 125.69f, 1.0f);
	expectedResultWStringVector3DList.insert(nb17, 0);
	expectedResultWStringVector3DList.insert(nb18, 1);

	Array1D<Vector3> resCharVector3DList(4);
	Array1D<Vector3> resWCharVector3DList(5);
	Array1D<Vector3> resStringVector3DList(6);
	Array1D<Vector3> resWStringVector3DList(2);

	toVector3DList(charVector3D, resCharVector3DList);
	toVector3DList(wcharVector3D, resWCharVector3DList);
	toVector3DList(strVector3D, resStringVector3DList);
	toVector3DList(wstrVector3D, resWStringVector3DList);

	BOOST_CHECK(resCharVector3DList.Size() == 4);
	BOOST_CHECK(resWCharVector3DList.Size() == 5);
	BOOST_CHECK(resStringVector3DList.Size() == 6);
	BOOST_CHECK(resWStringVector3DList.Size() == 2);
	BOOST_CHECK(resCharVector3DList == expectedResultCharVector3DList);
	BOOST_CHECK(resWCharVector3DList == expectedResultWCharVector3DList);
	BOOST_CHECK(resStringVector3DList == expectedResultStringVector3DList);
	BOOST_CHECK(resWStringVector3DList == expectedResultWStringVector3DList);
}

BOOST_AUTO_TEST_CASE(toVector4DList_test)
{
	CCHAR*  charVector4D  = "2.0 3.0 1.0 0.0 4.0 5.0 1.0 1.0 6.0 7.0 1.0 0.0 8.0 9.0 1.0 1.0"; //4
	WCHAR*  wcharVector4D = L"10.0 12.0 1.0 0.0 13.0 14.0 1.0 1.0 15.0 16.0 1.0 0.0 17.0 18.0 1.0 1.0 19.0 20.0 1.0 0.0"; //5
	STRING  strVector4D   = "0.6 1.2 1.0 1.0 5.4 7.9 1.0 0.0 3.1 150.0 1.0 1.0 160.0 15.0 1.0 0.0 16.0 17.0 1.0 1.0 18.0 19.0 1.0 0.0"; //6
	WSTRING wstrVector4D  = L"6325.0 7856.0 1.0 1.0 97.85 125.69 1.0"; //2

	Array1D<Vector4> expectedResultCharVector4DList(4);
	Vector4 nb1(2.0f, 3.0f, 1.0f, 0.0f);
	Vector4 nb2(4.0f, 5.0f, 1.0f, 1.0f);
	Vector4 nb3(6.0f, 7.0f, 1.0f, 0.0f);
	Vector4 nb4(8.0f, 9.0f, 1.0f, 1.0f);
	expectedResultCharVector4DList.insert(nb1, 0);
	expectedResultCharVector4DList.insert(nb2, 1);
	expectedResultCharVector4DList.insert(nb3, 2);
	expectedResultCharVector4DList.insert(nb4, 3);
	Array1D<Vector4> expectedResultWCharVector4DList(5);
	Vector4 nb5(10.0f, 12.0f, 1.0f, 0.0f);
	Vector4 nb6(13.0f, 14.0f, 1.0f, 1.0f);
	Vector4 nb7(15.0f, 16.0f, 1.0f, 0.0f);
	Vector4 nb8(17.0f, 18.0f, 1.0f, 1.0f);
	Vector4 nb9(19.0f, 20.0f, 1.0f, 0.0f);
	expectedResultWCharVector4DList.insert(nb5, 0);
	expectedResultWCharVector4DList.insert(nb6, 1);
	expectedResultWCharVector4DList.insert(nb7, 2);
	expectedResultWCharVector4DList.insert(nb8, 3);
	expectedResultWCharVector4DList.insert(nb9, 4);
	Array1D<Vector4> expectedResultStringVector4DList(6);
	Vector4 nb10(0.6f, 1.2f, 1.0f, 1.0f);
	Vector4 nb12(5.4f, 7.9f, 1.0f, 0.0f);
	Vector4 nb13(3.1f, 150.0f, 1.0f, 1.0f);
	Vector4 nb14(160.0f, 15.0f, 1.0f, 0.0f);
	Vector4 nb15(16.0f, 17.0f, 1.0f, 1.0f);
	Vector4 nb16(18.0f, 19.0f, 1.0f, 0.0f);
	expectedResultStringVector4DList.insert(nb10, 0);
	expectedResultStringVector4DList.insert(nb12, 1);
	expectedResultStringVector4DList.insert(nb13, 2);
	expectedResultStringVector4DList.insert(nb14, 3);
	expectedResultStringVector4DList.insert(nb15, 4);
	expectedResultStringVector4DList.insert(nb16, 5);
	Array1D<Vector4> expectedResultWStringVector4DList(2);
	Vector4 nb17(6325.0f, 7856.0f, 1.0f, 1.0f);
	Vector4 nb18(97.85f, 125.69f, 1.0f, 1.0f);
	expectedResultWStringVector4DList.insert(nb17, 0);
	expectedResultWStringVector4DList.insert(nb18, 1);

	Array1D<Vector4> resCharVector4DList(4);
	Array1D<Vector4> resWCharVector4DList(5);
	Array1D<Vector4> resStringVector4DList(6);
	Array1D<Vector4> resWStringVector4DList(2);

	toVector4DList(charVector4D, resCharVector4DList);
	toVector4DList(wcharVector4D, resWCharVector4DList);
	toVector4DList(strVector4D, resStringVector4DList);
	toVector4DList(wstrVector4D, resWStringVector4DList);

	BOOST_CHECK(resCharVector4DList.Size() == 4);
	BOOST_CHECK(resWCharVector4DList.Size() == 5);
	BOOST_CHECK(resStringVector4DList.Size() == 6);
	BOOST_CHECK(resWStringVector4DList.Size() == 2);
	BOOST_CHECK(resCharVector4DList == expectedResultCharVector4DList);
	BOOST_CHECK(resWCharVector4DList == expectedResultWCharVector4DList);
	BOOST_CHECK(resStringVector4DList == expectedResultStringVector4DList);
	BOOST_CHECK(resWStringVector4DList == expectedResultWStringVector4DList);
}

BOOST_AUTO_TEST_CASE(toMatrix4x4CMList_test)
{
	CCHAR*  charMatrix4x4  = "2.0 5.0 13.0 17.0 4.0 10.0 14.0 18.0 10.0 11.0 15.0 19.0 12.5 12.0 16.0 20.0 5.25 21.0 25.0 29.0 6.89 22.0 26.0 30.0 15.24 23.0 27.0 31.0 30.0 24.0 28.0 32.0";      // 2
	WCHAR*  wcharMatrix4x4 = L"4.62 33.0 37.0 41.0 2.59 34.0 38.0 42.0 25.6 35.0 39.0 43.0 27.2 36.0 40.0 44.0 10.75 45.0 49.0 53.0 22.33 46.0 50.0 54.0 32.5 47.0 51.0 55.0 52.8 48.0 52.0 56.0"; // 2
	STRING  strMatrix4x4   = "4.62 33.0 37.0 41.0 2.59 34.0 38.0 42.0 25.6 35.0 39.0 43.0 27.2 36.0 40.0 44.0 10.75 45.0 49.0 53.0 22.33 46.0 50.0 54.0 32.5 47.0 51.0 55.0 52.8 48.0 52.0 56.0";  // 2
	WSTRING wstrMatrix4x4  = L"2.0 5.0 13.0 17.0 4.0 10.0 14.0 18.0 10.0 11.0 15.0 19.0 12.5 12.0 16.0 20.0 5.25 21.0 25.0 29.0 6.89 22.0 26.0 30.0 15.24 23.0 27.0 31.0 30.0 24.0 28.0 32.0";     // 2

	Array1D<Matrix4x4> expectedResultCharMatrix4x4List(2);
	Matrix4x4 nb1(2.0f, 4.0f, 10.0f, 12.5f,
				  5.0f, 10.0f, 11.0f, 12.0f,
				  13.0f, 14.0f, 15.0f, 16.0f,
				  17.0f, 18.0f, 19.0f, 20.0f);
	Matrix4x4 nb2(5.25f, 6.89f, 15.24f, 30.0f,
				  21.0f, 22.0f, 23.0f, 24.0f,
				  25.0f, 26.0f, 27.0f, 28.0f,
				  29.0f, 30.0f, 31.0f, 32.0f);
	expectedResultCharMatrix4x4List.insert(nb1, 0);
	expectedResultCharMatrix4x4List.insert(nb2, 1);
	Array1D<Matrix4x4> expectedResultWCharMatrix4x4List(2);
	Matrix4x4 nb5(4.62f, 2.59f, 25.6f, 27.2f,
				  33.0f, 34.0f, 35.0f, 36.0f,
				  37.0f, 38.0f, 39.0f, 40.0f,
				  41.0f, 42.0f, 43.0f, 44.0f);
	Matrix4x4 nb6(10.75f, 22.33f, 32.5f, 52.8f,
				  45.0f, 46.0f, 47.0f, 48.0f,
				  49.0f, 50.0f, 51.0f, 52.0f,
				  53.0f, 54.0f, 55.0f, 56.0f);
	expectedResultWCharMatrix4x4List.insert(nb5, 0);
	expectedResultWCharMatrix4x4List.insert(nb6, 1);
	Array1D<Matrix4x4> expectedResultStringMatrix4x4List(2);
	Matrix4x4 nb10(4.62f, 2.59f, 25.6f, 27.2f,
				   33.0f, 34.0f, 35.0f, 36.0f,
				   37.0f, 38.0f, 39.0f, 40.0f,
				   41.0f, 42.0f, 43.0f, 44.0f);
	Matrix4x4 nb12(10.75f, 22.33f, 32.5f, 52.8f,
				   45.0f, 46.0f, 47.0f, 48.0f,
				   49.0f, 50.0f, 51.0f, 52.0f,
				   53.0f, 54.0f, 55.0f, 56.0f);
	expectedResultStringMatrix4x4List.insert(nb10, 0);
	expectedResultStringMatrix4x4List.insert(nb12, 1);
	Array1D<Matrix4x4> expectedResultWStringMatrix4x4List(2);
	Matrix4x4 nb17(2.0f, 4.0f, 10.0f, 12.5f,
				   5.0f, 10.0f, 11.0f, 12.0f,
				   13.0f, 14.0f, 15.0f, 16.0f,
				   17.0f, 18.0f, 19.0f, 20.0f);
	Matrix4x4 nb18(5.25f, 6.89f, 15.24f, 30.0f,
				   21.0f, 22.0f, 23.0f, 24.0f,
				   25.0f, 26.0f, 27.0f, 28.0f,
				   29.0f, 30.0f, 31.0f, 32.0f);
	expectedResultWStringMatrix4x4List.insert(nb17, 0);
	expectedResultWStringMatrix4x4List.insert(nb18, 1);

	Array1D<Matrix4x4> resCharMatrix4x4List(2);
	Array1D<Matrix4x4> resWCharMatrix4x4List(2);
	Array1D<Matrix4x4> resStringMatrix4x4List(2);
	Array1D<Matrix4x4> resWStringMatrix4x4List(2);

	toMatrix4x4CMList(charMatrix4x4, resCharMatrix4x4List);
	toMatrix4x4CMList(wcharMatrix4x4, resWCharMatrix4x4List);
	toMatrix4x4CMList(strMatrix4x4, resStringMatrix4x4List);
	toMatrix4x4CMList(wstrMatrix4x4, resWStringMatrix4x4List);

	BOOST_CHECK(resCharMatrix4x4List.Size() == 2);
	BOOST_CHECK(resWCharMatrix4x4List.Size() == 2);
	BOOST_CHECK(resStringMatrix4x4List.Size() == 2);
	BOOST_CHECK(resWStringMatrix4x4List.Size() == 2);
	BOOST_CHECK(resCharMatrix4x4List == expectedResultCharMatrix4x4List);
	BOOST_CHECK(resWCharMatrix4x4List == expectedResultWCharMatrix4x4List);
	BOOST_CHECK(resStringMatrix4x4List == expectedResultStringMatrix4x4List);
	BOOST_CHECK(resWStringMatrix4x4List == expectedResultWStringMatrix4x4List);
}

BOOST_AUTO_TEST_CASE(toMatrix4x4RMList_test)
{
	CCHAR*  charMatrix4x4  = "2.0 4.0 10.0 12.5 5.0 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0 5.25 6.89 15.24 30.0 21.0 22.0 23.0 24.0 25.0 26.0 27.0 28.0 29.0 30.0 31.0 32.0";      // 2
	WCHAR*  wcharMatrix4x4 = L"4.62 2.59 25.6 27.2 33.0 34.0 35.0 36.0 37.0 38.0 39.0 40.0 41.0 42.0 43.0 44.0 10.75 22.33 32.5 52.8 45.0 46.0 47.0 48.0 49.0 50.0 51.0 52.0 53.0 54.0 55.0 56.0"; // 2
	STRING  strMatrix4x4   = "4.62 2.59 25.6 27.2 33.0 34.0 35.0 36.0 37.0 38.0 39.0 40.0 41.0 42.0 43.0 44.0 10.75 22.33 32.5 52.8 45.0 46.0 47.0 48.0 49.0 50.0 51.0 52.0 53.0 54.0 55.0 56.0";  // 2
	WSTRING wstrMatrix4x4  = L"2.0 4.0 10.0 12.5 5.0 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0 5.25 6.89 15.24 30.0 21.0 22.0 23.0 24.0 25.0 26.0 27.0 28.0 29.0 30.0 31.0 32.0";     // 2

	Array1D<Matrix4x4> expectedResultCharMatrix4x4List(2);
	Matrix4x4 nb1(2.0f, 4.0f, 10.0f, 12.5f,
				  5.0f, 10.0f, 11.0f, 12.0f,
				  13.0f, 14.0f, 15.0f, 16.0f,
				  17.0f, 18.0f, 19.0f, 20.0f);
	Matrix4x4 nb2(5.25f, 6.89f, 15.24f, 30.0f,
				  21.0f, 22.0f, 23.0f, 24.0f,
				  25.0f, 26.0f, 27.0f, 28.0f,
				  29.0f, 30.0f, 31.0f, 32.0f);
	expectedResultCharMatrix4x4List.insert(nb1, 0);
	expectedResultCharMatrix4x4List.insert(nb2, 1);
	Array1D<Matrix4x4> expectedResultWCharMatrix4x4List(2);
	Matrix4x4 nb5(4.62f, 2.59f, 25.6f, 27.2f,
				  33.0f, 34.0f, 35.0f, 36.0f,
				  37.0f, 38.0f, 39.0f, 40.0f,
				  41.0f, 42.0f, 43.0f, 44.0f);
	Matrix4x4 nb6(10.75f, 22.33f, 32.5f, 52.8f,
				  45.0f, 46.0f, 47.0f, 48.0f,
				  49.0f, 50.0f, 51.0f, 52.0f,
				  53.0f, 54.0f, 55.0f, 56.0f);
	expectedResultWCharMatrix4x4List.insert(nb5, 0);
	expectedResultWCharMatrix4x4List.insert(nb6, 1);
	Array1D<Matrix4x4> expectedResultStringMatrix4x4List(2);
	Matrix4x4 nb10(4.62f, 2.59f, 25.6f, 27.2f,
				   33.0f, 34.0f, 35.0f, 36.0f,
				   37.0f, 38.0f, 39.0f, 40.0f,
				   41.0f, 42.0f, 43.0f, 44.0f);
	Matrix4x4 nb12(10.75f, 22.33f, 32.5f, 52.8f,
				   45.0f, 46.0f, 47.0f, 48.0f,
				   49.0f, 50.0f, 51.0f, 52.0f,
				   53.0f, 54.0f, 55.0f, 56.0f);
	expectedResultStringMatrix4x4List.insert(nb10, 0);
	expectedResultStringMatrix4x4List.insert(nb12, 1);
	Array1D<Matrix4x4> expectedResultWStringMatrix4x4List(2);
	Matrix4x4 nb17(2.0f, 4.0f, 10.0f, 12.5f,
				   5.0f, 10.0f, 11.0f, 12.0f,
				   13.0f, 14.0f, 15.0f, 16.0f,
				   17.0f, 18.0f, 19.0f, 20.0f);
	Matrix4x4 nb18(5.25f, 6.89f, 15.24f, 30.0f,
				   21.0f, 22.0f, 23.0f, 24.0f,
				   25.0f, 26.0f, 27.0f, 28.0f,
				   29.0f, 30.0f, 31.0f, 32.0f);
	expectedResultWStringMatrix4x4List.insert(nb17, 0);
	expectedResultWStringMatrix4x4List.insert(nb18, 1);

	Array1D<Matrix4x4> resCharMatrix4x4List(2);
	Array1D<Matrix4x4> resWCharMatrix4x4List(2);
	Array1D<Matrix4x4> resStringMatrix4x4List(2);
	Array1D<Matrix4x4> resWStringMatrix4x4List(2);

	toMatrix4x4RMList(charMatrix4x4, resCharMatrix4x4List);
	toMatrix4x4RMList(wcharMatrix4x4, resWCharMatrix4x4List);
	toMatrix4x4RMList(strMatrix4x4, resStringMatrix4x4List);
	toMatrix4x4RMList(wstrMatrix4x4, resWStringMatrix4x4List);

	BOOST_CHECK(resCharMatrix4x4List.Size() == 2);
	BOOST_CHECK(resWCharMatrix4x4List.Size() == 2);
	BOOST_CHECK(resStringMatrix4x4List.Size() == 2);
	BOOST_CHECK(resWStringMatrix4x4List.Size() == 2);
	BOOST_CHECK(resCharMatrix4x4List == expectedResultCharMatrix4x4List);
	BOOST_CHECK(resWCharMatrix4x4List == expectedResultWCharMatrix4x4List);
	BOOST_CHECK(resStringMatrix4x4List == expectedResultStringMatrix4x4List);
	BOOST_CHECK(resWStringMatrix4x4List == expectedResultWStringMatrix4x4List);
}

// Inverse operation.
BOOST_AUTO_TEST_CASE(FloatListToString_test)
{
	Array1D<FFLOAT> floatList(20);
	floatList.insert(2.0f, 0);
	floatList.insert(3.0f, 1);
	floatList.insert(4.0f, 2);
	floatList.insert(5.0f, 3);
	floatList.insert(6.0f, 4);
	floatList.insert(7.0f, 5);
	floatList.insert(8.0f, 6);
	floatList.insert(9.0f, 7);
	floatList.insert(10.0f, 8);
	floatList.insert(11.0f, 9);
	floatList.insert(12.0f, 10);
	floatList.insert(13.0f, 11);
	floatList.insert(14.0f, 12);
	floatList.insert(15.0f, 13);
	floatList.insert(16.0f, 14);
	floatList.insert(17.0f, 15);
	floatList.insert(18.0f, 16);
	floatList.insert(19.0f, 17);
	floatList.insert(20.0f, 18);
	floatList.insert(21.0f, 19);

	WSTRING resFloatList;
	WSTRING expectedWStringFloatList = L"2.000000 3.000000 4.000000 5.000000 6.000000 7.000000 8.000000 9.000000 10.000000 11.000000 12.000000 13.000000 14.000000 15.000000 16.000000 17.000000 18.000000 19.000000 20.000000 21.000000";

	toString(floatList, resFloatList);

	BOOST_CHECK(resFloatList == expectedWStringFloatList);
}

BOOST_AUTO_TEST_CASE(SIntListToString_test)
{
	Array1D<BIGINT> SIntList(20);
	SIntList.insert(2, 0);
	SIntList.insert(3, 1);
	SIntList.insert(4, 2);
	SIntList.insert(-5, 3);
	SIntList.insert(6, 4);
	SIntList.insert(7, 5);
	SIntList.insert(8, 6);
	SIntList.insert(-9, 7);
	SIntList.insert(10, 8);
	SIntList.insert(11, 9);
	SIntList.insert(12, 10);
	SIntList.insert(13, 11);
	SIntList.insert(14, 12);
	SIntList.insert(15, 13);
	SIntList.insert(16, 14);
	SIntList.insert(17, 15);
	SIntList.insert(18, 16);
	SIntList.insert(19, 17);
	SIntList.insert(20, 18);
	SIntList.insert(21, 19);

	WSTRING resSIntList;
	WSTRING expectedWStringSIntList = L"2 3 4 -5 6 7 8 -9 10 11 12 13 14 15 16 17 18 19 20 21";

	toString(SIntList, resSIntList);

	BOOST_CHECK(resSIntList == expectedWStringSIntList);
}

BOOST_AUTO_TEST_CASE(UIntListToString_test)
{
	Array1D<UBIGINT> UIntList(20);
	UIntList.insert(2, 0);
	UIntList.insert(3, 1);
	UIntList.insert(4, 2);
	UIntList.insert(5, 3);
	UIntList.insert(6, 4);
	UIntList.insert(7, 5);
	UIntList.insert(8, 6);
	UIntList.insert(9, 7);
	UIntList.insert(10, 8);
	UIntList.insert(11, 9);
	UIntList.insert(12, 10);
	UIntList.insert(13, 11);
	UIntList.insert(14, 12);
	UIntList.insert(15, 13);
	UIntList.insert(16, 14);
	UIntList.insert(17, 15);
	UIntList.insert(18, 16);
	UIntList.insert(19, 17);
	UIntList.insert(20, 18);
	UIntList.insert(21, 19);

	UBIGINT tab[20];
	tab[0] = 2;
	tab[1] = 3;
	tab[2] = 4;
	tab[3] = 5;
	tab[4] = 6;
	tab[5] = 7;
	tab[6] = 8;
	tab[7] = 9;
	tab[8] = 10;
	tab[9] = 11;
	tab[10] = 12;
	tab[11] = 13;
	tab[12] = 14;
	tab[13] = 15;
	tab[14] = 16;
	tab[15] = 17;
	tab[16] = 18;
	tab[17] = 19;
	tab[18] = 20;
	tab[19] = 21;

	WSTRING resUIntList;
	WSTRING resUIntTab;
	WSTRING expectedWStringUIntList = L"2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21";

	toString(UIntList, resUIntList);
	toString(tab, 20, resUIntTab);

	BOOST_CHECK(resUIntList == expectedWStringUIntList);
	BOOST_CHECK(resUIntTab == expectedWStringUIntList);
}

BOOST_AUTO_TEST_CASE(Vector2DToString_test)
{
	Vector2 vec(5.0f, 50.0f);

	STRING vecStrResult;
	WSTRING vecWStrResult;

	STRING expectedStrResult   = "5.000000 50.000000";
	WSTRING expectedWStrResult = L"5.000000 50.000000";

	toString(vec, vecStrResult);
	toWString(vec, vecWStrResult);

	BOOST_CHECK(vecStrResult == expectedStrResult);
	BOOST_CHECK(vecWStrResult == expectedWStrResult);
}

BOOST_AUTO_TEST_CASE(Vector3DToString_test)
{
	Vector3 vec(10.0f, 150.0f, 350.0f);

	STRING vecStrResult;
	WSTRING vecWStrResult;

	STRING expectedStrResult   = "10.000000 150.000000 350.000000";
	WSTRING expectedWStrResult = L"10.000000 150.000000 350.000000";

	toString(vec, vecStrResult);
	toWString(vec, vecWStrResult);

	BOOST_CHECK(vecStrResult == expectedStrResult);
	BOOST_CHECK(vecWStrResult == expectedWStrResult);
}

BOOST_AUTO_TEST_CASE(Vector4DToString_test)
{
	Vector4 vec(10.0f, 150.0f, 350.0f, 1.0f);

	STRING vecStrResult;
	WSTRING vecWStrResult;

	STRING expectedStrResult   = "10.000000 150.000000 350.000000 1.000000";
	WSTRING expectedWStrResult = L"10.000000 150.000000 350.000000 1.000000";

	toString(vec, vecStrResult);
	toWString(vec, vecWStrResult);

	BOOST_CHECK(vecStrResult == expectedStrResult);
	BOOST_CHECK(vecWStrResult == expectedWStrResult);
}

BOOST_AUTO_TEST_CASE(Matrix4x4ToStringCM_test)
{
	Matrix4x4 mat(1.0f, 0.2f, 0.8f, 1.56f,
				  5.0f, 1.0f, 6.0f, 2.65f,
				  6.89f, 3.0f, 1.0f, 5.0f,
				  3.0f, 4.0f, 5.62f, 1.0f);

	STRING matStrResult;
	WSTRING matWStrResult;

	STRING expectedStrResult   = "1.000000 5.000000 6.890000 3.000000 0.200000 1.000000 3.000000 4.000000 0.800000 6.000000 1.000000 5.620000 1.560000 2.650000 5.000000 1.000000";
	WSTRING expectedWStrResult = L"1.000000 5.000000 6.890000 3.000000 0.200000 1.000000 3.000000 4.000000 0.800000 6.000000 1.000000 5.620000 1.560000 2.650000 5.000000 1.000000";

	toStringCM(mat, matStrResult);
	toWStringCM(mat, matWStrResult);

	BOOST_CHECK(matStrResult == expectedStrResult);
	BOOST_CHECK(matWStrResult == expectedWStrResult);
}

BOOST_AUTO_TEST_CASE(Matrix4x4ToStringRM_test)
{
	Matrix4x4 mat(1.0f, 0.2f, 0.8f, 1.56f,
				  5.0f, 1.0f, 6.0f, 2.65f,
				  6.89f, 3.0f, 1.0f, 5.0f,
				  3.0f, 4.0f, 5.62f, 1.0f);

	STRING matStrResult;
	WSTRING matWStrResult;

	STRING expectedStrResult   = "1.000000 0.200000 0.800000 1.560000 5.000000 1.000000 6.000000 2.650000 6.890000 3.000000 1.000000 5.000000 3.000000 4.000000 5.620000 1.000000";
	WSTRING expectedWStrResult = L"1.000000 0.200000 0.800000 1.560000 5.000000 1.000000 6.000000 2.650000 6.890000 3.000000 1.000000 5.000000 3.000000 4.000000 5.620000 1.000000";

	toStringRM(mat, matStrResult);
	toWStringRM(mat, matWStrResult);

	BOOST_CHECK(matStrResult == expectedStrResult);
	BOOST_CHECK(matWStrResult == expectedWStrResult);
}

BOOST_AUTO_TEST_CASE(IntToString_test)
{
	BIGINT i1  = -6;
	BIGINT i2  = 19;
	UBIGINT i3 = 1;
	UBIGINT i4 = 46;

	STRING sintStrResult;
	WSTRING sintWStrResult;
	STRING uintStrResult;
	WSTRING uintWStrResult;

	STRING expectedSIntStrResult   = "-6";
	WSTRING expectedSIntWStrResult = L"19";
	STRING expectedUIntStrResult   = "1";
	WSTRING expectedUIntWStrResult = L"46";

	toString(i1, sintStrResult);
	toWString(i2, sintWStrResult);
	toString(i3, uintStrResult);
	toWString(i4, uintWStrResult);
	
	BOOST_CHECK(sintStrResult == expectedSIntStrResult);
	BOOST_CHECK(sintWStrResult == expectedSIntWStrResult);
	BOOST_CHECK(uintStrResult == expectedUIntStrResult);
	BOOST_CHECK(uintWStrResult == expectedUIntWStrResult);
}

BOOST_AUTO_TEST_CASE(FloatToString_test)
{
	FFLOAT f1 = 5.0f;
	FFLOAT f2 = 2.63f;

	STRING floatStrResult;
	WSTRING floatWStrResult;

	STRING expectedStrResult   = "5.000000";
	WSTRING expectedWStrResult = L"2.630000";

	toString(f1, floatStrResult);
	toWString(f2, floatWStrResult);
	
	BOOST_CHECK(floatStrResult == expectedStrResult);
	BOOST_CHECK(floatWStrResult == expectedWStrResult);
}

BOOST_AUTO_TEST_CASE(CountNbChar_test)
{
	CCHAR* test1 = "Julien is an awesome programmer :-)";
	WCHAR* test2 = L"12 50 30 27 18 20 563 208 102 3";

	BIGINT resultChar;
	BIGINT resultWChar;

	BIGINT expectedResultChar  = 6;  // count portion of char
	BIGINT expectedResultWChar = 10; // count portion of char

	resultChar  = count(test1);
	resultWChar = count(test2);

	BOOST_CHECK(resultChar == expectedResultChar);
	BOOST_CHECK(resultWChar == expectedResultWChar);
}

BOOST_AUTO_TEST_CASE(CRC_test)
{

	CCHAR* t1   = "Test1";
	WCHAR* t2   = L"Test2";
	STRING t3   = "Nice Test";
	WSTRING t4  = L"Global String";
	CCHAR* t5   = "Trying Line";
	WCHAR* t6   = L"Reticulating Splines";
	STRING t7   = "Youpi jappent";
	WSTRING t8  = L"Coucher, Youpi!";
	STRING t9   = "Textures/Hero.dds";
	WSTRING t10 = L"Aberration";

	BIGINT res1;
	BIGINT res2;
	BIGINT res3;
	BIGINT res4;
	BIGINT res5;
	BIGINT res6;
	BIGINT res7;
	BIGINT res8;
	BIGINT res9;
	BIGINT res10;

	BIGINT expectedResults[10] =
	{ 
		0x4B73F3E6, 
		0xD27AA25C, 
		0x45529E10, 
		0xDFDD49F8, 
		0x9D23677B, 
		0x571C0C0A, 
		0x37AED322, 
		0x8C460421, 
		0x3E310AE8, 
		0x0A17CC73 
	};

	res1  = computeCRC(t1);
	res2  = computeCRC(t2);
	res3  = computeCRC(t3);
	res4  = computeCRC(t4);
	res5  = computeCRC(t5);
	res6  = computeCRC(t6);
	res7  = computeCRC(t7);
	res8  = computeCRC(t8);
	res9  = computeCRC(t9);
	res10 = computeCRC(t10);

	BOOST_CHECK(res1 == expectedResults[0]);
	BOOST_CHECK(res2 == expectedResults[1]);
	BOOST_CHECK(res3 == expectedResults[2]);
	BOOST_CHECK(res4 == expectedResults[3]);
	BOOST_CHECK(res5 == expectedResults[4]);
	BOOST_CHECK(res6 == expectedResults[5]);
	BOOST_CHECK(res7 == expectedResults[6]);
	BOOST_CHECK(res8 == expectedResults[7]);
	BOOST_CHECK(res9 == expectedResults[8]);
	BOOST_CHECK(res10 == expectedResults[9]);
}

BOOST_AUTO_TEST_CASE(fileExtension_test)
{
	STRING fullPath = "C/:juju/desk/yo/oups/toto.txt";
	STRING resExt;
	STRING expectedExtResult = "txt";

	resExt = fileExtension(fullPath);

	BOOST_CHECK(resExt == expectedExtResult);
}

BOOST_AUTO_TEST_CASE(splitExtension_test)
{
	STRING fullPath = "C/:juju/desk/yo/oups/toto.txt";
	STRING expectedExtResult = "C/:juju/desk/yo/oups/toto";

	splitExtension(fullPath);

	BOOST_CHECK(fullPath == expectedExtResult);
}

BOOST_AUTO_TEST_CASE(areEqual_test)
{
	// case sensitive
	CCHAR* test1  = "Il etait une fois un chien";
	WCHAR* test2  = L"Il etait une fois un chien";
	STRING test3  = "Il etait une fois un chien";
	WSTRING test4 = L"Il etait une fois un chien";
	UCCHAR* xmlTest = (UCCHAR*)"Il etait une fois un chien";

	// case insensitive
	CCHAR* test5  = "IL ETAIT UNE fois un CHIEN";
	WCHAR* test6  = L"IL etait une FOIS un chien";
	STRING test7  = "IL etait une fois un CHIEN";
	WSTRING test8 = L"il ETAIT une FOIS un CHIEN";

	BBOOL icharChar;
	BBOOL icharstring;
	BBOOL iwcharWchar;
	BBOOL iwcharWstring;
	BBOOL charChar;
	BBOOL charstring;
	BBOOL wcharWchar;
	BBOOL wcharWstring;
	BBOOL xmltestChar;

	charChar = areEqual(test1, test1);
	charstring = areEqual(test1, test3);
	wcharWchar = areEqual(test2, test2);
	wcharWstring = areEqual(test2, test4);
	xmltestChar = areEqual(xmlTest, test1);
	BOOST_CHECK(charChar == true);
	BOOST_CHECK(charstring == true);
	BOOST_CHECK(wcharWchar == true);
	BOOST_CHECK(wcharWstring == true);
	BOOST_CHECK(xmltestChar == true);
	charChar = areEqual(test1, test5);
	wcharWchar = areEqual(test2, test6);
	BOOST_CHECK(charChar == false);
	BOOST_CHECK(wcharWchar == false);
	icharChar = areIEqual(test1, test5);
	icharstring = areIEqual(test1, test7);
	iwcharWchar = areIEqual(test2, test6);
	iwcharWstring = areIEqual(test2, test8);
	BOOST_CHECK(icharChar == true);
	BOOST_CHECK(icharstring == true);
	BOOST_CHECK(iwcharWchar == true);
	BOOST_CHECK(iwcharWstring == true);
}